# Kawaii3D
Cross platform lightweight 3D graphic engine on Qt6

# What will be represented
A library in C ++ for working with real-time 3D graphics. Part of the KawaiiWorlds game engine, but can be used as a standalone library for loading, processing and rendering 3D models, Shaders, Materials, etc.

# The main architectural idea
There is a parent-child tree of objects. Its state does not depend on the used api rendering (OpenGL 3.3 Core, OpenGL 4.5+ Core, Vulkan, DirectX, ...) or the operating system. The 'A' node is a child of the 'B' node, if the 'A' node does not make sense without the 'B' node (for example, a mesh instance without a scene). Some types of nodes can change their state depending on the parent or child nodes, which may not be the immediate parent / child of the particular node.

