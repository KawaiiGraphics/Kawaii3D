#include "Exceptions/KawaiiPluginFailed.hpp"
#include "Exceptions/KawaiiParseExcep.hpp"
#include "KawaiiPlugin.hpp"
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

KawaiiPlugin::KawaiiPlugin(const QString &jsonFileInfo):
  cfgFilePath(jsonFileInfo),
  initFunc(nullptr),
  onLoadFunc(nullptr),
  lib(std::make_unique<QLibrary>())
{
  QFile f(jsonFileInfo);
  if(!f.exists())
    throw KawaiiPluginFailed("PluginLoader", QString("load config file: \"%1\" (file not found)").arg(jsonFileInfo));

  f.open(QFile::ReadOnly | QFile::Text);
  auto buf = f.readAll();
  f.close();

  QJsonParseError err;
  auto json = QJsonDocument::fromJson(buf, &err);
  if(err.error != QJsonParseError::NoError)
    throw KawaiiParseExcep(err.errorString());

  jsonRoot = json.object();
  reparseConfig();
  load();
}

KawaiiPlugin::KawaiiPlugin(KawaiiPlugin &another):
  jsonRoot(another.jsonRoot),
  cfgFilePath(another.cfgFilePath),
  initFunc(nullptr),
  onLoadFunc(nullptr),
  lib(std::make_unique<QLibrary>())
{
  reparseConfig();
  load();
}

KawaiiPlugin::KawaiiPlugin(KawaiiPlugin &&another):
  jsonRoot(std::move(another.jsonRoot)),
  cfgFilePath(another.cfgFilePath),
  name(another.name),
  initFuncName(another.initFuncName),
  onLoadFuncName(another.onLoadFuncName),
  initFunc(another.initFunc),
  onLoadFunc(another.onLoadFunc),
  lib(std::move(another.lib)),
  loadedFunctions(std::move(another.loadedFunctions))
{ }

KawaiiPlugin &KawaiiPlugin::operator=(KawaiiPlugin &another)
{
  *this = KawaiiPlugin(another);
  return *this;
}

KawaiiPlugin &KawaiiPlugin::operator=(KawaiiPlugin &&another)
{
  jsonRoot = std::move(another.jsonRoot);
  cfgFilePath = another.cfgFilePath;
  name = another.name;
  initFuncName = another.initFuncName;
  onLoadFuncName = another.onLoadFuncName;
  initFunc = another.initFunc;
  onLoadFunc = another.onLoadFunc;
  lib = std::move(another.lib);
  loadedFunctions = std::move(another.loadedFunctions);

  return *this;
}

void KawaiiPlugin::init(KawaiiRoot *root)
{
  if(!lib->isLoaded())
    load();

  if(initFunc)
    initFunc(root);
}

QFunctionPointer KawaiiPlugin::resolveFunction(const QByteArray &symbol)
{
  if(!lib->isLoaded())
    load();

  auto el = loadedFunctions.find(symbol);
  if(el == loadedFunctions.end())
    {
      auto func_ptr = lib->resolve(symbol.constData());
      if(!func_ptr)
        throw KawaiiPluginFailed(getPluginName(), QString("resolve symbol \"%1\"").arg(QLatin1String(symbol)));

      el = loadedFunctions.insert(symbol, func_ptr);
    }
  return el.value();
}

const QString &KawaiiPlugin::getConfigFilePath()
{
  return cfgFilePath;
}

const QString &KawaiiPlugin::getPluginName() const
{
  return name;
}

KawaiiPlugin &KawaiiPlugin::findBasicPlugin(const QString &name)
{
  return basicPlugins.findPlugin(name);
}

void KawaiiPlugin::reparseConfig()
{
  //Read name
  int i0 = cfgFilePath.lastIndexOf('/'),
      i1 = cfgFilePath.lastIndexOf('.');
  if(i1 == -1)
    i1 = cfgFilePath.size();

  Q_ASSERT_X(i0 != -1 && i1 != -1, "plugin name detection", "Invalid JSON file name");

  name = jsonRoot.value("plugin_name").toString(/*defaultValue =*/cfgFilePath.mid(i0, i1-i0));

# ifdef KAWAII3D_DEBUG
  lib->setFileName(jsonRoot.value("library_path_debug").toString());
# else
  lib->setFileName(jsonRoot.value("library_path").toString());
# endif

  initFuncName = jsonRoot.value("registered_root_func").toString(QString()).toLatin1();
  onLoadFuncName = jsonRoot.value("plugin_load_func").toString(QString()).toLatin1();
}

const QJsonObject& KawaiiPlugin::getConfig() const
{
  return jsonRoot;
}

void KawaiiPlugin::load()
{
  const QString libname = lib->fileName();
  bool loaded = false;
  QString errorString;

  auto tryLoadPluginLib = [&errorString, &loaded, this] (const QString &libfile) {
      if(!loaded)
        {
          lib->setFileName(libfile);
          loaded = lib->load();
          if(!loaded)
            {
              errorString = lib->errorString();
              qWarning().noquote() << QStringLiteral("failed to load library file \"%1\": %2").arg(libfile, errorString);
            }
        }
    };
# ifndef Q_OS_WINDOWS
  tryLoadPluginLib("lib" + libname + ".so");
  tryLoadPluginLib("lib" + libname + ".dylib");
# endif
  tryLoadPluginLib("lib" + libname + ".dll");
  tryLoadPluginLib(libname + ".dll");
  tryLoadPluginLib(libname);

  if(!loaded)
    throw KawaiiPluginFailed(name, QString("load library file \"%1\": " + errorString).arg(lib->fileName()));

  if(!onLoadFuncName.isNull())
    {
      onLoadFunc = lib->resolve(onLoadFuncName.constData());
      if(!onLoadFunc)
        throw KawaiiPluginFailed(name, QString("resolve symbol \"%1\"").arg(QLatin1String(onLoadFuncName)));
    } else
    onLoadFunc = nullptr;

  if(!initFuncName.isNull())
    {
      initFunc = reinterpret_cast<InitFunc>(lib->resolve(initFuncName.constData()));
      if(!initFunc)
        throw KawaiiPluginFailed(name, QString("resolve symbol \"%1\"").arg(QLatin1String(initFuncName)));
    } else
    initFunc = nullptr;

  if(onLoadFunc)
    onLoadFunc();
}

bool KawaiiPlugin::SemanticVersion::isCompatible(const KawaiiPlugin::SemanticVersion &another) const
{
  return maj == another.maj && min >= another.min;
}

KawaiiPlugin::SemanticVersion KawaiiPlugin::SemanticVersion::fromString(const QString &str)
{
  QStringList version = str.split('.');
  return SemanticVersion (!version.isEmpty()? version.first().toInt(): 0,
                          version.size() > 1? version.at(1).toInt(): 0,
                          version.size() > 2? version.at(2).toInt(): 0);
}
