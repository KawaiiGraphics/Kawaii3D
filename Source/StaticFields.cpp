#include "KawaiiCamera.hpp"
#include "KawaiiConfig.hpp"
#include "KawaiiGpuBuf.hpp"
#include "KawaiiBufBinding.hpp"
#include "KawaiiMaterial.hpp"
#include "KawaiiPlugin.hpp"
#include "KawaiiProjection.hpp"
#include "KawaiiOrtho.hpp"
#include "KawaiiPerspective.hpp"
#include "KawaiiRoot.hpp"
#include "Geometry/KawaiiMesh3D.hpp"
#include "Geometry/KawaiiMeshInstance.hpp"
#include "Geometry/KawaiiModel3D.hpp"
#include "Geometry/KawaiiSkeletalAnimation.hpp"
#include "Geometry/KawaiiSkeleton3D.hpp"
#include "Renderpass/KawaiiImageSource.hpp"
#include "Renderpass/KawaiiSceneLayer.hpp"
#include "Renderpass/KawaiiGlPaintedLayer.hpp"
#include "Renderpass/KawaiiQPainterLayer.hpp"
#include "Renderpass/KawaiiTexLayer.hpp"
#include "Renderpass/KawaiiRenderpass.hpp"
#include "Shaders/KawaiiProgram.hpp"
#include "Shaders/KawaiiScene.hpp"
#include "Shaders/KawaiiShader.hpp"
#include "Textures/KawaiiCubemap.hpp"
#include "Textures/KawaiiDepthCubemapArray.hpp"
#include "Textures/KawaiiDepthTex2dArray.hpp"
#include "Textures/KawaiiEnvMap.hpp"
#include "Textures/KawaiiExternalQRhiRender.hpp"
#include "Textures/KawaiiFramebuffer.hpp"
#include "Textures/KawaiiImage.hpp"
#include "Surfaces/KawaiiRender.hpp"

#include <QUrl>
#include <QCoreApplication>

const int KawaiiPoint3D::qMetaId = qRegisterMetaType<KawaiiPoint3D>();
const int KawaiiTriangle3D::Instance::qMetaId = qRegisterMetaType<KawaiiTriangle3D::Instance>();

KAWAII_COMMON_IMPLEMENT(KawaiiDataUnit::mementoFactory);

KAWAII_COMMON_IMPLEMENT(KawaiiDataUnit::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiCamera::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiGpuBuf::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiBufBinding::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiMaterial::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiRoot::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiMesh3D::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiMeshInstance::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiModel3D::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiSkeletalAnimation::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiSkeleton3D::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiImageSource::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiSceneLayer::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiGlPaintedLayer::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiQPainterLayer::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiTexLayer::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiRenderpass::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiProgram::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiScene::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiShader::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiCubemap::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiDepthCubemapArray::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiDepthTex2dArray::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiEnvMap::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiExternalQRhiRender::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiFramebuffer::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiImage::reg);

KAWAII_COMMON_IMPLEMENT(KawaiiProjection::creators);
KAWAII_COMMON_IMPLEMENT(KawaiiOrtho::reg);
KAWAII_COMMON_IMPLEMENT(KawaiiPerspective::reg);

namespace
{
  bool tryLoadImage(const QString &str, KawaiiDataUnit *target)
  {
    auto img = dynamic_cast<KawaiiImage*>(target);
    if(!img) return false;

    bool ok = false;
    img->changeImage([&str, &ok] (QImage &img) {
        if(QFile::exists(str))
          ok = img.load(str);
        else
          {
            QUrl url(str);
            if(url.isLocalFile())
              ok = img.load(url.toLocalFile());
          }
        return ok;
      });
    return ok;
  }

  bool tryLoadCubemap(const QString &str, KawaiiDataUnit *target)
  {
    auto cubemap = dynamic_cast<KawaiiCubemap*>(target);
    if(!cubemap) return false;

    bool ok = false;
    QImage img;
    if(QFile::exists(str))
      ok = img.load(str);
    else
      {
        QUrl url(str);
        if(url.isLocalFile())
          ok = img.load(url.toLocalFile());
      }
    if(!ok) return false;

    cubemap->setFromImage(img);
    return true;
  }
}

decltype(KawaiiDataUnit::loadhooks) KawaiiDataUnit::loadhooks = { &tryLoadImage, &tryLoadCubemap };
KAWAII_COMMON_IMPLEMENT(KawaiiDataUnit::loadhookDescrs);

QReadWriteLock KawaiiConfig::cfgGuard(QReadWriteLock::Recursive);
std::unique_ptr<KawaiiConfig> KawaiiConfig::cfg;

KAWAII_COMMON_IMPLEMENT(KawaiiPlugin::basicPlugins);

KAWAII_COMMON_IMPLEMENT(KawaiiRender::loadedRenders);
KawaiiRender *KawaiiRender::preferredRenderer = nullptr;

KawaiiShadersHash
KawaiiRoot::vertexShadersGlob   (KawaiiShaderType::Vertex),
KawaiiRoot::tessCtrlShadersGlob (KawaiiShaderType::TesselationControll),
KawaiiRoot::tessEvalShadersGlob (KawaiiShaderType::TesselationEvaluion),
KawaiiRoot::geometryShadersGlob (KawaiiShaderType::Geometry),
KawaiiRoot::fragmentShadersGlob (KawaiiShaderType::Fragment),
KawaiiRoot::computeShadersGlob  (KawaiiShaderType::Compute);

class Kawaii3D_Initializer
{
public:
  inline Kawaii3D_Initializer()
  {
    qAddPreRoutine([] {
        std::vector<KawaiiPlugin> plugins;
        KawaiiPlugin::loadPlugins(KawaiiConfig::getInstance().getPluginsConfigDirs(), plugins);
        KawaiiPlugin::basicPlugins = std::move(plugins);

        std::vector<KawaiiRender> renderers;
        KawaiiPlugin::loadPlugins(KawaiiConfig::getInstance().getRendersConfigDirs(), renderers);
        KawaiiRender::loadedRenders = std::move(renderers);

        const auto &shaderModuleDirs = KawaiiConfig::getInstance().getShaderModuleDirs();
        KawaiiRoot::vertexShadersGlob.  loadGlslModules(shaderModuleDirs, "*.vert.glsl");
        KawaiiRoot::tessCtrlShadersGlob.loadGlslModules(shaderModuleDirs, "*.tesc.glsl");
        KawaiiRoot::tessEvalShadersGlob.loadGlslModules(shaderModuleDirs, "*.tese.glsl");
        KawaiiRoot::geometryShadersGlob.loadGlslModules(shaderModuleDirs, "*.geom.glsl");
        KawaiiRoot::fragmentShadersGlob.loadGlslModules(shaderModuleDirs, "*.frag.glsl");
        KawaiiRoot::computeShadersGlob. loadGlslModules(shaderModuleDirs, "*.comp.glsl");

        KawaiiRender::selectRenderer();
      });
  }
};

KAWAII3D_SHARED_EXPORT Kawaii3D_Initializer kawaii3D_Init = Kawaii3D_Initializer();
