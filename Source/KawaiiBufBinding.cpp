#include "KawaiiBufBinding.hpp"
#include "KawaiiRoot.hpp"

KawaiiBufBinding::KawaiiBufBinding():
  buf(nullptr),
  root(nullptr),
  bindingPoint(0),
  bindingTarget(KawaiiBufferTarget::UBO)
{
  connect(this, &KawaiiDataUnit::parentUpdated, this, &KawaiiBufBinding::onParentChanged);
}

KawaiiBufBinding::~KawaiiBufBinding()
{
  disconnect(onBufUpdated);
}

uint32_t KawaiiBufBinding::getBindingPoint() const
{
  return bindingPoint;
}

void KawaiiBufBinding::setBindingPoint(uint32_t value)
{
  if(bindingPoint != value)
    {
      bindingPoint = value;
      emit bindingPointChanged(bindingPoint);
      if(root)
        root->bindedBufferUpdated(getBindingPoint(), bindingTarget);
    }
}

KawaiiBufferTarget KawaiiBufBinding::getTarget() const
{
  return bindingTarget;
}

void KawaiiBufBinding::setTarget(KawaiiBufferTarget target)
{
  if(bindingTarget != target)
    {
      bindingTarget = target;
      emit targetChanged(bindingTarget);
    }
}

KawaiiGpuBuf *KawaiiBufBinding::getBuf() const
{
  return buf;
}

namespace {
#pragma pack(push,1)
  struct Blob {
    uint32_t point;
    KawaiiBufferTarget target;
  };
#pragma pack(pop)

  QString bufTargetToStr(KawaiiBufferTarget target)
  {
    switch(target)
      {
      case KawaiiBufferTarget::VBO:
        return "vbo";

      case KawaiiBufferTarget::AtomicCounter:
        return "atomic-counter";

      case KawaiiBufferTarget::CopyRead:
        return "copy-read";

      case KawaiiBufferTarget::CopyWrite:
        return "copy-write";

      case KawaiiBufferTarget::DispatchIndirect:
        return "dispatch-indirect";

      case KawaiiBufferTarget::DrawIndirect:
        return "draw-indirect";

      case KawaiiBufferTarget::VertexArrayIndices:
        return "vai";

      case KawaiiBufferTarget::PixelPack:
        return "pixel-pack";

      case KawaiiBufferTarget::PixelUnpack:
        return "pixel-unpack";

      case KawaiiBufferTarget::QueryBuffer:
        return "query-buffer";

      case KawaiiBufferTarget::SSBO:
        return "ssbo";

      case KawaiiBufferTarget::TextureBuffer:
        return "texture-buffer";

      case KawaiiBufferTarget::TransformFeedback:
        return "transform-feedback";

      case KawaiiBufferTarget::UBO:
        return "ubo";
      }
    return "invalid";
  }
}

void KawaiiBufBinding::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  const Blob blob = {bindingPoint, bindingTarget};
  memento.setData("blob", QByteArray(reinterpret_cast<const char*>(&blob), sizeof(blob)));
}

void KawaiiBufBinding::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setData("point", QString::number(bindingPoint))
      .setData("target", bufTargetToStr(bindingTarget));
}

void KawaiiBufBinding::read(sib_utils::memento::Memento::DataReader &memento)
{
  bool bindingReaded = false;
  bool targetReaded = false;

  memento.readText("point", [this, &bindingReaded](const QString &val) {
      uint32_t num = val.toInt(&bindingReaded);
      if(bindingReaded)
        setBindingPoint(num);
    }).
      readText("target", [this, &targetReaded](const QString &val){
      targetReaded = setTarget(val);
    }).
      read("blob", [this, bindingReaded, targetReaded](const QByteArray &val) {
      if((bindingReaded && targetReaded) || static_cast<size_t>(val.size()) < sizeof(Blob)) return;

      const Blob *blob = reinterpret_cast<const Blob*>(val.data());
      setBindingPoint(blob->point);
      setTarget(blob->target);
    });
}

void KawaiiBufBinding::onParentChanged()
{
  auto newBuf = KawaiiRoot::getRoot<KawaiiGpuBuf>(parent());
  root = KawaiiRoot::getRoot(buf);
  if(newBuf != buf)
    {
      disconnect(onBufUpdated);
      buf = newBuf;
      onBufUpdated = connect(buf, &KawaiiGpuBuf::updated, this, [this] {
          if(root)
            root->bindedBufferUpdated(getBindingPoint(), bindingTarget);
        });
      emit bufferChanged(buf);
    }
}

bool KawaiiBufBinding::setTarget(const QString &target)
{
  static const QHash<QString, KawaiiBufferTarget> known_targets = {
    {"vbo", KawaiiBufferTarget::VBO},
    {"atomic-counter", KawaiiBufferTarget::AtomicCounter},
    {"copy-read", KawaiiBufferTarget::CopyRead},
    {"copy-write", KawaiiBufferTarget::CopyWrite},
    {"dispatch-indirect", KawaiiBufferTarget::DispatchIndirect},
    {"draw-indirect", KawaiiBufferTarget::DrawIndirect},
    {"vai", KawaiiBufferTarget::VertexArrayIndices},
    {"pixel-pack", KawaiiBufferTarget::PixelPack},
    {"pixel-unpack", KawaiiBufferTarget::PixelUnpack},
    {"query-buffer", KawaiiBufferTarget::QueryBuffer},
    {"ssbo", KawaiiBufferTarget::SSBO},
    {"texture-buffer", KawaiiBufferTarget::TextureBuffer},
    {"transform-feedback", KawaiiBufferTarget::TransformFeedback},
    {"ubo", KawaiiBufferTarget::UBO},
  };

  QString lower = target.toLower();
  if(auto el = known_targets.find(lower); el != known_targets.end())
    {
      setTarget(el.value());
      return true;
    } else
    return false;
}
