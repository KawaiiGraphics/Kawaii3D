#include "KawaiiOrtho.hpp"
#include <glm/gtc/matrix_transform.hpp>

KawaiiOrtho::KawaiiOrtho(float minX, float maxX,
                         float minY, float maxY,
                         float nearClip, float farClip):
  KawaiiProjection(nearClip, farClip),
  minX(minX), maxX(maxX),
  minY(minY), maxY(maxY)
{ }

glm::mat4 KawaiiOrtho::calculate([[maybe_unused]] const glm::uvec2 &surfaceSize) const
{
  return glm::orthoRH_NO(minX, maxX,
                         minY, maxY,
                         getNearClip(), getFarClip());
}

KawaiiProjection *KawaiiOrtho::clone() const
{
  KawaiiOrtho *clone = new KawaiiOrtho(minX, maxX,
                                       minY, maxY,
                                       getNearClip(), getFarClip());
  clone->setClipCorrection(getClipCorrection());
  clone->setSize(getSize());
  return clone;
}

void KawaiiOrtho::writeTextMemento(sib_utils::memento::Memento::DataMutator &mem) const
{
  mem.setData(QStringLiteral("minX"), QString::number(minX, 'f', 10))
      .setData(QStringLiteral("maxX"), QString::number(maxX, 'f', 10))
      .setData(QStringLiteral("minY"), QString::number(minY, 'f', 10))
      .setData(QStringLiteral("maxY"), QString::number(maxY, 'f', 10))
      .setData(QStringLiteral("nearClip"), QString::number(getNearClip(), 'f', 10))
      .setData(QStringLiteral("farClip"), QString::number(getFarClip(), 'f', 10))
      .setData(QStringLiteral("projectionType"), QStringLiteral("KawaiiOrtho"));
}

namespace {
  struct Properties
  {
    float minX;
    float maxX;
    float minY;
    float maxY;
    float nearClip;
    float farClip;
  };
}

void KawaiiOrtho::writeBinaryMemento(sib_utils::memento::Memento::DataMutator &mem) const
{
  const Properties props = {
    .minX = minX,
    .maxX = maxX,
    .minY = minY,
    .maxY = maxY,
    .nearClip = getNearClip(),
    .farClip = getFarClip()
  };
  mem.setData(QStringLiteral("projection"), QByteArray(reinterpret_cast<const char*>(&props), sizeof(Properties)))
      .setData(QStringLiteral("projectionType"), QStringLiteral("KawaiiOrtho"));
}

void KawaiiOrtho::readMemento(sib_utils::memento::Memento::DataReader &mem)
{
  bool readedMaxX = false,
      readedMinX = false,
      readedMaxY = false,
      readedMinY = false,
      readedNearClip = false,
      readedFarClip = false;

  mem.readText(QStringLiteral("maxX"), [this, &readedMaxX] (const QString &val) {
      const float f = val.toFloat(&readedMaxX);
      if(readedMaxX)
        setMaxX(f);
    });
  mem.readText(QStringLiteral("minX"), [this, &readedMinX] (const QString &val) {
      const float f = val.toFloat(&readedMinX);
      if(readedMinX)
        setMinX(f);
    });
  mem.readText(QStringLiteral("maxY"), [this, &readedMaxY] (const QString &val) {
      const float f = val.toFloat(&readedMaxY);
      if(readedMaxY)
        setMaxY(f);
    });
  mem.readText(QStringLiteral("minY"), [this, &readedMinY] (const QString &val) {
      const float f = val.toFloat(&readedMinY);
      if(readedMinY)
        setMinY(f);
    });
  mem.readText(QStringLiteral("nearClip"), [this, &readedNearClip] (const QString &val) {
      const float f = val.toFloat(&readedNearClip);
      if(readedNearClip)
        setNearClip(f);
    });
  mem.readText(QStringLiteral("farClip"), [this, &readedFarClip] (const QString &val) {
      const float f = val.toFloat(&readedFarClip);
      if(readedFarClip)
        setFarClip(f);
    });

  if(!readedMaxX || !readedMinX
     || !readedMaxY || !readedMinY
     || !readedNearClip || !readedFarClip)
    mem.read(QStringLiteral("projection"), [this,
             readedMaxX, readedMinX,
             readedMaxY, readedMinY,
             readedNearClip, readedFarClip] (const QByteArray &val) {
        if(static_cast<size_t>(val.size()) > sizeof(Properties))
          {
            const Properties *props = reinterpret_cast<const Properties*>(val.constData());
            if(!readedMaxX)
              setMaxX(props->maxX);
            if(!readedMinX)
              setMinX(props->minX);
            if(!readedMaxY)
              setMaxY(props->maxY);
            if(!readedMinY)
              setMinY(props->minY);
            if(!readedNearClip)
              setNearClip(props->nearClip);
            if(!readedFarClip)
              setFarClip(props->farClip);
          }
      });
}

float KawaiiOrtho::getMinX() const
{
  return minX;
}

void KawaiiOrtho::setMinX(float newMinX)
{
  if(qFuzzyCompare(minX, newMinX)) return;

  minX = newMinX;
  updateRequest();
}

float KawaiiOrtho::getMaxX() const
{
  return maxX;
}

void KawaiiOrtho::setMaxX(float newMaxX)
{
  if(qFuzzyCompare(maxX, newMaxX)) return;

  maxX = newMaxX;
  updateRequest();
}

float KawaiiOrtho::getMinY() const
{
  return minY;
}

void KawaiiOrtho::setMinY(float newMinY)
{
  if(qFuzzyCompare(minY, newMinY)) return;

  minY = newMinY;
  updateRequest();
}

float KawaiiOrtho::getMaxY() const
{
  return maxY;
}

void KawaiiOrtho::setMaxY(float newMaxY)
{
  if(qFuzzyCompare(maxY, newMaxY)) return;

  maxY = newMaxY;
  updateRequest();
}

void KawaiiOrtho::set(float newMinX, float newMaxX, float newMinY, float newMaxY)
{
  if(qFuzzyCompare(maxX, newMaxX) && qFuzzyCompare(minX, newMinX)
     && qFuzzyCompare(maxY, newMaxY) && qFuzzyCompare(minY, newMinY)) return;

  minX = newMinX;
  maxX = newMaxX;
  minY = newMinY;
  maxY = newMaxY;
  updateRequest();
}
