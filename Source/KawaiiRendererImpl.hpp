#ifndef KAWAIIRENDERERIMPL_HPP
#define KAWAIIRENDERERIMPL_HPP

#include <QObject>
#include "Kawaii3D_global.hpp"

class KawaiiDataUnit;
class KawaiiImplFactory;
class KAWAII3D_SHARED_EXPORT KawaiiRendererImpl: public QObject
{
  Q_OBJECT
  friend class KawaiiRoot;
  friend class KawaiiDataUnit;
  friend class KawaiiImplFactory;

protected:
  explicit KawaiiRendererImpl(KawaiiDataUnit *data);

public:
  virtual ~KawaiiRendererImpl();

  auto getData() const { return _data; }

  KawaiiRendererImpl* createRenderer(KawaiiDataUnit &data);
  bool checkRendererCompatibility(const KawaiiDataUnit &data) const;

private:
  using QObject::setParent;

  void onModelObjectNameChanged(const QString &objectName);
  virtual inline void initConnections() {}



  //IMPLEMENT
  QString lastModelName;
  KawaiiDataUnit *_data;
  KawaiiImplFactory *factory;
};

#endif //KAWAIIRENDERERIMPL_HPP
