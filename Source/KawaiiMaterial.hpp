#ifndef KAWAIIMATERIAL_HPP
#define KAWAIIMATERIAL_HPP

#include "KawaiiDataUnit.hpp"
#include <QHash>

class KawaiiGpuBuf;

class KAWAII3D_SHARED_EXPORT KawaiiMaterial : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiMaterial);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiMaterial();
  ~KawaiiMaterial();

  void setUniforms(KawaiiGpuBuf *value);
  KawaiiGpuBuf* getUniforms() const;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void uniformsChanged(KawaiiGpuBuf *data);



  //IMPLEMENT
private:
  KawaiiGpuBuf* uniforms;
  inline void detachUniforms() { setUniforms(nullptr); }
};

#endif // KAWAIIMATERIAL_HPP
