#ifndef KAWAII1VIEW_HPP
#define KAWAII1VIEW_HPP

#include "KawaiiCamera.hpp"
#include <QReadWriteLock>
#include <glm/mat4x4.hpp>
#include <QQuaternion>
#include <QVector3D>
#include <QPointer>
#include <memory>

class KAWAII3D_SHARED_EXPORT Kawaii1View : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(Kawaii1View);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  class KAWAII3D_SHARED_EXPORT Transaction
  {
    QPointer<Kawaii1View> target;
  public:
    QQuaternion rotation;
    QVector3D position;

    Transaction(Kawaii1View *target);
    Transaction(Transaction &&orig);
    Transaction(const Transaction &orig) = delete;

    inline Transaction(const std::unique_ptr<Kawaii1View> &target):
      Transaction(target.get())
    {}

    inline Transaction(const std::shared_ptr<Kawaii1View> &target):
      Transaction(target.get())
    {}

    inline Transaction(const std::weak_ptr<Kawaii1View> &target):
      Transaction(target.lock())
    {}

    Transaction& operator =(Transaction &&orig);
    Transaction& operator =(const Transaction &orig) = delete;

    ~Transaction();
  };

  Kawaii1View(const QQuaternion &rotation, const QVector3D &position);
  Kawaii1View(const glm::mat4 &trMat);
  Kawaii1View(const KawaiiCamera &cam);

  ~Kawaii1View();

  void setRotation(const QQuaternion &value);
  const QQuaternion& getRotation() const;

  void setPosition(const QVector3D &value);
  const QVector3D& getPosition() const;

  glm::mat4 getCameraMatrix() const;
  glm::mat4 getModelMatrix() const;
  QVector3D transform(const QVector3D &localPos) const;

signals:
  void updated();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
  QQuaternion rotation;
  QVector3D position;
  std::unique_ptr<QReadWriteLock> m;
};

#endif // KAWAII1VIEW_HPP
