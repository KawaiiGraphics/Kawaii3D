#include <QDir>
#include <list>
#include "KawaiiRoot.hpp"
#include "KawaiiConfig.hpp"
#include "Shaders/KawaiiShader.hpp"
#include "Surfaces/KawaiiRender.hpp"
#include "Geometry/KawaiiMeshInstance.hpp"
#include "Exceptions/KawaiiNoRenderExcep.hpp"

KawaiiRoot::KawaiiRoot()
{
  addVertexShaders(vertexShadersGlob);
  addTesselationControllShaderss(tessCtrlShadersGlob);
  addTesselationEvalutionShaders(tessEvalShadersGlob);
  addGeometryShaders(geometryShadersGlob);
  addFragmentShaders(fragmentShadersGlob);
  addComputeShaders(computeShadersGlob);
}

KawaiiRoot::~KawaiiRoot()
{
  for(auto meshInst: meshes)
    meshInst->root = nullptr;
}

bool KawaiiRoot::createRendererImplementation(const QStringList &request)
{
  try {
    KawaiiRender::getPreferredRender(request).create(this);
  } catch (const KawaiiNoRenderExcep&)
  {
    return false;
  }

  auto rndr = getRendererImpl();
  if(!rndr)
    return false;

  std::list<KawaiiDataUnit*> stack;
  for(auto ch: children())
    if(auto data = dynamic_cast<KawaiiDataUnit*>(ch); data)
      stack.push_back(data);

  std::list<KawaiiRendererImpl*> created = { rndr };
  while(!stack.empty())
    {
      auto data = stack.front();
      stack.pop_front();

      auto childRndr = rndr->createRenderer(*data);
      if(childRndr)
        created.push_back(childRndr);

      for(auto ch = data->children().rbegin(); ch != data->children().rend(); ++ch)
        if(auto ch_data = dynamic_cast<KawaiiDataUnit*>(*ch); ch_data)
          stack.push_front(ch_data);
    }

  for(auto *i: created)
    i->initConnections();

  return true;
}

void KawaiiRoot::registerMesh(KawaiiMeshInstance *inst)
{
  meshes.insert(inst);
  emit meshAdded(inst);
}

void KawaiiRoot::unregisterMesh(KawaiiMeshInstance *inst)
{
  meshes.remove(inst);
  emit meshRemoved(inst);
}
