#include "KawaiiPluginFailed.hpp"

KawaiiPluginFailed::KawaiiPluginFailed(const QString &where, const QString &what):
  KawaiiException(QString("from plugin \"%1\": failed to %2").arg(where, what)),
  where(where),
  what(what)
{
}

const QString& KawaiiPluginFailed::getPluginName() const
{
  return where;
}

const QString& KawaiiPluginFailed::getAction() const
{
  return what;
}
