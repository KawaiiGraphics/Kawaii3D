#include "KawaiiPluginNotFound.hpp"

KawaiiPluginNotFound::KawaiiPluginNotFound(const QString &name):
  KawaiiPluginFailed(name, "find basic plugin")
{ }
