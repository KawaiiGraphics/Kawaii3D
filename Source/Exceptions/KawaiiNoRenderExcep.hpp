#ifndef KAWAIINORENDEREXCEP_HPP
#define KAWAIINORENDEREXCEP_HPP

#include "KawaiiException.hpp"

class KawaiiNoRenderExcep : public KawaiiException
{
public:
  KawaiiNoRenderExcep();
};

#endif // KAWAIINORENDEREXCEP_HPP
