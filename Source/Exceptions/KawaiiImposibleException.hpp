#ifndef KAWAIIIMPOSIBLEEXCEPTION_HPP
#define KAWAIIIMPOSIBLEEXCEPTION_HPP

#include "KawaiiException.hpp"
#include "../Kawaii3D_global.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiImposibleException : public KawaiiException
{
public:
  template<typename... ArgsT, class = typename std::enable_if<std::is_constructible<KawaiiException, ArgsT...>::value>::type >
  KawaiiImposibleException(ArgsT&&... args):
    KawaiiException(args...)
  { }

  virtual ~KawaiiImposibleException() = default;
};

#endif // KAWAIIIMPOSIBLEEXCEPTION_HPP
