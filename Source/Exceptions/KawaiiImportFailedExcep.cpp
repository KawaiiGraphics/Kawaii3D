#include "KawaiiImportFailedExcep.hpp"

KawaiiImportFailedExcep::KawaiiImportFailedExcep(const QString &module, const QString &importingModule):
  KawaiiParseExcep(QString("Failed at module \"%1\" to import module \"%2\"").arg(module, importingModule)),
  module(module),
  importingModule(importingModule)
{ }

const QString &KawaiiImportFailedExcep::getModule() const
{
  return module;
}

const QString &KawaiiImportFailedExcep::getImportingModule() const
{
  return importingModule;
}
