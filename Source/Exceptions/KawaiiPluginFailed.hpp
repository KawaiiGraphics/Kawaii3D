#ifndef KAWAIIPLUGINFAILED_HPP
#define KAWAIIPLUGINFAILED_HPP

#include "KawaiiException.hpp"
#include <QString>

class KawaiiPluginFailed : public KawaiiException
{
public:
  KawaiiPluginFailed(const QString &where, const QString &what);

  const QString &getPluginName() const;
  const QString &getAction() const;



  //IMPLEMENT
private:
  QString where;
  QString what;
};

#endif // KAWAIIPLUGINFAILED_HPP
