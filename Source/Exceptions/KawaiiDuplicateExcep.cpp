#include "KawaiiDuplicateExcep.hpp"

KawaiiDuplicateExcep::KawaiiDuplicateExcep(const QMetaObject *model, const QMetaObject *ctrl):
  KawaiiException("model is already registered in factory"),
  model(model), ctrl(ctrl)
{ }
