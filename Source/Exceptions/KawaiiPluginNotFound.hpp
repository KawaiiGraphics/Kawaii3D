#ifndef KAWAIIPLUGINNOTFOUND_HPP
#define KAWAIIPLUGINNOTFOUND_HPP

#include "KawaiiPluginFailed.hpp"

class KawaiiPluginNotFound: public KawaiiPluginFailed
{
public:
  KawaiiPluginNotFound(const QString &name);
};

#endif // KAWAIIPLUGINNOTFOUND_HPP
