#include "KawaiiIOExcep.hpp"

KawaiiIOExcep::KawaiiIOExcep(KawaiiIOExcep::Type t):
  KawaiiException(([t]{
    switch(t)
      {
      case Type::Read:
        return "Read error";
      case Type::Write:
        return "Write error";
      default:
        return "Unknown IO error";
      }
  })()),
  type(t)
{
}

KawaiiIOExcep::Type KawaiiIOExcep::getType() const
{
  return type;
}
