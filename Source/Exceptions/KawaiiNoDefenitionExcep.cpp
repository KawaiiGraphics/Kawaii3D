#include "KawaiiNoDefenitionExcep.hpp"

KawaiiNoDefenitionExcep::KawaiiNoDefenitionExcep(const QString &module, const QString &importingModule, const QString &askedDefenition):
  KawaiiImportFailedExcep(module, importingModule), askedDefenition(askedDefenition)
{ }

const QString &KawaiiNoDefenitionExcep::getAskedDefenition() const
{
  return askedDefenition;
}
