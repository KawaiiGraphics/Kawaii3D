#ifndef KAWAIIEXCEPTION_HPP
#define KAWAIIEXCEPTION_HPP

#include <stdexcept>

class QString;

class KawaiiException : public std::runtime_error
{
public:
  template<typename... ArgsT, class = typename std::enable_if<std::is_constructible<std::runtime_error, ArgsT...>::value>::type >
  KawaiiException(ArgsT&&... args):
    std::runtime_error(std::forward<ArgsT>(args)...)
  { }

  explicit KawaiiException(const QString &what);

  virtual ~KawaiiException() = default;
};

#endif // KAWAIIEXCEPTION_HPP
