#ifndef KAWAIIROOTMISMATCHEXCEPTION_HPP
#define KAWAIIROOTMISMATCHEXCEPTION_HPP

#include "KawaiiException.hpp"

class KawaiiRootMismatchException : public KawaiiException
{
public:
  KawaiiRootMismatchException();
};

#endif // KAWAIIROOTMISMATCHEXCEPTION_HPP
