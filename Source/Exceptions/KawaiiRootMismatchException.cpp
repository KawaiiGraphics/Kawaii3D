#include "KawaiiRootMismatchException.hpp"

KawaiiRootMismatchException::KawaiiRootMismatchException():
  KawaiiException("Root mismatch")
{ }
