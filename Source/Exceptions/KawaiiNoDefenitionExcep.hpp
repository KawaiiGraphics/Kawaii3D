#ifndef KAWAIINODEFENITIONEXCEP_HPP
#define KAWAIINODEFENITIONEXCEP_HPP

#include "KawaiiImportFailedExcep.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiNoDefenitionExcep: public KawaiiImportFailedExcep
{
public:
  KawaiiNoDefenitionExcep(const QString &module, const QString &importingModule, const QString &askedDefenition);
  ~KawaiiNoDefenitionExcep() = default;

  const QString& getAskedDefenition() const;



  //IMPLEMENT
private:
  QString askedDefenition;
};

#endif // KAWAIINODEFENITIONEXCEP_HPP
