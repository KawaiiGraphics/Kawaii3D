#ifndef KAWAIIDUPLICATEEXCEP_HPP
#define KAWAIIDUPLICATEEXCEP_HPP

#include "KawaiiException.hpp"
#include "../Kawaii3D_global.hpp"
#include <QMetaObject>

class KawaiiRendererImpl;

class KAWAII3D_SHARED_EXPORT KawaiiDuplicateExcep : public KawaiiException
{
public:
  KawaiiDuplicateExcep(const QMetaObject *model, const QMetaObject *ctrl);
  ~KawaiiDuplicateExcep() = default;

  inline auto getModel() const { return model; }
  inline auto getController() const { return ctrl; }



  //IMPLEMENT
private:
  const QMetaObject *model;
  const QMetaObject *ctrl;
};

#endif // KAWAIIDUPLICATEEXCEP_HPP
