#ifndef KAWAIIPARSEEXCEP_HPP
#define KAWAIIPARSEEXCEP_HPP

#include "KawaiiException.hpp"
#include "../Kawaii3D_global.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiParseExcep: public KawaiiException
{
public:
  template<typename... ArgsT, class = typename std::enable_if<std::is_constructible<KawaiiException, ArgsT...>::value>::type >
  KawaiiParseExcep(ArgsT&&... args):
    KawaiiException(args...)
  { }

  ~KawaiiParseExcep() = default;
};

#endif // KAWAIIPARSEEXCEP_HPP
