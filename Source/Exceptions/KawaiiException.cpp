#include "KawaiiException.hpp"
#include <QString>

KawaiiException::KawaiiException(const QString &what):
  std::runtime_error(what.toStdString())
{ }
