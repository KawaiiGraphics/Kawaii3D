#ifndef KAWAIIIOEXCEP_HPP
#define KAWAIIIOEXCEP_HPP

#include "KawaiiException.hpp"
#include "../Kawaii3D_global.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiIOExcep: public KawaiiException
{
public:
  enum class Type {
    Read,
    Write,
    Unknown
  };

  KawaiiIOExcep(Type t);
  ~KawaiiIOExcep() = default;

  Type getType() const;



  //IMPLEMENT
private:
  Type type;
};

#endif // KAWAIIIOEXCEP_HPP
