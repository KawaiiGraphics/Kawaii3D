#ifndef KAWAIIIMPORTFAILEDEXCEP_HPP
#define KAWAIIIMPORTFAILEDEXCEP_HPP

#include "KawaiiParseExcep.hpp"
#include <QString>

class KAWAII3D_SHARED_EXPORT KawaiiImportFailedExcep : public KawaiiParseExcep
{
public:
  KawaiiImportFailedExcep(const QString &module, const QString &importingModule);
  ~KawaiiImportFailedExcep() = default;

  const QString& getModule() const;
  const QString& getImportingModule() const;



  //IMPLEMENT:
private:
  QString module;
  QString importingModule;
};

#endif // KAWAIIIMPORTFAILEDEXCEP_HPP
