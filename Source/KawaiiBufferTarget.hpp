#ifndef KAWAIIBUFFERTARGET_HPP
#define KAWAIIBUFFERTARGET_HPP

#include <cstdint>

enum class KawaiiBufferTarget: uint8_t
{
  VBO,
  VertexArray = VBO,

  AtomicCounter,
  CopyRead,
  CopyWrite,
  DispatchIndirect,
  DrawIndirect,

  VertexArrayIndices,
  ElementArray = VertexArrayIndices,

  PixelPack,
  PixelUnpack,
  QueryBuffer,

  SSBO,
  ShaderStorage = SSBO,

  TextureBuffer,
  TransformFeedback,

  UBO,
  Uniform = UBO
};

#endif // KAWAIIBUFFERTARGET_HPP
