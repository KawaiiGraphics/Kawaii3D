#include "KawaiiSubsceneZygote.hpp"

KawaiiMaterialZygote &KawaiiSubsceneZygote::addMaterial()
{
  materials.emplace_back();
  return materials.back();
}

const KawaiiMaterialZygote &KawaiiSubsceneZygote::getMaterial(size_t i)
{
  return materials.at(i);
}

void KawaiiSubsceneZygote::forallMaterials(const std::function<void (KawaiiMaterialZygote &)> &func)
{
  for(auto &i: materials)
    func(i);
}

const KawaiiMeshInstanceZygote &KawaiiSubsceneZygote::getMeshInstance(size_t i)
{
  return meshInstances.at(i);
}

void KawaiiSubsceneZygote::forallMeshInstances(const std::function<void (KawaiiMeshInstanceZygote &)> &func)
{
  for(auto &i: meshInstances)
    func(i);
}

void KawaiiSubsceneZygote::replicate(KawaiiDataUnit &parent_node, KawaiiMaterialReplicator &replicator)
{
  for(const auto &i: materials)
    {
      replicator.setZygote(&i);
      auto material = replicator.createMaterial(parent_node);

      const auto uboCreator = std::bind(&KawaiiMaterialReplicator::createModelBuffer, &replicator, std::placeholders::_1);
      if(material)
        {
          for(auto &j: meshInstances)
            j.onMaterialReplicated(i, *material, uboCreator);
        } else
        for(auto &j: meshInstances)
          j.onNullMaterialReplicated(i, uboCreator);
    }

  for(const auto &j: meshInstances)
    j.replicate();
}

void KawaiiSubsceneZygote::clear()
{
  meshInstances.clear();
  materials.clear();
}
