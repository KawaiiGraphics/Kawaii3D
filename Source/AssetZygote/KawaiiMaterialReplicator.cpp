#include "KawaiiMaterialReplicator.hpp"
#include "KawaiiGpuBuf.hpp"

KawaiiMaterialReplicator::KawaiiMaterialReplicator():
  zygote(nullptr),
  processing_texture(nullptr)
{ }

void KawaiiMaterialReplicator::setZygote(const KawaiiMaterialZygote *val)
{
  zygote = val;
  textures.clear();
}

KawaiiMaterial *KawaiiMaterialReplicator::createMaterial(KawaiiDataUnit &parentNode)
{
  if(!zygote) return nullptr;

  for(const auto &i: zygote->textures)
    {
      processing_texture = &i.second;
      if(auto *tex = replicateTexture(std::get<0>(i.first), std::get<1>(i.first)); tex)
        {
          textures[i.first] = tex;
          if(!tex->parent())
            tex->updateParent(&parentNode);
        }
    }
  processing_texture = nullptr;

  auto result = replicateMaterial();
  if(result)
    {
      if(!result->parent())
        result->updateParent(&parentNode);

      if(result->objectName().isEmpty() || result->objectName().isNull())
        result->setObjectName(getMaterialName());
    }
  return result;
}

KawaiiGpuBuf *KawaiiMaterialReplicator::createModelBuffer(KawaiiDataUnit &parentNode)
{
  auto result = replicateModelBuffer();
  if(result)
    {
      if(!result->parent())
        result->updateParent(&parentNode);

      if(result->objectName().isEmpty() || result->objectName().isNull())
        result->setObjectName(getMaterialName());
    }
  return result;
}

KawaiiTexture *KawaiiMaterialReplicator::getTexture(KawaiiTextureRole texRole, size_t texId) const
{
  if(auto el = textures.find({texRole, texId}); el != textures.end())
    return el->second;
  else
    return nullptr;
}
