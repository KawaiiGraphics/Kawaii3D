#include "KawaiiMaterialZygote.hpp"

KawaiiMaterialZygote::KawaiiMaterialZygote()
{
}

void KawaiiMaterialZygote::setMaterialType(const QString &type) noexcept
{
  setParam(KawaiiRenderParamRole::Type, type.toStdString());
}

int KawaiiMaterialZygote::removeParam(KawaiiRenderParamRole key)
{
  auto el = params.find(key);
  if(el == params.end())
    return 0;

  params.erase(el);
  return 1;
}

int KawaiiMaterialZygote::removeTextureParam(KawaiiTextureRole texRole, size_t texId, KawaiiTextureParamRole key)
{
  if(auto el = textures.find({texRole, texId}); el != textures.end())
    return el->second.removeParam(key);
  else
    return 0;
}

std::size_t KawaiiMaterialZygote::HashTuple::operator()(const std::pair<KawaiiTextureRole, size_t> &val) const
{
  return static_cast<std::size_t>(val.first) ^ val.second;
}
