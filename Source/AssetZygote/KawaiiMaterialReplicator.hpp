#ifndef KAWAIIMATERIALREPLICATOR_HPP
#define KAWAIIMATERIALREPLICATOR_HPP

#include "KawaiiMaterialZygote.hpp"

#include "../Textures/KawaiiTexture.hpp"
#include "../KawaiiMaterial.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiMaterialReplicator
{
public:
  KawaiiMaterialReplicator();

  void setZygote(const KawaiiMaterialZygote* val);
  KawaiiMaterial* createMaterial(KawaiiDataUnit &parentNode);
  KawaiiGpuBuf* createModelBuffer(KawaiiDataUnit &parentNode);

protected:
  KawaiiTexture* getTexture(KawaiiTextureRole texRole, size_t texId) const;

  template<typename T>
  inline std::optional<T> getParam(KawaiiRenderParamRole key) const
  { return zygote->getParam<T>(key); }

  ///Returns param of processing texture; Should be used only inside of 'replicateTexture' implementation
  template<typename T>
  inline std::optional<T> getTextureParam(KawaiiTextureParamRole key) const
  { return processing_texture? processing_texture->getParam<T>(key): std::nullopt; }

  inline const QString& getMaterialName() const noexcept
  { return zygote->getMaterialName(); }


private:
  virtual KawaiiTexture* replicateTexture(KawaiiTextureRole texRole, size_t texId) = 0;
  virtual KawaiiMaterial* replicateMaterial() = 0;
  inline virtual KawaiiGpuBuf* replicateModelBuffer() { return nullptr; }



  //IMPLEMENT
  const KawaiiMaterialZygote* zygote;

  std::unordered_map<std::pair<KawaiiTextureRole, size_t>, KawaiiTexture*, KawaiiMaterialZygote::HashTuple> textures;
  const KawaiiTextureZygote* processing_texture;
};

#endif // KAWAIIMATERIALREPLICATOR_HPP
