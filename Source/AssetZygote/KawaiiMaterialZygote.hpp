#ifndef KAWAIIMATERIALZYGOTE_HPP
#define KAWAIIMATERIALZYGOTE_HPP

#include "../Geometry/KawaiiMesh3D.hpp"
#include "KawaiiTextureParamRole.hpp"
#include "KawaiiRenderParamRole.hpp"
#include "KawaiiTextureZygote.hpp"
#include "KawaiiTextureRole.hpp"
#include "KawaiiParam.hpp"
#include <unordered_map>

class KAWAII3D_SHARED_EXPORT KawaiiMaterialZygote
{
  friend class KawaiiMaterialReplicator;
public:
  KawaiiMaterialZygote();

  void setMaterialType(const QString &type) noexcept;
  inline const QString& getMaterialName() const noexcept
  { return materialName; }

  template<typename T>
  void setParam(KawaiiRenderParamRole key, const T &value)
  {
    params[key] = KawaiiParam(value);
    if(key == KawaiiRenderParamRole::Type)
      {
        if constexpr(std::is_constructible_v<QString, T>)
        {
          materialName = static_cast<QString>(value);
        } else
        if constexpr(std::is_invocable_v<decltype(&QString::fromStdString), const T&>)
        {
          materialName = QString::fromStdString(value);
        }
      }
  }

  int removeParam(KawaiiRenderParamRole key);

  template<typename T>
  std::optional<T> getParam(KawaiiRenderParamRole key) const
  {
    if(auto el = params.find(key); el != params.end())
      return el->second.getValue<T>();

    return {};
  }

  template<typename T>
  void setTextureParam(KawaiiTextureRole texRole, size_t texIndex, KawaiiTextureParamRole key, const T &value)
  {
    textures[{texRole, texIndex}].setParam(key, value);
  }

  int removeTextureParam(KawaiiTextureRole texRole, size_t texId, KawaiiTextureParamRole key);

  template<typename T>
  std::optional<T> getTextureParam(KawaiiTextureRole texRole, size_t texId, KawaiiTextureParamRole key) const
  {
    if(auto el = textures.find({texRole, texId}); el != textures.end())
      return el->second.getParam<T>(key);
    else
      return std::nullopt;
  }

  template<typename T>
  void forallTexturesParam(KawaiiTextureParamRole key, const std::function<void(T&)> &func)
  {
    for(auto &i: textures)
      if(auto var = i.second.getParam<T>(key); var)
        {
          T arg(std::move(*var));
          func(arg);
          i.second.setParam(key, arg);
        }
  }



  //IMPLEMENT
private:
  struct KAWAII3D_SHARED_EXPORT HashTuple {
    std::size_t operator()(const std::pair<KawaiiTextureRole, size_t> &val) const;
  };
  QPointer<KawaiiMesh3D> mesh;

  QString materialName;
  std::unordered_map<KawaiiRenderParamRole, KawaiiParam> params;
  std::unordered_map<std::pair<KawaiiTextureRole, size_t>, KawaiiTextureZygote, KawaiiMaterialZygote::HashTuple> textures;
};

#endif // KAWAIIMATERIALZYGOTE_HPP
