#ifndef KAWAIITEXTUREPARAMROLE_HPP
#define KAWAIITEXTUREPARAMROLE_HPP

#include <cstdint>
#include <qglobal.h>

enum class KawaiiTextureParamRole: uint8_t
{
  FileName,
  UvWSrc,
  Op,
  Mapping,
  Blend,
  MapmodeU,
  MapmodeV,
  MapAxis,
  UVtransform,
  Flags
};

inline uint qHash(KawaiiTextureParamRole param)
{
  return static_cast<uint>(param);
}

#endif // KAWAIITEXTUREPARAMROLE_HPP
