#ifndef KAWAIIPARAM_HPP
#define KAWAIIPARAM_HPP

#include "../Kawaii3D_global.hpp"

#include <QString>
#include <variant>
#include <optional>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QMatrix4x4>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_integer.hpp>

class KAWAII3D_SHARED_EXPORT KawaiiParam
{
public:
  using Data = std::variant<int32_t, uint32_t, int64_t, uint64_t,
  float, double,
  glm::vec2, glm::vec3, glm::vec4,
  glm::dvec2, glm::dvec3, glm::dvec4,
  glm::ivec2, glm::ivec3, glm::ivec4,
  glm::uvec2, glm::uvec3, glm::uvec4,
  glm::mat2, glm::mat3, glm::mat4,
  glm::dmat2, glm::dmat3, glm::dmat4,
  glm::imat2, glm::imat3, glm::imat4,
  glm::umat2, glm::umat3, glm::umat4,
  std::string, std::vector<std::byte>,
  std::monostate
  >;

  KawaiiParam();

  template<typename T>
  inline KawaiiParam(const T &value)
  {
    setValue(value);
  }

  KawaiiParam(const KawaiiParam &origin);
  KawaiiParam(KawaiiParam &&origin);

  KawaiiParam& operator=(const KawaiiParam &origin);
  KawaiiParam& operator=(KawaiiParam &&origin);

  template<typename T>
  inline void setValue(const T &val)
  {
    d = val;
  }

  template<typename T>
  std::optional<T> getValue() const
  {
    if(std::holds_alternative<T>(d))
    {
      return std::make_optional<T>(std::get<T>(d));
    } else
    return {};
  }



  //IMPLEMENT
private:
  Data d;
};


template<> KAWAII3D_SHARED_EXPORT std::optional<QVector2D> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QVector3D> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QVector4D> KawaiiParam::getValue() const;

template<> KAWAII3D_SHARED_EXPORT std::optional<QMatrix2x2> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QMatrix3x3> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QMatrix4x4> KawaiiParam::getValue() const;

template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<2, 2, double>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<3, 3, double>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<4, 4, double>> KawaiiParam::getValue() const;

template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<2, 2, int32_t>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<3, 3, int32_t>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<4, 4, int32_t>> KawaiiParam::getValue() const;

template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<2, 2, uint32_t>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<3, 3, uint32_t>> KawaiiParam::getValue() const;
template<> KAWAII3D_SHARED_EXPORT std::optional<QGenericMatrix<4, 4, uint32_t>> KawaiiParam::getValue() const;

template<> KAWAII3D_SHARED_EXPORT std::optional<QString> KawaiiParam::getValue() const;



template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QVector2D &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QVector3D &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QVector4D &val);

template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QMatrix2x2 &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QMatrix3x3 &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QMatrix4x4 &val);

template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<2, 2, double> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<3, 3, double> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<4, 4, double> &val);

template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<2, 2, int32_t> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<3, 3, int32_t> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<4, 4, int32_t> &val);

template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<2, 2, uint32_t> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<3, 3, uint32_t> &val);
template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QGenericMatrix<4, 4, uint32_t> &val);

template<> KAWAII3D_SHARED_EXPORT void KawaiiParam::setValue(const QString &val);

#endif // KAWAIIPARAM_HPP
