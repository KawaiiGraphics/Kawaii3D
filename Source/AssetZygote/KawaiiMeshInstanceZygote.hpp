#ifndef KAWAIIMESHINSTANCEZYGOTE_HPP
#define KAWAIIMESHINSTANCEZYGOTE_HPP

#include "../Geometry/KawaiiMeshInstance.hpp"
#include "../KawaiiMaterial.hpp"
#include "KawaiiMaterialZygote.hpp"
#include <QPointer>
#include <variant>

class KAWAII3D_SHARED_EXPORT KawaiiMeshInstanceZygote
{
public:
  KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh, const KawaiiMaterialZygote &material);
  KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh, KawaiiMaterial &material);
  KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh);

  ~KawaiiMeshInstanceZygote();

  const QString &getName() const;
  void setName(const QString &name);

  size_t getInstanceCount() const;
  void setInstanceCount(size_t count);

  void onNullMaterialReplicated(const KawaiiMaterialZygote &zygote, const std::function<KawaiiGpuBuf*(KawaiiDataUnit &)> &modelUboCreator);
  void onMaterialReplicated(const KawaiiMaterialZygote &zygote, KawaiiMaterial &material, const std::function<KawaiiGpuBuf*(KawaiiDataUnit &)> &modelUboCreator);

  KawaiiMeshInstance *replicate() const;



  //IMPLEMENT
private:
  QString name;
  size_t instanceCount;

  QMetaObject::Connection onMaterialDestroyed;

  std::variant<std::reference_wrapper<KawaiiMaterial>, std::reference_wrapper<const KawaiiMaterialZygote>, std::monostate> material;
  QPointer<KawaiiMesh3D> mesh;
  QPointer<KawaiiGpuBuf> modelUbo;
};

#endif // KAWAIIMESHINSTANCEZYGOTE_HPP
