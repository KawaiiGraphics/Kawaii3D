#ifndef KAWAIITEXTUREROLE_HPP
#define KAWAIITEXTUREROLE_HPP

#include <cstdint>
#include <qglobal.h>

enum class KawaiiTextureRole: uint8_t
{
  None = 0,
  Diffuse,
  Specular,
  Ambient,
  Emissive,
  Height,
  Normals,
  Shininess,
  Opacity,
  Displacement,
  Lightmap,
  Reflection,
  Unknown
};

inline uint qHash(KawaiiTextureRole textureRole)
{
  return static_cast<uint>(textureRole);
}

#endif // KAWAIITEXTUREROLE_HPP
