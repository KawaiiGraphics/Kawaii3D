#ifndef KAWAIISUBSCENEZYGOTE_HPP
#define KAWAIISUBSCENEZYGOTE_HPP

#include "KawaiiMaterialZygote.hpp"
#include "KawaiiMaterialReplicator.hpp"

#include "KawaiiMeshInstanceZygote.hpp"
#include <deque>

class KAWAII3D_SHARED_EXPORT KawaiiSubsceneZygote: public QObject
{
  Q_OBJECT
public:
  KawaiiMaterialZygote& addMaterial();
  const KawaiiMaterialZygote& getMaterial(size_t i);
  void forallMaterials(const std::function<void(KawaiiMaterialZygote&)> &func);

  template<typename... ArgsT, class = std::enable_if_t<std::is_constructible_v<KawaiiMeshInstanceZygote, ArgsT...>>>
  KawaiiMeshInstanceZygote& addMeshInstance(ArgsT&&... args)
  {
    meshInstances.emplace_back(std::forward<ArgsT>(args)...);
    return meshInstances.back();
  }

  const KawaiiMeshInstanceZygote& getMeshInstance(size_t i);
  void forallMeshInstances(const std::function<void(KawaiiMeshInstanceZygote&)> &func);


  void replicate(KawaiiDataUnit &parent_node, KawaiiMaterialReplicator &replicator);

  void clear();



  //IMPLEMENT
private:
  std::deque<KawaiiMaterialZygote> materials;
  std::deque<KawaiiMeshInstanceZygote> meshInstances;
};

#endif // KAWAIISUBSCENEZYGOTE_HPP
