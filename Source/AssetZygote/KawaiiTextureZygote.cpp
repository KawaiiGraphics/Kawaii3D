#include "KawaiiTextureZygote.hpp"

KawaiiTextureZygote::KawaiiTextureZygote()
{
}

int KawaiiTextureZygote::removeParam(KawaiiTextureParamRole key)
{
  auto el = params.find(key);
  if(el == params.end())
    return 0;

  params.erase(el);
  return 1;
}
