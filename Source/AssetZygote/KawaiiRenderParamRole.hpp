#ifndef KAWAIIRENDERPARAMROLE_HPP
#define KAWAIIRENDERPARAMROLE_HPP

#include <cstdint>
#include <qglobal.h>

enum class KawaiiRenderParamRole: uint8_t
{
  Type,
  Twosided,
  ShadingModel,
  EnableWireframe,
  BlendFunc,
  Opacity,
  Bumpscaling,
  Shininess,
  Reflectivity,
  ShininessStrength,
  Refracti,
  ColorDiffuse,
  ColorAmbient,
  ColorSpecular,
  ColorEmissive,
  ColorTransparent,
  ColorReflective,
  GlobalBackgroundImage
};

inline uint qHash(KawaiiRenderParamRole val)
{
  return static_cast<uint>(val);
}

#endif // KAWAIIRENDERPARAMROLE_HPP
