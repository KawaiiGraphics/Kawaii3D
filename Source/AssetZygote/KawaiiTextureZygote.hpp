#ifndef KAWAIITEXTUREZYGOTE_HPP
#define KAWAIITEXTUREZYGOTE_HPP

#include "KawaiiTextureParamRole.hpp"
#include "KawaiiParam.hpp"
#include <unordered_map>

class KAWAII3D_SHARED_EXPORT KawaiiTextureZygote
{
public:
  KawaiiTextureZygote();

  template<typename T>
  void setParam(KawaiiTextureParamRole key, const T &value)
  {
    params[key] = KawaiiParam(value);
  }

  int removeParam(KawaiiTextureParamRole key);

  template<typename T>
  std::optional<T> getParam(KawaiiTextureParamRole key) const
  {
    if(auto el = params.find(key); el != params.end())
      return el->second.getValue<T>();

    return std::nullopt;
  }



  //IMPLEMENT
private:
  std::unordered_map<KawaiiTextureParamRole, KawaiiParam> params;
};

#endif // KAWAIITEXTUREZYGOTE_HPP

