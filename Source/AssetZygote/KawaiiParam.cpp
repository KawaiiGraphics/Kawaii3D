#include "KawaiiParam.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <sib_utils/dirty_cast.hpp>

KawaiiParam::KawaiiParam():
  d(std::monostate{})
{
}

KawaiiParam::KawaiiParam(const KawaiiParam &origin):
  d(origin.d)
{ }

KawaiiParam::KawaiiParam(KawaiiParam &&origin):
  d(std::move(origin.d))
{
}

KawaiiParam &KawaiiParam::operator=(const KawaiiParam &origin)
{
  d = origin.d;
  return *this;
}

KawaiiParam &KawaiiParam::operator=(KawaiiParam &&origin)
{
  d = std::move(origin.d);
  return *this;
}

namespace
{
  template<typename T1, typename T0, typename Func>
  std::optional<T1> func_if(std::optional<T0> &&val, const Func &f)
  {
    if(val.has_value())
      return std::make_optional<T1>(f(*val));
    else
      return std::nullopt;
  }

  template<typename T1, typename T0>
  std::optional<T1> getOptVal(std::optional<T0> &&val)
  {
    return func_if<T1>(std::move(val), sib_utils::DirtyCaster<T1>::f);
  }

  template<typename T>
  struct GetQMat {
    inline static auto f = [](const auto &origin)
    { return T(glm::value_ptr(origin)); };
  };

  template<typename T>
  using qmat2 = QGenericMatrix<2, 2, T>;

  template<typename T>
  using qmat3 = QGenericMatrix<3, 3, T>;

  template<typename T>
  using qmat4 = QGenericMatrix<4, 4, T>;
}

template<>
std::optional<QVector2D> KawaiiParam::getValue() const
{
  return getOptVal<QVector2D>(getValue<glm::vec2>());
}

template<>
std::optional<QVector3D> KawaiiParam::getValue() const
{
  const auto v3 = getValue<glm::vec3>();
  if(v3)
    return QVector3D(v3->x, v3->y, v3->z);
  else
    return std::nullopt;
}

template<>
std::optional<QVector4D> KawaiiParam::getValue() const
{
  return getOptVal<QVector4D>(getValue<glm::vec4>());
}

template<>
std::optional<QMatrix2x2> KawaiiParam::getValue() const
{
  return func_if<QMatrix2x2>(getValue<glm::mat2>(), GetQMat<QMatrix2x2>::f);
}

template<>
std::optional<QMatrix3x3> KawaiiParam::getValue() const
{
  return func_if<QMatrix3x3>(getValue<glm::mat3>(), GetQMat<QMatrix3x3>::f);
}

template<>
std::optional<QMatrix4x4> KawaiiParam::getValue() const
{
  return func_if<QMatrix4x4>(getValue<glm::mat4>(), GetQMat<QMatrix4x4>::f);
}

template<>
std::optional<QGenericMatrix<2, 2, double> > KawaiiParam::getValue() const
{
  return func_if<qmat2<double>>(getValue<glm::dmat2>(), GetQMat<qmat2<double>>::f);
}

template<>
std::optional<QGenericMatrix<3, 3, double> > KawaiiParam::getValue() const
{
  return func_if<qmat3<double>>(getValue<glm::dmat3>(), GetQMat<qmat3<double>>::f);
}

template<>
std::optional<QGenericMatrix<4, 4, double> > KawaiiParam::getValue() const
{
  return func_if<qmat4<double>>(getValue<glm::dmat4>(), GetQMat<qmat4<double>>::f);
}

template<>
std::optional<QGenericMatrix<2, 2, int32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat2<int32_t>>(getValue<glm::imat2>(), GetQMat<qmat2<int32_t>>::f);
}

template<>
std::optional<QGenericMatrix<3, 3, int32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat3<int32_t>>(getValue<glm::imat3>(), GetQMat<qmat3<int32_t>>::f);
}

template<>
std::optional<QGenericMatrix<4, 4, int32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat4<int32_t>>(getValue<glm::imat4>(), GetQMat<qmat4<int32_t>>::f);
}

template<>
std::optional<QGenericMatrix<2, 2, uint32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat2<uint32_t>>(getValue<glm::umat2>(), GetQMat<qmat2<uint32_t>>::f);
}

template<>
std::optional<QGenericMatrix<3, 3, uint32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat3<uint32_t>>(getValue<glm::umat3>(), GetQMat<qmat3<uint32_t>>::f);
}

template<>
std::optional<QGenericMatrix<4, 4, uint32_t> > KawaiiParam::getValue() const
{
  return func_if<qmat4<uint32_t>>(getValue<glm::umat4>(), GetQMat<qmat4<uint32_t>>::f);
}

template<>
std::optional<QString> KawaiiParam::getValue() const
{
  return func_if<QString>(getValue<std::string>(), QString::fromStdString);
}

template<>
void KawaiiParam::setValue(const QVector2D &val)
{
  setValue(sib_utils::dirty_cast<glm::vec2>(val));
}

template<>
void KawaiiParam::setValue(const QVector3D &val)
{
  setValue(glm::vec3 {val.x(), val.y(), val.z()});
}

template<>
void KawaiiParam::setValue(const QVector4D &val)
{
  setValue(sib_utils::dirty_cast<glm::vec4>(val));
}

template<>
void KawaiiParam::setValue(const QMatrix2x2 &val)
{
  setValue(*reinterpret_cast<const glm::mat2*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QMatrix3x3 &val)
{
  setValue(*reinterpret_cast<const glm::mat3*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QMatrix4x4 &val)
{
  setValue(*reinterpret_cast<const glm::mat4*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<2, 2, double> &val)
{
  setValue(*reinterpret_cast<const glm::dmat2*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<3, 3, double> &val)
{
  setValue(*reinterpret_cast<const glm::dmat3*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<4, 4, double> &val)
{
  setValue(*reinterpret_cast<const glm::dmat4*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<2, 2, int32_t> &val)
{
  setValue(*reinterpret_cast<const glm::imat2*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<3, 3, int32_t> &val)
{
  setValue(*reinterpret_cast<const glm::imat3*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<4, 4, int32_t> &val)
{
  setValue(*reinterpret_cast<const glm::imat4*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<2, 2, uint32_t> &val)
{
  setValue(*reinterpret_cast<const glm::umat2*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<3, 3, uint32_t> &val)
{
  setValue(*reinterpret_cast<const glm::umat3*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QGenericMatrix<4, 4, uint32_t> &val)
{
  setValue(*reinterpret_cast<const glm::umat4*>(val.data()));
}

template<>
void KawaiiParam::setValue(const QString &val)
{
  return setValue(val.toStdString());
}
