#include "KawaiiMeshInstanceZygote.hpp"
#include "Exceptions/KawaiiImposibleException.hpp"

KawaiiMeshInstanceZygote::KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh, const KawaiiMaterialZygote &material):
  name(QString()),
  instanceCount(1),
  material(material),
  mesh(&mesh)
{
}

KawaiiMeshInstanceZygote::KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh, KawaiiMaterial &material):
  name(QString()),
  instanceCount(1),
  material(material),
  mesh(&mesh)
{
  onMaterialDestroyed = QObject::connect(&material, &QObject::destroyed, [this] { this->material = std::monostate{}; });
}

KawaiiMeshInstanceZygote::KawaiiMeshInstanceZygote(KawaiiMesh3D &mesh):
  name(QString()),
  instanceCount(1),
  material(std::monostate{}),
  mesh(&mesh)
{
}

KawaiiMeshInstanceZygote::~KawaiiMeshInstanceZygote()
{
  QObject::disconnect(onMaterialDestroyed);
}

const QString &KawaiiMeshInstanceZygote::getName() const
{
  return name;
}

void KawaiiMeshInstanceZygote::setName(const QString &val)
{
  name = val;
}

size_t KawaiiMeshInstanceZygote::getInstanceCount() const
{
  return instanceCount;
}

void KawaiiMeshInstanceZygote::setInstanceCount(size_t count)
{
  instanceCount = count;
}

void KawaiiMeshInstanceZygote::onNullMaterialReplicated(const KawaiiMaterialZygote &zygote, const std::function<KawaiiGpuBuf*(KawaiiDataUnit &)> &modelUboCreator)
{
  if(std::holds_alternative<std::reference_wrapper<const KawaiiMaterialZygote>>(this->material) &&
     &std::get<std::reference_wrapper<const KawaiiMaterialZygote>>(this->material).get() == &zygote)
    {
      material = std::monostate {};
      if(mesh)
        modelUbo = modelUboCreator(*mesh);
    }
}

void KawaiiMeshInstanceZygote::onMaterialReplicated(const KawaiiMaterialZygote &zygote, KawaiiMaterial &material, const std::function<KawaiiGpuBuf*(KawaiiDataUnit &)> &modelUboCreator)
{
  if(std::holds_alternative<std::reference_wrapper<const KawaiiMaterialZygote>>(this->material) &&
     &std::get<std::reference_wrapper<const KawaiiMaterialZygote>>(this->material).get() == &zygote)
    {
      this->material = material;
      onMaterialDestroyed = QObject::connect(&material, &QObject::destroyed, [this] { this->material = std::monostate{}; });
      if(mesh)
        modelUbo = modelUboCreator(*mesh);
    }
}

KawaiiMeshInstance *KawaiiMeshInstanceZygote::replicate() const
{
  if(!mesh)
    {
      if(modelUbo)
        delete modelUbo;
      return nullptr;
    }

  if(std::holds_alternative<std::reference_wrapper<const KawaiiMaterialZygote>>(material))
    throw KawaiiImposibleException("KawaiiMeshInstanceZygote::replicate: material was not instanced");

  KawaiiMeshInstance *inst;
  if(std::holds_alternative<std::monostate>(material))
    inst = mesh->createChild<KawaiiMeshInstance>();
  else
    inst = mesh->createChild<KawaiiMeshInstance>(&std::get<std::reference_wrapper<KawaiiMaterial>>(material).get());

  if(!name.isNull())
    inst->setObjectName(name);
  if(instanceCount != 1)
    inst->setInstanceCount(instanceCount);
  if(modelUbo)
    inst->setUniforms(modelUbo);
  return inst;
}
