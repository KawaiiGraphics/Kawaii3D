#ifndef KAWAIICONFIG_HPP
#define KAWAIICONFIG_HPP

#include "Kawaii3D_global.hpp"
#include "Textures/KawaiiImgFormat.hpp"
#include <QSurfaceFormat>
#include <QReadWriteLock>
#include <inttypes.h>
#include <optional>
#include <QString>
#include <memory>
#include <QList>
#include <QDir>

class KAWAII3D_SHARED_EXPORT KawaiiConfig
{
  friend std::unique_ptr<KawaiiConfig> std::make_unique<KawaiiConfig> ();
public:
  static KawaiiConfig& getInstance();

  void reset();

  uint16_t getDebugLevel() const;
  void setDebugLevel(uint16_t lvl);

  const QList<QDir>& getResourceDirs() const;

  const QList<QDir>& getPluginsConfigDirs() const;

  const QList<QDir>& getRendersConfigDirs() const;

  const QList<QDir>& getShaderModuleDirs() const;

  const QStringList& getPreferredRenderers() const;

  const QSurfaceFormat& getPreferredSfcFormat() const;

  const KawaiiImgFormat& getPreferredImgFormat() const;

  const std::optional<uint32_t> &getRenderbuffersCount() const;

  bool isSystemShadersProtected() const;



  //IMPLEMENT
private:
  KawaiiConfig();

  QSurfaceFormat fmt;
  std::optional<uint32_t> renderbuffersCount;
  QList<QDir> resDirs;
  QList<QDir> pluginDirs;
  QList<QDir> renderDirs;
  QList<QDir> shaderModuleDirs;
  QStringList preferredRenderers;
  KawaiiImgFormat imgFormat;

  uint8_t debug;

  bool writable;
  bool protectSystemShaders;

  static std::unique_ptr<KawaiiConfig> cfg;
  static QReadWriteLock cfgGuard;
};

#endif // KAWAIICONFIG_HPP
