#include "KawaiiCubemap.hpp"
#include "KawaiiConfig.hpp"

#include <QBuffer>
#include <QPainter>

KawaiiCubemap::KawaiiCubemap():
  KawaiiTexture(KawaiiTextureType::TextureCube)
{
  setWrapModeS(KawaiiTextureWrapMode::ClampToEdge);
  setWrapModeT(KawaiiTextureWrapMode::ClampToEdge);
  setWrapModeR(KawaiiTextureWrapMode::ClampToEdge);
  setResolution({1,1,6,1});

  connect(this, &KawaiiCubemap::imageUpdated, this, [this] (uint16_t side) {
    uint64_t w = getResolution().at(0),
        h = getResolution().at(1);

    if(d[side].width() > 0 && static_cast<uint64_t>(d[side].width()) > w)
      w = static_cast<uint64_t>(d[side].width());

    if(d[side].height() > 0 && static_cast<uint64_t>(d[side].height()) > h)
      h = static_cast<uint64_t>(d[side].height());
    setResolution({w,h,6,1});
    emit updated();
  });
}

KawaiiCubemap::~KawaiiCubemap()
{
}

void KawaiiCubemap::changeImage(uint16_t side, const std::function<bool (QImage &)> &func)
{
  if(func(d[side]))
    emit imageUpdated(side);
}

void KawaiiCubemap::setFromImage(const QImage &img)
{
  const QSize elemSz(img.width() / 4, img.height() / 3);
  auto getElem = [&img, &elemSz] (int x, int y) {
      return img.copy(elemSz.width() * x, elemSz.height() * y,
                      elemSz.width(), elemSz.height());
    };
  setImage(KawaiiCubemap::NegativeX, getElem(0, 1));
  setImage(KawaiiCubemap::PositiveZ, getElem(1, 1));
  setImage(KawaiiCubemap::PositiveX, getElem(2, 1));
  setImage(KawaiiCubemap::NegativeZ, getElem(3, 1));
  setImage(KawaiiCubemap::PositiveY, getElem(1, 0));
  setImage(KawaiiCubemap::NegativeY, getElem(1, 2));
}

QImage KawaiiCubemap::toImage() const
{
  QSize sz;
  for(uint16_t i = 0; i < 6; ++i)
    {
      if(sz.width() < getImage(i).width())
        sz.setWidth(getImage(i).width());

      if(sz.height() < getImage(i).height())
        sz.setHeight(getImage(i).height());
    }

  QImage img(QSize(sz.width() * 4, sz.height() * 3), QImage::Format_ARGB32);
  img.fill(Qt::white);
  QPainter p(&img);

  auto setElem = [&p, &sz] (int x, int y, const QImage &img0) {
      x *= sz.width();
      y *= sz.height();
      if(img0.size() != sz)
        {
          const auto img1 = img0.scaled(sz, Qt::IgnoreAspectRatio, Qt::FastTransformation);
          p.drawImage(QPoint(x, y), img1);
        } else
        p.drawImage(QPoint(x, y), img0);
    };

  setElem(0, 1, getImage(KawaiiCubemap::NegativeX));
  setElem(1, 1, getImage(KawaiiCubemap::PositiveZ));
  setElem(2, 1, getImage(KawaiiCubemap::PositiveX));
  setElem(3, 1, getImage(KawaiiCubemap::NegativeZ));
  setElem(1, 0, getImage(KawaiiCubemap::PositiveY));
  setElem(1, 2, getImage(KawaiiCubemap::NegativeY));
  p.end();

  return img;
}

void KawaiiCubemap::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  KawaiiTexture::writeBinary(memento);

  writeImgData(memento);
}

void KawaiiCubemap::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  KawaiiTexture::writeText(memento);

  //Data can not be stringified to human-readable text
  writeImgData(memento);
}

void KawaiiCubemap::read(sib_utils::memento::Memento::DataReader &memento)
{
  KawaiiTexture::read(memento);

  for(uint16_t i = 0; i < 6; ++i)
  memento.read("picture", [this, i] (const QByteArray &buf) {
      if(d[i].loadFromData(buf))
        emit imageUpdated(i);
    });
}

void KawaiiCubemap::writeImgData(sib_utils::memento::Memento::DataMutator &memento) const
{
  const auto &fmt = KawaiiConfig::getInstance().getPreferredImgFormat();

  for(uint16_t i = 0; i < 6; ++i)
    {
      QByteArray data;

      QBuffer buf(&data);
      buf.open(QBuffer::WriteOnly);
      d[i].save(&buf, fmt.name, fmt.quality);
      buf.close();

      memento.setData(QStringLiteral("picture_%1").arg(i), data);
    }
}
