#ifndef KAWAIIENVMAP_HPP
#define KAWAIIENVMAP_HPP

#include "KawaiiTexture.hpp"
#include "../Shaders/KawaiiScene.hpp"
#include "../KawaiiCamera.hpp"

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <array>

class KAWAII3D_SHARED_EXPORT KawaiiEnvMap: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiEnvMap);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  bool enabled;

  enum class AttachmentMode: uint8_t
  {
    Depth = 0,
    Stencil = 1,
    DepthStencil = 2,
    Color = 3,
  };
  struct Attachment
  {
    AttachmentMode mode;
    QPointer<KawaiiTexture> target;
    int layer;
  };

  KawaiiEnvMap(const glm::uvec2 &resolution = glm::uvec2(1024, 1024));
  ~KawaiiEnvMap();

  KawaiiScene *getScene() const;

  inline static constexpr size_t getSfcBufferSize()
  { return sizeof(SfcGpuData); }

  void setPosition(const glm::vec3 &p);
  inline const glm::vec3& getPosition() const { return pos; }

  void setResolution(const glm::uvec2 &res);
  inline const glm::uvec2& getResolution() const { return resolution; }

  const KawaiiGpuBuf *getSfcUbo(size_t i) const;
  const KawaiiGpuBuf *getCameraUbo(size_t i) const;

  void attachTexture(AttachmentMode mode, KawaiiTexture *texture, int layer = -1);
  void forallAtachments(const std::function<void (const Attachment&)> &func) const;
  QFuture<void> forallAtachmentsP(const std::function<void (const Attachment&)> &func) const;

  float getNearClip() const;
  void setNearClip(float value);

  float getFarClip() const;
  void setFarClip(float value);

  /// Should be called from renderer
  void setClipCorrectionMatrix(const glm::mat4 &mat);

  glm::mat4 getProjectionMat() const;
  glm::mat4 getViewProjectionMat(size_t i) const;

signals:
  void positionChanged(const glm::vec3 &pos);
  void resolutionChanged(const glm::uvec2 &res);

  void sceneChanged(KawaiiScene *scene);
  void drawPipelineChanged();

  void textureAttached(AttachmentMode mode, KawaiiTexture *texture, int layer);
  void textureDetached(AttachmentMode mode);
  void projectionMatChanged();



  //IMPLEMENT
private:
  struct SfcGpuData
  {
    glm::mat4 projectionMat;
    glm::mat4 viewProjectionMat;
  };

  std::array<KawaiiGpuBuf, 6> sfcUbo;
  std::array<SfcGpuData*, 6> sfcBuffersContents;

  std::array<KawaiiGpuBuf, 6> cameraUbo;
  std::array<KawaiiCamera::GpuBuffer*, 6> cameraBuffersContents;

  glm::mat4 clipCorrection;

  glm::vec3 pos;
  glm::uvec2 resolution;

  KawaiiScene *scene;

  std::unordered_map<AttachmentMode, Attachment> attachments;

  float nearClip;
  float farClip;


  void onParentChanged();

  void calculateViewMat();
  void calculateProjectionMat();
  void calculateViewProjectionMat();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;
};

#endif // KAWAIIENVMAP_HPP
