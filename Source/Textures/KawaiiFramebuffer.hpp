#ifndef KAWAIIFRAMEBUFFER_HPP
#define KAWAIIFRAMEBUFFER_HPP

#include "../Renderpass/KawaiiRenderTarget.hpp"
#include "../KawaiiGpuBuf.hpp"
#include "KawaiiTexture.hpp"

#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <QFuture>

class KawaiiCamera;

class KAWAII3D_SHARED_EXPORT KawaiiFramebuffer: public KawaiiDataUnit, public KawaiiRenderTarget
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiFramebuffer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  struct Attachment
  {
    QPointer<KawaiiTexture> target;
    int layer;
    quint32 useGbuf;
  };

  KawaiiFramebuffer(const glm::uvec2 &size = glm::uvec2(1024,1024));
  ~KawaiiFramebuffer();

  inline const glm::uvec2& getSize() const { return sz; }
  void setSize(const glm::uvec2 &size);

  void attachTexture(quint32 gbuf, KawaiiTexture *texture, int layer = -1);
  void forallAtachments(const std::function<void (const Attachment&)> &func) const;
  QFuture<void> forallAtachmentsP(const std::function<void (const Attachment&)> &func) const;

  // KawaiiRenderTarget interface
public:
  void setRenderpass(KawaiiRenderpass *newRenderpass) override;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void sizeChanged(const glm::uvec2 &size);
  void textureAttached(quint32 gbuf, KawaiiTexture *texture, int layer);
  void textureDetached(quint32 gbuf);
  void renderpassChanged(KawaiiRenderpass *renderpass);



  //IMPLEMENT
private:
  glm::uvec2 sz;
  std::unordered_map<quint32, Attachment> attachments;
};

#endif // KAWAIIFRAMEBUFFER_HPP
