#ifndef KAWAIIEXTERNALQRHIRENDER_HPP
#define KAWAIIEXTERNALQRHIRENDER_HPP

#include "KawaiiTexture.hpp"
#include <rhi/qrhi.h>
#include <functional>

class KAWAII3D_SHARED_EXPORT KawaiiExternalQRhiRender: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiExternalQRhiRender);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiExternalQRhiRender();

  void setRenderTargetTexture(KawaiiTexture *newTex);
  KawaiiTexture* getRenderTargetTexture() const;

  /// Should be called from a renderer plugin
  void setRhi(QRhi *newRhi, QRhiTextureRenderTarget *newRenderTarget);
  QRhi* getRhi() const;

  QRhiTextureRenderTarget* getRenderTargetRhi() const;

  bool render(const std::function<bool (QRhi *, QRhiTextureRenderTarget *)> &func);

signals:
  void rhiChanged(QRhi *rhi, QRhiTextureRenderTarget *renderTarget);
  void renderTargetTextureChanged(KawaiiTexture *renderTarget);
  void renderTargetTextureResized();
  void aboutToBeRendered();
  void rendered();



  // IMPLEMENT
private:
  KawaiiTexture *tex;

  QRhi *rhi;
  std::unique_ptr<QRhiTextureRenderTarget> renderTarget;

  void resetRenderTargetTexture();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void read(sib_utils::memento::Memento::DataReader &mem) override final;
};

#endif // KAWAIIEXTERNALQRHIRENDER_HPP
