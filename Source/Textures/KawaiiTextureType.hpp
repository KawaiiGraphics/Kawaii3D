#ifndef KAWAIITEXTURETYPE_HPP
#define KAWAIITEXTURETYPE_HPP

#include <QHashFunctions>
#include <functional>
#include <stdint.h>

enum class KawaiiTextureType: uint8_t {
  Texture1D = 0,
  Texture2D = 1,
  Texture3D = 2,
  Texture1D_Array = 3,
  Texture2D_Array = 4,
  TextureRectangle = 5,
  TextureCube = 6,
  TextureCube_Array = 7,
  TextureBuffer = 8,
  Texture2D_Multisample = 9,
  Texture2D_Multisample_Array = 10
};

inline uint qHash(KawaiiTextureType type)
{
  return qHash(static_cast<quint8>(type));
}

namespace std {
  template<>
  struct hash<KawaiiTextureType>
  {
    size_t operator()(const KawaiiTextureType &type) const
    {
      return std::hash<uint8_t>()(static_cast<uint8_t>(type));
    }
  };
}

#endif // KAWAIITEXTURETYPE_HPP
