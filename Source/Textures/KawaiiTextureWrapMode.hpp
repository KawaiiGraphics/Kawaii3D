#ifndef KAWAIITEXTUREWRAPMODE_HPP
#define KAWAIITEXTUREWRAPMODE_HPP
#include <stdint.h>
#include <functional>

enum class KawaiiTextureWrapMode: uint8_t
{
  ClampToEdge,
  ClampToBorder,
  MirroredRepeat,
  Repeat,
  MirroredClampToEdge
};

namespace std {
  template<>
  struct hash<KawaiiTextureWrapMode>
  {
    size_t operator()(const KawaiiTextureWrapMode &mode) const
    {
      return std::hash<uint8_t>()(static_cast<uint8_t>(mode));
    }
  };
}

#endif // KAWAIITEXTUREWRAPMODE_HPP
