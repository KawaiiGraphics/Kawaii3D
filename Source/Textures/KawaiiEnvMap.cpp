#include "KawaiiEnvMap.hpp"
#include "KawaiiRoot.hpp"

#include <QtConcurrent>

#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

KawaiiEnvMap::KawaiiEnvMap(const glm::uvec2 &resolution):
  enabled(true),

  sfcUbo({ KawaiiGpuBuf {nullptr, getSfcBufferSize()},
         KawaiiGpuBuf {nullptr, getSfcBufferSize()},
         KawaiiGpuBuf {nullptr, getSfcBufferSize()},
         KawaiiGpuBuf {nullptr, getSfcBufferSize()},
         KawaiiGpuBuf {nullptr, getSfcBufferSize()},
         KawaiiGpuBuf {nullptr, getSfcBufferSize()}
         }),

  sfcBuffersContents( { reinterpret_cast<SfcGpuData*>(sfcUbo[0].getData()),
  reinterpret_cast<SfcGpuData*>(sfcUbo[1].getData()),
  reinterpret_cast<SfcGpuData*>(sfcUbo[2].getData()),
  reinterpret_cast<SfcGpuData*>(sfcUbo[3].getData()),
  reinterpret_cast<SfcGpuData*>(sfcUbo[4].getData()),
  reinterpret_cast<SfcGpuData*>(sfcUbo[5].getData())
} ),

  cameraUbo({ KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()},
         KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()},
         KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()},
         KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()},
         KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()},
         KawaiiGpuBuf {nullptr, KawaiiCamera::getBufferSize()}
         }),

  cameraBuffersContents( { reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[0].getData()),
  reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[1].getData()),
  reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[2].getData()),
  reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[3].getData()),
  reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[4].getData()),
  reinterpret_cast<KawaiiCamera::GpuBuffer*>(cameraUbo[5].getData())
} ),

  clipCorrection(glm::mat4(1.0)),
  resolution(resolution),
  scene(nullptr),

  nearClip(0.1f),
  farClip(100.0f)
{
  for(size_t i = 0; i < 6; ++i)
    {
      sfcUbo[i].updateParent(this);
      sfcUbo[i].setObjectName(QStringLiteral("Surface uniforms %1").arg(i+1));
      *sfcBuffersContents[i] = SfcGpuData();

      cameraUbo[i].updateParent(this);
      cameraUbo[i].setObjectName(QStringLiteral("Camera uniforms %1").arg(i+1));
      *cameraBuffersContents[i] = KawaiiCamera::GpuBuffer();
    }

  setProperty("ExcludedChildren", QVariant::fromValue<QList<QVariant>>(
  {
                  QVariant::fromValue<QObject*>(&sfcUbo[0]),
                  QVariant::fromValue<QObject*>(&sfcUbo[1]),
                  QVariant::fromValue<QObject*>(&sfcUbo[2]),
                  QVariant::fromValue<QObject*>(&sfcUbo[3]),
                  QVariant::fromValue<QObject*>(&sfcUbo[4]),
                  QVariant::fromValue<QObject*>(&sfcUbo[5]),
                  QVariant::fromValue<QObject*>(&cameraUbo[0]),
                  QVariant::fromValue<QObject*>(&cameraUbo[1]),
                  QVariant::fromValue<QObject*>(&cameraUbo[2]),
                  QVariant::fromValue<QObject*>(&cameraUbo[3]),
                  QVariant::fromValue<QObject*>(&cameraUbo[4]),
                  QVariant::fromValue<QObject*>(&cameraUbo[5])
                }));

  connect(this, &sib_utils::TreeNode::parentUpdated, this, &KawaiiEnvMap::onParentChanged);

  calculateViewMat();
  calculateProjectionMat();
}

KawaiiEnvMap::~KawaiiEnvMap()
{
}

KawaiiScene *KawaiiEnvMap::getScene() const
{
  return scene;
}

void KawaiiEnvMap::setPosition(const glm::vec3 &p)
{
  pos = p;
  calculateViewMat();
  calculateViewProjectionMat();
  emit positionChanged(pos);
}

void KawaiiEnvMap::setResolution(const glm::uvec2 &res)
{
  resolution = res;
  calculateProjectionMat();
  emit resolutionChanged(resolution);
}

const KawaiiGpuBuf *KawaiiEnvMap::getSfcUbo(size_t i) const
{
  return &sfcUbo[i];
}

const KawaiiGpuBuf *KawaiiEnvMap::getCameraUbo(size_t i) const
{
  return &cameraUbo[i];
}

void KawaiiEnvMap::attachTexture(KawaiiEnvMap::AttachmentMode mode, KawaiiTexture *texture, int layer)
{
  auto el = attachments.find(mode);

  if(texture)
    {
      if(texture->getType() != KawaiiTextureType::TextureCube
         && texture->getType() != KawaiiTextureType::TextureCube_Array)
        throw std::invalid_argument("KawaiiEnvMap::attachTexture: texture must be cubemap or cubemap array");

      if(el != attachments.end())
        {
          if(el->second.target != texture || el->second.layer != layer)
            {
              el->second.target = texture;
              emit textureAttached(mode, texture, layer);
            }
        } else
        {
          attachments.insert(std::pair(mode, Attachment { mode, texture, layer }));
          emit textureAttached(mode, texture, layer);
        }
    } else
    if(el != attachments.end())
      {
        attachments.erase(el);
        emit textureDetached(mode);
      }
}

void KawaiiEnvMap::forallAtachments(const std::function<void (const KawaiiEnvMap::Attachment &)> &func) const
{
  for(const auto &i: attachments)
    func(i.second);
}

QFuture<void> KawaiiEnvMap::forallAtachmentsP(const std::function<void (const KawaiiEnvMap::Attachment &)> &func) const
{
  return QtConcurrent::map(attachments, [&func] (const std::pair<AttachmentMode, Attachment> &p) {
      func(p.second);
    });
}

float KawaiiEnvMap::getNearClip() const
{
  return nearClip;
}

void KawaiiEnvMap::setNearClip(float value)
{
  nearClip = value;
  calculateProjectionMat();
}

float KawaiiEnvMap::getFarClip() const
{
  return farClip;
}

void KawaiiEnvMap::setFarClip(float value)
{
  farClip = value;
  calculateProjectionMat();
}

void KawaiiEnvMap::setClipCorrectionMatrix(const glm::mat4 &mat)
{
  clipCorrection = mat;
  calculateProjectionMat();
}

glm::mat4 KawaiiEnvMap::getProjectionMat() const
{
  return sfcBuffersContents[0]->projectionMat;
}

glm::mat4 KawaiiEnvMap::getViewProjectionMat(size_t i) const
{
  return sfcBuffersContents[i]->viewProjectionMat;
}

void KawaiiEnvMap::onParentChanged()
{
  auto _scene = KawaiiRoot::getRoot<KawaiiScene>(parent());
  if(_scene != scene)
    {
      if(scene)
        disconnect(scene, &KawaiiScene::drawPipelineChanged, this, &KawaiiEnvMap::drawPipelineChanged);
      scene = _scene;
      if(scene)
        connect(scene, &KawaiiScene::drawPipelineChanged, this, &KawaiiEnvMap::drawPipelineChanged);
      emit sceneChanged(scene);
    }
}

void KawaiiEnvMap::calculateViewMat()
{
  const std::array<glm::mat4, 6> viewMats = {
    glm::lookAt(pos, pos + glm::vec3( 1,0,0), glm::vec3(0, -1,  0)),
    glm::lookAt(pos, pos + glm::vec3(-1,0,0), glm::vec3(0, -1,  0)),
    glm::lookAt(pos, pos + glm::vec3(0, 1,0), glm::vec3(0,  0,  1)),
    glm::lookAt(pos, pos + glm::vec3(0,-1,0), glm::vec3(0,  0, -1)),
    glm::lookAt(pos, pos + glm::vec3(0,0, 1), glm::vec3(0, -1,  0)),
    glm::lookAt(pos, pos + glm::vec3(0,0,-1), glm::vec3(0, -1,  0))
  };

  for(size_t i = 0; i < 6; ++i)
    {
      cameraBuffersContents[i]->view = viewMats[i];
      cameraBuffersContents[i]->normal = glm::inverseTranspose(glm::mat3(viewMats[i]));
      cameraBuffersContents[i]->viewInverted = glm::inverse(viewMats[i]);
      cameraBuffersContents[i]->pos = glm::vec4(pos, 1);
      cameraUbo[i].dataChanged(0, sizeof(KawaiiCamera::GpuBuffer));
    }
}

void KawaiiEnvMap::calculateProjectionMat()
{
  static const constexpr float fov = M_PI_2;

  const glm::mat4 projMat = clipCorrection * glm::perspectiveFov<float>(fov, resolution.x, resolution.y, nearClip, farClip);

  for(size_t i = 0; i < 6; ++i)
    {
      sfcBuffersContents[i]->projectionMat = projMat;
      sfcBuffersContents[i]->viewProjectionMat = projMat * cameraBuffersContents[i]->view;
      sfcUbo[i].dataChanged(0, sizeof(SfcGpuData));
    }

  emit projectionMatChanged();
}

void KawaiiEnvMap::calculateViewProjectionMat()
{
  for(size_t i = 0; i < 6; ++i)
    {
      sfcBuffersContents[i]->viewProjectionMat = sfcBuffersContents[i]->projectionMat * cameraBuffersContents[i]->view;
      sfcUbo[i].dataChanged(offsetof(SfcGpuData, viewProjectionMat), sizeof(glm::mat4));
    }
}

void KawaiiEnvMap::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  QByteArray properties(sizeof(resolution) +
                        sizeof(nearClip) +
                        sizeof(farClip) +
                        sizeof(pos),
                        '\0');

  char *ptr = properties.data();
  auto writeMemchunk = [&ptr](const void *chunk, size_t chunkSize) {
      memcpy(ptr, chunk, chunkSize);
      ptr += chunkSize;
    };
  writeMemchunk(&resolution, sizeof(resolution));
  writeMemchunk(&nearClip, sizeof(nearClip));
  writeMemchunk(&farClip, sizeof(farClip));
  writeMemchunk(&pos, sizeof(pos));

  memento.setData("properties", properties);

  forallAtachments([&memento] (const Attachment &att) {
      sib_utils::memento::Memento *mem;
      memento.createChildMemento("Attachment", mem);

      QByteArray properties(sizeof(att.layer) + sizeof(att.mode), '\0');
      auto ptr = properties.data();
      memcpy(ptr, &att.mode, sizeof(att.mode));
      memcpy(ptr + sizeof(att.mode), &att.layer, sizeof(att.layer));

      auto mutator = mem->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks);
      mutator.setData("properties", properties).setLink("texture", att.target.data());
    });
}

void KawaiiEnvMap::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  static const std::unordered_map<AttachmentMode, QString> mode_map =
  {{AttachmentMode::Color, QStringLiteral("Color")},
   {AttachmentMode::Depth, QStringLiteral("Depth")},
   {AttachmentMode::Stencil, QStringLiteral("Stencil")},
   {AttachmentMode::DepthStencil, QStringLiteral("DepthStencil")}};

  mem.setData("width", QString::number(resolution.x));
  mem.setData("height", QString::number(resolution.y));
  mem.setData("near_clip", QString::number(nearClip, 'f', 10));
  mem.setData("far_clip", QString::number(farClip, 'f', 10));
  mem.setData("x", QString::number(pos.x, 'f', 10));
  mem.setData("y", QString::number(pos.y, 'f', 10));
  mem.setData("z", QString::number(pos.z, 'f', 10));

  forallAtachments([&mem] (const Attachment &att) {
      sib_utils::memento::Memento *chMem;
      mem.createChildMemento("Attachment", chMem);

      auto mutator = chMem->mutator(mem.getCreatedMemento(), mem.preserveExternalLinks);
      mutator.setData("layer", QString::number(att.layer)).setData("mode", mode_map.at(att.mode)).setLink("texture", att.target.data());
    });
}

void KawaiiEnvMap::read(sib_utils::memento::Memento::DataReader &memento)
{
  //explicitly detach all textures
  std::vector<AttachmentMode> enabledAttModes;
  enabledAttModes.reserve(attachments.size());
  for(const auto &i: attachments)
    enabledAttModes.push_back(i.first);
  attachments.clear();
  for(const auto &i: enabledAttModes)
    emit textureDetached(i);

  //Try to read binary
  memento.read("properties", [this](const QByteArray &properties) {
      const char *ptr = properties.constData();
      auto readMemchunk = [&ptr] (void *chunk, size_t chunkSize) {
          memcpy(chunk, ptr, chunkSize);
          ptr += chunkSize;
        };

      if(static_cast<size_t>(properties.size()) < sizeof(resolution) +
         sizeof(nearClip) +
         sizeof(farClip) +
         sizeof(pos))
        return;

      readMemchunk(&resolution, sizeof(resolution));
      readMemchunk(&nearClip, sizeof(nearClip));
      readMemchunk(&farClip, sizeof(farClip));
      readMemchunk(&pos, sizeof(pos));
    });

  //Try to read text
  glm::uvec2 size;
  bool widthReaded = false, heightReaded = false;
  memento.readText("width", [&size, &widthReaded](const QString &str) {
      size.x = str.toUInt(&widthReaded);
    });

  memento.readText("height", [&size, &heightReaded](const QString &str) {
      size.y = str.toUInt(&heightReaded);
    });

  if(!widthReaded)
    size.x = getResolution().x;

  if(!heightReaded)
    size.y = getResolution().y;

  setResolution(size);

  memento.readText("near_clip", [this] (const QString &str) {
      bool ok;
      float f = str.toFloat(&ok);
      if(ok)
        nearClip = f;
    });

  memento.readText("far_clip", [this] (const QString &str) {
      bool ok;
      float f = str.toFloat(&ok);
      if(ok)
        farClip = f;
    });

  glm::vec3 readedPos;
  bool xReaded = false, yReaded = false, zReaded = false;
  memento.readText("x", [&readedPos, &xReaded](const QString &str) {
      readedPos.x = str.toFloat(&xReaded);
    });

  memento.readText("y", [&readedPos, &yReaded](const QString &str) {
      readedPos.y = str.toFloat(&yReaded);
    });

  memento.readText("z", [&readedPos, &zReaded](const QString &str) {
      readedPos.z = str.toFloat(&zReaded);
    });

  if(!xReaded)
    readedPos.x = 0;
  if(!yReaded)
    readedPos.y = 0;
  if(!zReaded)
    readedPos.z = 0;

  setPosition(pos);
  calculateProjectionMat();

  //read attachments
  static const std::unordered_map<std::string_view, AttachmentMode> mode_map =
  {{"Color", AttachmentMode::Color},
   {"Depth", AttachmentMode::Depth},
   {"Stencil", AttachmentMode::Stencil},
   {"DepthStencil", AttachmentMode::DepthStencil}};

  memento.forallChildren([&memento, this] (sib_utils::memento::Memento &ch) {
      auto reader = ch.reader(memento.getCreatedObjects());
      QString str;
      reader.getType(str);
      if(str != "Attachment") return;

      Attachment att { AttachmentMode::Color, nullptr, 0 };

      QByteArray properties; bool readed = false;
      reader.get("properties", properties, readed);

      if(readed && properties.size() == sizeof(att.layer) + sizeof(att.mode))
        att = Attachment { *reinterpret_cast<AttachmentMode*>(properties.data()), nullptr, *reinterpret_cast<int*>(properties.data() + sizeof(AttachmentMode)) };

      reader.get("layer", str, readed);
      if(readed)
        att.layer = str.toInt();

      reader.get("mode", str, readed);
      if(readed)
        {
          auto el = mode_map.find(str.toStdString());
          if(el!=mode_map.end())
            att.mode = el->second;
        }

      reader.read("texture", [&att](sib_utils::TreeNode *node) {
        att.target = dynamic_cast<KawaiiTexture*>(node);
      });

      attachTexture(att.mode, att.target.data(), att.layer);
    });
}
