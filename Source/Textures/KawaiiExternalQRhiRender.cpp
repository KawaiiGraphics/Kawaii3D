#include "KawaiiExternalQRhiRender.hpp"

KawaiiExternalQRhiRender::KawaiiExternalQRhiRender():
  tex(nullptr),
  rhi(nullptr)
{ }

void KawaiiExternalQRhiRender::setRenderTargetTexture(KawaiiTexture *newTex)
{
  if(tex)
    {
      disconnect(tex, &KawaiiTexture::resolutionChanged, this, &KawaiiExternalQRhiRender::renderTargetTextureResized);
      disconnect(tex, &QObject::destroyed, this, &KawaiiExternalQRhiRender::resetRenderTargetTexture);
    }

  if(newTex != tex)
    {
      tex = newTex;
      emit renderTargetTextureChanged(tex);

      if(tex)
        {
          connect(tex, &KawaiiTexture::resolutionChanged, this, &KawaiiExternalQRhiRender::renderTargetTextureResized);
          connect(tex, &QObject::destroyed, this, &KawaiiExternalQRhiRender::resetRenderTargetTexture);
        }
    }
}

KawaiiTexture *KawaiiExternalQRhiRender::getRenderTargetTexture() const
{
  return tex;
}

void KawaiiExternalQRhiRender::setRhi(QRhi *newRhi, QRhiTextureRenderTarget *newRenderTarget)
{
  if(newRhi != rhi || newRenderTarget != renderTarget.get())
    {
      rhi = newRhi;
      renderTarget.reset(newRenderTarget);
      emit rhiChanged(rhi, renderTarget.get());
    }
}

QRhi *KawaiiExternalQRhiRender::getRhi() const
{
  return rhi;
}

QRhiTextureRenderTarget *KawaiiExternalQRhiRender::getRenderTargetRhi() const
{
  return renderTarget.get();
}

bool KawaiiExternalQRhiRender::render(const std::function<bool (QRhi *, QRhiTextureRenderTarget *)> &func)
{
  if(Q_LIKELY(rhi && renderTarget && func))
    {
      emit aboutToBeRendered();
      bool result = func(rhi, getRenderTargetRhi());
      emit rendered();
      if(result)
        {
          emit tex->updated();
          return true;
        }
    }
  return false;
}

void KawaiiExternalQRhiRender::resetRenderTargetTexture()
{
  setRenderTargetTexture(nullptr);
}

void KawaiiExternalQRhiRender::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  mem.setLink(QStringLiteral("renderTargetTexture"), tex);
}

void KawaiiExternalQRhiRender::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiExternalQRhiRender::read(sib_utils::memento::Memento::DataReader &mem)
{
  mem.read(QStringLiteral("renderTargetTexture"), [this](sib_utils::TreeNode *node) {
    setRenderTargetTexture(dynamic_cast<KawaiiTexture*>(node));
  });
}
