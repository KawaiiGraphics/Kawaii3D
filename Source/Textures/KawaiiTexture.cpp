#include "KawaiiTexture.hpp"
#include <unordered_map>
#include <QVariant>
#include <cstring>

KawaiiTexture::KawaiiTexture(KawaiiTextureType type):
  resolution({1,1,1,1}),
  type(type),
  minFilter(KawaiiTextureFilter::Linear),
  magFilter(KawaiiTextureFilter::Linear),

  wrapModeS(KawaiiTextureWrapMode::Repeat),
  wrapModeT(KawaiiTextureWrapMode::Repeat),
  wrapModeR(KawaiiTextureWrapMode::Repeat)
{
  connect(this, &KawaiiTexture::resolutionChanged, this, &KawaiiTexture::updated);
}

void KawaiiTexture::setMinFilter(KawaiiTextureFilter value)
{
  if(minFilter != value)
    emit minFilterChanged(minFilter = value);
}

void KawaiiTexture::setMagFilter(KawaiiTextureFilter value)
{
  if(magFilter != value)
    emit magFilterChanged(magFilter = value);
}

void KawaiiTexture::setWrapModeS(KawaiiTextureWrapMode value)
{
  if(wrapModeS != value)
    emit wrapModeSChanged(wrapModeS = value);
}

void KawaiiTexture::setWrapModeT(KawaiiTextureWrapMode value)
{
  if(wrapModeT != value)
    emit wrapModeTChanged(wrapModeT = value);
}

void KawaiiTexture::setWrapModeR(KawaiiTextureWrapMode value)
{
  if(wrapModeR != value)
    emit wrapModeRChanged(wrapModeR = value);
}

void KawaiiTexture::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  QByteArray d(sizeof(minFilter) + sizeof(magFilter) +
               sizeof(wrapModeS) + sizeof(wrapModeT) + sizeof(wrapModeR), Qt::Uninitialized);

  size_t offset = 0;

  std::memcpy(d.data() + offset, &minFilter, sizeof(minFilter));
  offset += sizeof(minFilter);

  std::memcpy(d.data() + offset, &magFilter, sizeof(magFilter));
  offset += sizeof(magFilter);


  std::memcpy(d.data() + offset, &wrapModeS, sizeof(wrapModeS));
  offset += sizeof(wrapModeS);

  std::memcpy(d.data() + offset, &wrapModeT, sizeof(wrapModeT));
  offset += sizeof(wrapModeT);

  std::memcpy(d.data() + offset, &wrapModeR, sizeof(wrapModeR));

  mem.setData("TextureParams", d);
}

void KawaiiTexture::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  static const std::unordered_map<KawaiiTextureFilter, QString> filter_str = {
    {KawaiiTextureFilter::Linear, "linear"},
    {KawaiiTextureFilter::Nearest, "nearest"},
    {KawaiiTextureFilter::LinearMipmapLinear, "linear-mipmap-linear"},
    {KawaiiTextureFilter::LinearMipmapNearest, "linear-mipmap-nearest"},
    {KawaiiTextureFilter::NearestMipmapLinear, "nearest-mipmap-linear"},
    {KawaiiTextureFilter::NearestMipmapNearest, "nearest-mipmap-nearest"}
  };

  static const std::unordered_map<KawaiiTextureWrapMode, QString> wrapMode_str = {
    {KawaiiTextureWrapMode::ClampToEdge, "clamp-to-edge"},
    {KawaiiTextureWrapMode::ClampToBorder, "clamp-to-border"},
    {KawaiiTextureWrapMode::MirroredRepeat, "mirrored-repeat"},
    {KawaiiTextureWrapMode::Repeat, "repeat"},
    {KawaiiTextureWrapMode::MirroredClampToEdge, "mirrored-clamp-to-edge"}
  };

  mem.setData("MinFilter", filter_str.at(minFilter));
  mem.setData("MagFilter", filter_str.at(magFilter));

  mem.setData("WrapModeS", wrapMode_str.at(wrapModeS));
  mem.setData("WrapModeT", wrapMode_str.at(wrapModeT));
  mem.setData("WrapModeR", wrapMode_str.at(wrapModeR));
}

void KawaiiTexture::read(sib_utils::memento::Memento::DataReader &mem)
{
  struct MemDataParser
  {
    KawaiiTextureFilter minFilter;
    KawaiiTextureFilter magFilter;

    KawaiiTextureWrapMode wrapModeS;
    KawaiiTextureWrapMode wrapModeT;
    KawaiiTextureWrapMode wrapModeR;
  } memParser = {minFilter, magFilter, wrapModeS, wrapModeT, wrapModeR};

  mem.read("TextureParams", [&memParser] (const QByteArray &d) {
      if(d.size() == sizeof(MemDataParser))
        std::memcpy(&memParser, d.constData(), sizeof(MemDataParser));
    });

  static const std::unordered_map<std::string_view, KawaiiTextureFilter> filter_str = {
    {"linear", KawaiiTextureFilter::Linear},
    {"nearest", KawaiiTextureFilter::Nearest},
    {"linear-mipmap-linear", KawaiiTextureFilter::LinearMipmapLinear},
    {"linear-mipmap-nearest", KawaiiTextureFilter::LinearMipmapNearest},
    {"nearest-mipmap-linear", KawaiiTextureFilter::NearestMipmapLinear},
    {"nearest-mipmap-nearest", KawaiiTextureFilter::NearestMipmapNearest}
  };

  static const std::unordered_map<std::string_view, KawaiiTextureWrapMode> wrapMode_str = {
    {"clamp-to-edge", KawaiiTextureWrapMode::ClampToEdge},
    {"clamp-to-border", KawaiiTextureWrapMode::ClampToBorder},
    {"mirrored-repeat", KawaiiTextureWrapMode::MirroredRepeat},
    {"repeat", KawaiiTextureWrapMode::Repeat},
    {"mirrored-clamp-to-edge", KawaiiTextureWrapMode::MirroredClampToEdge}
  };

  mem.readText("MinFilter", [&memParser](const QString &str) { memParser.minFilter = filter_str.at(str.toLower().toStdString()); });
  mem.readText("MagFilter", [&memParser](const QString &str) { memParser.magFilter = filter_str.at(str.toLower().toStdString()); });

  mem.readText("WrapModeS", [&memParser](const QString &str) { memParser.wrapModeS = wrapMode_str.at(str.toLower().toStdString()); });
  mem.readText("WrapModeT", [&memParser](const QString &str) { memParser.wrapModeT = wrapMode_str.at(str.toLower().toStdString()); });
  mem.readText("WrapModeR", [&memParser](const QString &str) { memParser.wrapModeR = wrapMode_str.at(str.toLower().toStdString()); });

  setMinFilter(memParser.minFilter);
  setMagFilter(memParser.magFilter);
  setWrapModeS(memParser.wrapModeS);
  setWrapModeT(memParser.wrapModeT);
  setWrapModeR(memParser.wrapModeR);
}

const std::array<uint64_t, 4> &KawaiiTexture::getResolution() const
{
  return resolution;
}

void KawaiiTexture::setResolution(const std::array<uint64_t, 4> &newResolution)
{
  if(newResolution != resolution)
    {
      resolution = newResolution;
      emit resolutionChanged(resolution);
    }
}
