#ifndef KAWAIICUBEMAP_HPP
#define KAWAIICUBEMAP_HPP

#include <QImage>
#include "KawaiiTexture.hpp"
#include <functional>

class KAWAII3D_SHARED_EXPORT KawaiiCubemap: public KawaiiTexture
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiCubemap);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  enum Side {
    PositiveX = 0,
    NegativeX = 1,
    PositiveY = 2,
    NegativeY = 3,
    PositiveZ = 4,
    NegativeZ = 5
  };

  KawaiiCubemap();
  ~KawaiiCubemap();

  inline const QImage& getImage(uint16_t side) const { return d[side]; }

  void changeImage(uint16_t side, const std::function<bool(QImage&)> &func);

  template<typename T>
  inline void setImage(uint16_t side, T &&img)
  {
    d[side] = img;
    emit imageUpdated(side);
  }

  void setFromImage(const QImage &img);
  QImage toImage() const;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void imageUpdated(quint16 i);



  //IMPLEMENT
private:
  QImage d[6];

  void writeImgData(sib_utils::memento::Memento::DataMutator &memento) const;
};

#endif // KAWAIICUBEMAP_HPP
