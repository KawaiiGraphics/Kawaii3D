#ifndef KAWAIITEXTURE_HPP
#define KAWAIITEXTURE_HPP

#include "../KawaiiDataUnit.hpp"
#include "KawaiiTextureType.hpp"
#include "KawaiiTextureFilter.hpp"
#include "KawaiiTextureWrapMode.hpp"

#include <array>

class KAWAII3D_SHARED_EXPORT KawaiiTexture: public KawaiiDataUnit
{
  Q_OBJECT

protected:
  KawaiiTexture(KawaiiTextureType type);

public:
  ~KawaiiTexture() = default;

  inline auto getType() const
  { return type; }


  inline auto getMinFilter() const
  { return minFilter; }

  inline auto getMagFilter() const
  { return magFilter; }


  inline auto getWrapModeS() const
  { return wrapModeS; }

  inline auto getWrapModeT() const
  { return wrapModeT; }

  inline auto getWrapModeR() const
  { return wrapModeR; }


  void setMinFilter(KawaiiTextureFilter value);
  void setMagFilter(KawaiiTextureFilter value);

  void setWrapModeS(KawaiiTextureWrapMode value);
  void setWrapModeT(KawaiiTextureWrapMode value);
  void setWrapModeR(KawaiiTextureWrapMode value);


  const std::array<uint64_t, 4> &getResolution() const;

protected:
  void setResolution(const std::array<uint64_t, 4> &newResolution);

  // TreeNode interface
  void writeBinary(sib_utils::memento::Memento::DataMutator &) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &) const override;
  void read(sib_utils::memento::Memento::DataReader &) override;

signals:
  void minFilterChanged(KawaiiTextureFilter filter);
  void magFilterChanged(KawaiiTextureFilter filter);

  void wrapModeSChanged(KawaiiTextureWrapMode mode);
  void wrapModeTChanged(KawaiiTextureWrapMode mode);
  void wrapModeRChanged(KawaiiTextureWrapMode mode);

  void resolutionChanged(const std::array<uint64_t, 4> &newResolution);

  void updated();



  //IMPLEMENT
private:
  std::array<uint64_t, 4> resolution;

  KawaiiTextureType type;
  KawaiiTextureFilter minFilter;
  KawaiiTextureFilter magFilter;

  KawaiiTextureWrapMode wrapModeS;
  KawaiiTextureWrapMode wrapModeT;
  KawaiiTextureWrapMode wrapModeR;
};

#endif // KAWAIITEXTURE_HPP
