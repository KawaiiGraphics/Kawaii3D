#ifndef KAWAIIDEPTHCOMPAREOPERATION_HPP
#define KAWAIIDEPTHCOMPAREOPERATION_HPP

#include <cinttypes>
#include <functional>

enum class KawaiiDepthCompareOperation: uint8_t
{
  None = 0,
  Less = 1,
  LessEqual = 2,
  Greater = 3,
  GreaterEqual = 4
};

namespace std {
  template<>
  struct hash<KawaiiDepthCompareOperation>
  {
    size_t operator()(const KawaiiDepthCompareOperation &op) const
    {
      return std::hash<uint8_t>()(static_cast<uint8_t>(op));
    }
  };
}

#endif // KAWAIIDEPTHCOMPAREOPERATION_HPP
