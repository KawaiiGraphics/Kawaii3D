#include "KawaiiDepthCubemapArray.hpp"

KawaiiDepthCubemapArray::KawaiiDepthCubemapArray():
  KawaiiTexture(KawaiiTextureType::TextureCube_Array),
  sz(1,1),
  layers(0)
{
  setWrapModeS(KawaiiTextureWrapMode::ClampToEdge);
  setWrapModeT(KawaiiTextureWrapMode::ClampToEdge);
  setWrapModeR(KawaiiTextureWrapMode::ClampToEdge);

  auto updateResolution = [this] {
      setResolution({static_cast<uint64_t>(sz.width()),
                     static_cast<uint64_t>(sz.height()),
                     6,static_cast<uint64_t>(layers)});
    };

  updateResolution();
  connect(this, &KawaiiDepthCubemapArray::resized, this, updateResolution);
  connect(this, &KawaiiDepthCubemapArray::layersChanged, this, updateResolution);
}

KawaiiDepthCubemapArray::~KawaiiDepthCubemapArray()
{
}

const QSize &KawaiiDepthCubemapArray::getSize() const
{
  return sz;
}

void KawaiiDepthCubemapArray::setSize(const QSize &value)
{
  if(sz != value)
    {
      sz = value;
      emit resized(sz);
    }
}

uint32_t KawaiiDepthCubemapArray::getLayers() const
{
  return layers;
}

void KawaiiDepthCubemapArray::setLayers(const uint32_t &value)
{
  if(layers != value)
    {
      layers = value;
      emit layersChanged(layers);
    }
}

namespace {
  struct FullSz {
    int32_t w;
    int32_t h;
    uint32_t layers;
  };
}

void KawaiiDepthCubemapArray::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  const FullSz size = { .w = sz.width(), .h = sz.height(), .layers = layers };
  mem.setData(QStringLiteral("size"), QByteArray(reinterpret_cast<const char*>(&size), sizeof(size)));
  mem.setData(QStringLiteral("compare_operation"), QByteArray(reinterpret_cast<const char*>(&compareOperation), sizeof(compareOperation)));
  KawaiiTexture::writeBinary(mem);
}

void KawaiiDepthCubemapArray::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  static const std::unordered_map<KawaiiDepthCompareOperation, QString> compareOpHash = {
    { KawaiiDepthCompareOperation::None,         QStringLiteral("none") },
    { KawaiiDepthCompareOperation::Less,         QStringLiteral("less") },
    { KawaiiDepthCompareOperation::LessEqual,    QStringLiteral("less_equal") },
    { KawaiiDepthCompareOperation::Greater,      QStringLiteral("greater") },
    { KawaiiDepthCompareOperation::GreaterEqual, QStringLiteral("greater_equal") },
  };

  mem.setData(QStringLiteral("size"), QStringLiteral("%1 * %2 * %3").arg(sz.width()).arg(sz.height()).arg(layers));
  mem.setData(QStringLiteral("compare_operation"), compareOpHash.at(compareOperation));
  KawaiiTexture::writeText(mem);
}

void KawaiiDepthCubemapArray::read(sib_utils::memento::Memento::DataReader &mem)
{
  bool readed = false;
  mem.readText(QStringLiteral("size"), [this, &readed] (const QString &str) {
      auto terms = QStringView(str).split('*');
      if(terms.size() >= 3)
        {
          setSize(QSize(terms.at(0).trimmed().toInt(), terms.at(1).trimmed().toInt()));
          setLayers(terms.at(2).trimmed().toUInt());
          readed = true;
        }
    });
  if(!readed)
    mem.read(QStringLiteral("size"), [this] (const QByteArray &ar) {
        if(static_cast<size_t>(ar.size()) >= sizeof(FullSz))
          {
            const FullSz *size = reinterpret_cast<const FullSz*>(ar.data());
            setSize(QSize(size->w, size->h));
            setLayers(size->layers);
          }
      });

  readed = false;
  mem.readText(QStringLiteral("compare_operation"), [this, &readed] (const QString &str) {
      static const QHash<QString, KawaiiDepthCompareOperation> compareOpHash = {
        { QStringLiteral("none"),          KawaiiDepthCompareOperation::None },
        { QStringLiteral("less"),          KawaiiDepthCompareOperation::Less },
        { QStringLiteral("less_equal"),    KawaiiDepthCompareOperation::LessEqual },
        { QStringLiteral("greater"),       KawaiiDepthCompareOperation::Greater },
        { QStringLiteral("greater_equal"), KawaiiDepthCompareOperation::GreaterEqual },
      };
      if(auto el = compareOpHash.find(str.toLower()); el != compareOpHash.end())
        {
          setCompareOperation(el.value());
          readed = true;
        }
    });
  if(!readed)
    mem.read(QStringLiteral("compare_operation"), [this] (const QByteArray &ar) {
        if(static_cast<size_t>(ar.size()) >= sizeof(KawaiiDepthCompareOperation))
          setCompareOperation(*reinterpret_cast<const KawaiiDepthCompareOperation*>(ar.data()));
      });

  KawaiiTexture::read(mem);
}

KawaiiDepthCompareOperation KawaiiDepthCubemapArray::getCompareOperation() const
{
  return compareOperation;
}

void KawaiiDepthCubemapArray::setCompareOperation(KawaiiDepthCompareOperation value)
{
  if(compareOperation != value)
    {
      compareOperation = value;
      emit compareOperationChanged(compareOperation);
    }
}
