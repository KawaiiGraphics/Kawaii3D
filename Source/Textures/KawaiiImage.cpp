#include "KawaiiImage.hpp"

#include <QBuffer>
#include <QVariant>
#include "KawaiiConfig.hpp"

KawaiiImage::KawaiiImage():
  KawaiiTexture(KawaiiTextureType::Texture2D)
{
  auto onImgUpdated = [this] {
      setResolution({static_cast<uint64_t>(img.width()),
                     static_cast<uint64_t>(img.height()),
                     1, 1});
      emit updated();
    };
  connect(this, &KawaiiImage::imageUpdated, this, onImgUpdated);
}

KawaiiImage::~KawaiiImage()
{
}

const QImage *KawaiiImage::operator->() const
{
  return &img;
}

void KawaiiImage::changeImage(const std::function<bool (QImage &)> &func)
{
  if(func(img))
    emit imageUpdated();
}

void KawaiiImage::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  KawaiiTexture::writeBinary(memento);

  writeImgData(memento);
}

void KawaiiImage::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  KawaiiTexture::writeText(memento);

  //Data can not be stringified to human-readable text
  writeImgData(memento);
}

void KawaiiImage::read(sib_utils::memento::Memento::DataReader &memento)
{
  KawaiiTexture::read(memento);

  memento.read("picture", [this] (const QByteArray &buf) {
      if(img.loadFromData(buf))
        emit imageUpdated();
    });
}

void KawaiiImage::writeImgData(sib_utils::memento::Memento::DataMutator &memento) const
{
  const auto &fmt = KawaiiConfig::getInstance().getPreferredImgFormat();
  QByteArray data;

  QBuffer buf(&data);
  buf.open(QBuffer::WriteOnly);
  img.save(&buf, fmt.name, fmt.quality);
  buf.close();

  memento.setData("picture", data);
}
