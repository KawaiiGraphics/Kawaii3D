#ifndef KAWAIIIMGFORMAT_HPP
#define KAWAIIIMGFORMAT_HPP
#include <QByteArray>

struct KawaiiImgFormat
{
  QByteArray name;
  int quality;
};

#endif // KAWAIIIMGFORMAT_HPP
