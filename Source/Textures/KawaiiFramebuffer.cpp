#include <glm/gtc/matrix_transform.hpp>
#include <sib_utils/Strings.hpp>
#include "KawaiiFramebuffer.hpp"
#include <QtConcurrent>
#include <cmath>

KawaiiFramebuffer::KawaiiFramebuffer(const glm::uvec2 &size):
  sz(size)
{
  setRenderableSize(sz);
}

KawaiiFramebuffer::~KawaiiFramebuffer()
{
}

void KawaiiFramebuffer::setSize(const glm::uvec2 &size)
{
  if(sz == size)
    return;

  sz = size;
  setRenderableSize(sz);
  emit sizeChanged(sz);
}

void KawaiiFramebuffer::attachTexture(quint32 gbuf, KawaiiTexture *texture, int layer)
{
  auto el = attachments.find(gbuf);

  if(texture)
    {
      if(el != attachments.end())
        {
          if(el->second.target != texture || el->second.layer != layer)
            {
              el->second.target = texture;
              el->second.layer = layer;
              emit textureAttached(gbuf, texture, layer);
            }
        } else
        {
          attachments.insert(std::pair(gbuf, Attachment { texture, layer, gbuf }));
          emit textureAttached(gbuf, texture, layer);
        }
    } else
    if(el != attachments.end())
      {
        attachments.erase(el);
        emit textureDetached(gbuf);
      }
}

void KawaiiFramebuffer::forallAtachments(const std::function<void (const KawaiiFramebuffer::Attachment &)> &func) const
{
  if(Q_LIKELY(func))
    for(const auto &i: attachments)
      func(i.second);
}

QFuture<void> KawaiiFramebuffer::forallAtachmentsP(const std::function<void (const KawaiiFramebuffer::Attachment &)> &func) const
{
  if(Q_LIKELY(func))
    return QtConcurrent::map(attachments, [&func] (const std::pair<quint32, Attachment> &p) {
        func(p.second);
      });
  else
    return QFuture<void>();
}

void KawaiiFramebuffer::setRenderpass(KawaiiRenderpass *newRenderpass)
{
  if(newRenderpass != getRenderpass())
    {
      KawaiiRenderTarget::setRenderpass(newRenderpass);
      emit renderpassChanged(newRenderpass);
    }
}

void KawaiiFramebuffer::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  QByteArray bytes;
  QDataStream st(&bytes, QIODevice::WriteOnly);
  st << static_cast<quint32>(sz.x) << static_cast<quint32>(sz.y);
  st.setDevice(nullptr);

  memento.setData(QStringLiteral("size"), bytes)
      .setLink(QStringLiteral("renderpass"), getRenderpass());

  forallAtachments([&memento] (const Attachment &att) {
      sib_utils::memento::Memento *mem;
      memento.createChildMemento(QStringLiteral("Attachment"), mem);

      QByteArray bytes;
      QDataStream st(&bytes, QIODevice::WriteOnly);
      st << static_cast<qint32>(att.layer)
         << att.useGbuf;
      st.setDevice(nullptr);

      auto mutator = mem->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks);
      mutator.setData(QStringLiteral("properties"), bytes)
          .setLink(QStringLiteral("texture"), att.target.data());
    });
}

void KawaiiFramebuffer::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setData(QStringLiteral("width"), QString::number(sz.x))
      .setData(QStringLiteral("height"), QString::number(sz.y))
      .setLink(QStringLiteral("renderpass"), getRenderpass());

  forallAtachments([&memento] (const Attachment &att) {
      sib_utils::memento::Memento *mem;
      memento.createChildMemento(QStringLiteral("Attachment"), mem);

      auto mutator = mem->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks);
      mutator.setData(QStringLiteral("layer"), QString::number(att.layer))
          .setData(QStringLiteral("gbuf"), QString::number(att.useGbuf))
          .setLink(QStringLiteral("texture"), att.target.data());
    });
}

void KawaiiFramebuffer::read(sib_utils::memento::Memento::DataReader &memento)
{
  //explicitly detach all textures
  std::vector<quint32> enabledAttModes;
  enabledAttModes.reserve(attachments.size());
  for(const auto &i: attachments)
    enabledAttModes.push_back(i.first);
  attachments.clear();
  for(const auto &i: enabledAttModes)
    emit textureDetached(i);

  //Try to read text
  glm::uvec2 size;
  bool widthReaded = false,
      heightReaded = false;

  memento.readText(QStringLiteral("width"), [&size, &widthReaded](const QString &str) {
      size.x = str.toUInt(&widthReaded);
    });

  memento.readText(QStringLiteral("height"), [&size, &heightReaded](const QString &str) {
      size.y = str.toUInt(&heightReaded);
    });

  //Try to read binary
  if(!widthReaded || !heightReaded)
    memento.read(QStringLiteral("size"), [&size, widthReaded, heightReaded] (const QByteArray &bytes) {
        QDataStream st(bytes);
        quint32 w, h;
        st >> w >> h;
        if(!widthReaded)
          size.x = w;
        if(!heightReaded)
          size.y = h;
      });

  setSize(size);

  //read link to camera
  memento.read(QStringLiteral("renderpass"), [this](sib_utils::TreeNode *node) {
    setRenderpass(qobject_cast<KawaiiRenderpass*>(node));
  });

  //read attachments
  memento.forallChildren([&memento, this] (sib_utils::memento::Memento &ch) {
      auto reader = ch.reader(memento.getCreatedObjects());
      QString str;
      reader.getType(str);
      if(str != sib_utils::strings::getQLatin1Str("Attachment")) return;

      Attachment att { nullptr, 0, 0 };

      reader.read(QStringLiteral("properties"), [&att](const QByteArray &bytes) {
          qint32 layer;
          QDataStream st(bytes);
          st >> layer >> att.useGbuf;
          att.layer = layer;
        })
          .readText(QStringLiteral("layer"), [&att] (const QString &text) {
          att.layer = text.toInt();
        })
          .read(QStringLiteral("gbuf"), [&att](const QString &text) {
          bool ok = false;
          quint32 gbuf = text.toUInt(&ok);
          if(ok)
            att.useGbuf = gbuf;
        })
          .read("texture", [&att](sib_utils::TreeNode *node) {
        att.target = dynamic_cast<KawaiiTexture*>(node);
      });

      attachTexture(att.useGbuf, att.target.data(), att.layer);
    });
}
