#ifndef KAWAIITEXTUREFILTER_HPP
#define KAWAIITEXTUREFILTER_HPP
#include <stdint.h>
#include <functional>

enum class KawaiiTextureFilter: uint8_t
{
  Linear,
  Nearest,
  NearestMipmapNearest,
  LinearMipmapNearest,
  NearestMipmapLinear,
  LinearMipmapLinear
};

namespace std {
  template<>
  struct hash<KawaiiTextureFilter>
  {
    size_t operator()(const KawaiiTextureFilter &filter) const
    {
      return std::hash<uint8_t>()(static_cast<uint8_t>(filter));
    }
  };
}

#endif // KAWAIITEXTUREFILTER_HPP
