#include "KawaiiDepthTex2dArray.hpp"
#include <unordered_map>
#include <QHash>

KawaiiDepthTex2dArray::KawaiiDepthTex2dArray():
  KawaiiTexture(KawaiiTextureType::Texture2D_Array),
  sz(128, 128),
  layers(1),
  compareOperation(KawaiiDepthCompareOperation::None)
{
  setWrapModeS(KawaiiTextureWrapMode::ClampToBorder);
  setWrapModeT(KawaiiTextureWrapMode::ClampToBorder);

  auto updateResolution = [this] {
      setResolution({static_cast<uint64_t>(sz.width()),
                     static_cast<uint64_t>(sz.height()),
                     static_cast<uint64_t>(layers), 1});
    };

  updateResolution();
  connect(this, &KawaiiDepthTex2dArray::resized, this, updateResolution);
  connect(this, &KawaiiDepthTex2dArray::layersChanged, this, updateResolution);
}

KawaiiDepthTex2dArray::~KawaiiDepthTex2dArray()
{
}

const QSize &KawaiiDepthTex2dArray::getSize() const
{
  return sz;
}

void KawaiiDepthTex2dArray::setSize(const QSize &value)
{
  if(sz != value)
    {
      sz = value;
      emit resized(sz);
    }
}

uint32_t KawaiiDepthTex2dArray::getLayers() const
{
  return layers;
}

void KawaiiDepthTex2dArray::setLayers(const uint32_t &value)
{
  if(layers != value)
    {
      layers = value;
      emit layersChanged(layers);
    }
}

namespace {
  struct FullSz {
    int32_t w;
    int32_t h;
    uint32_t layers;
  };
}

void KawaiiDepthTex2dArray::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  const FullSz size = { .w = sz.width(), .h = sz.height(), .layers = layers };
  mem.setData(QStringLiteral("size"), QByteArray(reinterpret_cast<const char*>(&size), sizeof(size)));
  mem.setData(QStringLiteral("compare_operation"), QByteArray(reinterpret_cast<const char*>(&compareOperation), sizeof(compareOperation)));
  KawaiiTexture::writeBinary(mem);
}

void KawaiiDepthTex2dArray::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  static const std::unordered_map<KawaiiDepthCompareOperation, QString> compareOpHash = {
    { KawaiiDepthCompareOperation::None,         QStringLiteral("none") },
    { KawaiiDepthCompareOperation::Less,         QStringLiteral("less") },
    { KawaiiDepthCompareOperation::LessEqual,    QStringLiteral("less_equal") },
    { KawaiiDepthCompareOperation::Greater,      QStringLiteral("greater") },
    { KawaiiDepthCompareOperation::GreaterEqual, QStringLiteral("greater_equal") },
  };

  mem.setData(QStringLiteral("size"), QStringLiteral("%1 * %2 * %3").arg(sz.width()).arg(sz.height()).arg(layers));
  mem.setData(QStringLiteral("compare_operation"), compareOpHash.at(compareOperation));
  KawaiiTexture::writeText(mem);
}

void KawaiiDepthTex2dArray::read(sib_utils::memento::Memento::DataReader &mem)
{
  bool readed = false;
  mem.readText(QStringLiteral("size"), [this, &readed] (const QString &str) {
      auto terms = QStringView(str).split('*');
      if(terms.size() >= 3)
        {
          setSize(QSize(terms.at(0).trimmed().toInt(), terms.at(1).trimmed().toInt()));
          setLayers(terms.at(2).trimmed().toUInt());
          readed = true;
        }
    });
  if(!readed)
    mem.read(QStringLiteral("size"), [this] (const QByteArray &ar) {
        if(static_cast<size_t>(ar.size()) >= sizeof(FullSz))
          {
            const FullSz *size = reinterpret_cast<const FullSz*>(ar.data());
            setSize(QSize(size->w, size->h));
            setLayers(size->layers);
          }
      });

  readed = false;
  mem.readText(QStringLiteral("compare_operation"), [this, &readed] (const QString &str) {
      static const QHash<QString, KawaiiDepthCompareOperation> compareOpHash = {
        { QStringLiteral("none"),          KawaiiDepthCompareOperation::None },
        { QStringLiteral("less"),          KawaiiDepthCompareOperation::Less },
        { QStringLiteral("less_equal"),    KawaiiDepthCompareOperation::LessEqual },
        { QStringLiteral("greater"),       KawaiiDepthCompareOperation::Greater },
        { QStringLiteral("greater_equal"), KawaiiDepthCompareOperation::GreaterEqual },
      };
      if(auto el = compareOpHash.find(str.toLower()); el != compareOpHash.end())
        {
          setCompareOperation(el.value());
          readed = true;
        }
    });
  if(!readed)
    mem.read(QStringLiteral("compare_operation"), [this] (const QByteArray &ar) {
        if(static_cast<size_t>(ar.size()) >= sizeof(KawaiiDepthCompareOperation))
          setCompareOperation(*reinterpret_cast<const KawaiiDepthCompareOperation*>(ar.data()));
      });

  KawaiiTexture::read(mem);
}

KawaiiDepthCompareOperation KawaiiDepthTex2dArray::getCompareOperation() const
{
  return compareOperation;
}

void KawaiiDepthTex2dArray::setCompareOperation(KawaiiDepthCompareOperation value)
{
  if(compareOperation != value)
    {
      compareOperation = value;
      emit compareOperationChanged(compareOperation);
    }
}
