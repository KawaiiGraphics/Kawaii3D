#ifndef KAWAIIDEPTHCUBEMAPARRAY_HPP
#define KAWAIIDEPTHCUBEMAPARRAY_HPP

#include "KawaiiTexture.hpp"
#include "KawaiiDepthCompareOperation.hpp"

#include <QSize>

class KAWAII3D_SHARED_EXPORT KawaiiDepthCubemapArray : public KawaiiTexture
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiDepthCubemapArray);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiDepthCubemapArray();
  ~KawaiiDepthCubemapArray();

  const QSize &getSize() const;
  void setSize(const QSize &value);

  uint32_t getLayers() const;
  void setLayers(const uint32_t &value);

  KawaiiDepthCompareOperation getCompareOperation() const;
  void setCompareOperation(KawaiiDepthCompareOperation value);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;

signals:
  void resized(const QSize &size);
  void layersChanged(uint32_t layers);
  void compareOperationChanged(KawaiiDepthCompareOperation op);



  // IMPLEMENT
private:
  QSize sz;
  uint32_t layers;
  KawaiiDepthCompareOperation compareOperation;
};

#endif // KAWAIIDEPTHCUBEMAPARRAY_HPP
