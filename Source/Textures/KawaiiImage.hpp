#ifndef KAWAIIIMAGE_HPP
#define KAWAIIIMAGE_HPP

#include <QImage>
#include <QMetaMethod>
#include "KawaiiTexture.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiImage: public KawaiiTexture
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiImage);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiImage();
  ~KawaiiImage();

  //Methods for access to QImage
  inline auto& getImage() const
  { return img; }

  template<typename T>
  inline void setImage(T &&img)
  {
    this->img = std::move(img);
    emit imageUpdated();
  }

  const QImage* operator->() const;

  void changeImage(const std::function<bool(QImage&)> &func);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void imageUpdated();



  //IMPLEMENT
private:
  QImage img;

  void writeImgData(sib_utils::memento::Memento::DataMutator &memento) const;
};

#endif // KAWAIIIMAGE_HPP
