#include "KawaiiPerspective.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

namespace {
  constexpr float pi = M_PI;
  constexpr float deg_to_rad_k = pi / 180.0;
  constexpr float rad_to_deg_k = 180.0 / pi;
}

KawaiiPerspective::KawaiiPerspective(float fovRadians, float nearClip, float farClip):
  KawaiiProjection(nearClip, farClip),
  fovRadians(fovRadians)
{

}

KawaiiPerspective::KawaiiPerspective(float nearClip, float farClip):
  KawaiiPerspective(45.0 * deg_to_rad_k, nearClip, farClip)
{
}

glm::mat4 KawaiiPerspective::calculate(const glm::uvec2 &surfaceSize) const
{
  return glm::perspectiveRH_NO(fovRadians,
                               static_cast<float>(surfaceSize.x) / static_cast<float>(surfaceSize.y),
                               getNearClip(), getFarClip());
}

void KawaiiPerspective::setFovDegrees(float deg)
{
  setFovRadians(deg * deg_to_rad_k);
}

void KawaiiPerspective::setFovRadians(float rad)
{
  if(!qFuzzyCompare(fovRadians, rad))
    {
      fovRadians = rad;
      updateRequest();
    }
}

float KawaiiPerspective::getFovDegrees() const
{
  return fovRadians * rad_to_deg_k;
}

float KawaiiPerspective::getFovRadians() const
{
  return fovRadians;
}

KawaiiProjection *KawaiiPerspective::clone() const
{
  KawaiiPerspective *clone = new KawaiiPerspective(fovRadians, getNearClip(), getFarClip());
  clone->setClipCorrection(getClipCorrection());
  clone->setSize(getSize());
  return clone;
}

void KawaiiPerspective::writeTextMemento(sib_utils::memento::Memento::DataMutator &mem) const
{
  mem.setData(QStringLiteral("FOV"), QString::number(rad_to_deg_k * fovRadians, 'f', 10))
      .setData(QStringLiteral("nearClip"), QString::number(getNearClip(), 'f', 10))
      .setData(QStringLiteral("farClip"), QString::number(getFarClip(), 'f', 10))
      .setData(QStringLiteral("projectionType"), QStringLiteral("KawaiiPerspective"));
}

namespace {
  struct Properties
  {
    float fovRadians;
    float nearClip;
    float farClip;
  };
}

void KawaiiPerspective::writeBinaryMemento(sib_utils::memento::Memento::DataMutator &mem) const
{
  const Properties p = {
    .fovRadians = fovRadians,
    .nearClip = getNearClip(),
    .farClip = getFarClip()
  };
  mem.setData(QStringLiteral("projection"), QByteArray(reinterpret_cast<const char*>(&p), sizeof(p)))
      .setData(QStringLiteral("projectionType"), QStringLiteral("KawaiiPerspective"));;
}

void KawaiiPerspective::readMemento(sib_utils::memento::Memento::DataReader &mem)
{
  bool readedFov = false,
      readedNearClip = false,
      readedFarClip = false;

  mem.readText(QStringLiteral("FOV"), [this, &readedFov] (const QString &val) {
      const float f = val.toFloat(&readedFov);
      if(readedFov)
        setFovDegrees(f);
    });
  mem.readText(QStringLiteral("nearClip"), [this, &readedNearClip] (const QString &val) {
      const float f = val.toFloat(&readedNearClip);
      if(readedNearClip)
        setNearClip(f);
    });
  mem.readText(QStringLiteral("farClip"), [this, &readedFarClip] (const QString &val) {
      const float f = val.toFloat(&readedFarClip);
      if(readedFarClip)
        setFarClip(f);
    });

  if(!readedFov || !readedNearClip || !readedFarClip)
    mem.read(QStringLiteral("projection"), [this, readedFov, readedNearClip, readedFarClip] (const QByteArray &val) {
        if(static_cast<size_t>(val.size()) > sizeof(Properties))
          {
            const Properties *props = reinterpret_cast<const Properties*>(val.constData());
            if(!readedFov)
              setFovRadians(props->fovRadians);
            if(!readedNearClip)
              setNearClip(props->nearClip);
            if(!readedFarClip)
              setFarClip(props->farClip);
          }
      });
}
