#include "KawaiiMaterial.hpp"
#include "KawaiiGpuBuf.hpp"
#include <QVariant>

KawaiiMaterial::KawaiiMaterial():
  uniforms(nullptr)
{
}

KawaiiMaterial::~KawaiiMaterial()
{
}

void KawaiiMaterial::setUniforms(KawaiiGpuBuf *value)
{
  if(uniforms)
    disconnect(uniforms, &KawaiiGpuBuf::destroyed, this, &KawaiiMaterial::detachUniforms);
  uniforms = value;
  if(uniforms)
    connect(uniforms, &KawaiiGpuBuf::destroyed, this, &KawaiiMaterial::detachUniforms);
  emit uniformsChanged(value);
}

KawaiiGpuBuf *KawaiiMaterial::getUniforms() const
{
  return uniforms;
}

void KawaiiMaterial::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setLink("uniforms_buffer", uniforms);
}

void KawaiiMaterial::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  //Data can not be stringified to human-readable text
  writeBinary(memento);
}

void KawaiiMaterial::read(sib_utils::memento::Memento::DataReader &memento)
{
  memento.read("uniforms_buffer", [this] (TreeNode *uboCandidate) {
    setUniforms(dynamic_cast<KawaiiGpuBuf*>(uboCandidate));
  });
}
