#include "KawaiiGpuBuf.hpp"
#include "Textures/KawaiiTexture.hpp"
#include <QVariant>
#include <cstring>

int KawaiiGpuBuf::size_t_qMetaId = qRegisterMetaType<size_t>("size_t");

KawaiiGpuBuf::KawaiiGpuBuf():
  dataSize(0),
  data(nullptr)
{
  resetCopyFromFunc();
  connect(this, &KawaiiGpuBuf::relocated, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::dataChanged, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::textureBinded, this, &KawaiiGpuBuf::updated);
}

KawaiiGpuBuf::KawaiiGpuBuf(const void *ptr, size_t n):
  dataSize(n),
  data(n? malloc(n): nullptr)
{
  resetCopyFromFunc();
  if(n)
    {
      if(ptr)
        memcpy(data, ptr, n);
      else
        memset(data, 0, n);
    }
  connect(this, &KawaiiGpuBuf::relocated, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::dataChanged, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::textureBinded, this, &KawaiiGpuBuf::updated);
}

KawaiiGpuBuf::~KawaiiGpuBuf()
{
  freeData();
}

KawaiiGpuBuf::KawaiiGpuBuf(KawaiiGpuBuf &&orig):
  dataSize(orig.dataSize),
  data(orig.data)
{
  if(orig.data)
    {
      orig.data = nullptr;
      orig.dataSize = 0;
    }
  emit orig.wasAcquired(this);
  connect(this, &KawaiiGpuBuf::relocated, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::dataChanged, this, &KawaiiGpuBuf::updated);
  connect(this, &KawaiiGpuBuf::textureBinded, this, &KawaiiGpuBuf::updated);
}

KawaiiGpuBuf &KawaiiGpuBuf::operator=(KawaiiGpuBuf &&orig)
{
  freeData();

  data = orig.data;
  dataSize = orig.dataSize;

  if(orig.data)
    {
      orig.data = nullptr;
      orig.dataSize = 0;
    }
  emit orig.wasAcquired(this);

  emit relocated(data, dataSize);
  emit updated();
  return *this;
}

void KawaiiGpuBuf::setData(const void *ptr, size_t n)
{
  realloc(n);
  fill(0, ptr, n);
}

size_t KawaiiGpuBuf::getDataSize() const
{
  return dataSize;
}

void *KawaiiGpuBuf::getData()
{
  return data;
}

const void *KawaiiGpuBuf::getData() const
{
  return data;
}

void KawaiiGpuBuf::fill(size_t offset, const void *ptr, size_t bytes)
{
  void *subData = reinterpret_cast<std::byte*>(data) + offset;
  bytes = std::min(bytes, dataSize - offset);
  memcpy(subData, ptr, bytes);
  emit dataChanged(offset, bytes);
}

void KawaiiGpuBuf::dump(size_t offset, void *dest, size_t bytes) const
{
  bytes = std::min(bytes, dataSize - offset);
  memcpy(dest, reinterpret_cast<std::byte*>(data) + offset, bytes);
}

void KawaiiGpuBuf::realloc(size_t newSize)
{
  if(newSize == getDataSize())
    return;

  if(!newSize)
    {
      free(data);
      data = nullptr;
    } else
    {
      auto buf = ::realloc(data, newSize);
      if(buf)
        data = buf;
      else
        throw std::bad_alloc();
    }
  dataSize = newSize;
  emit relocated(data, dataSize);
}

void KawaiiGpuBuf::append(const void *buf, size_t bufSize)
{
  size_t sz = getDataSize();
  realloc(sz + bufSize);
  fill(sz, buf, bufSize);
}

void KawaiiGpuBuf::erase(size_t offset, size_t bytes)
{
  size_t n = dataSize - offset - bytes;

  if(offset + bytes < dataSize)
    memmove(static_cast<char*>(data) + offset, static_cast<char*>(data) + offset + bytes, n);
  realloc(dataSize - bytes);
}

std::list<KawaiiGpuBuf::PartialAlias>::iterator
KawaiiGpuBuf::addAlias(KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size)
{
  if(!src)
    return aliases.end();

  PartialAlias a(src, this, srcOffset, dstOffset, size);
  a.onSrcUpdated = connect(src, &KawaiiGpuBuf::dataChanged, this, [this, a] (size_t offset, size_t bytes) {
      aliasSrcUpdated(a, offset, bytes);
    });
  a.onSrcRelocated = connect(src, &KawaiiGpuBuf::relocated, this, [this, src] { aliasSrcRelocated(src); });
  a.onSrcWasAquired = connect(src, &KawaiiGpuBuf::wasAcquired, this, [this, src] (KawaiiGpuBuf *newSrc) { aliasSrcWasAquired(src, newSrc); });
  a.onSrcDestroyed = connect(src, &QObject::destroyed, this, &KawaiiGpuBuf::aliasSrcDestroyed, Qt::UniqueConnection);
  if(!a.onSrcDestroyed)
    for(const auto &i: aliases)
      if(i.src == src && Q_LIKELY(i.onSrcDestroyed))
        {
          a.onSrcDestroyed = i.onSrcDestroyed;
          break;
        }
  auto el = aliases.insert(aliases.end(), a);
  aliasSrcUpdated(a, 0, src->getDataSize());
  return el;
}

std::list<KawaiiGpuBuf::PartialAlias>::iterator
KawaiiGpuBuf::removeAlias(const std::list<KawaiiGpuBuf::PartialAlias>::iterator &iterator)
{
  if(aliases.end() != iterator && iterator->dst == this)
    {
      QObject::disconnect(iterator->onSrcUpdated);
      QObject::disconnect(iterator->onSrcRelocated);
      QObject::disconnect(iterator->onSrcDestroyed);
      return aliases.erase(iterator);
    } else
    return iterator;
}

void KawaiiGpuBuf::bindTexture(const QString &bindingName, KawaiiTexture *tex)
{
  if(!tex)
    return unbindTexture(bindingName);

  auto el = bindedTextures.find(bindingName);
  if(el != bindedTextures.end())
    {
      if(el.value() == tex)
        return;
      detachTexture(el.value());
      *el = tex;
    } else
    el = bindedTextures.insert(bindingName, tex);
  connect(el.value(), &KawaiiTexture::updated, this, &KawaiiGpuBuf::updated);
  connect(el.value(), &QObject::destroyed, this, qOverload<QObject*>(&KawaiiGpuBuf::unbindTexture));
  emit textureBinded(bindingName, tex);
}

void KawaiiGpuBuf::unbindTexture(const QString &bindingName)
{
  auto el = bindedTextures.find(bindingName);
  if(el != bindedTextures.end())
    unbindTexture(el);
}

void KawaiiGpuBuf::unbindTexture(QObject *texture)
{
  for(auto i = bindedTextures.begin(); i != bindedTextures.end(); ++i)
    if(i.value() == texture)
      {
        unbindTexture(i);
        return;
      }
}

void KawaiiGpuBuf::forallTextureBindings(const std::function<void (const QString &, KawaiiTexture *)> &func) const
{
  if(Q_LIKELY(func))
    for(auto i = bindedTextures.cbegin(); i != bindedTextures.cend(); ++i)
      func(i.key(), i.value());
}

size_t KawaiiGpuBuf::bindedTexturesCount() const
{
  if(Q_LIKELY(bindedTextures.size() >= 0))
    return static_cast<size_t>(bindedTextures.size());
  else
    return 0;
}

KawaiiTexture *KawaiiGpuBuf::getBindedTexture(const QString &bindingName) const
{
  return bindedTextures.value(bindingName, nullptr);
}

void KawaiiGpuBuf::setCopyFromFunc(const std::function<void (KawaiiGpuBuf *, size_t, size_t, size_t)> &newCopyFrom)
{
  if(newCopyFrom)
    copyFrom = newCopyFrom;
  else
    copyFrom = std::bind(&KawaiiGpuBuf::naiveCopyFrom, this,
                         std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
}

void KawaiiGpuBuf::resetCopyFromFunc()
{
  setCopyFromFunc({});
}

namespace {
  struct AliasMemento {
    const size_t srcOffset;
    const size_t dstOffset;
    const size_t size;
  };

  const QString aliasMementoType = QStringLiteral("alias");
  const QString textureBindingMementoType = QStringLiteral("texture_binding");
}

void KawaiiGpuBuf::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setData("data", QByteArray(reinterpret_cast<char*>(data), dataSize));
  for(const auto &i: aliases)
    {
      const AliasMemento aliasMem = {
        .srcOffset = i.srcOffset,
        .dstOffset = i.dstOffset,
        .size = i.size
      };
      sib_utils::memento::Memento *childMem;
      memento.createChildMemento(aliasMementoType, childMem);
      childMem->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks)
          .setLink(QStringLiteral("target"), i.src)
          .setData(QStringLiteral("data"), QByteArray(reinterpret_cast<const char*>(&aliasMem), sizeof(AliasMemento)));
    }
  forallTextureBindings([&memento] (const QString &bindingName, KawaiiTexture *tex) {
      sib_utils::memento::Memento *childMem;
      memento.createChildMemento(textureBindingMementoType, childMem);
      childMem->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks)
          .setLink(QStringLiteral("target"), tex)
          .setData(QStringLiteral("name"), bindingName);
    });
}

void KawaiiGpuBuf::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  //Data can not be stringified to human-readable text
  writeBinary(memento);
}

void KawaiiGpuBuf::read(sib_utils::memento::Memento::DataReader &memento)
{
  const auto &createdObjects = memento.getCreatedObjects();

  memento.read("data", [this](const QByteArray &bytes) {
      setData(bytes.constData(), bytes.size());
    })
      .forallChildren([this, &createdObjects] (sib_utils::memento::Memento &childMem) {
      QString childType;
      auto reader = childMem.reader(createdObjects);
      reader.getType(childType);
      if(childType == aliasMementoType)
        {
          reader.read(QStringLiteral("data"), [this, &reader] (const QByteArray &data) {
              auto *aliasMem = reinterpret_cast<const AliasMemento*>(data.data());
              reader.read(QStringLiteral("target"), [this, aliasMem] (sib_utils::TreeNode *node) {
                  if(auto target = qobject_cast<KawaiiGpuBuf*>(node); target)
                    addAlias(target, aliasMem->srcOffset, aliasMem->dstOffset, aliasMem->size);
                });
            });
        }
      else if(childType == textureBindingMementoType)
        {
          reader.read(QStringLiteral("target"), [this, &reader] (sib_utils::TreeNode *node) {
              if(auto target = qobject_cast<KawaiiTexture*>(node); target)
                reader.readText(QStringLiteral("name"), [this, target] (const QString &bindingName) {
                    bindTexture(bindingName, target);
                  });
            });
        }
    });
}

void KawaiiGpuBuf::freeData()
{
  for(auto &i: aliases)
    {
      disconnect(i.onSrcDestroyed);
      disconnect(i.onSrcRelocated);
      disconnect(i.onSrcUpdated);
    }
  aliases.clear();

  while(!bindedTextures.empty())
    unbindTexture(bindedTextures.begin());

  if(data)
    free(data);
}

namespace {
  std::pair<size_t, size_t> intersect(size_t a0, size_t b0, size_t a1, size_t b1)
  {
    using namespace std;
    return { max(a0, a1), min(b0, b1) };
  }
}

void KawaiiGpuBuf::aliasSrcUpdated(const PartialAlias &a, size_t offset, size_t bytes)
{
  if(Q_LIKELY(copyFrom))
    {
      const auto interval = intersect(a.srcOffset, a.srcOffset + a.size, offset, offset + bytes);
      if(interval.first < interval.second)
        {
          const size_t relOffset = interval.first - a.srcOffset;
          const size_t sz = interval.second - interval.first;
          copyFrom(a.src, interval.first, a.dstOffset + relOffset, sz);
        }
    }
}

void KawaiiGpuBuf::aliasSrcRelocated(KawaiiGpuBuf *src)
{
  if(Q_LIKELY(copyFrom))
    for(const auto &i: aliases)
      if(i.src == src)
        copyFrom(src, i.srcOffset, i.dstOffset, i.size);
}

void KawaiiGpuBuf::aliasSrcWasAquired(KawaiiGpuBuf *oldSrc, KawaiiGpuBuf *newSrc)
{
  for(auto &a: aliases)
    if(a.src == oldSrc)
      {
        disconnect(a.onSrcDestroyed);
        disconnect(a.onSrcRelocated);
        disconnect(a.onSrcUpdated);

        a.onSrcUpdated = connect(newSrc, &KawaiiGpuBuf::dataChanged, this, [this, a] (size_t offset, size_t bytes) {
            aliasSrcUpdated(a, offset, bytes);
          });
        a.onSrcRelocated = connect(newSrc, &KawaiiGpuBuf::relocated, this, [this, newSrc] { aliasSrcRelocated(newSrc); });
        a.onSrcWasAquired = connect(newSrc, &KawaiiGpuBuf::wasAcquired, this, [this, newSrc] (KawaiiGpuBuf *src2) { aliasSrcWasAquired(newSrc, src2); });
        a.onSrcDestroyed = connect(newSrc, &QObject::destroyed, this, &KawaiiGpuBuf::aliasSrcDestroyed, Qt::UniqueConnection);
        if(!a.onSrcDestroyed)
          for(const auto &i: aliases)
            if(i.src == newSrc && Q_LIKELY(i.onSrcDestroyed))
              {
                a.onSrcDestroyed = i.onSrcDestroyed;
                break;
              }
      }
}

void KawaiiGpuBuf::aliasSrcDestroyed(QObject *obj)
{
  for(auto i = aliases.begin(); i != aliases.end(); )
    {
      if(Q_UNLIKELY(i->src == obj))
        i = removeAlias(i);
      else
        ++i;
    }
}

void KawaiiGpuBuf::naiveCopyFrom(KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size)
{
  if(Q_LIKELY(src && srcOffset + size <= src->getDataSize() && dstOffset + size <= getDataSize()))
    {
      std::memcpy(static_cast<std::byte*>(getData()) + dstOffset,
                  static_cast<std::byte*>(src->getData()) + srcOffset,
                  size);
      emit dataChanged(dstOffset, size);
    }
}

void KawaiiGpuBuf::unbindTexture(QHash<QString, KawaiiTexture*>::iterator el)
{
  detachTexture(el.value());
  emit textureBinded(el.key(), nullptr);
  bindedTextures.erase(el);
}

void KawaiiGpuBuf::detachTexture(KawaiiTexture *texture)
{
  disconnect(texture, &KawaiiTexture::updated, this, &KawaiiGpuBuf::updated);
  disconnect(texture, &QObject::destroyed, this, qOverload<QObject*>(&KawaiiGpuBuf::unbindTexture));
}
