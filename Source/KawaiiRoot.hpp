#ifndef KAWAIIROOT_HPP
#define KAWAIIROOT_HPP

#include "Shaders/KawaiiPackage.hpp"
#include "KawaiiBufferTarget.hpp"
#include <QSet>

class KawaiiMeshInstance;
class KawaiiMaterial;

class KAWAII3D_SHARED_EXPORT KawaiiRoot : public KawaiiPackage
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiRoot);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
  friend class KawaiiMeshInstance; // to access void {un}registerMesh(KawaiiMeshInstance *inst)
  friend class Kawaii3D_Initializer;

public:
  explicit KawaiiRoot();
  virtual ~KawaiiRoot();

  using KawaiiDataUnit::createChild;

  template<typename T = KawaiiRoot, class = typename std::enable_if<std::is_base_of<QObject, T>::value > >
  static T *getRoot(QObject *obj)
  {
    T *root = nullptr;
    while (obj && !(root=dynamic_cast<T*>(obj)) )
      obj = obj->parent();

    return root;
  }

  inline auto& getMeshes() const
  { return meshes; }

  bool createRendererImplementation(const QStringList &request = {});

signals:
  void meshAboutToChangeMaterial(KawaiiMeshInstance *mesh, KawaiiMaterial *material);
  void meshAdded(KawaiiMeshInstance *mesh);
  void meshRemoved(KawaiiMeshInstance *mesh);
  void bindedBufferUpdated(uint32_t binding, KawaiiBufferTarget target);



  //IMPLEMENT  
private:
  QSet<KawaiiMeshInstance*> meshes;

  static KawaiiShadersHash vertexShadersGlob;
  static KawaiiShadersHash tessCtrlShadersGlob;
  static KawaiiShadersHash tessEvalShadersGlob;
  static KawaiiShadersHash geometryShadersGlob;
  static KawaiiShadersHash fragmentShadersGlob;
  static KawaiiShadersHash computeShadersGlob;

  void registerMesh(KawaiiMeshInstance *inst);
  void unregisterMesh(KawaiiMeshInstance *inst);
};

#endif // KAWAIIROOT_HPP
