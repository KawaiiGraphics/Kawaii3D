#ifndef KAWAIIORTHO_HPP
#define KAWAIIORTHO_HPP

#include "KawaiiProjection.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiOrtho : public KawaiiProjection
{
public:
  KAWAII_UNIT_DEF(KawaiiOrtho);

  KawaiiOrtho(float minX = -1, float maxX = 1,
              float minY = -1, float maxY = 1,
              float nearClip = 0.1, float farClip = 100.0f);

  glm::mat4 calculate(const glm::uvec2 &surfaceSize) const override final;

  // KawaiiProjection interface
  KawaiiProjection *clone() const override;

  void writeTextMemento(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeBinaryMemento(sib_utils::memento::Memento::DataMutator &mem) const override;

  float getMinX() const;
  void setMinX(float newMinX);

  float getMaxX() const;
  void setMaxX(float newMaxX);

  float getMinY() const;
  void setMinY(float newMinY);

  float getMaxY() const;
  void setMaxY(float newMaxY);

  void set(float newMinX, float newMaxX, float newMinY, float newMaxY);

private:
  void readMemento(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
private:
  float minX;
  float maxX;
  float minY;
  float maxY;
};

#endif // KAWAIIORTHO_HPP
