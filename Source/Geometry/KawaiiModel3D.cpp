#include "KawaiiModel3D.hpp"

#include "AssetZygote/KawaiiParam.hpp"
#include "KawaiiMesh3D.hpp"

#include <QtConcurrent>
#include <QChildEvent>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <algorithm>
#include <limits>

class KawaiiModel3D::ChildQObjectObserver: public QObject
{
  KawaiiModel3D *model;

  // QObject interface
public:
  ChildQObjectObserver(KawaiiModel3D *model):
    model(model)
  {
    setParent(model);
  }

  bool eventFilter(QObject*, QEvent *event) override final;
};



class KawaiiModel3D::NestedModelObserver: public QObject
{
  KawaiiModel3D *model;

  // QObject interface
public:
  NestedModelObserver(KawaiiModel3D *model):
    model(model)
  {
    setParent(model);
  }

  bool eventFilter(QObject*, QEvent *event) override final
  {
    switch(event->type())
      {
      case QEvent::ChildAdded:
        if(auto nestedModel = dynamic_cast<KawaiiModel3D*>(static_cast<QChildEvent*>(event)->child()); nestedModel)
          model->onChildAdded(nestedModel);

        if(!dynamic_cast<ChildQObjectObserver*>(static_cast<QChildEvent*>(event)->child())
           && !dynamic_cast<NestedModelObserver*>(static_cast<QChildEvent*>(event)->child())) {

            static_cast<QChildEvent*>(event)->child()->installEventFilter(this);
          }
        break;

      case QEvent::ChildRemoved:
        if(auto nestedModel = dynamic_cast<KawaiiModel3D*>(static_cast<QChildEvent*>(event)->child()); nestedModel)
          model->onChildRemoved(nestedModel);

        static_cast<QChildEvent*>(event)->child()->removeEventFilter(this);
        break;

      default: break;
      }
    return false;
  }
};


bool KawaiiModel3D::ChildQObjectObserver::eventFilter(QObject*, QEvent *event)
{
  switch(event->type())
    {
    case QEvent::ChildAdded:
      model->onChildAdded(static_cast<QChildEvent*>(event)->child());

      if(!dynamic_cast<ChildQObjectObserver*>(static_cast<QChildEvent*>(event)->child())
         && !dynamic_cast<NestedModelObserver*>(static_cast<QChildEvent*>(event)->child())
         && !dynamic_cast<KawaiiModel3D*>(static_cast<QChildEvent*>(event)->child())) {

          static_cast<QChildEvent*>(event)->child()->installEventFilter(this);
        }
      break;

    case QEvent::ChildRemoved:
      model->onChildRemoved(static_cast<QChildEvent*>(event)->child());
      static_cast<QChildEvent*>(event)->child()->removeEventFilter(this);
      break;

    default: break;
    }
  return false;
}


KawaiiModel3D::KawaiiModel3D():
  meshes_lock(std::make_unique<QRecursiveMutex>()),
  nestedModels_lock(std::make_unique<QRecursiveMutex>())
{
  installEventFilter(new ChildQObjectObserver(this));
  installEventFilter(new NestedModelObserver(this));

  connect(this, &KawaiiModel3D::topologyChanged, this, &KawaiiModel3D::geometryChanged);
  connect(this, &KawaiiModel3D::nestedModelAdded, this, &KawaiiModel3D::topologyChanged);
  connect(this, &KawaiiModel3D::nestedModelRemoved, this, &KawaiiModel3D::topologyChanged);
  connect(this, &KawaiiModel3D::meshAdded, this, &KawaiiModel3D::topologyChanged);
  connect(this, &KawaiiModel3D::meshRemoved, this, &KawaiiModel3D::topologyChanged);
}

KawaiiModel3D::~KawaiiModel3D()
{
  skeleton.reset();
}

KawaiiMesh3D *KawaiiModel3D::addMesh(const QString &name)
{
  auto m = createChild<KawaiiMesh3D>();
  m->setObjectName(name);
  return m;
}

size_t KawaiiModel3D::meshCount() const
{
  QMutexLocker l(meshes_lock.get());
  return meshes.size();
}

void KawaiiModel3D::forallMeshes(const std::function<void (KawaiiMesh3D&)> &func) const
{
  if(Q_UNLIKELY(!func)) return;

  QMutexLocker l(meshes_lock.get());
  for(auto *m: meshes)
    func(*m);
}

namespace {
  class MeshesListOwner
  {
    std::list<KawaiiMesh3D*> meshes;
    QFutureWatcher<void> futureWatcher;

  public:
    MeshesListOwner(const std::list<KawaiiMesh3D*> &meshes):
      meshes(meshes)
    {
      QObject::connect(&futureWatcher, &QFutureWatcher<void>::finished, [this] { delete this; });
    }

    QFuture<void> map(const std::function<void (KawaiiMesh3D &)> &func)
    {
      futureWatcher.setFuture(QtConcurrent::map(meshes, [&func](KawaiiMesh3D *mesh){ func(*mesh); }));
      return futureWatcher.future();
    }
  };
}

QFuture<void> KawaiiModel3D::forallMeshesP(const std::function<void (KawaiiMesh3D &)> &func) const
{
  if(Q_UNLIKELY(!func)) return {};

  QMutexLocker l(meshes_lock.get());
  return (new MeshesListOwner(meshes))->map(func);
}

void KawaiiModel3D::forallNestedMeshes(const std::function<void (KawaiiMesh3D &)> &func) const
{
  if(Q_UNLIKELY(!func)) return;

  const auto _meshes = recursiveFindMeshes();
    for(auto *m: _meshes)
      func(*m);
}

QFuture<void> KawaiiModel3D::forallNestedMeshesP(const std::function<void (KawaiiMesh3D &)> &func) const
{
  const auto _meshes = recursiveFindMeshes();
  return (new MeshesListOwner(_meshes))->map(func);
}

void KawaiiModel3D::computeNormals()
{
  auto _meshes = recursiveFindMeshes();
  QtConcurrent::blockingMap(_meshes, std::bind(&KawaiiMesh3D::computeNormals, std::placeholders::_1));
}

void KawaiiModel3D::computeTexCoordsSpheral()
{
  QMutexLocker l(nestedModels_lock.get());
  for(auto *nestedModel: nestedModels)
    nestedModel->forallMeshes(std::bind(&KawaiiMesh3D::computeTexCoordsSpheral, std::placeholders::_1));
  l.unlock();

  forallMeshes(std::bind(&KawaiiMesh3D::computeTexCoordsSpheral, std::placeholders::_1));
}

namespace {
  float div(float a, float b)
  {
    return a? a/b: 0.0;
  }

  inline QVector3D v3(const QVector4D &v)
  {
    return QVector3D(div(v.x(), v.w()),
                     div(v.y(), v.w()),
                     div(v.z(), v.w()));
  }
}

QVector3D KawaiiModel3D::computeCenter() const
{
  double x=0.0, y=x, z=y, s = z;

  const auto _meshes = recursiveFindMeshes();
  for(const auto *m: _meshes)
    for(size_t i = 0; i < m->trianglesCount(); ++i)
      {
        auto &triangle = *(m->getRawTriangles().cbegin() + i);

        const QVector3D
            v0 = v3(m->getVertex(triangle[0]).positionRef()),
            v1 = v3(m->getVertex(triangle[1]).positionRef()),
            v2 = v3(m->getVertex(triangle[2]).positionRef());

        QVector3D A = v1 - v0,
            B = v2 - v0;

        double ab = sqrt(A.lengthSquared() * B.lengthSquared()),
            cosPhi = QVector3D::dotProduct(A, B) / ab;

        if(!std::isnan(cosPhi) && cosPhi < 1)
          {
            QVector3D c = v0 + v1 + v2; //Centroid is not divided by 3. Agressive optimization
            double S = ab * sqrt(1.0 - cosPhi*cosPhi);

            if(!std::isnan(S) && !std::isnan(c.x()) && !std::isnan(c.y()) && !std::isnan(c.z()))
              {
                s += S;
                S /= 3.0; //Centroid is not divided by 3. Agressive optimization
                x += c.x() * S;
                y += c.y() * S;
                z += c.z() * S;
              }
          }
      }
  return QVector3D(x/s, y/s, z/s);
}

void KawaiiModel3D::setCenter(const QVector3D &center)
{
  translate(center - computeCenter());
}

float KawaiiModel3D::computeMajorSide() const
{
  auto sz = computeSize();
  using namespace std;
  return max(sz.x(), max(sz.y(), sz.z()));
}

void KawaiiModel3D::setMajorSide(float a)
{
  float delta = a / computeMajorSide();
  if(std::isnan(delta))
    return;

  QList<QFuture<void>> tasks;
  scale(delta);
}

std::pair<QVector3D, QVector3D> KawaiiModel3D::computeBox() const
{
  QVector3D min(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity()),
      max(-min);

  const auto _meshes = recursiveFindMeshes();
  for(const auto *m: _meshes)
    m->forallVertices([&min, &max] (const KawaiiPoint3D &vert) {
        auto v = v3(vert.positionRef());

        if(v.x() > max.x())
          max.setX(v.x());
        if(v.y() > max.y())
          max.setY(v.y());
        if(v.z() > max.z())
          max.setZ(v.z());


        if(v.x() < min.x())
          min.setX(v.x());
        if(v.y() < min.y())
          min.setY(v.y());
        if(v.z() < min.z())
          min.setZ(v.z());
      });

  return {min, max};
}

QVector3D KawaiiModel3D::computeSize() const
{
  QVector3D min, max;
  std::tie(min, max) = computeBox();
  return max - min;
}

void KawaiiModel3D::setSize(const QVector3D &size)
{
  QVector3D sz = computeSize();
  if(!sz.x() || !sz.y() || !sz.z())
    return;

  QVector3D k(size.x() / sz.x(),
              size.y() / sz.y(),
              size.z() / sz.z());

  scale(k);
}

namespace {
  QMatrix4x4 mat4(const QMatrix3x3 &m)
  {
    QMatrix4x4 result;
    for(int i = 0; i < 3; ++i)
      for(int j = 0; j < 3; ++j)
        result(i,j) = m(i, j);

    for(int i = 0; i < 3; ++i)
      result(3, i) = result(i, 3) = 0;
    result(3, 3) = 1;

    return result;
  }
}

void KawaiiModel3D::transform(const QMatrix4x4 &mat)
{
  auto normMatrix = mat4(mat.normalMatrix());

  QList<QFuture<void>> tasks;
  const auto _meshes = recursiveFindMeshes();
  for(auto *m: _meshes)
    {
      tasks
          << m->forallVerticesP([&mat, &normMatrix] (KawaiiPoint3D &v) {
             v.positionRef() = mat * v.positionRef();
             v.normalRef() = normMatrix.map(v.normalRef()); });
    }

  const glm::mat4 glmTr = KawaiiParam(mat).getValue<glm::mat4>().value();

  if(skeleton)
    skeleton->transform(glmTr);

  QMutexLocker l(nestedModels_lock.get());
  for(const auto &model: nestedModels)
    if(model->skeleton)
      model->skeleton->transform(glmTr);
  l.unlock();

  for(auto &i: tasks)
    i.waitForFinished();
}

void KawaiiModel3D::transform(const QVector3D &translate, const QQuaternion &rotation, const QVector3D &scale)
{
  QMatrix4x4 mat;
  mat.rotate(rotation);
  mat.translate(translate);
  mat.scale(scale);
  transform(mat);
}

void KawaiiModel3D::translate(const QVector3D &vec)
{
  QMatrix4x4 mat;
  mat.translate(vec);
  transform(mat);
}

void KawaiiModel3D::scale(const QVector3D &factor)
{
  QMatrix4x4 mat;
  mat.scale(factor);
  transform(mat);
}

void KawaiiModel3D::scale(float factor)
{
  scale(QVector3D(factor, factor, factor));
}

void KawaiiModel3D::rotate(const QQuaternion &q)
{
  QMatrix4x4 mat;
  mat.rotate(q);
  transform(mat);
}

KawaiiSkeleton3D *KawaiiModel3D::getSkeleton() const
{
  return skeleton.get();
}

KawaiiSkeleton3D &KawaiiModel3D::createSkeleton(const std::string &rootNodeName, const glm::mat4 &transform)
{
  return *createChild<KawaiiSkeleton3D>(transform, rootNodeName);
}

void KawaiiModel3D::enableSkeleton()
{
  skeleton->updateBones();
  forallMeshes([](KawaiiMesh3D &mesh) { mesh.enableSkeleton(); });
  skeleton->clearRecentlyUpdatedFlag();
}

void KawaiiModel3D::disableSkeleton()
{
  forallMeshes([] (KawaiiMesh3D &mesh) { mesh.disableSkeleton(); });
}

void KawaiiModel3D::joinIdenticalVertices()
{
  std::vector<KawaiiMesh3D*> _meshes;
  {
    QMutexLocker l(meshes_lock.get());
    if(Q_UNLIKELY(meshes.size() >= std::numeric_limits<int64_t>::max()))
      throw std::invalid_argument("KawaiiModel3D::joinIdenticalVertices: too many meshes!");
    _meshes.reserve(meshes.size());
    _meshes.insert(_meshes.end(), meshes.cbegin(), meshes.cend());
  }
  std::vector<std::vector<std::pair<uint32_t, uint32_t>>> identicalVertices(_meshes.size());

# pragma omp parallel for
  for(int64_t i = 0; i < static_cast<int64_t>(_meshes.size()); ++i)
    identicalVertices[i] = _meshes[i]->findIdenticalVertices();

  for(size_t i = 0; i < _meshes.size(); ++i)
    _meshes[i]->joinVertices(std::move(identicalVertices[i]));
}

void KawaiiModel3D::onChildAdded(KawaiiMesh3D *mesh)
{
  QMutexLocker l(meshes_lock.get());
  meshes.push_back(mesh);
  l.unlock();
  connect(mesh, &KawaiiMesh3D::indicesUpdated,   this, &KawaiiModel3D::topologyChanged);
  connect(mesh, &KawaiiMesh3D::triangleAdded,    this, &KawaiiModel3D::topologyChanged);
  connect(mesh, &KawaiiMesh3D::trianglesRemoved, this, &KawaiiModel3D::topologyChanged);
  connect(mesh, &KawaiiMesh3D::vertexAdded,      this, &KawaiiModel3D::topologyChanged);
  connect(mesh, &KawaiiMesh3D::verticesRemoved,  this, &KawaiiModel3D::topologyChanged);
  connect(mesh, &KawaiiMesh3D::verticesUpdated,  this, &KawaiiModel3D::geometryChanged);
  mesh->skeleton = skeleton.get();
  emit meshAdded(mesh);
}

void KawaiiModel3D::onChildRemoved(KawaiiMesh3D *mesh)
{
  QMutexLocker l(meshes_lock.get());
  if(auto el = std::find(meshes.begin(), meshes.end(), mesh); el != meshes.end())
    {
      meshes.erase(el);
      l.unlock();
      disconnect(mesh, &KawaiiMesh3D::indicesUpdated,   this, &KawaiiModel3D::topologyChanged);
      disconnect(mesh, &KawaiiMesh3D::triangleAdded,    this, &KawaiiModel3D::topologyChanged);
      disconnect(mesh, &KawaiiMesh3D::trianglesRemoved, this, &KawaiiModel3D::topologyChanged);
      disconnect(mesh, &KawaiiMesh3D::vertexAdded,      this, &KawaiiModel3D::topologyChanged);
      disconnect(mesh, &KawaiiMesh3D::verticesRemoved,  this, &KawaiiModel3D::topologyChanged);
      disconnect(mesh, &KawaiiMesh3D::verticesUpdated,  this, &KawaiiModel3D::geometryChanged);
      if(mesh->skeleton == skeleton.get())
        mesh->skeleton = nullptr;
      emit meshRemoved(mesh);
    }
}

void KawaiiModel3D::onChildAdded(KawaiiModel3D *nestedModel)
{
  QMutexLocker l(nestedModels_lock.get());
  nestedModels.push_back(nestedModel);
  l.unlock();
  connect(nestedModel, &KawaiiModel3D::geometryChanged, this, &KawaiiModel3D::geometryChanged);
  connect(nestedModel, &KawaiiModel3D::topologyChanged, this, &KawaiiModel3D::topologyChanged);
  emit nestedModelAdded(nestedModel);
}

void KawaiiModel3D::onChildRemoved(KawaiiModel3D *nestedModel)
{
  QMutexLocker l(nestedModels_lock.get());
  if(auto el = std::find(nestedModels.begin(), nestedModels.end(), nestedModel); el != nestedModels.end())
    {
      nestedModels.erase(el);
      l.unlock();
      disconnect(nestedModel, &KawaiiModel3D::geometryChanged, this, &KawaiiModel3D::geometryChanged);
      disconnect(nestedModel, &KawaiiModel3D::topologyChanged, this, &KawaiiModel3D::topologyChanged);
      emit nestedModelRemoved(nestedModel);
    }
}

void KawaiiModel3D::onChildAdded(QObject *child)
{
  if(auto mesh = dynamic_cast<KawaiiMesh3D*>(child); mesh)
    {
      onChildAdded(mesh);
      return;
    }
  else if(auto sk = dynamic_cast<KawaiiSkeleton3D*>(child); sk)
    {
      if(skeleton.get() != sk)
        {
          skeleton.reset(sk);
          QMutexLocker l(meshes_lock.get());
          for(auto *i: meshes)
            i->skeleton = sk;
          l.unlock();
          connect(sk, &KawaiiSkeleton3D::animationTick, this, &KawaiiModel3D::enableSkeleton);
        }
      return;
    }
}

void KawaiiModel3D::onChildRemoved(QObject *child)
{
  if(auto mesh = dynamic_cast<KawaiiMesh3D*>(child); mesh)
    {
      onChildRemoved(mesh);
      return;
    }
  else if(auto sk = dynamic_cast<KawaiiSkeleton3D*>(child); sk)
    {
      if(skeleton.get() == sk)
        {
          skeleton.release();
          QMutexLocker l(meshes_lock.get());
          for(auto *i: meshes)
            if(i->skeleton == sk)
              i->skeleton = nullptr;
          l.unlock();
          disconnect(sk, &KawaiiSkeleton3D::animationTick, this, &KawaiiModel3D::enableSkeleton);
          disableSkeleton();
        }
      return;
    }
}

std::list<KawaiiMesh3D*> KawaiiModel3D::recursiveFindMeshes() const
{
  QMutexLocker l(meshes_lock.get());
  std::list<KawaiiMesh3D*> result(meshes);
  l.unlock();

  QMutexLocker modelsL(nestedModels_lock.get());
  for(const auto& i: nestedModels)
    {
      QMutexLocker l(i->meshes_lock.get());
      result.insert(result.end(), i->meshes.cbegin(), i->meshes.cend());
    }
  return result;
}
