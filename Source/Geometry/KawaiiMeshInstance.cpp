#include "KawaiiMeshInstance.hpp"
#include "KawaiiMaterial.hpp"
#include "Geometry/KawaiiMesh3D.hpp"
#include "KawaiiRoot.hpp"
#include "Textures/KawaiiTexture.hpp"

KawaiiMeshInstance::KawaiiMeshInstance(KawaiiMaterial *material, KawaiiGpuBuf *uniforms):
  uniforms(uniforms),
  mesh(nullptr),
  material(material),
  root(nullptr),
  instanceCount(1)
{
  connect(this, &KawaiiDataUnit::parentUpdated, this, &KawaiiMeshInstance::updateRoot);
}

KawaiiMeshInstance::~KawaiiMeshInstance()
{
  if(root)
    root->unregisterMesh(this);
}

KawaiiMaterial *KawaiiMeshInstance::getMaterial() const
{
  return material;
}

void KawaiiMeshInstance::setMaterial(KawaiiMaterial *value)
{
  if(material)
    disconnect(material, &QObject::destroyed, this, &KawaiiMeshInstance::detachMaterial);

  if(root)
    emit root->meshAboutToChangeMaterial(this, value);
  material = value;
  if(material)
    connect(material, &QObject::destroyed, this, &KawaiiMeshInstance::detachMaterial);
}

KawaiiMesh3D *KawaiiMeshInstance::getMesh() const
{
  return mesh;
}

KawaiiGpuBuf *KawaiiMeshInstance::getUniforms() const
{
  return uniforms;
}

void KawaiiMeshInstance::setUniforms(KawaiiGpuBuf *var)
{
  if(uniforms == var) return;

  if(uniforms)
    {
      disconnect(uniforms, &QObject::destroyed, this, &KawaiiMeshInstance::detachUniforms);
      disconnect(uniforms, &KawaiiGpuBuf::updated, this, &KawaiiMeshInstance::askedRedraw);
      disconnect(uniforms, &KawaiiGpuBuf::textureBinded, this, &KawaiiMeshInstance::drawPipelineChanged);
    }
  uniforms = var;
  if(uniforms)
    {
      connect(uniforms, &QObject::destroyed, this, &KawaiiMeshInstance::detachUniforms);
      connect(uniforms, &KawaiiGpuBuf::updated, this, &KawaiiMeshInstance::askedRedraw);
      connect(uniforms, &KawaiiGpuBuf::textureBinded, this, &KawaiiMeshInstance::drawPipelineChanged);
    }
  emit uniformsChanged(uniforms);
  emit drawPipelineChanged();
}

void KawaiiMeshInstance::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setLink("material", material).setLink("uniforms", uniforms).
      setData("instance_count", QByteArray(reinterpret_cast<const char*>(&instanceCount), sizeof(instanceCount)));
}

void KawaiiMeshInstance::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setLink("material", material).setLink("uniforms", uniforms).
      setData("instance_count", QString::number(instanceCount));
}

void KawaiiMeshInstance::read(sib_utils::memento::Memento::DataReader &memento)
{
  memento.read("material", [this](TreeNode *materialCandidate) {
    setMaterial(dynamic_cast<KawaiiMaterial*>(materialCandidate));
  }).read("uniforms", [this](TreeNode *uboCandidate) {
    setUniforms(dynamic_cast<KawaiiGpuBuf*>(uboCandidate));
  });

  bool readed = false;
  memento.readText("instance_count", [this, &readed] (const QString &strNum) {
      instanceCount = strNum.toULong(&readed);
    });

  if(!readed)
    memento.read("instance_count", [this, &readed] (const QByteArray &bytes) {
        if(static_cast<size_t>(bytes.size()) >= sizeof(instanceCount))
          {
            instanceCount = *reinterpret_cast<const size_t*>(bytes.data());
            readed = true;
          }
      });

  if(!readed)
    instanceCount = 1;
}

size_t KawaiiMeshInstance::getInstanceCount() const
{
  return instanceCount;
}

void KawaiiMeshInstance::setInstanceCount(size_t value)
{
  if(instanceCount != value)
    {
      instanceCount = value;
      emit instanceCountChanged(value);
      emit askedRedraw();
    }
}

void KawaiiMeshInstance::updateRoot()
{
  auto newRoot = KawaiiRoot::getRoot(parent());
  bool rootChanged = newRoot != root;

  if(root && rootChanged)
    root->unregisterMesh(this);

  root = newRoot;
  auto _mesh = KawaiiRoot::getRoot<KawaiiMesh3D>(parent());
  if(_mesh != mesh)
    {
      if(mesh)
        {
          disconnect(mesh, &KawaiiMesh3D::triangleAdded, this, &KawaiiMeshInstance::drawPipelineChanged);
          disconnect(mesh, &KawaiiMesh3D::vertexAdded, this, &KawaiiMeshInstance::drawPipelineChanged);
          disconnect(mesh, &KawaiiMesh3D::trianglesRemoved, this, &KawaiiMeshInstance::drawPipelineChanged);
          disconnect(mesh, &KawaiiMesh3D::verticesRemoved, this, &KawaiiMeshInstance::drawPipelineChanged);
          disconnect(mesh, &KawaiiMesh3D::indicesUpdated, this, &KawaiiMeshInstance::askedRedraw);
          disconnect(mesh, &KawaiiMesh3D::verticesUpdated, this, &KawaiiMeshInstance::askedRedraw);
        }
      mesh = _mesh;
      if(mesh)
        {
          connect(mesh, &KawaiiMesh3D::triangleAdded, this, &KawaiiMeshInstance::drawPipelineChanged);
          connect(mesh, &KawaiiMesh3D::vertexAdded, this, &KawaiiMeshInstance::drawPipelineChanged);
          connect(mesh, &KawaiiMesh3D::trianglesRemoved, this, &KawaiiMeshInstance::drawPipelineChanged);
          connect(mesh, &KawaiiMesh3D::verticesRemoved, this, &KawaiiMeshInstance::drawPipelineChanged);
          connect(mesh, &KawaiiMesh3D::indicesUpdated, this, &KawaiiMeshInstance::askedRedraw);
          connect(mesh, &KawaiiMesh3D::verticesUpdated, this, &KawaiiMeshInstance::askedRedraw);
        }
      emit meshChanged(mesh);
      emit drawPipelineChanged();
    }

  if(root && rootChanged)
    root->registerMesh(this);
}

void KawaiiMeshInstance::detachUniforms()
{
  setUniforms(nullptr);
}

void KawaiiMeshInstance::detachMaterial()
{
  setMaterial(nullptr);
}
