#ifndef KAWAIITRIANGLE3D_HPP
#define KAWAIITRIANGLE3D_HPP

#include "KawaiiPoint3D.hpp"
#include <QVector3D>
#include <QMetaType>
#include <stdexcept>

class KawaiiMesh3D;
class KAWAII3D_SHARED_EXPORT KawaiiTriangle3D
{
  friend class KawaiiMesh3D;
  friend KAWAII3D_SHARED_EXPORT QDataStream& operator << (QDataStream &st, const KawaiiTriangle3D &tr);
  friend KAWAII3D_SHARED_EXPORT QDataStream& operator >> (QDataStream &st, KawaiiTriangle3D &tr);

public:
  class KAWAII3D_SHARED_EXPORT Instance {
    KawaiiTriangle3D *data;
    KawaiiMesh3D *parent;
    size_t index;
    static const int qMetaId;
  public:
    inline Instance(KawaiiTriangle3D *data, size_t index, KawaiiMesh3D *parent):
      data(data), parent(parent), index(index)
    {}

    inline Instance():
      Instance(nullptr, 0, nullptr)
    {}

    const KawaiiPoint3D &getFirstVertex() const;
    const KawaiiPoint3D &getSecondVertex() const;
    const KawaiiPoint3D &getThirdVertex() const;

    void changeFirstVertex(const std::function<bool(KawaiiPoint3D&, size_t)> &func);
    void changeSecondVertex(const std::function<bool(KawaiiPoint3D&, size_t)> &func);
    void changeThirdVertex(const std::function<bool(KawaiiPoint3D&, size_t)> &func);

    QVector3D computeNormal() const;

    void setFirstIndex(uint32_t index);
    void setSecondIndex(uint32_t index);
    void setThirdIndex(uint32_t index);

    uint32_t getFirstIndex() const;
    uint32_t getSecondIndex() const;
    uint32_t getThirdIndex() const;

    inline bool isValid() const
    { return data && parent; }
  };

  KawaiiTriangle3D(uint32_t a, uint32_t b, uint32_t c);
  KawaiiTriangle3D();

  uint32_t operator[] (size_t i) const
  {
    switch(i)
      {
      case 0: return a;
      case 1: return b;
      case 2: return c;
      default: throw std::out_of_range("KawaiiTriangle3D::operator[]");
      }
  }



  //IMPLEMENT
private:
  uint32_t a, b, c;
};

KAWAII3D_SHARED_EXPORT QDataStream& operator << (QDataStream &st, const KawaiiTriangle3D &tr);
KAWAII3D_SHARED_EXPORT QDataStream& operator >> (QDataStream &st, KawaiiTriangle3D &tr);

Q_DECLARE_METATYPE(KawaiiTriangle3D::Instance);

#endif // KAWAIITRIANGLE3D_HPP
