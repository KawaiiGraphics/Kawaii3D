#include "KawaiiTriangle3D.hpp"
#include "KawaiiMesh3D.hpp"
#include <QDataStream>

KawaiiTriangle3D::KawaiiTriangle3D(uint32_t a, uint32_t b, uint32_t c):
  a(a), b(b), c(c)
{}

KawaiiTriangle3D::KawaiiTriangle3D():
  KawaiiTriangle3D(0, 0, 0)
{}

void KawaiiTriangle3D::Instance::setFirstIndex(uint32_t index)
{
  if(data->a != index)
    {
      data->a = index;
      parent->indicesUpdated(3 * this->index, 1);
    }
}

void KawaiiTriangle3D::Instance::setSecondIndex(uint32_t index)
{
  if(data->b != index)
    {
      data->b = index;
      parent->indicesUpdated(1 + 3 * this->index, 1);
    }
}

void KawaiiTriangle3D::Instance::setThirdIndex(uint32_t index)
{
  if(data->c != index)
    {
      data->c = index;
      parent->indicesUpdated(2 + 3 * this->index, 1);
    }
}

uint32_t KawaiiTriangle3D::Instance::getFirstIndex() const
{
  return data->a;
}

uint32_t KawaiiTriangle3D::Instance::getSecondIndex() const
{
  return data->b;
}

uint32_t KawaiiTriangle3D::Instance::getThirdIndex() const
{
  return data->c;
}

const KawaiiPoint3D &KawaiiTriangle3D::Instance::getFirstVertex() const
{
  return parent->getVertex(data->a);
}

const KawaiiPoint3D &KawaiiTriangle3D::Instance::getSecondVertex() const
{
  return parent->getVertex(data->b);
}

const KawaiiPoint3D &KawaiiTriangle3D::Instance::getThirdVertex() const
{
  return parent->getVertex(data->c);
}

void KawaiiTriangle3D::Instance::changeFirstVertex(const std::function<bool (KawaiiPoint3D&, size_t)> &func)
{
  parent->changeVertex(data->a, func);
}

void KawaiiTriangle3D::Instance::changeSecondVertex(const std::function<bool (KawaiiPoint3D&, size_t)> &func)
{
  parent->changeVertex(data->b, func);
}

void KawaiiTriangle3D::Instance::changeThirdVertex(const std::function<bool (KawaiiPoint3D&, size_t)> &func)
{
  parent->changeVertex(data->c, func);
}

QVector3D KawaiiTriangle3D::Instance::computeNormal() const
{
  return QVector3D::normal(getFirstVertex().positionRef().toVector3DAffine(),
                           getSecondVertex().positionRef().toVector3DAffine(),
                           getThirdVertex().positionRef().toVector3DAffine());
}

KAWAII3D_SHARED_EXPORT QDataStream &operator <<(QDataStream &st, const KawaiiTriangle3D &tr)
{
  return st << tr.a << tr.b << tr.c;
}

KAWAII3D_SHARED_EXPORT QDataStream &operator >>(QDataStream &st, KawaiiTriangle3D &tr)
{
  return st >> tr.a >> tr.b >> tr.c;
}
