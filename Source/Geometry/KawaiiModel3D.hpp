#ifndef KAWAIIMODEL3D_HPP
#define KAWAIIMODEL3D_HPP

#include "KawaiiSkeleton3D.hpp"
#include <QFuture>
#include <list>

class KawaiiRoot;
class KawaiiMesh3D;
class KAWAII3D_SHARED_EXPORT KawaiiModel3D : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiModel3D);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  explicit KawaiiModel3D();
  virtual ~KawaiiModel3D();

  KawaiiMesh3D* addMesh(const QString &name);

  size_t meshCount() const;
  void forallMeshes(const std::function<void (KawaiiMesh3D&)> &func) const;
  QFuture<void> forallMeshesP(const std::function<void (KawaiiMesh3D&)> &func) const;

  void forallNestedMeshes(const std::function<void (KawaiiMesh3D&)> &func) const;
  QFuture<void> forallNestedMeshesP(const std::function<void (KawaiiMesh3D&)> &func) const;

  void computeNormals();
  void computeTexCoordsSpheral();

  QVector3D computeCenter() const;
  void setCenter(const QVector3D &center);

  float computeMajorSide() const;
  void setMajorSide(float a);

  std::pair<QVector3D, QVector3D> computeBox() const; //(min; max);
  QVector3D computeSize() const;
  void setSize(const QVector3D &size);

  void transform(const QMatrix4x4 &mat);
  void transform(const QVector3D &translate, const QQuaternion &rotation, const QVector3D &scale);

  void translate(const QVector3D &vec);
  void scale(const QVector3D &factor);
  void scale(float factor);
  void rotate(const QQuaternion &q);

  KawaiiSkeleton3D *getSkeleton() const;
  KawaiiSkeleton3D& createSkeleton(const std::string &rootNodeName, const glm::mat4 &transform);

  void enableSkeleton();
  void disableSkeleton();

  void joinIdenticalVertices();


signals:
  void meshAdded(KawaiiMesh3D *mesh);
  void meshRemoved(KawaiiMesh3D *mesh);

  void nestedModelAdded(KawaiiModel3D *nestedModel);
  void nestedModelRemoved(KawaiiModel3D *nestedModel);

  void geometryChanged();
  void topologyChanged();



  //IMPLEMENT
private:
  class ChildQObjectObserver;
  class NestedModelObserver;

  std::unique_ptr<KawaiiSkeleton3D> skeleton;

  std::list<KawaiiMesh3D*> meshes;
  std::unique_ptr<QRecursiveMutex> meshes_lock;

  std::list<KawaiiModel3D*> nestedModels;
  std::unique_ptr<QRecursiveMutex> nestedModels_lock;

  void onChildAdded(KawaiiMesh3D *mesh);
  void onChildRemoved(KawaiiMesh3D *mesh);

  void onChildAdded(KawaiiModel3D *nestedModel);
  void onChildRemoved(KawaiiModel3D *nestedModel);

  void onChildAdded(QObject *child);
  void onChildRemoved(QObject *child);

  std::list<KawaiiMesh3D*> recursiveFindMeshes() const;
};

#endif // KAWAIIMODEL3D_HPP
