#include "KawaiiBone3D.hpp"
#include <cstring>

KawaiiBone3D::KawaiiBone3D(KawaiiSkeleton3D::Node *parent):
  parent(parent),
  recentlyUpdated(false)
{
  dirty.test_and_set();
  update();
}

KawaiiBone3D::KawaiiBone3D(KawaiiBone3D &&origin):
  QObject(),
  own_tr(std::move(origin.own_tr)),
  tr(std::move(origin.tr)),
  influence(std::move(origin.influence)),
  parent(origin.parent),
  recentlyUpdated(false)
{
  dirty.test_and_set();
}

KawaiiBone3D::KawaiiBone3D(const KawaiiBone3D &origin):
  QObject(),
  own_tr(origin.own_tr),
  tr(origin.tr),
  influence(origin.influence),
  parent(origin.parent),
  recentlyUpdated(false)
{
  dirty.test_and_set();
}

KawaiiBone3D &KawaiiBone3D::operator=(KawaiiBone3D &&origin)
{
  own_tr = std::move(origin.own_tr);
  tr = std::move(origin.tr);
  influence = std::move(origin.influence);
  parent = origin.parent;
  dirty.test_and_set();

  return *this;
}

KawaiiBone3D &KawaiiBone3D::operator=(const KawaiiBone3D &origin)
{
  own_tr = origin.own_tr;
  tr = origin.tr;
  influence = origin.influence;
  parent = origin.parent;
  dirty.test_and_set();

  return *this;
}

KawaiiBone3D::~KawaiiBone3D()
{
}

void KawaiiBone3D::markDirty()
{
  dirty.test_and_set(std::memory_order_acquire);
}

void KawaiiBone3D::setTransform(const glm::mat4 &transform)
{
  own_tr = transform;
  markDirty();
}

void KawaiiBone3D::setInfluence(std::vector<KawaiiBone3D::Influence> &&influence)
{
  this->influence = std::move(influence);
}

bool KawaiiBone3D::update()
{
  const bool needUpdate = dirty.test_and_set(std::memory_order_acquire);
  dirty.clear(std::memory_order_release);
  if(!needUpdate)
    return false;

  if(parent)
    tr = parent->getTransform() * own_tr;
  else
    tr = own_tr;

  recentlyUpdated = true;
  return true;
}

void KawaiiBone3D::forallInfluence(const std::function<void (const Influence&)> &func) const
{
  if(Q_LIKELY(func))
    for(auto &i: influence)
      func(i);
}

void KawaiiBone3D::forallInfluence(const std::function<void (Influence &)> &func)
{
  if(Q_LIKELY(func))
    for(auto &i: influence)
      func(i);
}

void KawaiiBone3D::removeVertices(const std::vector<uint32_t> &removedVertexIndices)
{
  auto newInfluenceEnd = std::remove_if(influence.begin(), influence.end(), [&removedVertexIndices](const Influence &influence) {
      return std::binary_search(removedVertexIndices.rbegin(), removedVertexIndices.rend(), influence.affectedVertex);
    });
  influence.erase(newInfluenceEnd, influence.end());

  for(auto i = influence.begin(); i != influence.end(); ++i)
    {
      size_t rank = std::lower_bound(removedVertexIndices.rbegin(), removedVertexIndices.rend(), i->affectedVertex)
          - removedVertexIndices.rbegin();
      i->affectedVertex -= rank;
    }
}

const glm::mat4 &KawaiiBone3D::getTr() const
{
  return tr;
}

const glm::mat4 &KawaiiBone3D::getOwnTr() const
{
  return own_tr;
}

bool KawaiiBone3D::wasRecentlyUpdated() const
{
  return recentlyUpdated;
}

void KawaiiBone3D::clearRecentlyUpdated()
{
  recentlyUpdated = false;
}

void KawaiiBone3D::writeToMemento(sib_utils::memento::Memento::DataMutator &mem) const
{
  mem.setData("tr_matrix", QByteArray(reinterpret_cast<const char*>(&own_tr), sizeof(glm::mat4)));
  mem.setData("influence", QByteArray(reinterpret_cast<const char*>(influence.data()), sizeof(Influence) * influence.size()));
  mem.setData("parent", QString::fromStdString(parent->getName()));
  mem.setData("objectName", objectName());
}

KawaiiBone3D *KawaiiBone3D::createFromMemento(KawaiiSkeleton3D *parent, sib_utils::memento::Memento::DataReader &mem)
{
  QString nodeName;
  bool ok;
  mem.get("parent", nodeName, ok);
  if(!ok)
    return nullptr;

  auto *result = new KawaiiBone3D(parent->findNode(nodeName.toStdString()));

  mem.read("influence", [result] (const QByteArray &data) {
      result->influence.resize(data.size() / sizeof(Influence));
      std::memcpy(result->influence.data(), data.data(), data.size());
    });

  mem.read("tr_matrix", [result](const QByteArray &data) {
      if(data.size() == sizeof(glm::mat4))
        result->setTransform(*reinterpret_cast<const glm::mat4*>(data.data()));
    });

  mem.readText("objectName", [result](const QString &objName){ result->setObjectName(objName); });

  return result;
}
