#ifndef KAWAIIBONE3D_HPP
#define KAWAIIBONE3D_HPP

#include "KawaiiSkeleton3D.hpp"
#include <unordered_set>
#include <atomic>

class KawaiiMesh3D;

//todo: memento stuff
class KAWAII3D_SHARED_EXPORT KawaiiBone3D: public QObject
{
  friend class KawaiiSkeleton3D;

public:
  struct Influence
  {
    uint32_t affectedVertex;
    float value;
  };

  explicit KawaiiBone3D(KawaiiSkeleton3D::Node *parent = nullptr);

  KawaiiBone3D(KawaiiBone3D &&origin);
  KawaiiBone3D(const KawaiiBone3D &origin);

  KawaiiBone3D& operator=(KawaiiBone3D &&origin);
  KawaiiBone3D& operator=(const KawaiiBone3D &origin);

  ~KawaiiBone3D();

  void markDirty();
  void setTransform(const glm::mat4 &transform);
  void setInfluence(std::vector<Influence> &&influence);

  bool update();

  void forallInfluence(const std::function<void (const Influence&)> &func) const;
  void forallInfluence(const std::function<void (Influence&)> &func);
  void removeVertices(const std::vector<uint32_t> &removedVertexIndices);

  const glm::mat4 &getTr() const;
  const glm::mat4 &getOwnTr() const;

  bool wasRecentlyUpdated() const;
  void clearRecentlyUpdated();

  void writeToMemento(sib_utils::memento::Memento::DataMutator &mem) const;
  static KawaiiBone3D *createFromMemento(KawaiiSkeleton3D *parent, sib_utils::memento::Memento::DataReader &mem);



  //IMPLEMENT
private:
  glm::mat4 own_tr;
  glm::mat4 tr;
  std::vector<Influence> influence;
  KawaiiSkeleton3D::Node *parent;
  std::atomic_flag dirty;
  bool recentlyUpdated;
};

#endif // KAWAIIBONE3D_HPP
