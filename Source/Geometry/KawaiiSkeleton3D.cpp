#include "KawaiiSkeletalAnimation.hpp"
#include "KawaiiSkeleton3D.hpp"
#include "KawaiiBone3D.hpp"
#include <list>

KawaiiSkeleton3D::KawaiiSkeleton3D(const glm::mat4 &transform, const std::string &name):
  root(transform, name),
  animator(nullptr),
  extrernalTransformationL(std::make_unique<QMutex>())
{
  root.skeleton = this;
  allNodes.insert({root.name, &root});
}

KawaiiSkeleton3D::~KawaiiSkeleton3D()
{
  if(animator)
    animator.reset();

  for(const auto *i: bones)
    delete i;
}

KawaiiSkeleton3D::Node &KawaiiSkeleton3D::getRoot()
{
  return root;
}

const KawaiiSkeleton3D::Node &KawaiiSkeleton3D::getRoot() const
{
  return root;
}

KawaiiSkeleton3D::Node *KawaiiSkeleton3D::findNode(const std::string &name)
{
  if(auto el = allNodes.find(name); el != allNodes.end())
    return el->second;
  else
    return nullptr;
}

const KawaiiSkeleton3D::Node *KawaiiSkeleton3D::findNode(const std::string &name) const
{
  if(auto el = allNodes.find(name); el != allNodes.end())
    return el->second;
  else
    return nullptr;
}

KawaiiSkeleton3D::Node *KawaiiSkeleton3D::findNode(const std::string &name, KawaiiSkeleton3D::Node *parent)
{
  auto el = allNodes.find(name);
  while(el != allNodes.end() && el->first != name)
    {
      if(el->second->parent == parent)
        return el->second;

      ++el;
    }
  return nullptr;
}

KawaiiSkeleton3D::Node *KawaiiSkeleton3D::findNode(const std::string &name, const std::string &parentName)
{
  auto el = allNodes.find(name);
  while(el != allNodes.end() && el->first != name)
    {
      if(el->second->parent->name == parentName)
        return el->second;

      ++el;
    }
  return nullptr;
}

void KawaiiSkeleton3D::forallNodes(const std::function<void (const KawaiiSkeleton3D::Node &)> &func) const
{
  for(const auto &i: allNodes)
    func(*i.second);
}

KawaiiBone3D *KawaiiSkeleton3D::getBone(size_t boneIndex) const
{
  return bones[boneIndex];
}

size_t KawaiiSkeleton3D::getBoneCount() const
{
  return bones.size();
}

void KawaiiSkeleton3D::updateBones()
{
  for(auto i = bones.begin(); i != bones.end();)
    {
      if(*i)
        {
          (*i)->update();
          ++i;
        } else
        i = bones.erase(i);
    }
}

void KawaiiSkeleton3D::clearRecentlyUpdatedFlag()
{
  for(auto i = bones.begin(); i != bones.end();)
    {
      if(*i)
        {
          (*i)->clearRecentlyUpdated();
          ++i;
        } else
        i = bones.erase(i);
    }
}

namespace {
  class SkeletalAnimator: public QObject {
    KawaiiSkeletalAnimation::Instance animation;

  public:
    SkeletalAnimator(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation):
      QObject(&skeleton),
      animation(skeleton, animation)
    {
    }

    SkeletalAnimator(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation, const std::unordered_map<std::string, std::string> &nodeMapping):
      QObject(&skeleton),
      animation(skeleton, animation, nodeMapping)
    {
    }

    // QObject interface
  protected:
    void timerEvent(QTimerEvent*) override final
    {
      animation.tick();
    }
  };
}

void KawaiiSkeleton3D::setAnimation(const KawaiiSkeletalAnimation &animation, int timerIntervalMs)
{
  animator = std::make_unique<SkeletalAnimator>(*this, animation);
  animator->startTimer(timerIntervalMs);
}

void KawaiiSkeleton3D::setAnimation(const KawaiiSkeletalAnimation &animation, const std::unordered_map<std::string, std::string> &nodeMapping, int timerIntervalMs)
{
  animator = std::make_unique<SkeletalAnimator>(*this, animation, nodeMapping);
  animator->startTimer(timerIntervalMs);
}

void KawaiiSkeleton3D::stopAnimation()
{
  animator.reset();
}

void KawaiiSkeleton3D::transform(const glm::mat4 &mat)
{
  QMutexLocker l(extrernalTransformationL.get());
  root.setTransform(mat * root.getOwnTransform());
}

void KawaiiSkeleton3D::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  for(const auto &i: allNodes)
    {
      sib_utils::memento::Memento *childMem;
      mem.createChildMemento(QStringLiteral("Node"), childMem);
      auto nodeMem = childMem->mutator(mem.getCreatedMemento(), mem.preserveExternalLinks);
      nodeMem.setData(QStringLiteral("name"), QString::fromStdString(i.first));
      nodeMem.setData(QStringLiteral("tr_matrix"), QByteArray(reinterpret_cast<const char*>(&i.second->own_transform), sizeof(glm::mat4)));
      if(i.second->parent)
        nodeMem.setData(QStringLiteral("parent"), QString::fromStdString(i.second->parent->name));
    }

  for(size_t i = 0; i < bones.size(); ++i)
    {
      sib_utils::memento::Memento *childMem;
      mem.createChildMemento(QStringLiteral("KawaiiBone3D"), childMem);
      auto mutator = childMem->mutator(mem.getCreatedMemento(), mem.preserveExternalLinks);
      bones[i]->writeToMemento(mutator);
      mutator.setData("index", QString::number(i));
    }
}

void KawaiiSkeleton3D::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiSkeleton3D::read(sib_utils::memento::Memento::DataReader &mem)
{
  allNodes.clear();
  root.children.clear();

  struct NodeZygote {
    std::string name;
    glm::mat4 tr_matrix;
  };
  QMutex hierarchyLock;
  std::unordered_map<std::string, std::list<NodeZygote>> hierarchy;

  mem.forallChildren([&hierarchy, &hierarchyLock, this, &mem] (sib_utils::memento::Memento &childMem) {
    auto nodeMem = childMem.reader(mem.getCreatedObjects());
    QString memType;
    nodeMem.getType(memType);
    if(memType != QStringLiteral("Node")) return;

    NodeZygote creatingNode;

    nodeMem.readText(QStringLiteral("name"), [&creatingNode] (const QString &name) {
        creatingNode.name = name.toStdString();
      });
    nodeMem.read(QStringLiteral("tr_matrix"), [&creatingNode] (const QByteArray &data) {
        if(data.size() == sizeof(glm::mat4))
          creatingNode.tr_matrix = *reinterpret_cast<const glm::mat4*>(data.data());
      });

    bool ok;
    QString parent;
    nodeMem.get(QStringLiteral("parent"), parent, ok);
    if(ok)
      {
        QMutexLocker l(&hierarchyLock);
        hierarchy[parent.toStdString()].push_back(creatingNode);
      } else
      {
        root.name = creatingNode.name;
        root.setTransform(creatingNode.tr_matrix);
        allNodes.insert( { root.name, &root } );
      }
    });

  auto creatingNode = hierarchy.begin();
  while(!hierarchy.empty())
    {
      if(creatingNode == hierarchy.end()) break;

      if(auto el = allNodes.find(creatingNode->first); el != allNodes.end())
        {
          for(const auto &i: creatingNode->second)
            el->second->createChild(i.tr_matrix, i.name);

          hierarchy.erase(creatingNode);
          creatingNode = hierarchy.begin();
        } else
        creatingNode++;
    }

  std::atomic_uint64_t childrenCount = 0;
  mem.forallChildren([&mem, &childrenCount] (sib_utils::memento::Memento &childMem) {
      auto boneMem = childMem.reader(mem.getCreatedObjects());
      QString memType;
      boneMem.getType(memType);
      if(memType == QStringLiteral("KawaiiBone3D"))
        ++childrenCount;
    });

  bones.resize(childrenCount, nullptr);
  mem.forallChildren([this, &mem] (sib_utils::memento::Memento &childMem) {
      auto boneMem = childMem.reader(mem.getCreatedObjects());
      QString memType;
      boneMem.getType(memType);
      if(memType != QStringLiteral("KawaiiBone3D"))
        return;

      QString indexStr; bool ok;
      boneMem.get("index", indexStr, ok);
      if(ok)
        bones[indexStr.toULongLong()] = KawaiiBone3D::createFromMemento(this, boneMem);
    });
}

KawaiiSkeleton3D::Node::Node(const glm::mat4 &transform, const std::string &name):
  own_transform(transform),
  abs_transform(transform),
  name(name),
  parent(nullptr),
  skeleton(nullptr),
  dirty()
{
  dirty.clear();
}

const glm::mat4 &KawaiiSkeleton3D::Node::getOwnTransform() const
{
  return own_transform;
}

KawaiiSkeleton3D::Node *KawaiiSkeleton3D::Node::getParent() const
{
  return parent;
}

const glm::mat4 &KawaiiSkeleton3D::Node::getTransform()
{
  update();
  return abs_transform;
}

void KawaiiSkeleton3D::Node::setTransform(const glm::mat4 &tr, bool markDirty)
{
  own_transform = tr;
  if(markDirty)
    this->markDirty();
}

const std::string &KawaiiSkeleton3D::Node::getName() const
{
  return name;
}

KawaiiSkeleton3D *KawaiiSkeleton3D::Node::getSkeleton() const
{
  return skeleton;
}

KawaiiSkeleton3D::Node &KawaiiSkeleton3D::Node::createChild(const glm::mat4 &own_transform, const std::string &name)
{
  children.emplace_back(own_transform, name);
  if(dirty.test_and_set())
    children.back().dirty.test_and_set();
  else
    {
      children.back().abs_transform = abs_transform * own_transform;
      dirty.clear();
    }
  children.back().skeleton = skeleton;
  children.back().parent = this;
  skeleton->allNodes.insert({name, &children.back()});
  return children.back();
}

size_t KawaiiSkeleton3D::Node::createBone()
{
  auto bone = new KawaiiBone3D(this);
  skeleton->bones.emplace_back(bone);
  return skeleton->bones.size()-1;
}

void KawaiiSkeleton3D::Node::updateOnlySelf()
{
  if(parent)
    abs_transform = parent->abs_transform * own_transform;
  else
    abs_transform = own_transform;

  dirty.clear(std::memory_order_release);
}

void KawaiiSkeleton3D::Node::update()
{
  std::list<Node*> updatingNodes;

  Node *checkingNode = this;
  while(checkingNode && checkingNode->dirty.test_and_set(std::memory_order_acquire))
    {
      updatingNodes.push_front(checkingNode);
      checkingNode = checkingNode->parent;
    }
  if(checkingNode)
    checkingNode->dirty.clear();

  for(auto *i: updatingNodes)
    i->updateOnlySelf();
}

void KawaiiSkeleton3D::Node::markDirty()
{
  std::list<Node*> stack = {this};

  while(!stack.empty())
    {
      auto *node = stack.front();
      stack.pop_front();

      node->dirty.test_and_set(std::memory_order_acquire);

      for(const auto &i: skeleton->bones)
        if(i->parent == node)
          i->markDirty();

      for(auto i = node->children.rbegin(); i != node->children.rend(); ++i)
        stack.push_front(&*i);
    }
}
