#include "KawaiiMesh3D.hpp"

#include "KawaiiTriangle3D.hpp"
#include "KawaiiBone3D.hpp"

#include <QtConcurrent>
#include <QDataStream>
#include <QVariant>
#include <QBuffer>
#include <cstring>

#include <sib_utils/dirty_cast.hpp>
#include <sib_utils/PairHash.hpp>
#include <sib_utils/Strings.hpp>

#include <glm/simd/matrix.h>

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_TARGET_OPENCL_VERSION 300
#include <CL/cl.h>

KawaiiMesh3D::KawaiiMesh3D():
  skeleton(nullptr),
  openCLMemUntransformedVertices(nullptr),
  openCLMemBoneWeights(nullptr),
  openCLMemBoneWeightHints(nullptr),
  openCLMemBoneMatrices(nullptr),
  openCLMemResult(nullptr),
  openCLKernel(nullptr),
  skeletonActive(false)
{
}

KawaiiMesh3D::~KawaiiMesh3D()
{
  releaseOpenCLResources();
}

KawaiiPoint3D &KawaiiMesh3D::addVertex()
{
  return emplaceVertex();
}

KawaiiPoint3D &KawaiiMesh3D::addVertex(const QVector4D &pos)
{
  return emplaceVertex(pos);
}

KawaiiPoint3D &KawaiiMesh3D::addVertex(const QVector4D &pos, const QVector3D &normal)
{
  return emplaceVertex(pos, normal);
}

KawaiiPoint3D &KawaiiMesh3D::addVertex(const QVector4D &pos, const QVector3D &normal, const QVector3D &texcoord)
{
  return emplaceVertex(pos, normal, texcoord);
}

const KawaiiPoint3D &KawaiiMesh3D::getVertex(size_t index) const
{
  return vertices.at(index);
}

void KawaiiMesh3D::changeVertex(size_t index, const std::function<bool (KawaiiPoint3D &, size_t)> &func)
{
  if(func(vertices.at(index), index))
    emit verticesUpdated(index, 1);
}

void KawaiiMesh3D::forallVertices(const std::function<void (KawaiiPoint3D&, size_t)> &func)
{
  for(size_t i = 0; i < vertexCount(); ++i)
    func(vertices[i], i);

  emit verticesUpdated(0, vertexCount());
}

void KawaiiMesh3D::forallVertices(const std::function<void (KawaiiPoint3D &)> &func)
{
  for(auto &i: vertices)
    func(i);
  emit verticesUpdated(0, vertexCount());
}

namespace {
  class AsyncForallVertHandler
  {
    std::vector<std::pair<KawaiiPoint3D*, size_t>> seq;
    std::function<void(const std::pair<KawaiiPoint3D*, size_t> &)> seqFunc;
    QFutureWatcher<void> task;

    QMetaObject::Connection onFinished;
    QMetaObject::Connection onMeshDestroyed;

    void apply(KawaiiMesh3D *mesh)
    {
      emit mesh->verticesUpdated(0, mesh->vertexCount());
      delete this;
    }

  protected:
    QFuture<void> getResult() const { return task.future(); }

  public:
    AsyncForallVertHandler(KawaiiMesh3D *mesh, const std::function<void (KawaiiPoint3D&, size_t)> &func)
    {
      onMeshDestroyed = QObject::connect(mesh, &QObject::destroyed, [this] { delete this; });

      seq.reserve(mesh->vertexCount());
      mesh->forallVerticesConst([this] (const KawaiiPoint3D &p, size_t i) {
        seq.emplace_back(const_cast<KawaiiPoint3D*>(&p), i);
      });

      seqFunc = [func] (const std::pair<KawaiiPoint3D*, size_t> &el) {
          return func(*el.first, el.second);
        };

      onFinished = QObject::connect(&task, &QFutureWatcherBase::finished, std::bind(&AsyncForallVertHandler::apply, this, mesh));
    }

    ~AsyncForallVertHandler()
    {
      QObject::disconnect(onMeshDestroyed);
      QObject::disconnect(onFinished);
    }

    QFuture<void> start() {
      task.setFuture(QtConcurrent::map(seq, seqFunc));
      return task.future();
    }
  };
}

QFuture<void> KawaiiMesh3D::forallVerticesP(const std::function<void (KawaiiPoint3D&, size_t)> &func)
{
  auto handler = new AsyncForallVertHandler(this, func);
  return handler->start();
}

QFuture<void> KawaiiMesh3D::forallVerticesP(const std::function<void (KawaiiPoint3D &)> &func)
{
  QFutureWatcher<void> *watcher(new QFutureWatcher<void>());
  connect(this, &QObject::destroyed, watcher, &QFutureWatcher<void>::deleteLater);
  connect(watcher, &QFutureWatcherBase::finished, this, [this, watcher] { emit verticesUpdated(0, vertexCount()); delete watcher; });
  const auto future = QtConcurrent::map(vertices, func);
  watcher->setFuture(future);
  return future;
}

void KawaiiMesh3D::forallVerticesConst(const std::function<void (const KawaiiPoint3D &, size_t)> &func) const
{
  for(size_t i = 0; i < vertexCount(); ++i)
    func(vertices[i], i);
}

void KawaiiMesh3D::forallVerticesConst(const std::function<void (const KawaiiPoint3D &)> &func) const
{
  for(const auto &vert: vertices)
    func(vert);
}

QFuture<void> KawaiiMesh3D::forallVerticesConstP(const std::function<void (const KawaiiPoint3D &)> &func) const
{
  return QtConcurrent::map(vertices, func);
}

void KawaiiMesh3D::setVertex(size_t index, const KawaiiPoint3D &value)
{
  vertices.at(index) = value;
  emit verticesUpdated(index, 1);
}

void KawaiiMesh3D::setVertexPos(size_t index, const QVector4D &value)
{
  vertices.at(index).positionRef() = value;
  emit verticesUpdated(index, 1);
}

void KawaiiMesh3D::setVertexNormal(size_t index, const QVector3D &value)
{
  vertices.at(index).normalRef() = value.normalized();
  emit verticesUpdated(index, 1);
}

void KawaiiMesh3D::setVertexTexCoord(size_t index, const QVector3D &value)
{
  vertices.at(index).texCoordRef() = value;
  emit verticesUpdated(index, 1);
}

size_t KawaiiMesh3D::vertexCount() const
{
  return vertices.size();
}

void KawaiiMesh3D::removeVertices(size_t index, size_t count)
{
  if(skeletonActive)
    throw std::runtime_error("KawaiiMesh3D::removeVertices is not allowed with active skeleton");

  vertices.erase(vertices.begin() + index, vertices.begin() + index + count);
  emit verticesRemoved(index, count);
}

KawaiiTriangle3D::Instance KawaiiMesh3D::addTriangle(uint32_t firstIndex, uint32_t secondIndex, uint32_t thirdIndex)
{
  int i0 = triangles.size();
  triangles.emplace_back(firstIndex, secondIndex, thirdIndex);
  auto triangle = KawaiiTriangle3D::Instance(&triangles.back(), i0, this);

  emit triangleAdded(triangle);
  return triangle;
}

KawaiiTriangle3D::Instance KawaiiMesh3D::getTriangle(int index)
{
  return KawaiiTriangle3D::Instance(&triangles[index], index, this);
}

size_t KawaiiMesh3D::trianglesCount() const
{
  return triangles.size();
}

void KawaiiMesh3D::removeTriangles(size_t index, size_t count)
{
  triangles.erase(triangles.begin() + index,
                  triangles.begin() + index + count);
  emit trianglesRemoved(index, count);
}

void KawaiiMesh3D::clear()
{
  const size_t vertN = vertexCount(),
      trN = trianglesCount();

  triangles.clear();
  vertices.clear();

  emit verticesRemoved(0, vertN);
  emit trianglesRemoved(0, trN);
}

void KawaiiMesh3D::computeNormals()
{
  for(auto &vert: vertices)
    vert.normalRef() = QVector3D(0,0,0);

  for(const auto &triangle: triangles)
    {
      auto &a = vertices[triangle.a],
          &b = vertices[triangle.b],
          &c = vertices[triangle.c];

      QVector3D norm = QVector3D::normal(a.positionRef().toVector3DAffine(),
                                         b.positionRef().toVector3DAffine(),
                                         c.positionRef().toVector3DAffine());

      a.normalRef() += norm;
      b.normalRef() += norm;
      c.normalRef() += norm;
    }

  for(auto &vert: vertices)
    vert.normalRef().normalize();
}

namespace {
  QVector3D reflect(const QVector3D &I, const QVector3D &N)
  {
    return I - 2.0 * QVector3D::dotProduct(N, I) * N;
  }
}

void KawaiiMesh3D::computeTexCoordsSpheral()
{
  forallVerticesP([](KawaiiPoint3D &vert) {
    QVector3D u = vert.positionRef().toVector3DAffine().normalized(),
        r = reflect(u, vert.normalRef());

    r.setZ(r.z() + 1.0);
    qreal m = 2.0 * r.length() + 0.5;
    vert.texCoordRef() = r / m;
  }).waitForFinished();
}

void KawaiiMesh3D::assignBone(size_t bone)
{
  bones.push_back(bone);
  if(skeleton)
    bonesTr.push_back(&skeleton->getBone(bone)->getTr());
}

void KawaiiMesh3D::rebindSkeleton()
{
  boneAtt.resize(vertexCount());
  for(auto &i: boneAtt)
    i.boneWeights.clear();

  bonesTr.resize(bones.size());
  for(uint32_t i = 0; i < bones.size(); ++i)
    {
      skeleton->getBone(bones[i])->forallInfluence( [this, i] (const KawaiiBone3D::Influence &j) {
          boneAtt[j.affectedVertex].boneWeights.emplace_back(i, j.value);
        });
      bonesTr[i] = &skeleton->getBone(bones[i])->getTr();
    }
}

void KawaiiMesh3D::enableSkeleton()
{
  if(bones.empty())
    {
      if(skeletonActive)
        disableSkeleton();
      return;
    }

  if(!skeletonActive)
    {
      untransformedVertices.reserve(getVertices().size());
      for(const auto &i: getVertices())
        untransformedVertices.emplace_back(i.positionRef(), i.normalRef());
      rebindSkeleton();
    }

  if(!enableSkeletonOpenCL())
    {
      skeletonActive = true;

      if(Q_UNLIKELY(vertices.size() >= std::numeric_limits<int64_t>::max()))
        throw std::invalid_argument("KawaiiMesh3D::enableSkeleton: too many vertices!");

#     pragma omp parallel for
      for(int64_t i = 0; i < static_cast<int64_t>(vertices.size()); ++i)
        {
          glm_vec4 tr[4] = {0, 0, 0, 0};

          for(const auto &j: boneAtt[i].boneWeights)
            {
              const glm_vec4 k = _mm_load1_ps(&j.second);

              const glm_vec4* boneTr = reinterpret_cast<const glm_vec4*>(bonesTr[j.first]);

              tr[0] = _mm_add_ps(tr[0], _mm_mul_ps(k, boneTr[0]));
              tr[1] = _mm_add_ps(tr[1], _mm_mul_ps(k, boneTr[1]));
              tr[2] = _mm_add_ps(tr[2], _mm_mul_ps(k, boneTr[2]));
              tr[3] = _mm_add_ps(tr[3], _mm_mul_ps(k, boneTr[3]));
            }

          auto &vert = vertices[i];
          untransformedVertices[i].transformInPlace(tr, vert.positionRef(), vert.normalRef());
        }
      emit verticesUpdated(0, vertexCount());
    } else {
      emit verticesUpdated(0, vertexCount());
    }
}

void KawaiiMesh3D::disableSkeleton()
{
  forallVerticesP([this] (KawaiiPoint3D &vert, size_t i) {
      const auto &v = untransformedVertices[i];
      vert.positionRef() = v.getPos();
      vert.normalRef() = v.getNorm();
    }).waitForFinished();

  skeletonActive = false;
  untransformedVertices.clear();
  releaseOpenCLResources();
}

std::vector<std::pair<uint32_t, uint32_t>> KawaiiMesh3D::findIdenticalVertices() const
{
  std::vector<std::pair<KawaiiPoint3D, BoneAttachment>> vertexData(vertices.size());
  if(!boneAtt.empty()) {
      for(size_t i = 0; i < vertexData.size(); ++i)
        vertexData[i] = { vertices[i], boneAtt[i] };
    } else {
      for(size_t i = 0; i < vertexData.size(); ++i)
        vertexData[i] = { vertices[i], {} };
    }

  class BoneAttachmentHash
  {
  public:
    std::size_t operator()(const BoneAttachment &att) const noexcept
    {
      return qHash(QByteArray::fromRawData(reinterpret_cast<const char*>(att.boneWeights.data()), att.boneWeights.size()));
    }
  };

  std::unordered_map<std::pair<KawaiiPoint3D, BoneAttachment>,
      std::vector<uint32_t>,
      sib_utils::CustomPairHash<std::hash<KawaiiPoint3D>, BoneAttachmentHash>> vertexToIndicesUMap;

  for(uint32_t i = 0; i != vertexData.size(); ++i)
    vertexToIndicesUMap[vertexData[i]].push_back(i);

  std::vector<std::pair<uint32_t, uint32_t>> result;
  for(auto &i: vertexToIndicesUMap)
    if(i.second.size() > 1)
      for(auto j = i.second.cbegin()+1; j != i.second.cend(); ++j)
        result.emplace_back(*i.second.cbegin(), *j);

  return result;
}

void KawaiiMesh3D::joinVertices(std::vector<std::pair<uint32_t, uint32_t>> &&joiningVertices)
{
  std::sort(joiningVertices.begin(), joiningVertices.end(),
            [](const std::pair<uint32_t, uint32_t> &a, const std::pair<uint32_t, uint32_t> &b) { return a.second > b.second; });

  auto markVerticesToRemovceTask = QtConcurrent::map(joiningVertices, [this](const std::pair<uint32_t, uint32_t> &i) {
      vertices[i.second].positionRef().setX(std::numeric_limits<float>::quiet_NaN());
      if(skeletonActive)
        {
          boneAtt[i.second].boneWeights.clear();
          untransformedVertices[i.second].pos.x = std::numeric_limits<float>::quiet_NaN();
        }
    });

  std::vector<uint32_t> removedVertexIndices;
  removedVertexIndices.reserve(joiningVertices.size());
  for(auto i = joiningVertices.begin(); i != joiningVertices.end(); ++i)
    {
      emit verticesRemoved(i->second, 1);
      removedVertexIndices.push_back(i->second);
    }

  QVector<bool> indicesChanged (triangles.size() * 3, false);
  {
    auto *indexesArr = reinterpret_cast<uint32_t*>(triangles.data());
    std::vector<size_t> indSequence(indicesChanged.size());
    std::iota(indSequence.begin(), indSequence.end(), 0);

    QtConcurrent::map(indSequence, [&joiningVertices, &removedVertexIndices, &indicesChanged, indexesArr](size_t ind) {
      auto rankIter = std::lower_bound(removedVertexIndices.rbegin(), removedVertexIndices.rend(), indexesArr[ind]);
      if(rankIter == removedVertexIndices.rend()) return;

      size_t rank = rankIter - removedVertexIndices.rbegin();
      if(*rankIter == indexesArr[ind])
        {
          indexesArr[ind] = joiningVertices[joiningVertices.size() - rank - 1].first;
          indicesChanged[ind] = true;
        }
    }).waitForFinished();

    QtConcurrent::map(indSequence, [&removedVertexIndices, &indicesChanged, indexesArr](size_t ind) {
        auto rankIter = std::lower_bound(removedVertexIndices.rbegin(), removedVertexIndices.rend(), indexesArr[ind]);
        size_t rank = rankIter - removedVertexIndices.rbegin();
        if(rank > 0)
          {
            indexesArr[ind] -= rank;
            indicesChanged[ind] = true;
          }
      }).waitForFinished();
  }

  if(skeleton)
    {
      std::vector<KawaiiBone3D*> bones(skeleton->getBoneCount());
      for(size_t boneIndex = 0; boneIndex < bones.size(); ++boneIndex)
        bones[boneIndex] = skeleton->getBone(boneIndex);

      QtConcurrent::map(bones, [&removedVertexIndices](KawaiiBone3D *bone) {
          bone->removeVertices(removedVertexIndices);
        }).waitForFinished();
    }

  markVerticesToRemovceTask.waitForFinished();

  vertices.erase(std::remove_if(vertices.begin(), vertices.end(), [](const KawaiiPoint3D &i) { return i.isNAN(); }),
                 vertices.end());

  boneAtt.erase(std::remove_if(boneAtt.begin(), boneAtt.end(), [](const BoneAttachment &i) { return i.boneWeights.empty(); }),
                boneAtt.end());

  untransformedVertices.erase(std::remove_if(untransformedVertices.begin(), untransformedVertices.end(),
                                             [](const TransformableVertex &i) { return i.isNAN(); }),
                              untransformedVertices.end());
  if(openCLMemUntransformedVertices)
    {
      clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemUntransformedVertices));
      openCLMemUntransformedVertices = nullptr;
    }

  auto firstChangedIndexEl = std::find(indicesChanged.cbegin(), indicesChanged.cend(), true);
  while(firstChangedIndexEl != indicesChanged.cend())
    {
      auto endChangedIndexEl = std::find(firstChangedIndexEl+1, indicesChanged.cend(), false);

      const size_t i = firstChangedIndexEl - indicesChanged.cbegin();
      const size_t n = endChangedIndexEl - firstChangedIndexEl;
      emit indicesUpdated(i, n);

      firstChangedIndexEl = std::find(endChangedIndexEl, indicesChanged.cend(), true);
    }
}

void KawaiiMesh3D::joinIdenticalVertices()
{
  joinVertices(findIdenticalVertices());
}

void KawaiiMesh3D::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  if(skeletonActive)
    {
      std::vector<KawaiiPoint3D> v;
      v.reserve(vertices.size());
      for(size_t i = 0; i < vertices.size(); ++i)
        v.emplace_back(untransformedVertices[i].getPos(), untransformedVertices[i].getNorm(), vertices[i].texCoordRef());
      memento.setData("vertices", QByteArray(reinterpret_cast<const char*>(v.data()), v.size() * sizeof(KawaiiPoint3D)));
    } else
    memento.setData("vertices", QByteArray(reinterpret_cast<const char*>(vertices.data()), vertices.size() * sizeof(KawaiiPoint3D)));
  memento.setData("faces", QByteArray(reinterpret_cast<const char*>(triangles.data()), triangles.size() * sizeof(KawaiiTriangle3D)));

  std::vector<uint64_t> bonesData(bones.cbegin(), bones.cend());
  memento.setData("bones", QByteArray(reinterpret_cast<const char*>(bonesData.data()), bonesData.size() * sizeof(uint64_t)));
}

void KawaiiMesh3D::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  QString tmpStr;
  for(auto i = triangles.cbegin(); i < triangles.cend(); ++i)
    {
      tmpStr += QString::number(i->a);
      tmpStr.push_back(' ');

      tmpStr += QString::number(i->b);
      tmpStr.push_back(' ');

      tmpStr += QString::number(i->c);
      tmpStr.push_back('\n');
    }
  memento.setData("faces", tmpStr);

  tmpStr.clear();

  auto printFloat = [&tmpStr] (float f, const QChar &sep=' ') {
      tmpStr += QString::number(f, 'f', 10);
      tmpStr.push_back(sep);
    };


  for(size_t j = 0; j < vertices.size(); ++j)
    {
      if(skeletonActive)
        {
          printFloat(untransformedVertices[j].getPos().x());
          printFloat(untransformedVertices[j].getPos().y());
          printFloat(untransformedVertices[j].getPos().z());
          printFloat(untransformedVertices[j].getPos().w());

          printFloat(untransformedVertices[j].getNorm().x());
          printFloat(untransformedVertices[j].getNorm().y());
          printFloat(untransformedVertices[j].getNorm().z());
        } else
        {
          printFloat(vertices[j].positionRef().x());
          printFloat(vertices[j].positionRef().y());
          printFloat(vertices[j].positionRef().z());
          printFloat(vertices[j].positionRef().w());

          printFloat(vertices[j].normalRef().x());
          printFloat(vertices[j].normalRef().y());
          printFloat(vertices[j].normalRef().z());
        }

      printFloat(vertices[j].texCoordRef().x());
      printFloat(vertices[j].texCoordRef().y());
      printFloat(vertices[j].texCoordRef().z(), '\n');
    }
  memento.setData("vertices", tmpStr);

  tmpStr.clear();
  for(const auto i: bones)
    tmpStr += QStringLiteral("%1\n").arg(i);
  memento.setData("bones", tmpStr);
}

void KawaiiMesh3D::read(sib_utils::memento::Memento::DataReader &memento)
{
  bones.clear();
  skeletonActive = false;

  //Try read binary data
  memento.read("vertices", [this](const QByteArray &bytes) {
      vertices.resize(bytes.size() / sizeof(KawaiiPoint3D));
      memcpy(vertices.data(), bytes.constData(), vertices.size() * sizeof(KawaiiPoint3D));
    }).read("faces", [this](const QByteArray &bytes) {
      triangles.resize(bytes.size() / sizeof(KawaiiTriangle3D));
      memcpy(triangles.data(), bytes.constData(), triangles.size() * sizeof(KawaiiTriangle3D));
    });

  //Trty read text data
  memento.readText("vertices", [this](const QString &vertText) {
      QVector<QStringView> lines = QStringView(vertText).split('\n', Qt::SkipEmptyParts);
      QVector<QVector<QStringView>> terms(lines.size());
      vertices.resize(lines.size());
#     pragma omp parallel shared(terms)
      {
#       pragma omp for
        for(int i = 0; i < terms.size(); ++i)
          terms[i] = lines.at(i).split(' ', Qt::SkipEmptyParts);

#       pragma omp for
        for(auto i = 0; i < terms.size(); ++i)
          {
            auto &vert = vertices[i];
            const auto &term = terms.at(i);

            vert.positionRef().setX(term.at(0).toFloat());
            vert.positionRef().setY(term.at(1).toFloat());
            vert.positionRef().setZ(term.at(2).toFloat());
            vert.positionRef().setW(term.at(3).toFloat());

            vert.normalRef().setX(term.at(4).toFloat());
            vert.normalRef().setY(term.at(5).toFloat());
            vert.normalRef().setZ(term.at(6).toFloat());

            vert.texCoordRef().setX(term.at(7).toFloat());
            vert.texCoordRef().setY(term.at(8).toFloat());
            vert.texCoordRef().setZ(term.at(9).toFloat());
          }
      }
    });

  memento.readText("faces", [this](const QString &vertText) {
      QVector<QStringView> lines = QStringView(vertText).split('\n', Qt::SkipEmptyParts);
      QVector<QVector<QStringView>> terms(lines.size());
      triangles.resize(lines.size());
#     pragma omp parallel shared(terms)
      {
#       pragma omp for
        for(int i = 0; i < terms.size(); ++i)
          terms[i] = lines.at(i).split(' ', Qt::SkipEmptyParts);

#       pragma omp for
        for(auto i = 0; i < terms.size(); ++i)
          {
            auto &triangle = triangles[i];
            const auto &term = terms.at(i);

            triangle.a = term.at(0).toInt();
            triangle.b = term.at(1).toInt();
            triangle.c = term.at(2).toInt();
          }
      }
    });

  memento.readText("bones", [this] (const QString &bonesText) {
      QVector<QStringView> lines = QStringView(bonesText).split('\n', Qt::SkipEmptyParts);
      bones.reserve(lines.size());
      bonesTr.reserve(lines.size());
      for(const auto &i: lines)
        {
          bones.push_back(i.toUInt());
          if(skeleton && skeleton->getBoneCount() > 0)
            bonesTr.push_back(&skeleton->getBone(i.toUInt())->getTr());
        }
    });

  if(bones.empty())
    memento.read("bones", [this] (const QByteArray &bonesdata) {
        std::vector<uint64_t> bones_uint64(bonesdata.size() / sizeof(uint64_t));
        std::memcpy(bones_uint64.data(), bonesdata.data(), bonesdata.size());
        bones = std::vector<size_t>(bones_uint64.cbegin(), bones_uint64.cend());

        if(skeleton && skeleton->getBoneCount() > 0)
          {
            bonesTr.reserve(bones.size());
            for(auto boneIdx: bones)
              bonesTr.push_back(&skeleton->getBone(boneIdx)->getTr());
          }
      });
}

void KawaiiMesh3D::releaseOpenCLResources()
{
  if(openCLMemUntransformedVertices)
    clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemUntransformedVertices));
  if(openCLMemBoneWeights)
    clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemBoneWeights));
  if(openCLMemBoneWeightHints)
    clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemBoneWeightHints));
  if(openCLMemBoneMatrices)
    clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemBoneMatrices));
  if(openCLMemResult)
    clReleaseMemObject(reinterpret_cast<cl_mem>(openCLMemResult));
  if(openCLKernel)
    clReleaseKernel(reinterpret_cast<cl_kernel>(openCLKernel));

  openCLMemUntransformedVertices =
      openCLMemBoneWeights =
      openCLMemBoneWeightHints =
      openCLMemBoneMatrices =
      openCLMemResult =
      openCLKernel = nullptr;
}

namespace {
  class OpenCLInstance
  {
  public:
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_int lastInitErr;
    bool initialized;

    OpenCLInstance():
      platform(nullptr),
      device(nullptr),
      context(nullptr),
      queue(nullptr),
      lastInitErr(CL_SUCCESS),
      initialized(false)
    { }

    ~OpenCLInstance()
    {
      if(queue)
        clReleaseCommandQueue(queue);
      if(context)
        clReleaseContext(context);
      if(device)
        clReleaseDevice(device);
    }

    static void onOpenCLError(const char *errinfo,
                       const void */*private_info*/,
                       size_t /*cb*/,
                       void */*user_data*/)
    {
      qWarning().noquote() << QLatin1String("OpenCL error: ") << QLatin1String(errinfo);
    }

    cl_int init()
    {
      cl_int result = CL_SUCCESS;
      if(initialized)
        return result;
      else if(lastInitErr != CL_SUCCESS)
        return lastInitErr;

      cl_uint num_platforms = 0;

      result = clGetPlatformIDs(0, NULL, &num_platforms);
      if(result != CL_SUCCESS)
        {
          qWarning("Kawaii3D could not get OpenCL platform count!");
          return lastInitErr = result;
        }
      if(num_platforms == 0)
        {
          qWarning("Kawaii3D: There is no supported OpenCL platforms!");
          return lastInitErr = CL_DEVICE_NOT_FOUND;
        }

      std::vector<cl_platform_id> platforms(num_platforms);
      result = clGetPlatformIDs(platforms.size(), platforms.data(), nullptr);
      if(result != CL_SUCCESS)
        {
          qWarning("Kawaii3D could not get OpenCL platform IDs!");
          return lastInitErr = result;
        }

      cl_uint preferredQueueSize = 0;
      for(const auto &i: platforms)
        {
          cl_uint num_devices = 0;
          result = clGetDeviceIDs(i, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);
          if(result != CL_SUCCESS || num_devices == 0)
            continue;

          result = clGetDeviceIDs(i, CL_DEVICE_TYPE_GPU, 1, &device, nullptr);
          if(result == CL_SUCCESS)
            {
              platform = i;
              size_t sz;
              clGetDeviceInfo(device, CL_DEVICE_NAME, 0, nullptr, &sz);
              std::string devName(sz, '\0');
              clGetDeviceInfo(device, CL_DEVICE_NAME, devName.size(), devName.data(), &sz);
              clGetDeviceInfo(device, CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, sizeof(cl_uint), &preferredQueueSize, &sz);
              preferredQueueSize = std::max<cl_uint>(preferredQueueSize, 16*1024);
              break;
            }
        }
      if(result != CL_SUCCESS)
        {
          platform = nullptr;
          device = nullptr;
          qWarning("Kawaii3D could not select OpenCL device!");
          return lastInitErr = result;
        }

      struct CtxProperties {
        cl_context_properties propertyName;
        cl_platform_id platform;
        int zero = 0;
      } ctx_properties = {
        CL_CONTEXT_PLATFORM, platform, 0
      };
      context = clCreateContext(&ctx_properties.propertyName, 1, &device, onOpenCLError, nullptr, &result);
      if(result != CL_SUCCESS)
        {
          if(device)
            {
              clReleaseDevice(device);
              device = nullptr;
            }
          context = nullptr;
          queue = nullptr;
          platform = nullptr;
          qWarning("Kawaii3D could not create OpenCL context!");
          return lastInitErr = result;
        }

      queue = clCreateCommandQueue(context, device, 0, &result);
      if(result != CL_SUCCESS)
        {
          if(context)
            {
              clReleaseContext(context);
              context = nullptr;
            }
          if(device)
            {
              clReleaseDevice(device);
              device = nullptr;
            }
          queue = nullptr;
          platform = nullptr;
          qWarning("Kawaii3D could not create OpenCL device queue!");
          return lastInitErr = result;
        }

      initialized = true;
      return CL_SUCCESS;
    }

    cl_program buildProgram(const std::string &src, const QByteArray &kernelName)
    {
      cl_int result;
      const char *srcData = src.c_str();
      const size_t srcLength = src.size();
      cl_program out = clCreateProgramWithSource(context, 1, &srcData, &srcLength, &result);
      if(result != CL_SUCCESS)
        {
          qWarning("Kawaii3D could not create the \"%s\" OpenCL program!", kernelName.data());
          return {};
        }

      result = clBuildProgram(out, 1, &device, "-cl-single-precision-constant -cl-fast-relaxed-math", 0, 0);
      if(result != CL_SUCCESS)
        {
          qWarning("Kawaii3D could not build the \"%s\" OpenCL program!", kernelName.data());

          size_t build_log_len;
          result = clGetProgramBuildInfo(out, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &build_log_len);
          QByteArray buff_erro(build_log_len, '\0');
          result = clGetProgramBuildInfo(out, device, CL_PROGRAM_BUILD_LOG, build_log_len, buff_erro.data(), NULL);
          qWarning().noquote() << QLatin1String("Build log: ") << buff_erro;

          if(out)
            clReleaseProgram(out);
          return {};
        }
      return out;
    }

    cl_kernel buildKernel(cl_program prog, const QByteArray &kernelName, size_t &workgroupSize)
    {
      cl_int result;
      cl_kernel out;
      out = clCreateKernel(prog, kernelName.data(), &result);
      if(result != CL_SUCCESS)
        {
          qWarning("Kawaii3D could not create the \"%s\" OpenCL kernel!", kernelName.data());
          if(out)
            clReleaseKernel(out);
          return {};
        }

      clGetKernelWorkGroupInfo(out, device, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &workgroupSize, nullptr);
      return out;
    }

    void ensureMemBytes(cl_mem &mem, size_t sz, void *data, cl_mem_flags memFlags, bool &memRecreated)
    {
      cl_int clResult = CL_SUCCESS;

      if(mem)
        {
          size_t actualSz = 0;
          clGetMemObjectInfo(mem, CL_MEM_SIZE, sizeof(size_t), &actualSz, nullptr);
          if(actualSz != sz)
            {
              clReleaseMemObject(mem);
              mem = nullptr;
            }
          else if(data && (memFlags & CL_MEM_COPY_HOST_PTR) == CL_MEM_COPY_HOST_PTR) {
              clResult = clEnqueueWriteBuffer(queue, mem, CL_FALSE, 0, sz, data, 0, nullptr, nullptr);
              if(Q_UNLIKELY(clResult != CL_SUCCESS))
                {
                  clReleaseMemObject(mem);
                  mem = nullptr;
                }
            }
        }

      if(!mem)
        {
          mem = clCreateBuffer(context, memFlags,
                               sz, data,
                               &clResult);
          memRecreated = true;
        }

      if(clResult != CL_SUCCESS)
        mem = nullptr;
    }
  };
}

bool KawaiiMesh3D::enableSkeletonOpenCL()
{
  static OpenCLInstance openCLInstance;
  if(openCLInstance.init() != CL_SUCCESS)
    return false;

  static const std::string src =
      "typedef struct __attribute__ ((packed)) _BonesWeight\n"
      "{\n"
      "  uint boneIdx;\n"
      "  float weight;\n"
      "} BonesWeight;\n"

      "typedef struct __attribute__ ((packed)) _BonesWeightHint\n"
      "{\n"
      "  uint firstWeightIdx;\n"
      "  uint endWeightIdx;\n"
      "} BonesWeightHint;\n"

      "typedef struct __attribute__ ((packed)) _SourceVertex\n"
      "{\n"
      "  float4 pos;\n"
      "  float4 norm;\n"
      "} SourceVertex;\n"

      "typedef struct __attribute__ ((packed)) _DstVertex\n"
      "{\n"
      "  float4 pos;\n"
      "  float normX;\n"
      "  float normY;\n"
      "  float normZ;\n"
      "  float texcX;\n"
      "  float texcY;\n"
      "  float texcZ;\n"
      "} DstVertex;\n"

      "__kernel void enableSkeleton(__global DstVertex* outVerts,\n"
      "__global SourceVertex* srcVerts,\n"
      "__global BonesWeight* srcBoneWeights,\n"
      "__global BonesWeightHint* srcBoneWeightHints,\n"
      "__global float4* srcBoneTrs,\n"
      "int srcVertCount\n"
      ")\n"
      "{\n"
      "  int gid = get_global_id(0);\n"
      "  if(gid >= srcVertCount) return;\n"

      "  float4 trMat[4] = {(float4)(0.0), (float4)(0.0), (float4)(0.0), (float4)(0.0)};\n"
      "  for(uint i = srcBoneWeightHints[gid].firstWeightIdx; i < srcBoneWeightHints[gid].endWeightIdx; ++i) {\n"
      "    trMat[0] += srcBoneTrs[4*srcBoneWeights[i].boneIdx] * srcBoneWeights[i].weight;\n"
      "    trMat[1] += srcBoneTrs[4*srcBoneWeights[i].boneIdx + 1] * srcBoneWeights[i].weight;\n"
      "    trMat[2] += srcBoneTrs[4*srcBoneWeights[i].boneIdx + 2] * srcBoneWeights[i].weight;\n"
      "    trMat[3] += srcBoneTrs[4*srcBoneWeights[i].boneIdx + 3] * srcBoneWeights[i].weight;\n"
      "  }\n"

      "  outVerts[gid].pos = (float4)("
      "    (srcVerts[gid].pos[0]*trMat[0][0])+(srcVerts[gid].pos[1]*trMat[1][0])+(srcVerts[gid].pos[2]*trMat[2][0])+(srcVerts[gid].pos[3]*trMat[3][0]),\n"
      "    (srcVerts[gid].pos[0]*trMat[0][1])+(srcVerts[gid].pos[1]*trMat[1][1])+(srcVerts[gid].pos[2]*trMat[2][1])+(srcVerts[gid].pos[3]*trMat[3][1]),\n"
      "    (srcVerts[gid].pos[0]*trMat[0][2])+(srcVerts[gid].pos[1]*trMat[1][2])+(srcVerts[gid].pos[2]*trMat[2][2])+(srcVerts[gid].pos[3]*trMat[3][2]),\n"
      "    (srcVerts[gid].pos[0]*trMat[0][3])+(srcVerts[gid].pos[1]*trMat[1][3])+(srcVerts[gid].pos[2]*trMat[2][3])+(srcVerts[gid].pos[3]*trMat[3][3]));\n"

      "  outVerts[gid].normX = (srcVerts[gid].norm[0]*trMat[0][0])+(srcVerts[gid].norm[1]*trMat[1][0])+(srcVerts[gid].norm[2]*trMat[2][0]);\n"
      "  outVerts[gid].normY = (srcVerts[gid].norm[0]*trMat[0][1])+(srcVerts[gid].norm[1]*trMat[1][1])+(srcVerts[gid].norm[2]*trMat[2][1]);\n"
      "  outVerts[gid].normZ = (srcVerts[gid].norm[0]*trMat[0][2])+(srcVerts[gid].norm[1]*trMat[1][2])+(srcVerts[gid].norm[2]*trMat[2][2]);\n"
      "}\n";

  struct ClProg {
    cl_program prog = nullptr;

    ~ClProg()
    {
      if(prog)
        clReleaseProgram(prog);
    }
  };

  static ClProg clProgram;
  if(!clProgram.prog)
    clProgram.prog = openCLInstance.buildProgram(src, "enableSkeleton");

  if(!clProgram.prog)
    return false;

  bool needSetupArgs = false;
  if(!openCLKernel)
    {
      openCLKernel = openCLInstance.buildKernel(clProgram.prog, "enableSkeleton", openCLWorkgroupSize);
      needSetupArgs = true;
    }

  if(!openCLKernel)
    return false;

  if(!skeletonActive || !openCLMemUntransformedVertices)
    {
      openCLInstance.ensureMemBytes(reinterpret_cast<cl_mem&>(openCLMemUntransformedVertices),
                                    2*sizeof(glm::vec4)*untransformedVertices.size(), untransformedVertices.data(),
                                    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_WRITE_ONLY, needSetupArgs);

      openCLInstance.ensureMemBytes(reinterpret_cast<cl_mem&>(openCLMemResult),
                                    sizeof(KawaiiPoint3D)*vertexCount(), vertices.data(),
                                    CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, needSetupArgs);

#     pragma pack(push,1)
      struct BonesWeightHint
      {
        uint32_t firstWeightIdx;
        uint32_t endWeightIdx;
      };
#     pragma pack(pop)

      std::vector<std::pair<uint32_t, float>> boneWeights;
      std::vector<BonesWeightHint> boneWeightHints;
      boneWeightHints.reserve(boneAtt.size());
      for(size_t i = 0; i < boneAtt.size(); ++i)
        {
          boneWeightHints.push_back(BonesWeightHint {
                                      .firstWeightIdx = static_cast<uint32_t>(boneWeights.size()),
                                      .endWeightIdx = static_cast<uint32_t>(boneWeights.size() + boneAtt[i].boneWeights.size())
                                    });

          boneWeights.insert(boneWeights.end(), boneAtt[i].boneWeights.cbegin(), boneAtt[i].boneWeights.cend());
        }

      openCLInstance.ensureMemBytes(reinterpret_cast<cl_mem&>(openCLMemBoneWeights),
                                    sizeof(std::pair<uint32_t, float>)*boneWeights.size(), boneWeights.data(),
                                    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_WRITE_ONLY, needSetupArgs);

      openCLInstance.ensureMemBytes(reinterpret_cast<cl_mem&>(openCLMemBoneWeightHints),
                                    sizeof(BonesWeightHint)*boneWeightHints.size(), boneWeightHints.data(),
                                    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_WRITE_ONLY, needSetupArgs);

      skeletonActive = true;
    }

  openCLInstance.ensureMemBytes(reinterpret_cast<cl_mem&>(openCLMemBoneMatrices),
                                sizeof(glm::mat4)*bonesTr.size(), nullptr,
                                CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, needSetupArgs);

  cl_int clResult;
  glm::mat4 *clBoneTr = static_cast<glm::mat4*>(
        clEnqueueMapBuffer(openCLInstance.queue, reinterpret_cast<cl_mem&>(openCLMemBoneMatrices), CL_TRUE,
                           CL_MAP_WRITE_INVALIDATE_REGION, 0, sizeof(glm::mat4)*bonesTr.size(), 0, nullptr, nullptr, &clResult));
  if(clResult != CL_SUCCESS)
    return false;
  for(size_t i = 0; i < bones.size(); ++i)
    clBoneTr[i] = *bonesTr[i];
  clEnqueueUnmapMemObject(openCLInstance.queue, reinterpret_cast<cl_mem&>(openCLMemBoneMatrices), clBoneTr, 0, nullptr, nullptr);

  if(!openCLMemResult
     || !openCLMemUntransformedVertices
     || !openCLMemBoneWeights
     || !openCLMemBoneWeightHints
     || !openCLMemBoneMatrices)
    return false;

  if(needSetupArgs)
    {
      clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 0, sizeof(cl_mem), &openCLMemResult);
      clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 1, sizeof(cl_mem), &openCLMemUntransformedVertices);
      clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 2, sizeof(cl_mem), &openCLMemBoneWeights);
      clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 3, sizeof(cl_mem), &openCLMemBoneWeightHints);
      clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 4, sizeof(cl_mem), &openCLMemBoneMatrices);
    }
  int vertCount = static_cast<int>(vertexCount());
  clSetKernelArg(reinterpret_cast<cl_kernel>(openCLKernel), 5, sizeof(int), &vertCount);

  size_t clGlobalWorkSize = openCLWorkgroupSize * std::ceil(static_cast<double>(vertexCount()) / static_cast<double>(openCLWorkgroupSize));
  clResult = clEnqueueNDRangeKernel(openCLInstance.queue, reinterpret_cast<cl_kernel>(openCLKernel), 1, nullptr, &clGlobalWorkSize, &openCLWorkgroupSize, 0, nullptr, nullptr);

  if(clResult != CL_SUCCESS)
    {
      qWarning("Kawaii3D could not enqueue OpenCL kernel!");
      return false;
    }

  clEnqueueReadBuffer(openCLInstance.queue, reinterpret_cast<cl_mem>(openCLMemResult), CL_TRUE,
                      0, sizeof(KawaiiPoint3D)*vertexCount(), vertices.data(),
                      0, nullptr, nullptr);

  return true;
}

KawaiiMesh3D::TransformableVertex::TransformableVertex(const glm::vec4 &pos, const glm::vec4 &norm):
  pos(pos),
  norm(norm)
{
}

KawaiiMesh3D::TransformableVertex::TransformableVertex(const QVector4D &pos, const QVector3D &norm):
  pos(pos.x(), pos.y(), pos.z(), 1),
  norm(norm.x(), norm.y(), norm.z(), 0)
{
}

QVector4D KawaiiMesh3D::TransformableVertex::getPos() const
{
  return QVector4D(pos.x, pos.y, pos.z, 1);
}

QVector3D KawaiiMesh3D::TransformableVertex::getNorm() const
{
  return QVector3D(norm.x, norm.y, norm.z);
}

void KawaiiMesh3D::TransformableVertex::transformInPlace(const glm_vec4 *mat, QVector4D &outPos, QVector3D &outNormal) const
{
  const glm_vec4 *pos_data = reinterpret_cast<const glm_vec4*>(&pos);
  const glm_vec4 *norm_data = reinterpret_cast<const glm_vec4*>(&norm);

  const auto glmPos = glm_mat4_mul_vec4(mat, *pos_data);
  outPos = *reinterpret_cast<const QVector4D*>(&glmPos);

  const auto glmNormal = glm_mat4_mul_vec4(mat, *norm_data);
  outNormal = reinterpret_cast<const QVector4D*>(&glmNormal)->toVector3D();
}

bool KawaiiMesh3D::TransformableVertex::isNAN() const
{
  return std::isnan(pos.x)
      || std::isnan(pos.y)
      || std::isnan(pos.z)
      || std::isnan(pos.w)

      || std::isnan(norm.x)
      || std::isnan(norm.y)
      || std::isnan(norm.z);
}


bool KawaiiMesh3D::BoneAttachment::operator==(const BoneAttachment &b) const
{
  if(boneWeights.size() != b.boneWeights.size())
    return false;

  for(size_t i = 0; i < boneWeights.size(); ++i)
    if(boneWeights[i].first != b.boneWeights[i].first)
      return false;

  for(size_t i = 0; i < boneWeights.size(); ++i)
    if(!qFuzzyCompare(boneWeights[i].second, b.boneWeights[i].second))
      return false;

  return true;
}
