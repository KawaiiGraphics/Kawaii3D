#ifndef KAWAIISKELETON3D_HPP
#define KAWAIISKELETON3D_HPP

#include "../KawaiiDataUnit.hpp"
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <atomic>
#include <vector>

class KawaiiBone3D;
class KawaiiSkeletalAnimation;

//todo: memento stuff
class KAWAII3D_SHARED_EXPORT KawaiiSkeleton3D : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiSkeleton3D);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiSkeleton3D(const glm::mat4 &transform = glm::mat4(1), const std::string &name = "");
  ~KawaiiSkeleton3D();

  class KAWAII3D_SHARED_EXPORT Node {
    friend class KawaiiSkeleton3D;
  public:
    Node(const glm::mat4 &own_transform, const std::string &name);

    const glm::mat4& getOwnTransform() const;
    Node *getParent() const;

    const glm::mat4& getTransform();
    void setTransform(const glm::mat4 &tr, bool markDirty = true);

    const std::string& getName() const;

    KawaiiSkeleton3D *getSkeleton() const;

    Node& createChild(const glm::mat4 &own_transform, const std::string &name);
    size_t createBone();

    void markDirty();

  private:
    glm::mat4 own_transform;
    glm::mat4 abs_transform;
    std::string name;

    std::list<Node> children;
    Node *parent;

    KawaiiSkeleton3D *skeleton;
    std::atomic_flag dirty;

    void updateOnlySelf();
    void update();
  };

  Node& getRoot();
  const Node& getRoot() const;

  Node* findNode(const std::string &name);
  const Node* findNode(const std::string &name) const;
  Node* findNode(const std::string &name, Node *parent);
  Node* findNode(const std::string &name, const std::string &parentName);

  void forallNodes(const std::function<void(const Node&)> &func) const;

  KawaiiBone3D *getBone(size_t boneIndex) const;
  size_t getBoneCount() const;

  void updateBones();
  void clearRecentlyUpdatedFlag();

  void setAnimation(const KawaiiSkeletalAnimation &animation, int timerIntervalMs = 10);
  void setAnimation(const KawaiiSkeletalAnimation &animation, const std::unordered_map<std::string, std::string> &nodeMapping, int timerIntervalMs = 10);
  void stopAnimation();

  void transform(const glm::mat4 &mat);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;

signals:
  void animationTick();



  //IMPLEMENT
private:
  Node root;
  std::unordered_multimap<std::string, Node*> allNodes;
  std::vector<KawaiiBone3D*> bones;

  std::unique_ptr<QObject> animator;
  std::unique_ptr<QMutex> extrernalTransformationL;
};

#endif // KAWAIISKELETON3D_HPP
