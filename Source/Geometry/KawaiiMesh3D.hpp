#ifndef KAWAIIMESH3D_HPP
#define KAWAIIMESH3D_HPP

#include "KawaiiTriangle3D.hpp"
#include "KawaiiSkeleton3D.hpp"
#include "KawaiiPoint3D.hpp"
#include <QFuture>

class KawaiiTriangle3D;
class KawaiiModel3D;
class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiMesh3D : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiMesh3D);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  friend class KawaiiModel3D;

public:
  explicit KawaiiMesh3D();
  ~KawaiiMesh3D();

  KawaiiPoint3D& addVertex();
  KawaiiPoint3D& addVertex(const QVector4D &pos);
  KawaiiPoint3D& addVertex(const QVector4D &pos, const QVector3D &normal);
  KawaiiPoint3D& addVertex(const QVector4D &pos, const QVector3D &normal, const QVector3D &texcoord);

  const KawaiiPoint3D& getVertex(size_t index) const;
  void changeVertex(size_t index, const std::function<bool(KawaiiPoint3D&, size_t)> &func);

  void forallVertices(const std::function<void(KawaiiPoint3D&, size_t)> &func);
  void forallVertices(const std::function<void(KawaiiPoint3D&)> &func);
  QFuture<void> forallVerticesP(const std::function<void(KawaiiPoint3D&, size_t)> &func);
  QFuture<void> forallVerticesP(const std::function<void(KawaiiPoint3D&)> &func);

  void forallVerticesConst(const std::function<void(const KawaiiPoint3D&, size_t)> &func) const;
  void forallVerticesConst(const std::function<void(const KawaiiPoint3D&)> &func) const;
  QFuture<void> forallVerticesConstP(const std::function<void(const KawaiiPoint3D&)> &func) const;

  inline void forallVertices(const std::function<void(const KawaiiPoint3D&, size_t)> &func) const
  { return forallVerticesConst(func); }

  inline void forallVertices(const std::function<void(const KawaiiPoint3D&)> &func) const
  { return forallVerticesConst(func); }

  inline QFuture<void> forallVerticesP(const std::function<void(const KawaiiPoint3D&)> &func) const
  { return forallVerticesConstP(func); }

  void setVertex(size_t index, const KawaiiPoint3D &value);
  void setVertexPos(size_t index, const QVector4D &value);
  void setVertexNormal(size_t index, const QVector3D &value);
  void setVertexTexCoord(size_t index, const QVector3D &value);

  size_t vertexCount() const;
  void removeVertices(size_t index, size_t count);

  KawaiiTriangle3D::Instance addTriangle(uint32_t firstIndex, uint32_t secondIndex, uint32_t thirdIndex);
  KawaiiTriangle3D::Instance getTriangle(int index);
  const KawaiiTriangle3D::Instance& getTriangle(int index) const;
  size_t trianglesCount() const;
  void removeTriangles(size_t index, size_t count);

  void clear();

  inline auto& getVertices() const { return vertices; }
  inline auto& getRawTriangles() const { return triangles; }

  void computeNormals();
  void computeTexCoordsSpheral();

  void assignBone(size_t bone);

  void rebindSkeleton();

  void enableSkeleton();
  void disableSkeleton();

  std::vector<std::pair<uint32_t, uint32_t>> findIdenticalVertices() const;
  void joinVertices(std::vector<std::pair<uint32_t, uint32_t>> &&joiningVertices);
  void joinIdenticalVertices();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void triangleAdded(KawaiiTriangle3D::Instance triangle);
  void vertexAdded(KawaiiPoint3D *vertex);

  void trianglesRemoved(size_t i, size_t n);
  void verticesRemoved(size_t i, size_t n);

  void verticesUpdated(size_t i, size_t n);
  void indicesUpdated(size_t i, size_t n);



  //IMPLEMENT
private:
  std::vector<KawaiiPoint3D> vertices;
  std::vector<KawaiiTriangle3D> triangles;

  struct TransformableVertex {
    glm::vec4 pos;
    glm::vec4 norm;

    TransformableVertex(const glm::vec4 &pos, const glm::vec4 &norm);
    TransformableVertex(const QVector4D &pos, const QVector3D &norm);

    QVector4D getPos() const;
    QVector3D getNorm() const;

    void transformInPlace(const glm_vec4 *mat, QVector4D &outPos, QVector3D &outNormal) const;

    bool isNAN() const;
  };

  //Bone attachment to single vertex
  struct BoneAttachment
  {
    //boneIndex; influence
    std::vector<std::pair<uint32_t, float>> boneWeights;

    bool operator==(const BoneAttachment &another) const;
  };

  std::vector<TransformableVertex> untransformedVertices;
  std::vector<BoneAttachment> boneAtt;
  std::vector<size_t> bones;
  std::vector<const glm::mat4*> bonesTr;
  KawaiiSkeleton3D* skeleton;

  void* openCLMemUntransformedVertices;
  void* openCLMemBoneWeights;
  void* openCLMemBoneWeightHints;
  void* openCLMemBoneMatrices;
  void* openCLMemResult;
  void* openCLKernel;
  size_t openCLWorkgroupSize;

  void releaseOpenCLResources();

  bool skeletonActive;

  template<typename... ArgsT>
  KawaiiPoint3D& emplaceVertex(ArgsT&&... args)
  {
    if(skeletonActive)
      throw std::runtime_error("KawaiiMesh3D::addVertex is not allowed with active skeleton");

    vertices.emplace_back(std::forward<ArgsT>(args)...);
    emit vertexAdded(&vertices.back());
    return vertices.back();
  }

  bool enableSkeletonOpenCL();
};

#endif // KAWAIIMESH3D_HPP
