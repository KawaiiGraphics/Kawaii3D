#ifndef KAWAIIPOINT3D_HPP
#define KAWAIIPOINT3D_HPP

#include "../Kawaii3D_global.hpp"

#include <memory>
#include <QVector4D>
#include <QVector3D>
#include <QMetaType>
#include <functional>

#include <sib_utils/TupleHash.hpp>
#include <sib_utils/dirty_cast.hpp>

class KawaiiMesh3D;

class KAWAII3D_SHARED_EXPORT KawaiiPoint3D
{
public:
  KawaiiPoint3D() = default;
  KawaiiPoint3D(const QVector4D &position);
  KawaiiPoint3D(const QVector4D &position, const QVector3D &normal);
  KawaiiPoint3D(const QVector4D &position, const QVector3D &normal, const QVector3D &texCoord);

  ~KawaiiPoint3D() = default;

  QVector4D& positionRef();
  QVector3D& normalRef();
  QVector3D& texCoordRef();

  const QVector4D& positionRef() const;
  const QVector3D& normalRef() const;
  const QVector3D& texCoordRef() const;

  inline bool operator==(const KawaiiPoint3D & another) const
  { return pos == another.pos && normal == another.normal && texCoord == another.texCoord; }

  inline bool operator !=(const KawaiiPoint3D & another) const
  { return !(*this == another); }

  bool fuzzyCompare(const KawaiiPoint3D & another) const;

  bool isNAN() const;



  //IMPLEMENT
private:
  QVector4D pos;
  QVector3D normal;
  QVector3D texCoord;

  static const int qMetaId;
};

KAWAII3D_SHARED_EXPORT QDataStream& operator << (QDataStream &st, const KawaiiPoint3D &point);
KAWAII3D_SHARED_EXPORT QDataStream& operator >> (QDataStream &st, KawaiiPoint3D &point);

Q_DECLARE_METATYPE(KawaiiPoint3D);

namespace std
{
  template<>
  class hash<KawaiiPoint3D>
  {
    inline std::size_t vec4Hash(const QVector4D &vec) const noexcept
    {
      std::size_t h1 = sib_utils::dirty_cast<uint32_t>(vec.x());
      std::size_t h2 = sib_utils::dirty_cast<uint32_t>(vec.y());
      std::size_t h3 = sib_utils::dirty_cast<uint32_t>(vec.z());
      std::size_t h4 = sib_utils::dirty_cast<uint32_t>(vec.w());

      return std::hash<size_t>{}(h1 ^ (h2 << 1) ^ (h3 << 2) ^ (h4 << 3));
    }

    inline std::size_t vec3Hash(const QVector3D &vec) const noexcept
    {
      std::size_t h1 = sib_utils::dirty_cast<uint32_t>(vec.x());
      std::size_t h2 = sib_utils::dirty_cast<uint32_t>(vec.y());
      std::size_t h3 = sib_utils::dirty_cast<uint32_t>(vec.z());

      return std::hash<size_t>{}(h1 ^ (h2 << 1) ^ (h3 << 2));
    }

  public:
    inline std::size_t operator()(const KawaiiPoint3D &x) const noexcept
    {
      std::size_t h1 = vec4Hash(x.positionRef());
      std::size_t h2 = vec3Hash(x.normalRef());
      std::size_t h3 = vec3Hash(x.texCoordRef());

      return h1 ^ (h2 << 1) ^ (h3 << 2);
    }
  };
}

#endif // KAWAIIPOINT3D_HPP
