#ifndef KAWAIISKELETALANIMATION_HPP
#define KAWAIISKELETALANIMATION_HPP

#include "KawaiiSkeleton3D.hpp"

#include <glm/gtc/quaternion.hpp>
#include <unordered_map>
#include <functional>
#include <vector>
#include <chrono>

//todo: memento stuff

class KAWAII3D_SHARED_EXPORT KawaiiSkeletalAnimation : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiSkeletalAnimation);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  struct KAWAII3D_SHARED_EXPORT Transform {
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 scale;

    Transform() = default;
    Transform(const glm::quat &rotation, const glm::vec3 &translation, const glm::vec3 &scale);

    glm::mat4 toMat4() const;
    Transform interpolate(const Transform &next, float factor) const;
  };

  class KAWAII3D_SHARED_EXPORT Channel {
    std::vector<Transform> keyframes;
    float ticksPerSec;
    float durationSec;

  public:
    Channel() = default;
    Channel(std::vector<Transform> &&keyframes, float ticksPerSec);
    Channel(const Channel &orig);
    Channel(Channel &&orig);

    Channel& operator=(const Channel &orig);
    Channel& operator=(Channel &&orig);

    glm::mat4 getTransform(float timeSec) const;

    void reset(std::vector<Transform> &&keyframes, float ticksPerSec);
    void modify(const std::function<void(std::vector<Transform>&, float&)> &func);

    const std::vector<Transform> &getKeyframes() const;
    float getTicksPerSecond() const;
    float getDurationSec() const;

    QByteArray serialize() const;
    static Channel deserialize(const QByteArray &bytes);
  };

  KawaiiSkeletalAnimation();

  void setChannel(const std::string &nodeName, Channel &&val);
  Channel* getChannel(const std::string &nodeName);
  const Channel *getChannel(const std::string &nodeName) const;


  class KAWAII3D_SHARED_EXPORT Instance {
    std::unordered_map<KawaiiSkeleton3D::Node*, Channel> channels;
    std::chrono::steady_clock::time_point t0;
    KawaiiSkeleton3D &skeleton;

  public:
    Instance(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation);
    Instance(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation, const std::unordered_map<std::string, std::string> &nodeMapping);

    void start();
    void tick();
    void tick(float elapsedSec);
  };

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
private:
  std::unordered_map<std::string, Channel> channels;
};

#endif // KAWAIISKELETALANIMATION_HPP
