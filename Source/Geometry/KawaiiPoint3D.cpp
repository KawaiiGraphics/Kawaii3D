#include "KawaiiPoint3D.hpp"
#include <cmath>

KawaiiPoint3D::KawaiiPoint3D(const QVector4D &position):
  pos(position)
{
}

KawaiiPoint3D::KawaiiPoint3D(const QVector4D &position, const QVector3D &normal):
  pos(position),
  normal(normal.normalized())
{
}

KawaiiPoint3D::KawaiiPoint3D(const QVector4D &position, const QVector3D &normal, const QVector3D &texCoord):
  pos(position),
  normal(normal.normalized()),
  texCoord(texCoord)
{
}

QVector4D &KawaiiPoint3D::positionRef()
{
  return pos;
}

QVector3D &KawaiiPoint3D::normalRef()
{
  return normal;
}

QVector3D &KawaiiPoint3D::texCoordRef()
{
  return texCoord;
}

const QVector4D &KawaiiPoint3D::positionRef() const
{
  return pos;
}

const QVector3D &KawaiiPoint3D::normalRef() const
{
  return normal;
}

const QVector3D &KawaiiPoint3D::texCoordRef() const
{
  return texCoord;
}

bool KawaiiPoint3D::fuzzyCompare(const KawaiiPoint3D &another) const
{
  auto fuzzyCompare = [] (float a, float b) {
      return std::abs(a-b) <= 1e-3f;
    };

  return fuzzyCompare(pos.x(), another.pos.x())
      && fuzzyCompare(pos.y(), another.pos.y())
      && fuzzyCompare(pos.z(), another.pos.z())
      && fuzzyCompare(pos.w(), another.pos.w())

      && fuzzyCompare(normal.x(), another.normal.x())
      && fuzzyCompare(normal.y(), another.normal.y())
      && fuzzyCompare(normal.z(), another.normal.z())

      && fuzzyCompare(texCoord.x(), another.texCoord.x())
      && fuzzyCompare(texCoord.y(), another.texCoord.y())
      && fuzzyCompare(texCoord.z(), another.texCoord.z());
}

bool KawaiiPoint3D::isNAN() const
{
  return std::isnan(pos.x())
      || std::isnan(pos.y())
      || std::isnan(pos.z())
      || std::isnan(pos.w())

      || std::isnan(normal.x())
      || std::isnan(normal.y())
      || std::isnan(normal.z())

      || std::isnan(texCoord.x())
      || std::isnan(texCoord.y())
      || std::isnan(texCoord.z());
}

KAWAII3D_SHARED_EXPORT QDataStream &operator <<(QDataStream &st, const KawaiiPoint3D &point)
{
  return st << point.positionRef() << point.normalRef() << point.texCoordRef();
}

KAWAII3D_SHARED_EXPORT QDataStream &operator >>(QDataStream &st, KawaiiPoint3D &point)
{
  return st >> point.positionRef() >> point.normalRef() >> point.texCoordRef();
}

