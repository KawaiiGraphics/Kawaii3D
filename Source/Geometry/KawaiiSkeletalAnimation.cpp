#include "KawaiiSkeletalAnimation.hpp"
#include <QtConcurrent>
#include <math.h>
#include <glm/gtc/matrix_transform.hpp>
#include <cstring>

namespace {
  glm::vec3 interpolateVec3(const glm::vec3 &a, const glm::vec3 &b, float factor)
  {
    return b*factor + a * (1.0f - factor);
  }
}

KawaiiSkeletalAnimation::KawaiiSkeletalAnimation()
{
}

void KawaiiSkeletalAnimation::setChannel(const std::string &nodeName, KawaiiSkeletalAnimation::Channel &&val)
{
  channels[nodeName] = std::move(val);
}

KawaiiSkeletalAnimation::Channel *KawaiiSkeletalAnimation::getChannel(const std::string &nodeName)
{
  if(auto el = channels.find(nodeName); el != channels.end())
    return &el->second;
  return nullptr;
}

const KawaiiSkeletalAnimation::Channel *KawaiiSkeletalAnimation::getChannel(const std::string &nodeName) const
{
  if(auto el = channels.find(nodeName); el != channels.end())
    return &el->second;
  return nullptr;
}

void KawaiiSkeletalAnimation::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  for(const auto &i: channels)
    mem.setData(QString::fromStdString(i.first), i.second.serialize());
}

void KawaiiSkeletalAnimation::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiSkeletalAnimation::read(sib_utils::memento::Memento::DataReader &mem)
{
  channels.clear();
  QMutex channelsLock;
  mem.readAll([this, &channelsLock] (const QString &name, const QByteArray &data) {
      auto channel = Channel::deserialize(data);
      {
        QMutexLocker l(&channelsLock);
        channels.emplace(std::piecewise_construct, std::tuple { name.toStdString() }, std::tuple { std::move(channel) });
      }
    });
}

KawaiiSkeletalAnimation::Instance::Instance(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation):
  skeleton(skeleton)
{
  for(const auto &i: animation.channels)
    {
      auto node = skeleton.findNode(i.first);
      if(node)
        channels[node] = i.second;
      else
        qWarning("KawaiiSkeletalAnimation::Instance: bone \"%s\" was not found in skeleton", i.first.c_str());
    }
  start();
}

KawaiiSkeletalAnimation::Instance::Instance(KawaiiSkeleton3D &skeleton, const KawaiiSkeletalAnimation &animation, const std::unordered_map<std::string, std::string> &nodeMapping):
  skeleton(skeleton)
{
  for(const auto &i: animation.channels)
    {
      auto node = skeleton.findNode(i.first);
      if(!node)
        if(auto el = nodeMapping.find(i.first); el != nodeMapping.end())
          node = skeleton.findNode(el->second);

      if(node)
        channels[node] = i.second;
      else
        qWarning("KawaiiSkeletalAnimation::Instance: bone \"%s\" was not found in skeleton", i.first.c_str());
    }
  start();
}

void KawaiiSkeletalAnimation::Instance::start()
{
  t0 = std::chrono::steady_clock::now();
  tick();
}

void KawaiiSkeletalAnimation::Instance::tick()
{
  const auto t = std::chrono::steady_clock::now();
  const float elapsedTimeSec = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(t-t0).count()) / 1000.0f;
  tick(elapsedTimeSec);
}

void KawaiiSkeletalAnimation::Instance::tick(float elapsedSec)
{
  QtConcurrent::map(channels, [elapsedSec] (const std::pair<KawaiiSkeleton3D::Node*, KawaiiSkeletalAnimation::Channel> &i) {
      i.first->setTransform(i.second.getTransform(elapsedSec), false);
    }).waitForFinished();
  skeleton.getRoot().markDirty();
  emit skeleton.animationTick();
}

KawaiiSkeletalAnimation::Transform::Transform(const glm::quat &rotation, const glm::vec3 &translation, const glm::vec3 &scale):
  rotation(rotation),
  translation(translation),
  scale(scale)
{
}

glm::mat4 KawaiiSkeletalAnimation::Transform::toMat4() const
{
  const glm::mat4 rotationMat = glm::mat4_cast(rotation);

  glm::mat4 translationMat(1.0);
  translationMat = glm::translate (translationMat, translation);

  glm::mat4 scaleMat(1.0);
  scaleMat = glm::scale (scaleMat, scale);

  return translationMat * rotationMat * scaleMat;
}

KawaiiSkeletalAnimation::Transform KawaiiSkeletalAnimation::Transform::interpolate(const KawaiiSkeletalAnimation::Transform &next, float factor) const
{
  assert(factor >= 0.0 && factor <= 1.0);
  return Transform(glm::slerp(rotation, next.rotation, factor), interpolateVec3(translation, next.translation, factor), interpolateVec3(scale, next.scale, factor));
}

KawaiiSkeletalAnimation::Channel::Channel(std::vector<KawaiiSkeletalAnimation::Transform> &&keyframes, float ticksPerSec):
  keyframes(std::move(keyframes)),
  ticksPerSec(ticksPerSec),
  durationSec(static_cast<float>(this->keyframes.size()) / ticksPerSec)
{
}

KawaiiSkeletalAnimation::Channel::Channel(const KawaiiSkeletalAnimation::Channel &orig):
  keyframes(orig.keyframes),
  ticksPerSec(orig.ticksPerSec),
  durationSec(orig.durationSec)
{ }

KawaiiSkeletalAnimation::Channel::Channel(KawaiiSkeletalAnimation::Channel &&orig):
  keyframes(std::move(orig.keyframes)),
  ticksPerSec(orig.ticksPerSec),
  durationSec(orig.durationSec)
{ }

KawaiiSkeletalAnimation::Channel &KawaiiSkeletalAnimation::Channel::operator=(const KawaiiSkeletalAnimation::Channel &orig)
{
  keyframes = orig.keyframes;
  ticksPerSec = orig.ticksPerSec;
  durationSec = orig.durationSec;
  return *this;
}

KawaiiSkeletalAnimation::Channel &KawaiiSkeletalAnimation::Channel::operator=(KawaiiSkeletalAnimation::Channel &&orig)
{
  keyframes = std::move(orig.keyframes);
  ticksPerSec = orig.ticksPerSec;
  durationSec = orig.durationSec;
  return *this;
}

glm::mat4 KawaiiSkeletalAnimation::Channel::getTransform(float timeSec) const
{
  const float tickNum = std::fmod(timeSec, durationSec) * ticksPerSec,
      i1 = std::ceil(tickNum),
      i0 = i1 - 1,
      factor = tickNum - i0;

  if(i0 < 0)
    return keyframes.front().toMat4();
  else if(static_cast<size_t>(i1) >= keyframes.size())
    return keyframes.back().toMat4();

  return keyframes[static_cast<size_t>(i0)].interpolate(keyframes[static_cast<size_t>(i1)], factor).toMat4();
}

void KawaiiSkeletalAnimation::Channel::reset(std::vector<KawaiiSkeletalAnimation::Transform> &&keyframes, float ticksPerSec)
{
  this->keyframes = std::move(keyframes);
  this->ticksPerSec = ticksPerSec;
  durationSec = static_cast<float>(this->keyframes.size()) / ticksPerSec;
}

void KawaiiSkeletalAnimation::Channel::modify(const std::function<void (std::vector<KawaiiSkeletalAnimation::Transform> &, float &)> &func)
{
  func(keyframes, ticksPerSec);
  durationSec = static_cast<float>(this->keyframes.size()) / ticksPerSec;
}

const std::vector<KawaiiSkeletalAnimation::Transform> &KawaiiSkeletalAnimation::Channel::getKeyframes() const
{
  return keyframes;
}

float KawaiiSkeletalAnimation::Channel::getTicksPerSecond() const
{
  return ticksPerSec;
}

float KawaiiSkeletalAnimation::Channel::getDurationSec() const
{
  return durationSec;
}

QByteArray KawaiiSkeletalAnimation::Channel::serialize() const
{
  QByteArray result(sizeof(float) + sizeof(Transform) * keyframes.size(), '\0');

  *reinterpret_cast<float*>(result.data()) = ticksPerSec;
  std::memcpy(result.data() + sizeof(float), keyframes.data(), sizeof(Transform) * keyframes.size());

  return result;
}

KawaiiSkeletalAnimation::Channel KawaiiSkeletalAnimation::Channel::deserialize(const QByteArray &bytes)
{
  const float ticksPerSec = *reinterpret_cast<const float*>(bytes.data());
  std::vector<Transform> keyframes((bytes.size() - sizeof(float)) / sizeof(Transform));
  std::memcpy(keyframes.data(), bytes.data() + sizeof(float), sizeof(Transform) * keyframes.size());
  return Channel(std::move(keyframes), ticksPerSec);
}
