#ifndef KAWAIIMESHINSTANCE_HPP
#define KAWAIIMESHINSTANCE_HPP

#include "../KawaiiGpuBuf.hpp"
#include "../KawaiiDataUnit.hpp"

class KawaiiMesh3D;
class KawaiiMaterial;
class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiMeshInstance: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiMeshInstance);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
  friend class KawaiiRoot;

public:
  KawaiiMeshInstance(KawaiiMaterial *material = nullptr, KawaiiGpuBuf *uniforms = nullptr);
  ~KawaiiMeshInstance();

  KawaiiMaterial *getMaterial() const;
  void setMaterial(KawaiiMaterial *value);

  KawaiiMesh3D *getMesh() const;

  KawaiiGpuBuf *getUniforms() const;
  void setUniforms(KawaiiGpuBuf *var);

  size_t getInstanceCount() const;
  void setInstanceCount(size_t value);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void meshChanged(KawaiiMesh3D *mesh);
  void uniformsChanged(KawaiiGpuBuf *uniforms);
  void instanceCountChanged(size_t count);
  void drawPipelineChanged();
  void askedRedraw();



  //IMPLEMENT
private:
  KawaiiGpuBuf *uniforms;
  KawaiiMesh3D *mesh;
  KawaiiMaterial *material;
  KawaiiRoot *root;
  size_t instanceCount;

  void updateRoot();
  void detachUniforms();
  void detachMaterial();
};

#endif // KAWAIIMESHINSTANCE_HPP
