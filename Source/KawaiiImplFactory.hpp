#ifndef KAWAIIIMPLFACTORY_HPP
#define KAWAIIIMPLFACTORY_HPP

#include "KawaiiRendererImpl.hpp"
#include "KawaiiDataUnit.hpp"
#include "Exceptions/KawaiiDuplicateExcep.hpp"
#include <QHash>
#include <typeinfo>
#include <typeindex>
#include <type_traits>

class KAWAII3D_SHARED_EXPORT KawaiiImplFactory : public QObject
{
  Q_OBJECT
public:
  explicit KawaiiImplFactory(QObject *parent = nullptr);
  ~KawaiiImplFactory();

  KawaiiRendererImpl* createRenderer(KawaiiDataUnit &data);

  template<typename Model, typename RendererImpl, bool override = true>
  void registerImpl()
  {
    static_assert(std::is_base_of<KawaiiDataUnit, Model>::value, "Model must be based on KawaiiAbstractData");
    static_assert(std::is_base_of<KawaiiRendererImpl, RendererImpl>::value, "Controller must be based on KawaiiController");

    registerImpl<override>(&Model::staticMetaObject, &RendererImpl::staticMetaObject, [](KawaiiDataUnit *data) -> KawaiiRendererImpl* { return new RendererImpl(dynamic_cast<Model*>(data)); });
  }

  template<typename Model, std::nullptr_t, bool override = true>
  void registerImpl()
  {
    static_assert(std::is_base_of<KawaiiDataUnit, Model>::value, "Model must be based on KawaiiAbstractData");
    registerImpl<override>(&Model::staticMetaObject, nullptr, [](KawaiiDataUnit*) -> KawaiiRendererImpl* { return nullptr; });
  }

  bool checkCompatibility(const KawaiiDataUnit &data);



  //IMPLEMENT
private:
  QHash<const QMetaObject*, std::function<KawaiiRendererImpl* (KawaiiDataUnit*)>> creators;
  QHash<const QMetaObject*, const QMetaObject*> metaObjects;

  template<bool override = true>
  void registerImpl(const QMetaObject *modelType, const QMetaObject *rendererType, const std::function<KawaiiRendererImpl* (KawaiiDataUnit*)> &ctrlCreator)
  {
    if(auto el = creators.find(modelType); el != creators.end())
      {
        if constexpr(override)
        {
          el.value() = ctrlCreator;
        } else
        {
          throw KawaiiDuplicateExcep(el.key(), nullptr);
        }
      } else
      {
        creators.insert(modelType, ctrlCreator);
        metaObjects.insert(modelType, rendererType);
      }
  }
};

#endif // KAWAIIIMPLFACTORY_HPP
