#ifndef KAWAIIGPUBUF_HPP
#define KAWAIIGPUBUF_HPP

#include "KawaiiDataUnit.hpp"
#include <list>

class KawaiiTexture;

class KAWAII3D_SHARED_EXPORT KawaiiGpuBuf: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiGpuBuf);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  KawaiiGpuBuf(const KawaiiGpuBuf&) = delete;
  KawaiiGpuBuf& operator= (const KawaiiGpuBuf&) = delete;

public:
  class PartialAlias
  {
    friend class ::KawaiiGpuBuf;

    QMetaObject::Connection onSrcUpdated;
    QMetaObject::Connection onSrcRelocated;
    QMetaObject::Connection onSrcWasAquired;
    QMetaObject::Connection onSrcDestroyed;
    KawaiiGpuBuf * const src;
    KawaiiGpuBuf * const dst;
    const size_t srcOffset;
    const size_t dstOffset;
    const size_t size;

    PartialAlias(KawaiiGpuBuf *src, KawaiiGpuBuf *dst, size_t srcOffset, size_t dstOffset, size_t size):
      src(src),
      dst(dst),
      srcOffset(srcOffset),
      dstOffset(dstOffset),
      size(size)
    {}
  };

  KawaiiGpuBuf();
  KawaiiGpuBuf(const void *ptr, size_t n);
  virtual ~KawaiiGpuBuf();

  KawaiiGpuBuf(KawaiiGpuBuf&&orig);
  KawaiiGpuBuf& operator= (KawaiiGpuBuf&&);

  void setData(const void *ptr, size_t n);

  size_t getDataSize() const;
  void* getData();
  const void* getData() const;

  inline const void* getConstData() const
  { return getData(); }

  void fill(size_t offset, const void *ptr, size_t bytes);
  void dump(size_t offset, void *dest, size_t bytes) const;
  void realloc(size_t newSize);
  void append(const void *buf, size_t bufSize);
  void erase(size_t offset, size_t bytes);

  std::list<PartialAlias>::iterator addAlias(KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size);
  std::list<PartialAlias>::iterator removeAlias(const std::list<PartialAlias>::iterator &iterator);

  void bindTexture(const QString &bindingName, KawaiiTexture *tex);
  void unbindTexture(const QString &bindingName);
  void unbindTexture(QObject *texture);
  void forallTextureBindings(const std::function<void(const QString&, KawaiiTexture*)> &func) const;
  size_t bindedTexturesCount() const;
  KawaiiTexture *getBindedTexture(const QString &bindingName) const;

  /// Should be called from Renderer
  void setCopyFromFunc(const std::function<void (KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size)> &newCopyFrom);

  /// Should be called from Renderer
  void resetCopyFromFunc();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void relocated(const void *data, size_t size);
  void dataChanged(size_t offset, size_t bytes);
  void wasAcquired(KawaiiGpuBuf *by);
  void updated();
  void textureBinded(const QString &textureName, KawaiiTexture *texture);



  //IMPLEMENT
private:
  std::list<PartialAlias> aliases;
  QHash<QString, KawaiiTexture*> bindedTextures;
  std::function<void (KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size)> copyFrom;
  size_t dataSize;
  void *data;

  void freeData();
  void aliasSrcUpdated(const PartialAlias &a, size_t offset, size_t bytes);
  void aliasSrcRelocated(KawaiiGpuBuf *src);
  void aliasSrcWasAquired(KawaiiGpuBuf *oldSrc, KawaiiGpuBuf *newSrc);
  void aliasSrcDestroyed(QObject *obj);
  void naiveCopyFrom(KawaiiGpuBuf *src, size_t srcOffset, size_t dstOffset, size_t size);
  void unbindTexture(QHash<QString, KawaiiTexture*>::iterator el);
  void detachTexture(KawaiiTexture *texture);

  static int size_t_qMetaId;
};

#endif // KAWAIIGPUBUF_HPP
