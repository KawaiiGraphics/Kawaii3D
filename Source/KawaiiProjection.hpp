#ifndef KAWAIIPROJECTION_HPP
#define KAWAIIPROJECTION_HPP

#include "KawaiiDataUnit.hpp"
#include <sib_utils/Memento/Memento.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <functional>

class KAWAII3D_SHARED_EXPORT KawaiiProjection
{
public:
  std::function<void()> onUpdate;

  KawaiiProjection(float nearClip, float farClip);
  virtual ~KawaiiProjection() = default;

  const glm::mat4 &getClipCorrection() const;
  const glm::mat4 &getClipCorrectionInverted() const;
  void setClipCorrection(const glm::mat4 &value);

  const glm::uvec2 &getSize() const;
  void setSize(const glm::uvec2 &value);

  float getNearClip() const;
  void setNearClip(float value);

  float getFarClip() const;
  void setFarClip(float value);

  glm::mat4 getMatrix() const;

  virtual KawaiiProjection* clone() const = 0;

  virtual void writeTextMemento(sib_utils::memento::Memento::DataMutator &mem) const = 0;
  virtual void writeBinaryMemento(sib_utils::memento::Memento::DataMutator &mem) const = 0;
  static KawaiiProjection* createProjection(sib_utils::memento::Memento::DataReader &mem);

private:
  virtual glm::mat4 calculate(const glm::uvec2 &surfaceSize) const = 0;
  virtual void readMemento(sib_utils::memento::Memento::DataReader &mem) = 0;

protected:
  void updateRequest();



  //IMPLEMENT
private:
  static QHash<QString, std::function<KawaiiProjection*()>> creators;

  glm::mat4 clipCorrection;
  glm::mat4 clipCorrectionInverted;
  glm::uvec2 size;

  float nearClip;
  float farClip;

protected:
  template<typename T>
  class TypeRegistrator {
  public:
    inline TypeRegistrator()
    {
      creators[T::nodeType] = [] { return new T; };
    }
  };
};

class KAWAII3D_SHARED_EXPORT KawaiiProjectionHandler
{
public:
  KawaiiProjectionHandler(const glm::uvec2 *surfaceSize);
  virtual ~KawaiiProjectionHandler();

  glm::mat4 calculateProjectionMatrix() const;

  inline KawaiiProjection *getProjectionPtr() const
  { return p; }

  template<typename T, class = std::enable_if_t<std::is_base_of_v<KawaiiProjection, T>>>
  T& getProjection() const
  {
    return dynamic_cast<T&>(*getProjectionPtr());
  }

  const glm::mat4 &getClipCorrection() const;
  const glm::mat4 &getClipCorrectionInverted() const;
  void setClipCorrection(const glm::mat4 &value);
  void setProjection(KawaiiProjection *ptr);

  template<typename T, typename... ArgsT, class = std::enable_if_t<std::is_base_of_v<KawaiiProjection, T>>>
  T& createProjection(ArgsT&&... args)
  {
    T *ptr = new T(std::forward<ArgsT>(args)...);
    setProjection(ptr);
    return *ptr;
  }

  void copyProjectionFrom(const KawaiiProjectionHandler &from);

  void sizeChanged();

protected:
  virtual void onProjectionMatChanged() = 0;



  //IMPLEMENT
private:
  KawaiiProjection *p;
  const glm::uvec2 &size;
};

#endif // KAWAIIPROJECTION_HPP
