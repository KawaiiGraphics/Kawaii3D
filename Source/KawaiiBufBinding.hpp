#ifndef KAWAIIBUFBINDING_HPP
#define KAWAIIBUFBINDING_HPP

#include "KawaiiGpuBuf.hpp"
#include "KawaiiBufferTarget.hpp"

class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiBufBinding : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiBufBinding);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiBufBinding();
  ~KawaiiBufBinding();

  uint32_t getBindingPoint() const;
  void setBindingPoint(uint32_t value);

  KawaiiBufferTarget getTarget() const;
  void setTarget(KawaiiBufferTarget target);

  KawaiiGpuBuf *getBuf() const;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void bindingPointChanged(uint32_t binding);
  void bufferChanged(KawaiiGpuBuf *buf);
  void targetChanged(KawaiiBufferTarget target);



  //IMPLEMENT
private:
  KawaiiGpuBuf *buf;
  KawaiiRoot *root;
  QMetaObject::Connection onBufUpdated;
  uint32_t bindingPoint;
  KawaiiBufferTarget bindingTarget;

  void onParentChanged();

  bool setTarget(const QString &target);
};

#endif // KAWAIIBUFBINDING_HPP
