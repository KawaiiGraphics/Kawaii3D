#ifndef KAWAIISHADOWMAPPER_HPP
#define KAWAIISHADOWMAPPER_HPP

#include "KawaiiLamp.hpp"
#include "../Textures/KawaiiFramebuffer.hpp"
#include "../Renderpass/KawaiiSceneLayer.hpp"
#include "../Textures/KawaiiEnvMap.hpp"
#include "../Textures/KawaiiDepthCubemapArray.hpp"
#include "../Textures/KawaiiDepthTex2dArray.hpp"

#include <QPointer>
#include <vector>

class KawaiiLampsLayoutAbstract;

class KAWAII3D_SHARED_EXPORT KawaiiShadowMapper : public KawaiiDataUnit
{
  Q_OBJECT

public:
  KawaiiShadowMapper(KawaiiLampsLayoutAbstract *lampProvider, KawaiiScene *scene);
  ~KawaiiShadowMapper();

  KawaiiDepthTex2dArray *getDirShadowMap() const;
  KawaiiDepthTex2dArray *getSpotShadowMap() const;
  KawaiiDepthCubemapArray *getDotShadowMap() const;

  const QSize &getResolution() const;
  void setResolution(const QSize &value);

  void dirLightsCountChanged(size_t n);
  void spotLightsCountChanged(size_t n);
  void dotLightsCountChanged(size_t n);

  void updateDotLight(KawaiiLamp &l, size_t i);
  void updateSpotLight(KawaiiProjector &l, size_t i);
  void updateDirLight(KawaiiDirLight &l, size_t i);

  float getNearClip() const;
  void setNearClip(float value);

  float getFarClip() const;
  void setFarClip(float value);

  const QVector3D &getDirLightsCenter() const;
  void setDirLightsCenter(const QVector3D &newDirLightsCenter);

  const QVector3D &getDirLightsArea() const;
  void setDirLightsArea(const QVector3D &newDirLightsArea);

  KawaiiSceneLayer *getDirLightsAreaLayer() const;
  void setDirLightsAreaLayer(KawaiiSceneLayer *newDirLightsAreaLayer);



  //IMPLEMENT
private:
  struct ShadowRender {
    std::unique_ptr<KawaiiFramebuffer> fbo;
    std::unique_ptr<KawaiiRenderpass> renderpass;
    std::unique_ptr<KawaiiSceneLayer> sceneLayer;
    std::unique_ptr<KawaiiCamera> cam;
    float shadowCenterStep;
    bool matUpdateSupressed;
  };

  KawaiiLampsLayoutAbstract *lampLayout;
  KawaiiScene *scene;
  std::vector<ShadowRender> dirShadowRender;
  std::vector<ShadowRender> spotShadowRender;
  std::vector<std::unique_ptr<KawaiiEnvMap>> dotShadowRender;

  KawaiiDepthTex2dArray *dirShadowMap;
  KawaiiDepthTex2dArray *spotShadowMap;
  KawaiiDepthCubemapArray *dotShadowMap;

  KawaiiSceneLayer *dirLightsAreaLayer;

  QSize resolution;
  QVector3D dirLightsCenter;
  QVector3D dirLightsArea;
  float nearClip;
  float farClip;

  float resolutionInverted;

  void setDirLightMat(size_t i);
  void setSpotLightMat(size_t i);
  void setDotLightMat(size_t i);

  void onDirLightsAreaLayerMatUpdate();
};

#endif // KAWAIISHADOWMAPPER_HPP
