#include "KawaiiShadowMapper.hpp"
#include "KawaiiLampsLayout.hpp"

#include <KawaiiPerspective.hpp>
#include <KawaiiOrtho.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <QQuaternion>
#include <QMatrix4x4>

namespace {
  const glm::mat4 bias_matrix(
      0.5, 0.0, 0.0, 0.0,
      0.0, 0.5, 0.0, 0.0,
      0.0, 0.0, 1.0, 0.0,
      0.5, 0.5, 0.0, 1.0
      );
}

KawaiiShadowMapper::KawaiiShadowMapper(KawaiiLampsLayoutAbstract *lampLayout, KawaiiScene *scene):
  lampLayout(lampLayout),
  scene(scene),
  dirLightsAreaLayer(nullptr),
  resolution(1024, 1024),
  dirLightsArea(20,20,20),
  nearClip(0.1),
  farClip(100.0),
  resolutionInverted(1)
{
  dirShadowMap = createChild<KawaiiDepthTex2dArray>();
  spotShadowMap = createChild<KawaiiDepthTex2dArray>();
  dotShadowMap = createChild<KawaiiDepthCubemapArray>();

  dirShadowMap->setSize(resolution);
  dirShadowMap->setCompareOperation(KawaiiDepthCompareOperation::Less);
  spotShadowMap->setSize(resolution);
  spotShadowMap->setCompareOperation(KawaiiDepthCompareOperation::LessEqual);
  dotShadowMap->setSize(resolution);
  dotShadowMap->setCompareOperation(KawaiiDepthCompareOperation::LessEqual);

  dirLightsCountChanged(lampLayout->getDirLightCount());
  spotLightsCountChanged(lampLayout->getSpotLightCount());
  dotLightsCountChanged(lampLayout->getDotLightCount());

  connect(scene, &QObject::destroyed, this, [this] {
      for(auto &i: dirShadowRender)
        {
          i.cam.release();
          i.renderpass.release();
          i.sceneLayer.release();
          i.cam.reset();
          i.renderpass.reset();
          i.sceneLayer.reset();
        }
      for(auto &i: spotShadowRender)
        {
          i.cam.release();
          i.renderpass.release();
          i.sceneLayer.release();
          i.cam.reset();
          i.renderpass.reset();
          i.sceneLayer.reset();
        }
      for(auto &i: dotShadowRender)
        {
          i.release();
          i.reset();
        }
      this->scene = nullptr;
    });
}

KawaiiShadowMapper::~KawaiiShadowMapper()
{
  for(auto &el: dirShadowRender)
    disconnect(el.sceneLayer.get(), &KawaiiSceneLayer::viewProjectionMatChanged, this, nullptr);
  for(auto &el: spotShadowRender)
    disconnect(el.sceneLayer.get(), &KawaiiSceneLayer::viewProjectionMatChanged, this, nullptr);
  for(auto &el: dotShadowRender)
    disconnect(el.get(), &KawaiiEnvMap::projectionMatChanged, this, nullptr);
}

KawaiiDepthTex2dArray *KawaiiShadowMapper::getDirShadowMap() const
{
  return dirShadowMap;
}

KawaiiDepthTex2dArray *KawaiiShadowMapper::getSpotShadowMap() const
{
  return spotShadowMap;
}

KawaiiDepthCubemapArray *KawaiiShadowMapper::getDotShadowMap() const
{
  return dotShadowMap;
}

const QSize &KawaiiShadowMapper::getResolution() const
{
  return resolution;
}

void KawaiiShadowMapper::setResolution(const QSize &value)
{
  if(resolution != value)
    {
      resolution = value;
      dirShadowMap->setSize(resolution);
      spotShadowMap->setSize(resolution);
      dotShadowMap->setSize(resolution);
      for(auto &i: dirShadowRender)
        {
          i.fbo->setSize(glm::uvec2(resolution.width(), resolution.height()));
          i.shadowCenterStep = 0;
        }
      for(const auto &i: spotShadowRender)
        i.fbo->setSize(glm::uvec2(resolution.width(), resolution.height()));
      for(const auto &i: dotShadowRender)
        i->setResolution(glm::uvec2(resolution.width(), resolution.height()));
      resolutionInverted = 1.0 / std::max(getResolution().width(), getResolution().height());
    }
}

void KawaiiShadowMapper::dirLightsCountChanged(size_t n)
{
  dirShadowMap->setLayers(std::max<size_t>(n, 1));
  dirShadowRender.resize(n);
  for(size_t i = 0; i < dirShadowRender.size(); ++i)
    {
      auto &el = dirShadowRender[i];
      auto &l = lampLayout->getDirLight(i);
      if(!el.fbo)
        {
          el.cam = scene->createChild_uptr<KawaiiCamera>();
          el.renderpass = scene->createChild_uptr<KawaiiRenderpass>();
          el.sceneLayer = el.renderpass->createChild_uptr<KawaiiSceneLayer>();
          el.renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth32, .needClear = true, .needStore = true });
          el.renderpass->addSubpass( KawaiiRenderpass::Subpass {
                                       .imgSrc = el.sceneLayer.get(),
                                       .depthGBuf = 0
                                     } );
          el.sceneLayer->setCamera(el.cam.get());
          el.sceneLayer->createProjection<KawaiiOrtho>();

          el.fbo = createChild_uptr<KawaiiFramebuffer>(glm::uvec2(resolution.width(), resolution.height()));
          el.fbo->attachTexture(0, dirShadowMap, i);
          el.fbo->setRenderpass(el.renderpass.get());

          el.shadowCenterStep = 0;
          el.matUpdateSupressed = false;

          connect(el.sceneLayer.get(), &KawaiiSceneLayer::viewProjectionMatChanged, this, [this, i] {
              setDirLightMat(i);
            });
        }
      updateDirLight(l, i);
    }
}

void KawaiiShadowMapper::spotLightsCountChanged(size_t n)
{
  spotShadowMap->setLayers(std::max<size_t>(n, 1));
  spotShadowRender.resize(n);
  for(size_t i = 0; i < spotShadowRender.size(); ++i)
    {
      auto &el = spotShadowRender[i];
      auto &l = lampLayout->getSpotLight(i);
      if(!el.fbo)
        {
          el.cam = scene->createChild_uptr<KawaiiCamera>();
          el.renderpass = scene->createChild_uptr<KawaiiRenderpass>();
          el.sceneLayer = el.renderpass->createChild_uptr<KawaiiSceneLayer>();
          el.renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth32, .needClear = true, .needStore = true });
          el.renderpass->addSubpass(KawaiiRenderpass::Subpass {
                                       .imgSrc = el.sceneLayer.get(),
                                       .depthGBuf = 0
                                     });
          el.sceneLayer->setCamera(el.cam.get());
          auto *projection = el.sceneLayer->getProjectionPtr();
          projection->setNearClip(nearClip);
          projection->setFarClip(farClip);

          el.fbo = createChild_uptr<KawaiiFramebuffer>(glm::uvec2(resolution.width(), resolution.height()));
          el.fbo->attachTexture(0, spotShadowMap, i);
          el.fbo->setRenderpass(el.renderpass.get());

          el.shadowCenterStep = 0;
          el.matUpdateSupressed = false;

          connect(el.sceneLayer.get(), &KawaiiSceneLayer::viewProjectionMatChanged, this, [this, i] {
              setSpotLightMat(i);
            });
        }
      updateSpotLight(l, i);
    }
}

void KawaiiShadowMapper::dotLightsCountChanged(size_t n)
{
  dotShadowMap->setLayers(std::max<size_t>(n, 1));
  dotShadowRender.resize(n);
  for(size_t i = 0; i < dotShadowRender.size(); ++i)
    {
      auto &el = dotShadowRender[i];
      auto &l = lampLayout->getDotLight(i);
      if(!el)
        {
          el = scene->createChild_uptr<KawaiiEnvMap>(glm::uvec2(resolution.width(), resolution.height()));
          el->attachTexture(KawaiiEnvMap::AttachmentMode::Depth, dotShadowMap, i);
          el->setNearClip(nearClip);
          el->setFarClip(farClip);

          connect(el.get(), &KawaiiEnvMap::projectionMatChanged, this, [this, i] {
              setDotLightMat(i);
            });
        }
      updateDotLight(l, i);
    }
}

void KawaiiShadowMapper::updateDotLight(KawaiiLamp &l, size_t i)
{
  const auto &el = dotShadowRender[i];
  el->setPosition(glm::vec3(l.position.x(), l.position.y(), l.position.z()));
  setDotLightMat(i);
}

void KawaiiShadowMapper::updateSpotLight(KawaiiProjector &l, size_t i)
{
  auto &el = spotShadowRender[i];
  const QQuaternion q = QQuaternion::rotationTo(QVector3D(0, 0, 1), l.direction);
  const QVector3D up = q.rotatedVector(QVector3D(0, 1, 0));
  const glm::vec3 pos(l.position.x(), l.position.y(), l.position.z());
  el.matUpdateSupressed = true;
  el.sceneLayer->getProjection<KawaiiPerspective>().setFovRadians(2.0 * l.spotCutoffRadians);
  el.cam->setViewMat(glm::lookAt(pos,
                                 pos + glm::vec3(l.direction.x(), l.direction.y(), l.direction.z()),
                                 glm::vec3(up.x(), up.y(), up.z())));
  el.matUpdateSupressed = false;
  setSpotLightMat(i);
}

namespace {
  glm::vec3 glm_vec3(const QVector3D &v)
  {
    return {v.x(), v.y(), v.z()};
  }

  void quantize(glm::vec3 &v, float step)
  {
    v /= step;
    v = { std::round(v.x), std::round(v.y), std::round(v.z) };
    v *= step;
  }

  glm::mat4 dirLightViewMat(KawaiiDirLight &l, glm::vec3 &&viewCenter, float distance, float step = 0)
  {
    const QQuaternion q = QQuaternion::rotationTo(QVector3D(0, 0, 1), l.direction);
    const QVector3D up = q.rotatedVector(QVector3D(0, 1, 0));
    const QVector3D dir = l.direction * distance;

    glm::vec3 eyePos = viewCenter - glm_vec3(dir);
    if(!qFuzzyIsNull(step))
      {
        quantize(eyePos, step);
        quantize(viewCenter, step);
      }

    return glm::lookAt(eyePos,
                       viewCenter,
                       glm::vec3(up.x(), up.y(), up.z()));
  }
}

void KawaiiShadowMapper::updateDirLight(KawaiiDirLight &l, size_t i)
{
  auto &r = dirShadowRender[i];
  auto &proj = r.sceneLayer->getProjection<KawaiiOrtho>();

  float maxX, minX,
      maxY, minY,
      maxZ, minZ;

  glm::mat4 lightView;
  auto computeMinMax = [&minX, &maxX, &minY, &maxY, &minZ, &maxZ] (const std::vector<glm::vec3> &tr_points) {
      minX = std::min_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.x < b.x; })->x;
      maxX = std::max_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.x < b.x; })->x;
      minY = std::min_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.y < b.y; })->y;
      maxY = std::max_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.y < b.y; })->y;
      minZ = std::min_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.z < b.z; })->z;
      maxZ = std::max_element(tr_points.cbegin(), tr_points.cend(),
                              [](const glm::vec3 &a, const glm::vec3 &b) { return a.z < b.z; })->z;
    };

  auto getAreaBBoxPoints = [this, &lightView] (const glm::vec3 &center, std::vector<glm::vec3> &out) {
      const auto points = {
        center + 0.5f * glm::vec3( dirLightsArea.x(),  dirLightsArea.y(),  dirLightsArea.z()),
        center + 0.5f * glm::vec3(-dirLightsArea.x(),  dirLightsArea.y(),  dirLightsArea.z()),
        center + 0.5f * glm::vec3( dirLightsArea.x(), -dirLightsArea.y(),  dirLightsArea.z()),
        center + 0.5f * glm::vec3(-dirLightsArea.x(), -dirLightsArea.y(),  dirLightsArea.z()),
        center + 0.5f * glm::vec3( dirLightsArea.x(),  dirLightsArea.y(), -dirLightsArea.z()),
        center + 0.5f * glm::vec3(-dirLightsArea.x(),  dirLightsArea.y(), -dirLightsArea.z()),
        center + 0.5f * glm::vec3( dirLightsArea.x(), -dirLightsArea.y(), -dirLightsArea.z()),
        center + 0.5f * glm::vec3(-dirLightsArea.x(), -dirLightsArea.y(), -dirLightsArea.z()),
      };
      for(const auto &p: points)
        {
          const auto tr_p = lightView * glm::vec4(p, 1);
          out.emplace_back(tr_p.x / tr_p.w,
                           tr_p.y / tr_p.w,
                           tr_p.z / tr_p.w);
        }
  };

  if(dirLightsAreaLayer && dirLightsAreaLayer->getCamera())
    lightView = dirLightViewMat(l, dirLightsAreaLayer->getCamera()->getViewCenter(), 1);
  else
    lightView = dirLightViewMat(l, glm::vec3(dirLightsCenter.x(), dirLightsCenter.y(), dirLightsCenter.z()), 1);

  if(getDirLightsAreaLayer())
    {
      const auto points = {
        glm::vec3(1,1,1), glm::vec3(-1,1,1),
        glm::vec3(1,-1,1), glm::vec3(-1,-1,1),
        glm::vec3(1,1,-1), glm::vec3(-1,1,-1),
        glm::vec3(1,-1,-1), glm::vec3(-1,-1,-1),
      };
      const auto mat = glm::inverse(dirLightsAreaLayer->getViewProjectionMatrix());

      std::vector<glm::vec3> tr_points;
      tr_points.reserve(2*points.size());
      for(const auto &p: points)
        {
          const auto tr_p = lightView * (mat * glm::vec4(p, 1));
          tr_points.emplace_back(tr_p.x / tr_p.w,
                                 tr_p.y / tr_p.w,
                                 tr_p.z / tr_p.w);
        }
      if(Q_LIKELY(dirLightsAreaLayer->getCamera()))
        getAreaBBoxPoints(dirLightsAreaLayer->getCamera()->getEyePos(), tr_points);
      computeMinMax(tr_points);
    } else
    {
      const glm::vec3 center(dirLightsCenter.x(), dirLightsCenter.y(), dirLightsCenter.z());
      std::vector<glm::vec3> tr_points;
      tr_points.reserve(8);
      getAreaBBoxPoints(center, tr_points);

      computeMinMax(tr_points);
    }

  const float frustumWidth = maxX - minX,
      frustumHeight = maxY - minY,
      frustumLength = maxZ - minZ;

  float x = 0.5*(minX + maxX);
  float y = 0.5*(minY + maxY);
  float z = 0.5*(minZ + maxZ);
  glm::vec4 frustumCenter = glm::vec4(x, y, z, 1.0f);

  const auto invertedLight = glm::inverse(lightView);
  frustumCenter = invertedLight * frustumCenter;

  const float currentStep = std::max(frustumWidth, frustumHeight) * resolutionInverted;
  r.shadowCenterStep = std::max(r.shadowCenterStep, currentStep);

  r.matUpdateSupressed = true;
  proj.set(-0.5*frustumWidth,
           +0.5*frustumWidth,
           -0.5*frustumHeight,
           +0.5*frustumHeight);

  r.cam->setViewMat(dirLightViewMat(l, glm::vec3(frustumCenter), 0.5*frustumLength, r.shadowCenterStep));
  r.matUpdateSupressed = false;
  setDirLightMat(i);
}

float KawaiiShadowMapper::getNearClip() const
{
  return nearClip;
}

void KawaiiShadowMapper::setNearClip(float value)
{
  if(!qFuzzyCompare(nearClip, value))
    {
      nearClip = value;
      for(const auto &i: dirShadowRender)
        i.sceneLayer->getProjectionPtr()->setNearClip(nearClip);
      for(const auto &i: spotShadowRender)
        i.sceneLayer->getProjectionPtr()->setNearClip(nearClip);
      for(const auto &i: dotShadowRender)
        i->setNearClip(nearClip);
    }
}

float KawaiiShadowMapper::getFarClip() const
{
  return farClip;
}

void KawaiiShadowMapper::setFarClip(float value)
{
  if(!qFuzzyCompare(farClip, value))
    {
      farClip = value;
      for(const auto &i: dirShadowRender)
        i.sceneLayer->getProjectionPtr()->setFarClip(farClip);
      for(const auto &i: spotShadowRender)
        i.sceneLayer->getProjectionPtr()->setFarClip(farClip);
      for(const auto &i: dotShadowRender)
        i->setFarClip(farClip);
    }
}

const QVector3D &KawaiiShadowMapper::getDirLightsCenter() const
{
  return dirLightsCenter;
}

void KawaiiShadowMapper::setDirLightsCenter(const QVector3D &newDirLightsCenter)
{
  if(qFuzzyCompare(dirLightsCenter, newDirLightsCenter))
    return;

  dirLightsCenter = newDirLightsCenter;
  if(!getDirLightsAreaLayer())
    for(size_t i = 0; i < dirShadowRender.size(); ++i)
      updateDirLight(lampLayout->getDirLight(i), i);
}

const QVector3D &KawaiiShadowMapper::getDirLightsArea() const
{
  return dirLightsArea;
}

void KawaiiShadowMapper::setDirLightsArea(const QVector3D &newDirLightsArea)
{
  if(qFuzzyCompare(dirLightsArea, newDirLightsArea))
    return;

  if(!getDirLightsAreaLayer())
    for(size_t i = 0; i < dirShadowRender.size(); ++i)
      updateDirLight(lampLayout->getDirLight(i), i);
}

KawaiiSceneLayer *KawaiiShadowMapper::getDirLightsAreaLayer() const
{
  return dirLightsAreaLayer;
}

void KawaiiShadowMapper::setDirLightsAreaLayer(KawaiiSceneLayer *newDirLightsAreaLayer)
{
  if(dirLightsAreaLayer == newDirLightsAreaLayer) return;

  if(dirLightsAreaLayer)
    disconnect(dirLightsAreaLayer, &KawaiiSceneLayer::viewProjectionMatChanged, this, &KawaiiShadowMapper::onDirLightsAreaLayerMatUpdate);
  dirLightsAreaLayer = newDirLightsAreaLayer;
  if(dirLightsAreaLayer)
    connect(dirLightsAreaLayer, &KawaiiSceneLayer::viewProjectionMatChanged, this, &KawaiiShadowMapper::onDirLightsAreaLayerMatUpdate);

  onDirLightsAreaLayerMatUpdate();
}

void KawaiiShadowMapper::setDirLightMat(size_t i)
{
  if(!dirShadowRender[i].matUpdateSupressed)
    {
      lampLayout->getDirLight(i).mat = bias_matrix * dirShadowRender[i].sceneLayer->getViewProjectionMatrix();
      lampLayout->updateDirLight(i, false);
    }
}

void KawaiiShadowMapper::setSpotLightMat(size_t i)
{
  if(!spotShadowRender[i].matUpdateSupressed)
    {
      lampLayout->getSpotLight(i).mat = bias_matrix * spotShadowRender[i].sceneLayer->getViewProjectionMatrix();
      lampLayout->updateSpotLight(i, false);
    }
}

void KawaiiShadowMapper::setDotLightMat(size_t i)
{
  auto &l = lampLayout->getDotLight(i);
  const auto &r = dotShadowRender[i];
  for(size_t j = 0; j < 6; ++j)
    l.mat[j] = r->getViewProjectionMat(j);
  lampLayout->updateDotLight(i, false);
}

void KawaiiShadowMapper::onDirLightsAreaLayerMatUpdate()
{
  for(size_t i = 0; i < dirShadowRender.size(); ++i)
    updateDirLight(lampLayout->getDirLight(i), i);
}

