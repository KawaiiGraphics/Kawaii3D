#ifndef KAWAIIILLUMINATION_HPP
#define KAWAIIILLUMINATION_HPP

#include "KawaiiLampsUbo.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiIllumination: public KawaiiDataUnit
{
  Q_OBJECT

public:
  KawaiiIllumination(const QString &illuminationName);
  ~KawaiiIllumination();

  KawaiiGpuBuf *createUBO(uint32_t bindingPoint);
  KawaiiGpuBuf *setGpuBuffer(KawaiiGpuBuf *buf, size_t lightArraysOffset, const QString &signature);

  template<typename T>
  void createLampArrays()
  {
    setLayout(createChild<KawaiiLampsUbo<T>>());
  }

  bool tryEnable();

  inline KawaiiLampsLayoutAbstract& getLampsLayout()
  { return *lampsLayout; }



  //IMPLEMENT
private:
  QString illuminationName;

  QString dirLightProperties;
  QString projectorProperties;
  QString lampProperties;

  QPointer<KawaiiShader> lampSignaturesFragModule;
  QPointer<KawaiiShader> fragBaseModule;

  KawaiiLampsLayoutAbstract *lampsLayout;

  void setLayout(KawaiiLampsLayoutAbstract *layout);
};

#endif // KAWAIIILLUMINATION_HPP
