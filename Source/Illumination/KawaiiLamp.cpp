#include "KawaiiLamp.hpp"

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiLamp &l)
{
  return st << l.position << l.constAttenuation << l.linearAttenuation << l.quadraticAttenuation << l.ambient;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiLamp &l)
{
  return st >> l.position >> l.constAttenuation >> l.linearAttenuation >> l.quadraticAttenuation >> l.ambient;
}

//==========================

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiDirLight &l)
{
  return st << l.direction << l.ambient;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiDirLight &l)
{
  return st >> l.direction >> l.ambient;
}

//==========================

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiProjector &l)
{
  return st << l.position << l.spotExponent
            << l.constAttenuation << l.linearAttenuation << l.quadraticAttenuation
            << l.spotCutoffRadians << l.ambient;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiProjector &l)
{
  return st >> l.position >> l.spotExponent
            >> l.constAttenuation >> l.linearAttenuation >> l.quadraticAttenuation
            >> l.spotCutoffRadians >> l.ambient;
}
