#ifndef KAWAIICEL_HPP
#define KAWAIICEL_HPP
#include <QVector3D>
#include <QDataStream>

///Fields, specific for Gouraud, Phong and Blinn-Phong illumination models
struct KawaiiCelLightBase
{
  QVector3D diffuse;
  float ignored0;

  QVector3D specular;
  float ignored1;

  inline static constexpr char shaderBufSignature[] = "vec3 diffuse;\n"
                                                      "vec3 specular;\n";
};

struct KawaiiCelLight: KawaiiCelLightBase
{
  inline static constexpr char shaderModuleName[] = "CelIlluminationCore";
};

inline QDataStream& operator<<(QDataStream &st, const KawaiiCelLightBase &l)
{ return st << l.diffuse << l.specular; }

inline QDataStream& operator>>(QDataStream &st, KawaiiCelLightBase &l)
{ return st >> l.diffuse >> l.specular; }
#endif // KAWAIICEL_HPP
