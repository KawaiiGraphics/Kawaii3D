#include "KawaiiLampsLayout.hpp"
#include "KawaiiShadowMapper.hpp"
#include "KawaiiBufBinding.hpp"
#include <math.h>

KawaiiLampsLayoutAbstract::KawaiiLampsLayoutAbstract():
  ownerShaderSignature("{};"),
  owner(nullptr),
  offset(0),
  origBufSz(0),
  shadowMapper(nullptr)
{
}

KawaiiLampsLayoutAbstract::~KawaiiLampsLayoutAbstract()
{
  auto _shadowMapper = shadowMapper;
  detachShadowMapper();
  if(_shadowMapper)
    delete _shadowMapper;
  detachBuf();
}

KawaiiGpuBuf *KawaiiLampsLayoutAbstract::createUBO(uint32_t bindingPoint)
{
  Q_ASSERT_X(bindingPoint > 0, "KawaiiLampsLayoutAbstract::createBuffer", "binding point must be greater than 0");

  detachBuf();

  auto buf = createChild<KawaiiGpuBuf>(nullptr, getDataSize());
  copyDataTo(buf->getData());
  owner = buf;
  offset = 0;
  origBufSz = 0;
  ownerShaderSignature = QStringLiteral("Global_%1\n"
                                        "{\n"
                                        "} global_%1;\n").arg(bindingPoint);

  buf->createChild<KawaiiBufBinding>()->setBindingPoint(bindingPoint);
  assignShadowmaps(buf);
  emit ownerChanged();
  return buf;
}

void KawaiiLampsLayoutAbstract::arrange(KawaiiGpuBuf *buf, size_t offset, const QString &shaderSignature)
{
  detachBuf();

  owner = buf;
  this->offset = offset;
  origBufSz = owner->getDataSize();
  ownerShaderSignature = shaderSignature;
  assignShadowmaps(buf);
  emit ownerChanged();
  repack();
}

void KawaiiLampsLayoutAbstract::addDirLight()
{
  addDirLightData();
  repack();
  emit dirLightsCountChanged(getDirLightCount());
}

void KawaiiLampsLayoutAbstract::addSpotLight()
{
  addSpotLightData();
  repack();
  emit spotLightsCountChanged(getSpotLightCount());
}

void KawaiiLampsLayoutAbstract::addDotLight()
{
  addDotLightData();
  repack();
  emit dotLightsCountChanged(getDotLightCount());
}

void KawaiiLampsLayoutAbstract::removeDirLight(size_t i)
{
  removeDirLightData(i);
  repack();
  emit dirLightsCountChanged(getDirLightCount());
}

void KawaiiLampsLayoutAbstract::removeSpotLight(size_t i)
{
  removeSpotLightData(i);
  repack();
  emit spotLightsCountChanged(getSpotLightCount());
}

void KawaiiLampsLayoutAbstract::removeDotLight(size_t i)
{
  removeDotLightData(i);
  repack();
  emit dotLightsCountChanged(getDotLightCount());
}

void KawaiiLampsLayoutAbstract::repack()
{
  if(owner)
    {
      size_t N = offset + getDataSize();
      if(owner->getDataSize() < N
         || origBufSz < owner->getDataSize())
        {
          owner->realloc(N);
        }

      copyDataTo(static_cast<char*>(owner->getData()) + offset);
      owner->dataChanged(offset, N-offset);
    }
}

void KawaiiLampsLayoutAbstract::detachBuf()
{
  if(owner)
    {
      if(origBufSz == 0)
        delete owner;
      else
        {
          owner->realloc(origBufSz);
          if(shadowMapper)
            {
              owner->unbindTexture(QStringLiteral("dirLightsShadowmap"));
              owner->unbindTexture(QStringLiteral("spotLightsShadowmap"));
              owner->unbindTexture(QStringLiteral("dotLightsShadowmap"));
            }
        }
    }
}

void KawaiiLampsLayoutAbstract::updateBufferBytes(const void *data, size_t offset, size_t count)
{
  if(owner)
    {
      memcpy(static_cast<char*>(owner->getData()) + this->offset + offset, data, count);
      owner->dataChanged(offset + this->offset, count);
    }
}

KawaiiShadowMapper *KawaiiLampsLayoutAbstract::getShadowMapper() const
{
  return shadowMapper;
}

KawaiiShadowMapper *KawaiiLampsLayoutAbstract::createShadowMapper(KawaiiScene *scene)
{
  auto shadowmapper = createChild<KawaiiShadowMapper>(this, scene);
  setShadowMapper(shadowmapper);
  return shadowmapper;
}

void KawaiiLampsLayoutAbstract::removeShadowMapper()
{
  setShadowMapper(nullptr);
}

void KawaiiLampsLayoutAbstract::setShadowMapper(KawaiiShadowMapper *value)
{
  if(shadowMapper != value)
    {
      if(shadowMapper)
        delete shadowMapper;
      shadowMapper = value;
      if(shadowMapper)
        {
          connect(this, &KawaiiLampsLayoutAbstract::dirLightsCountChanged, shadowMapper, &KawaiiShadowMapper::dirLightsCountChanged);
          connect(this, &KawaiiLampsLayoutAbstract::dotLightsCountChanged, shadowMapper, &KawaiiShadowMapper::dotLightsCountChanged);
          connect(this, &KawaiiLampsLayoutAbstract::spotLightsCountChanged, shadowMapper, &KawaiiShadowMapper::spotLightsCountChanged);
          onShadowMapperDestroyed = connect(shadowMapper, &QObject::destroyed, this, [this] {
              detachShadowMapper();
              emit shadowMapperChanged(shadowMapper);
            });
        }
      if(owner)
        assignShadowmaps(owner);
      emit shadowMapperChanged(shadowMapper);
    }
}

void KawaiiLampsLayoutAbstract::detachShadowMapper()
{
  if(shadowMapper)
    {
      disconnect(this, &KawaiiLampsLayoutAbstract::dirLightsCountChanged, shadowMapper, &KawaiiShadowMapper::dirLightsCountChanged);
      disconnect(this, &KawaiiLampsLayoutAbstract::dotLightsCountChanged, shadowMapper, &KawaiiShadowMapper::dotLightsCountChanged);
      disconnect(this, &KawaiiLampsLayoutAbstract::spotLightsCountChanged, shadowMapper, &KawaiiShadowMapper::spotLightsCountChanged);
      disconnect(onShadowMapperDestroyed);
      shadowMapper = nullptr;
      if(owner)
        assignShadowmaps(owner);
    }
}

void KawaiiLampsLayoutAbstract::assignShadowmaps(KawaiiGpuBuf *buf)
{
  if(shadowMapper)
    {
      buf->bindTexture(QStringLiteral("dirLightsShadowmap"), shadowMapper->getDirShadowMap());
      buf->bindTexture(QStringLiteral("spotLightsShadowmap"), shadowMapper->getSpotShadowMap());
      buf->bindTexture(QStringLiteral("dotLightsShadowmap"), shadowMapper->getDotShadowMap());
    } else
    {
      owner->unbindTexture(QStringLiteral("dirLightsShadowmap"));
      owner->unbindTexture(QStringLiteral("spotLightsShadowmap"));
      owner->unbindTexture(QStringLiteral("dotLightsShadowmap"));
    }
}
