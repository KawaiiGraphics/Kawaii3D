#ifndef KAWAIILAMPSUBO_HPP
#define KAWAIILAMPSUBO_HPP
#include "KawaiiLampsLayout.hpp"
#include "KawaiiShadowMapper.hpp"
#include <vector>

template<typename T>
class KawaiiLampsUbo: public KawaiiLampsLayoutAbstract
{
  using lamps = KawaiiIlluminationStructs<T>;

  using DirLight = typename lamps::DirLight;
  using Projector = typename lamps::Projector;
  using Lamp = typename lamps::Lamp;

  inline static const QString shaderCoreName = sib_utils::strings::getQStr(T::shaderModuleName);
  inline static const QString shaderLampProperies = sib_utils::strings::getQStr(T::shaderBufSignature);

  inline static const QString dirLightSignature = sib_utils::strings::getQStr(DirLight::shaderBufSignature);
  inline static const QString projectorSignature = sib_utils::strings::getQStr(Projector::shaderBufSignature);
  inline static const QString lampSignature = sib_utils::strings::getQStr(Lamp::shaderBufSignature);

  std::vector<DirLight> dirLights;
  std::vector<Projector> spotLights;
  std::vector<Lamp> dotLights;

  inline size_t getDirSz() const
  { return sizeof(DirLight) * dirLights.size(); }

  inline size_t getSpotSz() const
  { return sizeof(Projector) * spotLights.size(); }

  inline size_t getDotSz() const
  { return sizeof(Lamp) * dotLights.size(); }



  // KawaiiLampsLayoutAbstract interface
private:
  size_t getDataSize() const override final
  {
    return getDirSz() + getSpotSz() + getDotSz();
  }

  void copyDataTo(void *dest) const override final
  {
    char *buf = static_cast<char*>(dest);
    size_t n = getDirSz();
    memcpy(buf, dirLights.data(), n);
    buf += n;

    n = getSpotSz();
    memcpy(buf, spotLights.data(), n);
    buf += n;

    n = getDotSz();
    memcpy(buf, dotLights.data(), n);
  }

public:
  KawaiiShader* createShaderLampLayoutModule(KawaiiShaderType type) override final
  {
    KawaiiShader *result = createChild<KawaiiShader>(type);
    result->setParameter(QStringLiteral("DIR_LIGHTS_BUF"), dirLightSignature);
    result->setParameter(QStringLiteral("SPOT_LIGHTS_BUF"), projectorSignature);
    result->setParameter(QStringLiteral("DOT_LIGHTS_BUF"), lampSignature);
    result->setParameter(QStringLiteral("SHADOW_MAPPING"), getShadowMapper()? QStringLiteral("1"): QStringLiteral("0"));

    auto dirLRelocated_f = std::bind(&KawaiiShader::setVarParameter, result, QStringLiteral("DIR_LIGHTS"), std::placeholders::_1);

    auto spotLRelocated_f = std::bind(&KawaiiShader::setVarParameter, result, QStringLiteral("SPOT_LIGHTS"), std::placeholders::_1);

    auto dotLRelocated_f = std::bind(&KawaiiShader::setVarParameter, result, QStringLiteral("DOT_LIGHTS"), std::placeholders::_1);

    auto bufChanged = [this, result] {
        int i = ownerShaderSignature.lastIndexOf('}');
        const QString bufPrefix = ownerShaderSignature.left(i);
        QString bufPostfix = QStringView(ownerShaderSignature.constData() + i+1, ownerShaderSignature.length()-i-1).toString();
        if(!bufPostfix.isEmpty())
          {
            int tailSz = 0;
            while(tailSz < bufPostfix.size()
                  && (bufPostfix[bufPostfix.length()-1-tailSz]=='\n'
                      || bufPostfix[bufPostfix.length()-1-tailSz]==';'))
              ++tailSz;
            bufPostfix.chop(tailSz);
          }
        result->setParameter(QStringLiteral("LIGHT_BUFFER_PREFIX"), bufPrefix);
        result->setParameter(QStringLiteral("LIGHT_BUFFER_POSTFIX"), bufPostfix);
      };

    auto shadowmapperChanged_f = [result] (KawaiiShadowMapper *shadowmapper) {
      result->setParameter(QStringLiteral("SHADOW_MAPPING"), shadowmapper? QStringLiteral("1"): QStringLiteral("0"));
    };

    dirLRelocated_f(static_cast<quint64>(dirLights.size()));
    spotLRelocated_f(static_cast<quint64>(spotLights.size()));
    dotLRelocated_f(static_cast<quint64>(dotLights.size()));
    bufChanged();
    result->readFrom(QFile(":/Kawaii_illumination/LampSignatures.glsl"));

    QMetaObject::Connection onDirLRelocated = connect(this, &KawaiiLampsLayoutAbstract::dirLightsCountChanged, result, dirLRelocated_f);
    QMetaObject::Connection onSpotLRelocated = connect(this, &KawaiiLampsLayoutAbstract::spotLightsCountChanged, result, spotLRelocated_f);
    QMetaObject::Connection onDotLRelocated = connect(this, &KawaiiLampsLayoutAbstract::dotLightsCountChanged, result, dotLRelocated_f);
    QMetaObject::Connection onBufChanged = connect(this, &KawaiiLampsLayoutAbstract::ownerChanged, result, bufChanged);
    QMetaObject::Connection onShadowmapperChanged = connect(this, &KawaiiLampsLayoutAbstract::shadowMapperChanged, result, shadowmapperChanged_f);

    return result;
  }

  inline QString getShaderCoreName() const override final
  { return shaderCoreName; }

  inline QString getShaderLampProperties() const override final
  { return shaderLampProperies; }

private:
  void addDirLightData() override final
  {
    dirLights.push_back(DirLight());
  }

  void addSpotLightData() override final
  {
    spotLights.push_back(Projector());
  }

  void addDotLightData() override final
  {
    dotLights.push_back(Lamp());
  }

  void removeDirLightData(size_t i) override final
  {
    dirLights.erase(dirLights.cbegin() + i);
  }

  void removeSpotLightData(size_t i) override final
  {
    spotLights.erase(spotLights.cbegin() + i);
  }

  void removeDotLightData(size_t i) override final
  {
    dotLights.erase(dotLights.cbegin() + i);
  }

public:
  size_t getDirLightCount() const override final { return dirLights.size(); }
  size_t getSpotLightCount() const override final { return spotLights.size(); }
  size_t getDotLightCount() const override final { return dotLights.size(); }

  KawaiiDirLight &getDirLight(size_t i) override final { return dirLights[i]; }
  const KawaiiDirLight &getDirLight(size_t i) const override final { return dirLights[i]; }

  KawaiiProjector &getSpotLight(size_t i) override final { return spotLights[i]; }
  const KawaiiProjector &getSpotLight(size_t i) const override final { return spotLights[i]; }

  KawaiiLamp &getDotLight(size_t i) override final { return dotLights[i]; }
  const KawaiiLamp &getDotLight(size_t i) const override final { return dotLights[i]; }

  void updateDirLight(size_t i, bool updateShadowmapper = true) override final
  {
    if(updateShadowmapper && getShadowMapper())
      getShadowMapper()->updateDirLight(dirLights[i], i);
    updateBufferBytes(dirLights.data() + i, i * sizeof(DirLight), sizeof(DirLight));
  }

  void updateSpotLight(size_t i, bool updateShadowmapper = true) override final
  {
    if(updateShadowmapper && getShadowMapper())
      getShadowMapper()->updateSpotLight(spotLights[i], i);
    updateBufferBytes(spotLights.data() + i, getDirSz() + i * sizeof(Projector), sizeof(Projector));
  }

  void updateDotLight(size_t i, bool updateShadowmapper = true) override final
  {
    if(updateShadowmapper && getShadowMapper())
      getShadowMapper()->updateDotLight(dotLights[i], i);
    updateBufferBytes(dotLights.data() + i, getDirSz() + getSpotSz() + i * sizeof(Lamp), sizeof(Lamp));
  }
};

#endif // KAWAIILAMPSUBO_HPP
