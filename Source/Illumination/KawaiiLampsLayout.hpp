#ifndef KAWAIILAMPSLAYOUT_HPP
#define KAWAIILAMPSLAYOUT_HPP

#include "KawaiiLamp.hpp"
#include "../KawaiiGpuBuf.hpp"
#include "../Shaders/KawaiiShader.hpp"

#include <QPointer>
#include <QFile>

class KawaiiShadowMapper;
class KawaiiScene;

class KAWAII3D_SHARED_EXPORT KawaiiLampsLayoutAbstract: public KawaiiDataUnit
{
  Q_OBJECT

public:
  KawaiiLampsLayoutAbstract();
  virtual ~KawaiiLampsLayoutAbstract();

  KawaiiGpuBuf* createUBO(uint32_t bindingPoint);
  void arrange(KawaiiGpuBuf *buf, size_t offset, const QString &shaderSignature);

  virtual KawaiiShader* createShaderLampLayoutModule(KawaiiShaderType type) = 0;

  virtual QString getShaderCoreName() const = 0;

  virtual QString getShaderLampProperties() const = 0;

  virtual size_t getDirLightCount() const = 0;
  virtual size_t getSpotLightCount() const = 0;
  virtual size_t getDotLightCount() const = 0;

  virtual KawaiiDirLight& getDirLight(size_t i) = 0;
  virtual const KawaiiDirLight& getDirLight(size_t i) const = 0;

  virtual KawaiiProjector& getSpotLight(size_t i) = 0;
  virtual const KawaiiProjector& getSpotLight(size_t i) const = 0;

  virtual KawaiiLamp& getDotLight(size_t i) = 0;
  virtual const KawaiiLamp& getDotLight(size_t i) const = 0;

  virtual void updateDirLight(size_t i, bool updateShadowmapper = true) = 0;
  virtual void updateSpotLight(size_t i, bool updateShadowmapper = true) = 0;
  virtual void updateDotLight(size_t i, bool updateShadowmapper = true) = 0;

  void addDirLight();
  void addSpotLight();
  void addDotLight();

  void removeDirLight(size_t i);
  void removeSpotLight(size_t i);
  void removeDotLight(size_t i);

  KawaiiShadowMapper *getShadowMapper() const;
  KawaiiShadowMapper *createShadowMapper(KawaiiScene *scene);
  void removeShadowMapper();

private:
  virtual size_t getDataSize() const = 0;
  virtual void copyDataTo(void *dest) const = 0;

  virtual void addDirLightData() = 0;
  virtual void addSpotLightData() = 0;
  virtual void addDotLightData() = 0;

  virtual void removeDirLightData(size_t i) = 0;
  virtual void removeSpotLightData(size_t i) = 0;
  virtual void removeDotLightData(size_t i) = 0;

  void repack();
  void detachBuf();

protected:
  void updateBufferBytes(const void *data, size_t offset, size_t count);

signals:
  void ownerChanged();

  void dirLightsCountChanged(quint64 n);
  void spotLightsCountChanged(quint64 n);
  void dotLightsCountChanged(quint64 n);

  void shadowMapperChanged(KawaiiShadowMapper *shadowMapper);



  //IMPLEMENT
protected:
  QString ownerShaderSignature;

private:
  QPointer<KawaiiGpuBuf> owner;
  size_t offset;
  size_t origBufSz;
  KawaiiShadowMapper *shadowMapper;
  QMetaObject::Connection onShadowMapperDestroyed;

  void setShadowMapper(KawaiiShadowMapper *value);
  void detachShadowMapper();
  void assignShadowmaps(KawaiiGpuBuf *buf);
};

#endif // KAWAIILAMPSLAYOUT_HPP
