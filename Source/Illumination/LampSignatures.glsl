module LampSignatures_${illumination_name};
precision mediump int; precision highp float;

struct DirLight
{
  ${DIR_LIGHTS_BUF}
};

struct SpotLight
{
  ${SPOT_LIGHTS_BUF}
};

struct DotLight
{
  ${DOT_LIGHTS_BUF}
};

# if (${DIR_LIGHTS} > 0 || ${SPOT_LIGHTS} > 0 || ${DOT_LIGHTS} > 0)

${LIGHT_BUFFER_PREFIX}
  sampler2DArrayShadow dirLightsShadowmap;
  sampler2DArrayShadow spotLightsShadowmap;
  samplerCubeArrayShadow dotLightsShadowmap;
# if (${DIR_LIGHTS} > 0)
  DirLight dirLights[${DIR_LIGHTS}];
# endif
# if (${SPOT_LIGHTS} > 0)
  SpotLight spotLights[${SPOT_LIGHTS}];
# endif
# if (${DOT_LIGHTS} > 0)
  DotLight dotLights[${DOT_LIGHTS}];
# endif
} ${LIGHT_BUFFER_POSTFIX};

# endif


int getDirLightCount() { return ${DIR_LIGHTS}; }
int getSpotLightCount() { return ${SPOT_LIGHTS}; }
int getDotLightCount() { return ${DOT_LIGHTS}; }

DirLight getDirLight(in int i)
{
#if (${DIR_LIGHTS} > 0)
    return ${LIGHT_BUFFER_POSTFIX}.dirLights[i];
#else
    DirLight result;
    return result;
#endif
}

SpotLight getSpotLight(in int i)
{
#if (${SPOT_LIGHTS} > 0)
    return ${LIGHT_BUFFER_POSTFIX}.spotLights[i];
#else
    SpotLight result;
    return result;
#endif
}

DotLight getDotLight(in int i)
{
#if (${DOT_LIGHTS} > 0)
    return ${LIGHT_BUFFER_POSTFIX}.dotLights[i];
#else
    DotLight result;
    return result;
#endif
}

vec2 poissonDisk[16] = vec2[](
  vec2(-0.94201624,  -0.39906216),
  vec2( 0.94558609,  -0.76890725),
  vec2(-0.094184101, -0.92938870),
  vec2( 0.34495938,   0.29387760),
  vec2(-0.91588581,   0.45771432),
  vec2(-0.81544232,  -0.87912464),
  vec2(-0.38277543,   0.27676845),
  vec2( 0.97484398,   0.75648379),
  vec2( 0.44323325,  -0.97511554),
  vec2( 0.53742981,  -0.47373420),
  vec2(-0.26496911,  -0.41893023),
  vec2( 0.79197514,   0.19090188),
  vec2(-0.24188840,   0.99706507),
  vec2(-0.81409955,   0.91437590),
  vec2( 0.19984126,   0.78641367),
  vec2( 0.14383161,  -0.14100790)
);

// Returns a random number based on a vec3 and an int.
float random(vec3 seed, int i){
    vec4 seed4 = vec4(seed,i);
    float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
    return fract(sin(dot_product) * 43758.5453);
}

#define ShadowedValue 0.0
#define zBias 0.005

float getDirLightShadow(in int i, in vec3 fragPosWorld, in float cosTheta)
{
# if (${DIR_LIGHTS} > 0 && ${SHADOW_MAPPING} == 1)
    vec4 shadowCoord = ${LIGHT_BUFFER_POSTFIX}.dirLights[i].mat * vec4(fragPosWorld, 1.0);
    shadowCoord.xyz /= shadowCoord.w;
#ifndef VULKAN
        shadowCoord.z = 0.5 * shadowCoord.z + 0.5;
#endif
    if(shadowCoord.x < 0 || shadowCoord.x > 1 || shadowCoord.y < 0 || shadowCoord.y > 1
        || shadowCoord.z > 1.0)
        return 1.0;
    else
    {
        float bias = zBias * tan(acos(cosTheta));
        bias = clamp(bias, 0, 0.01);
        shadowCoord.z -= bias;
        shadowCoord.w = float(i);

        float visibility = ShadowedValue + 0.25*(1.0 - ShadowedValue)*texture(dirLightsShadowmap, shadowCoord.xywz);
        for(int j = 0; j < 3; ++j)
        {
            int index   = int(16.0*random(floor(fragPosWorld*1000.0), j))%16;
            vec2 texC   = shadowCoord.xy + poissonDisk[index]/2750.0;
            visibility += 0.25 * (1.0 - ShadowedValue) * texture(dirLightsShadowmap, vec4(texC, shadowCoord.wz));
        }

        return visibility;
    }
#else
    return 1.0;
#endif
}

float getSpotLightShadow(in int i, in vec3 fragPosWorld)
{
# if (${SPOT_LIGHTS} > 0 && ${SHADOW_MAPPING} == 1)
    vec4 shadowCoord = ${LIGHT_BUFFER_POSTFIX}.spotLights[i].mat * vec4(fragPosWorld, 1.0);
    shadowCoord.z -= zBias;
    shadowCoord.xyz /= shadowCoord.w;
#ifndef VULKAN
        shadowCoord.z = 0.5 * shadowCoord.z + 0.5;
#endif
    if(shadowCoord.z > 1.0)
        return 1.0;
    else
    {
        shadowCoord.w = float(i);
        return ShadowedValue + (1.0 - ShadowedValue) * texture(spotLightsShadowmap, shadowCoord.xywz);
    }
#else
    return 1.0;
#endif
}

#if (${DOT_LIGHTS} > 0 && ${SHADOW_MAPPING} == 1)
int getCubeside(in vec3 v)
{
    vec3 abs_v = abs(v);
    if(abs_v.x >= abs_v.y)
    {
        if(abs_v.x >= abs_v.z)
            return v.x >= 0? 0: 1;
        else
            return v.z >= 0? 4: 5;
    } else
    {
        if(abs_v.y >= abs_v.z)
            return v.y >= 0? 2: 3;
        else
            return v.z >= 0? 4: 5;
    }
}
#endif

float getDotLightShadow(in int i, in vec3 fragPosWorld)
{
# if (${DOT_LIGHTS} > 0 && ${SHADOW_MAPPING} == 1)
    vec4 fragToLight = vec4(fragPosWorld - ${LIGHT_BUFFER_POSTFIX}.dotLights[i].pos, 0.0);
    int j = getCubeside(fragToLight.xyz);

    vec4 v = ${LIGHT_BUFFER_POSTFIX}.dotLights[i].mat[j] * vec4(fragPosWorld, 1.0);
    v.z -= zBias;
    v.z /= v.w;
#ifndef VULKAN
        v.z = 0.5 * v.z + 0.5;
#endif
    if(v.z > 1.0)
        return 1.0;
    else
    {
        fragToLight.w = float(i);
        return ShadowedValue + (1.0 - ShadowedValue) * texture(dotLightsShadowmap, fragToLight, v.z);
    }
#else
    return 1.0;
#endif
}

export DirLight, SpotLight, DotLight;
export getDirLightCount, getSpotLightCount, getDotLightCount;
export getDirLight, getSpotLight, getDotLight;
export getDirLightShadow, getSpotLightShadow, getDotLightShadow;
