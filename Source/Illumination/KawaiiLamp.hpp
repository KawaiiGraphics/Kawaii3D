#ifndef KAWAIILAMP_HPP
#define KAWAIILAMP_HPP

#include "../Kawaii3D_global.hpp"

#include <QVector3D>
#include <QVector4D>
#include <glm/mat4x4.hpp>
#include <QDataStream>
#include <sib_utils/Strings.hpp>

///Dot light source
struct KawaiiLamp
{
  ///Light source matrix for rendering shadowmaps
  glm::mat4 mat[6];

  ///Position of lamp in world space
  QVector3D position;

  ///Param just for packing when sending to GPU
  float ignored0;

  float constAttenuation;
  float linearAttenuation;
  float quadraticAttenuation;

  ///Param just for packing when sending to GPU
  float ignored1;

  ///Ambient color of lamp
  QVector3D ambient;

  ///Param just for packing when sending to GPU
  float ignored2;

  inline static constexpr char shaderBufSignature[] = "mat4 mat[6];\nvec3 pos;\nvec3 attenuation;\nvec3 ambient;\n";
};



///Directional light source
struct KawaiiDirLight
{
  ///Light source matrix for rendering shadowmaps
  glm::mat4 mat;

  ///Direction of light
  QVector3D direction;

  ///Param just for packing when sending to GPU
  float ignored0;

  ///Ambient color of light source
  QVector3D ambient;

  ///Param just for packing when sending to GPU
  float ignored1;

  inline static constexpr char shaderBufSignature[] = "mat4 mat;\nvec3 direction;\nvec3 ambient;\n";
};



///Spot light source
struct KawaiiProjector
{
  ///Light source matrix for rendering shadowmaps
  glm::mat4 mat;

  ///Position of lamp in world space
  QVector3D position;

  ///Param just for packing when sending to GPU
  float ignored0;

  ///Direction of light
  QVector3D direction;

  float spotExponent;

  float constAttenuation;
  float linearAttenuation;
  float quadraticAttenuation;

  ///angle of projector (in radians)
  float spotCutoffRadians;

  ///Ambient color of projector
  QVector3D ambient;

  ///Param just for packing when sending to GPU
  float ignored1;

  inline static constexpr char shaderBufSignature[] = "mat4 mat;\nvec3 pos;\nvec4 direction;\nvec4 attenuation;\nvec3 ambient;\n";
};

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiLamp &l);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiLamp &l);

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiDirLight &l);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiDirLight &l);

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiProjector &l);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiProjector &l);

template<typename T>
class KawaiiIlluminationStructs
{
  KawaiiIlluminationStructs() = delete;
public:
  struct Lamp: public KawaiiLamp, public T
  {
    inline static constexpr auto shaderBufSignature = sib_utils::strings::concat(KawaiiLamp::shaderBufSignature, T::shaderBufSignature);
  };

  struct DirLight: public KawaiiDirLight, public T
  {
    inline static constexpr auto shaderBufSignature = sib_utils::strings::concat(KawaiiDirLight::shaderBufSignature, T::shaderBufSignature);
  };

  struct Projector: public KawaiiProjector, public T
  {
    inline static constexpr auto shaderBufSignature = sib_utils::strings::concat(KawaiiProjector::shaderBufSignature, T::shaderBufSignature);
  };
};

#endif // KAWAIILAMP_HPP
