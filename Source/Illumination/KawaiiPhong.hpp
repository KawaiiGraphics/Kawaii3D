#ifndef KAWAIIPHONG_HPP
#define KAWAIIPHONG_HPP
#include <QVector3D>
#include <QDataStream>

///Fields, specific for Gouraud, Phong and Blinn-Phong illumination models
struct KawaiiPhongLightBase
{
  QVector3D diffuse;
  float ignored0;

  QVector3D specular;
  float ignored1;

  inline static constexpr char shaderBufSignature[] = "vec3 diffuse;\n"
                                                      "vec3 specular;\n";
};

struct KawaiiPhongLight: KawaiiPhongLightBase
{
  inline static constexpr char shaderModuleName[] = "PhongIlluminationCore";
};

inline QDataStream& operator<<(QDataStream &st, const KawaiiPhongLightBase &l)
{ return st << l.diffuse << l.specular; }

inline QDataStream& operator>>(QDataStream &st, KawaiiPhongLightBase &l)
{ return st >> l.diffuse >> l.specular; }

#endif // KAWAIIPHONG_HPP
