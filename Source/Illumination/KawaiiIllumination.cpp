#include "KawaiiIllumination.hpp"
#include "KawaiiBufBinding.hpp"
#include <QRegularExpression>
#include <QVector>
#include <QFile>

namespace {
  QString getLampProperties(const QString &lampPrefix, const QStringList &properties)
  {
    QString result;
    for(auto &str: properties)
      {
        QVector<QStringView> terms = QStringView(str).trimmed().split(QRegularExpression("\\s+"));
        if(!terms.back().isEmpty())
          {
            QString prop = terms.back().toString();
            result += QStringLiteral(", %1(i).%2").arg(lampPrefix, prop);
          }
      }
    result.remove(0, 2);
    return result;
  }
}

// Shader modules:
//   lamps signatures (lamps layout),
//   frag model (light model),
//   vert model (light model),
//   frag base (illumination)

KawaiiIllumination::KawaiiIllumination(const QString &illuminationName):
  illuminationName(illuminationName),
  lampsLayout(nullptr)
{
  setObjectName(illuminationName);
}

KawaiiIllumination::~KawaiiIllumination()
{
  if(lampSignaturesFragModule)
    {
      auto d = lampSignaturesFragModule.data();
      lampSignaturesFragModule.clear();
      delete d;
    }
  if(fragBaseModule)
    {
      auto d = fragBaseModule.data();
      fragBaseModule.clear();
      delete d;
    }
  if(lampsLayout)
    delete lampsLayout;
}

KawaiiGpuBuf *KawaiiIllumination::createUBO(uint32_t bindingPoint)
{
  if(!lampsLayout)
    return nullptr;

  return lampsLayout->createUBO(bindingPoint);
}

KawaiiGpuBuf *KawaiiIllumination::setGpuBuffer(KawaiiGpuBuf *buf, size_t lightArraysOffset, const QString &signature)
{
  if(!lampsLayout)
    return nullptr;

  lampsLayout->arrange(buf, lightArraysOffset, signature);
  return buf;
}

bool KawaiiIllumination::tryEnable()
{
  if(!lampsLayout)
    return false;

  if(!fragBaseModule)
    fragBaseModule = createChild<KawaiiShader>(KawaiiShaderType::Fragment);

  fragBaseModule->setParameter("illumination_name", illuminationName);
  fragBaseModule->setParameter("dir_i_properties", dirLightProperties);
  fragBaseModule->setParameter("spot_i_properties", projectorProperties);
  fragBaseModule->setParameter("dot_i_properties", lampProperties);
  fragBaseModule->setParameter("CORE_MODULE", lampsLayout->getShaderCoreName());

  fragBaseModule->readFrom(QFile(":/Kawaii_illumination/Illumination.glsl"));

  return lampSignaturesFragModule;
}

void KawaiiIllumination::setLayout(KawaiiLampsLayoutAbstract *layout)
{
  if(lampsLayout)
    delete lampsLayout;

  lampsLayout = layout;

  if(lampSignaturesFragModule)
    {
      auto d = lampSignaturesFragModule.data();
      lampSignaturesFragModule.clear();
      delete d;
    }

  lampSignaturesFragModule = layout->createShaderLampLayoutModule(KawaiiShaderType::Fragment);
  lampSignaturesFragModule->setParameter(QStringLiteral("illumination_name"), illuminationName);

  const QStringList lightModelProperties = layout->getShaderLampProperties().split(';');
  dirLightProperties = getLampProperties("getDirLight", lightModelProperties);
  lampProperties = getLampProperties("getDotLight", lightModelProperties);
  projectorProperties = getLampProperties("getSpotLight", lightModelProperties);
}
