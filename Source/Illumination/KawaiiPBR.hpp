#ifndef KAWAIIPBR_HPP
#define KAWAIIPBR_HPP
#include <QVector3D>
#include <QDataStream>

///Fields, specific for PBR illumination model
struct KawaiiPbrLightBase
{
  QVector3D color;
  float ignored;

  inline static constexpr char shaderBufSignature[] = "vec3 color;\n";
};

struct KawaiiPbrLight: KawaiiPbrLightBase
{
  inline static constexpr char shaderModuleName[] = "PbrIlluminationCore";
};

inline QDataStream& operator<<(QDataStream &st, const KawaiiPbrLightBase &l)
{ return st << l.color; }

inline QDataStream& operator>>(QDataStream &st, KawaiiPbrLightBase &l)
{ return st >> l.color; }

#endif // KAWAIIPBR_HPP
