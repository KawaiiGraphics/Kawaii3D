module ${illumination_name}_illumination;
precision mediump int; precision highp float;

import getLo, getAmbient from ${CORE_MODULE};
import DirLight, SpotLight, DotLight from LampSignatures_${illumination_name};
import getDirLightCount, getSpotLightCount, getDotLightCount from LampSignatures_${illumination_name};
import getDirLight, getSpotLight, getDotLight from LampSignatures_${illumination_name};
import getDirLightShadow, getSpotLightShadow, getDotLightShadow from LampSignatures_${illumination_name};

vec3 get_illumination(in vec3 normal_frag, in vec3 fragPosWorld, in vec3 eyePos)
{
  vec3 Lo = vec3(0.0);
  vec3 ambient = vec3(0.0);
  vec3 toCamera = normalize(eyePos - fragPosWorld);
  vec3 N = normalize(normal_frag);
  if(!gl_FrontFacing) N *= -1.0;

  for(int i = 0; i < getDirLightCount(); ++i)
  {
    ambient += getDirLight(i).ambient;
    vec3 lightDir = -normalize(getDirLight(i).direction);
    Lo += getLo(N, lightDir, toCamera, ${dir_i_properties})
            * getDirLightShadow(i, fragPosWorld, clamp(dot(N, lightDir), 0, 1));
  }

  for(int i = 0; i < getSpotLightCount(); ++i)
  {
    vec3 lightDir = getSpotLight(i).pos - fragPosWorld;
    float distance = length(lightDir);

    lightDir /= distance;

    vec4 attenuation = getSpotLight(i).attenuation;
    attenuation.zy *= distance;
    attenuation.z  *= distance;
    float att = 1.0 / ( attenuation.z + attenuation.y + attenuation.x );

    float spot_cutoff = cos(attenuation.w);
    vec4 spot = getSpotLight(i).direction;
    float spot_exponent = spot.w;

    vec3 spot_direction = spot.xyz;
    float spot_effect = dot(normalize(spot_direction), -lightDir);
    if(spot_effect < spot_cutoff)
      continue;
    spot_effect = max(pow(spot_effect, spot_exponent), 0.0);
    att *= spot_effect;

    ambient += max(att * getSpotLight(i).ambient, vec3(0));
    Lo += max(att * getLo(N, lightDir, toCamera, ${spot_i_properties}) * getSpotLightShadow(i, fragPosWorld),
              vec3(0));
  }

  for(int i = 0; i < getDotLightCount(); ++i)
  {
    vec3 lightDir = getDotLight(i).pos - fragPosWorld;
    float distance = length(lightDir);

    lightDir /= distance;

    vec3 attenuation = getDotLight(i).attenuation;
    attenuation.zy *= distance;
    attenuation.z  *= distance;
    float att = 1.0 / ( attenuation.z + attenuation.y + attenuation.x );

    ambient += att * getDotLight(i).ambient;
    Lo += att * getLo(N, lightDir, toCamera, ${dot_i_properties}) * getDotLightShadow(i, fragPosWorld);
  }

  return Lo + getAmbient(N, toCamera, ambient);
}

export get_illumination;

