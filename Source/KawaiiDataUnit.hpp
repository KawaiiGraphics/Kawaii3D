#ifndef KAWAIIDATAUNIT_HPP
#define KAWAIIDATAUNIT_HPP

#include <list>
#include <QHash>
#include <QFuture>
#include <QPointer>
#include <type_traits>
#include <variant>

#include <sib_utils/TreeNode.hpp>
#include <sib_utils/Memento/Factory.hpp>

#include "KawaiiRendererImpl.hpp"

#define KAWAII_UNIT_DEF(Class) static const TypeRegistrator<Class> reg;\
  inline static constexpr char nodeType[] = #Class;

#define KAWAII_COMMON_IMPLEMENT(field) decltype(field) field;


class KAWAII3D_SHARED_EXPORT KawaiiDataUnit : public sib_utils::TreeNode
{
  Q_OBJECT
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
  friend class KawaiiImplFactory;

public:
  class LoadhookDescr;

  KawaiiDataUnit();
  virtual ~KawaiiDataUnit();

  KawaiiDataUnit* loadChild(sib_utils::memento::Memento &memento);

  inline static const sib_utils::memento::Factory& getMementoFactory()
  { return mementoFactory; }

  KawaiiRendererImpl* getRendererImpl() const;
  void removeRendererImplRecursive();

  bool loadAsset(const QString &str);
  QFuture<bool> loadAssetP(const QString &str, QThreadPool *threadPool = nullptr);

  bool loadAsset(const QString &str, const QMatrix4x4 &trMat);
  QFuture<bool> loadAssetP(const QString &str, const QMatrix4x4 &trMat, QThreadPool *threadPool = nullptr);

  std::unique_ptr<KawaiiDataUnit> clone(bool preserveExternalLinks) const;

  // QObject interface
protected:
  void childEvent(QChildEvent *event) override;



  //IMPLEMENT
private:
  static sib_utils::memento::Factory mementoFactory;
  static std::vector<std::variant<std::function<bool (const QString&, KawaiiDataUnit*)>, std::function<bool (const QString&, const QMatrix4x4&, KawaiiDataUnit*)>>> loadhooks;
  static std::list<LoadhookDescr*> loadhookDescrs;

  QPointer<KawaiiRendererImpl> rendererImpl;
  void removeRendererImpl();

protected:
  template<typename T>
  class TypeRegistrator {
  public:
    inline TypeRegistrator()
    {
      mementoFactory.registerClass<T>();
    }
  };

private:
  KAWAII_UNIT_DEF(KawaiiDataUnit)

  void createChildRenderer(KawaiiDataUnit *childUnit);
};



class KAWAII3D_SHARED_EXPORT KawaiiDataUnit::LoadhookDescr
{
  size_t i;
  LoadhookDescr(const LoadhookDescr &) = delete;
  LoadhookDescr& operator=(const LoadhookDescr &) = delete;

  explicit LoadhookDescr(size_t i);
public:
  LoadhookDescr(const std::function<bool (const QString &, KawaiiDataUnit*)> &hook);
  LoadhookDescr(const std::function<bool (const QString &, const QMatrix4x4 &, KawaiiDataUnit*)> &hook);

  LoadhookDescr(LoadhookDescr &&orig);
  LoadhookDescr& operator=(LoadhookDescr &&orig);
  ~LoadhookDescr();
};

#endif // KAWAIIDATAUNIT_HPP
