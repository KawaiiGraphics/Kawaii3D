#include "KawaiiShader.hpp"

#include "Exceptions/KawaiiImportFailedExcep.hpp"
#include "Exceptions/KawaiiNoDefenitionExcep.hpp"

#include "KawaiiProgram.hpp"
#include "KawaiiConfig.hpp"
#include "KawaiiRoot.hpp"

#include <sib_utils/ioReadAll.hpp>
#include <QProcessEnvironment>
#include <QRegularExpression>
#include <QDataStream>
#include <QDebug>

KawaiiShader::KawaiiShader(KawaiiShaderType type):
  parameters({std::pair(QStringLiteral("SIB_CAMERA_UBO_BINDING"),  QString::number(getCameraUboLocation())),
             std::pair(QStringLiteral("SIB_MATERIAL_UBO_BINDING"), QString::number(getMaterialUboLocation())),
             std::pair(QStringLiteral("SIB_MODEL_UBO_BINDING"),    QString::number(getModelUboLocation())),
             std::pair(QStringLiteral("SIB_SURFACE_UBO_BINDING"),  QString::number(getSurfaceUboLocation())),
             std::pair(QStringLiteral("SIB_VERTEX_ATTR_LOCATION"),  QString::number(getPosAttrLocation())),
             std::pair(QStringLiteral("SIB_NORMAL_ATTR_LOCATION"),  QString::number(getNormAttrLocation())),
             std::pair(QStringLiteral("SIB_TEXCOORD_ATTR_LOCATION"),  QString::number(getTexcoordAttrLocation()))
             }),
  owner(nullptr),
  prog(nullptr),
  type(type),
  cameraBlockEnd(-1),
  surfaceBlockEnd(-1),
  materialBlockEnd(-1),
  modelBlockEnd(-1),
  wasGloballyLoaded(false)
{
  connect(this, &KawaiiDataUnit::parentUpdated, this, &KawaiiShader::readParent);
}

KawaiiShader::KawaiiShader(const QString &source, KawaiiShaderType type):
  KawaiiShader(type)
{
  setSource(source);
}

KawaiiShader::KawaiiShader(QIODevice &io, KawaiiShaderType type):
  KawaiiShader(type)
{
  readFrom(io);
}

KawaiiShader::KawaiiShader(QIODevice &&io, KawaiiShaderType type):
  KawaiiShader(type)
{
  readFrom(io);
}

KawaiiShader::~KawaiiShader()
{
  if(owner)
    {
      auto *owner_ = owner;
      owner = nullptr;
      owner_->unregisterShader(this);
    }
}

void KawaiiShader::setSource(const QString &src)
{
  if(source != src)
    {
      source = src;
      wasGloballyLoaded = false;
      parseSource();
    }
}

void KawaiiShader::parseSource()
{
  if(!moduleName.isNull())
    emit moduleNameAboutToChange(QString());
  moduleName = QString();
  failedDep = false;
  dependencies.clear();
  exported.clear();
  cameraTextures.clear();
  surfaceTextures.clear();
  materialTextures.clear();
  modelTextures.clear();
  globalTextures.clear();

  cameraBlockEnd       =
      surfaceBlockEnd  =
      materialBlockEnd =
      modelBlockEnd    = -1;
  globalUboBockEnd.clear();
  inputGbufs.clear();
  storageGbufs.clear();

  globalScope = QBitArray(source.size(), true);
  code = source;

  parseParameters();

  code.replace('\t', QStringLiteral("    "));
  code.replace('\n', '\t');

  readGlobalScope();
  readName();
  parseExport();

  if(owner)
    {
      try {
        parseImport();
      } catch (KawaiiImportFailedExcep &e) {
        failedDep = true;
        code = QStringLiteral("/*%1*/\n#error Kawaii Shader Parser failed to parse import: %2!\n").arg(source, QString(e.what()).replace('\n', QStringLiteral("\\n")));
        readGlobalScope();
        emit reparsed(this);
        return;
      }
    }

  parseUbo();

  parseParameters();

  readGlobalScope();

  parseGbufs();

  code.replace('\t', '\n');

  emit reparsed(this);
}

bool KawaiiShader::tryParseSource()
{
  try {
    parseSource();
  } catch (const KawaiiParseExcep &e) {
    dependencies.clear();
    exported.clear();
    cameraTextures.clear();
    surfaceTextures.clear();
    materialTextures.clear();
    modelTextures.clear();

    cameraBlockEnd       =
        surfaceBlockEnd  =
        materialBlockEnd =
        modelBlockEnd    = -1;
    globalUboBockEnd.clear();

    globalScope = QBitArray(source.size(), true);
    code = QStringLiteral("/*%1*/\n#error Kawaii Shader Parser failed to parse: %2!\n").arg(source, QString(e.what()).replace('\n', QStringLiteral("\\n")));
    return false;
  }
  return true;
}

void KawaiiShader::readFrom(QIODevice &io)
{
  setSource(sib_utils::ioReadAll(io));
}

const QString &KawaiiShader::getModuleName() const
{
  return moduleName;
}

void KawaiiShader::copyFrom(const KawaiiShader &another)
{
  setSource(another.getSource());
  wasGloballyLoaded = another.wasGloballyLoaded;
}

bool KawaiiShader::isImportFailed() const
{
  return failedDep;
}

const QSet<KawaiiShader*>& KawaiiShader::getDependencies() const
{
  return dependencies;
}

const QHash<QString, QString> &KawaiiShader::getExported() const
{
  return exported;
}

KawaiiShaderType KawaiiShader::getType() const
{
  return type;
}

const QHash<QString, QString> &KawaiiShader::getCameraTextures() const
{
  return cameraTextures;
}

const QHash<QString, QString> &KawaiiShader::getSurfaceTextures() const
{
  return surfaceTextures;
}

const QHash<QString, QString> &KawaiiShader::getMaterialTextures() const
{
  return materialTextures;
}

const QHash<QString, QString> &KawaiiShader::getModelTextures() const
{
  return modelTextures;
}

const QHash<QString, QString> &KawaiiShader::getGlobalTextures() const
{
  return globalTextures;
}

int KawaiiShader::getCameraBlockEnd() const
{
  return cameraBlockEnd;
}

int KawaiiShader::getSurfaceBlockEnd() const
{
  return surfaceBlockEnd;
}

int KawaiiShader::getMaterialBlockEnd() const
{
  return materialBlockEnd;
}

int KawaiiShader::getModelBlockEnd() const
{
  return modelBlockEnd;
}

int KawaiiShader::getGlobalUboBlockEnd(uint32_t index) const
{
  return globalUboBockEnd.value(index, -1);
}

KawaiiShader::GBufDefinition KawaiiShader::getInputGbuf(uint32_t index) const
{
  return inputGbufs.value(index, GBufDefinition { .format = QString(), .i0 = -1, .sz = -1, .readonly = true });
}

KawaiiShader::GBufDefinition KawaiiShader::getStorageGbuf(uint32_t index) const
{
  return storageGbufs.value(index, GBufDefinition { .format = QString(), .i0 = -1, .sz = -1, .readonly = false });
}

KawaiiShader::GBufDefinition KawaiiShader::getSampledGbuf(uint32_t index) const
{
  return sampledGbufs.value(index, GBufDefinition { .format = QString(), .i0 = -1, .sz = -1, .readonly = false });
}

void KawaiiShader::forallInputGbuf(const std::function<void (uint32_t, const GBufDefinition&)> &func)
{
  if(Q_LIKELY(func))
    for(auto i = inputGbufs.cbegin(); i != inputGbufs.cend(); ++i)
      func(i.key(), i.value());
}

void KawaiiShader::forallStorageGbuf(const std::function<void (uint32_t, const GBufDefinition &)> &func)
{
  if(Q_LIKELY(func))
    for(auto i = storageGbufs.cbegin(); i != storageGbufs.cend(); ++i)
      func(i.key(), i.value());
}

void KawaiiShader::forallSampledGbuf(const std::function<void (uint32_t, const GBufDefinition &)> &func)
{
  if(Q_LIKELY(func))
    for(auto i = sampledGbufs.cbegin(); i != sampledGbufs.cend(); ++i)
      func(i.key(), i.value());
}

KawaiiShader *KawaiiShader::createFromMemento(sib_utils::memento::Memento::DataReader &memento)
{
  KawaiiShader *shader = nullptr;

  memento.readText(QStringLiteral("type"), [&shader](const QString &str) {
      const static QHash<QString, KawaiiShaderType> hash = {
        std::pair(QStringLiteral("fragment"), KawaiiShaderType::Fragment),
        std::pair(QStringLiteral("geometry"), KawaiiShaderType::Geometry),
        std::pair(QStringLiteral("tesselation-controll"), KawaiiShaderType::TesselationControll),
        std::pair(QStringLiteral("tesselation-evaluion"), KawaiiShaderType::TesselationEvaluion),
        std::pair(QStringLiteral("vertex"), KawaiiShaderType::Vertex),
        std::pair(QStringLiteral("compute"), KawaiiShaderType::Compute),
      };
      if(auto el = hash.find(str.toLower()); el != hash.end())
        shader = new KawaiiShader(*el);
    });

  if(!shader)
    memento.read(QStringLiteral("type"), [&shader](const QByteArray &bytes) {
        char c = bytes.at(0);
        shader = new KawaiiShader(static_cast<KawaiiShaderType>(c));
      });

  return shader;
}

QString KawaiiShader::getParameter(const QString &key) const
{
  return parameters.value(key, QProcessEnvironment::systemEnvironment().value(key, key));
}

void KawaiiShader::setParameter(const QString &key, const QString &value)
{
  if(value == QString())
    return removeParameter(key);

  parameters[key] = value;
  tryParseSource();
}

void KawaiiShader::setVarParameter(const QString &key, const QVariant &value)
{
  setParameter(key, value.toString());
}

void KawaiiShader::removeParameter(const QString &key)
{
  parameters.remove(key);
  tryParseSource();
}

bool KawaiiShader::isGloballyLoadedModule() const
{
  return wasGloballyLoaded;
}

KawaiiShader *KawaiiShader::clone()
{
  KawaiiShader *result = new KawaiiShader(getSource(), getType());
  for(auto i = parameters.cbegin(); i != parameters.cend(); ++i)
    result->setParameter(i.key(), i.value());

  auto onReparsed = connect(this, &KawaiiShader::reparsed, [result, this] {
      for(auto i = parameters.cbegin(); i != parameters.cend(); ++i)
        result->parameters[i.key()] = i.value();
      result->setSource(getSource());
    });

  connect(result, &QObject::destroyed, [onReparsed] { disconnect(onReparsed); });
  connect(this, &QObject::destroyed, result, &QObject::deleteLater);
  return result;
}

void KawaiiShader::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setData(QStringLiteral("type"), QByteArray(1, static_cast<char>(type)));

  QByteArray bytes;
  QDataStream st(&bytes, QIODevice::WriteOnly);

  st << wasGloballyLoaded
     << parameters
     << source;

  memento.setData(QStringLiteral("data"), bytes);
}

namespace {
  const auto was_globally_loaded_literal = QStringLiteral("was_globally_loaded");
}

void KawaiiShader::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  if(wasGloballyLoaded)
    {
      memento.setData(was_globally_loaded_literal, QStringLiteral("Yes"));
      return;
    }

  memento.setData("source", source).setData(was_globally_loaded_literal, QStringLiteral("No"));
  static const std::unordered_map<KawaiiShaderType, QString> shaderTypesStr = {
    { KawaiiShaderType::Fragment, QStringLiteral("Fragment") },
    { KawaiiShaderType::Geometry, QStringLiteral("Geometry") },
    { KawaiiShaderType::TesselationControll, QStringLiteral("Tesselation-Controll") },
    { KawaiiShaderType::TesselationEvaluion, QStringLiteral("Tesselation-Evaluion") },
    { KawaiiShaderType::Vertex, QStringLiteral("Vertex") },
    { KawaiiShaderType::Compute, QStringLiteral("Compute") },
  };
  memento.setData(QStringLiteral("type"), shaderTypesStr.at(type));

  for(auto i = parameters.cbegin(); i != parameters.cend(); ++i)
    memento.setData(QStringLiteral("\"%1\"").arg(i.key()), i.value());
}

void KawaiiShader::read(sib_utils::memento::Memento::DataReader &memento)
{
  bool readedBinary = false;
  memento.read(QStringLiteral("data"), [this, &readedBinary] (const QByteArray &bytes) {
      QDataStream st(bytes);
      st >> wasGloballyLoaded
         >> parameters
         >> source;
      readedBinary = true;
    });

  bool readed = false;
  memento.readText(was_globally_loaded_literal, [&readed, this](const QString &loaded) {
      QString lower = loaded.toLower();
      if(lower == QLatin1String("yes"))
        {
          wasGloballyLoaded = true;
          readed = true;
        } else
        if(lower == QLatin1String("no"))
          {
            wasGloballyLoaded = false;
            readed = true;
          }
    });

  if(wasGloballyLoaded)
    throw std::runtime_error("memento loading failed!");
  else
    {
      memento.get(QStringLiteral("source"), source, readed);
      memento.readAllText([this] (const QString &key, const QString &value) {
          if(key.startsWith('"') && key.endsWith('"'))
            parameters[key.mid(1, key.size()-2)] = value;
        });

      if(readed || readedBinary)
        parseSource();
    }
}

void KawaiiShader::readParent()
{
  if(owner)
    owner->unregisterShader(this);

  owner = KawaiiRoot::getRoot<KawaiiPackage>(parent());
  prog = KawaiiRoot::getRoot<KawaiiProgram>(owner);

  if(owner)
    owner->registerShader(this);

  if(!source.isEmpty())
    parseSource();
}

bool KawaiiShader::isExportAvaliable(const QStringView &str)
{
  auto position = str.constData() - code.constData();

  return globalScope.at(position) &&
      !(str.startsWith(QStringLiteral("uniform"))
        || (str.startsWith(QStringLiteral("in")) && !str.startsWith(QStringLiteral("int")))
        || str.startsWith(QStringLiteral("out")));
}

void KawaiiShader::readGlobalScope()
{
  globalScope.resize(code.size());
  readGlobalScope(0, globalScope.size());
}

void KawaiiShader::readGlobalScope(int i0, int i1)
{
  unsigned level = 0,
      level_rnd = 0,
      level_sq = 0;


  enum {
    Normal,
    PreparingComment,
    IsComment,
    IsOneLineComment,
    PreparingUncomment
  } state = Normal;

  auto execNormal = [&level, &level_rnd, &level_sq, &state](const QChar &ch) {
      switch(ch.unicode())
        {
        case L'{':
          ++level;
          break;

        case L'}':
          --level;
          break;

        case L'(':
          ++level_rnd;
          break;

        case L')':
          --level_rnd;
          break;

        case L'[':
          ++level_sq;
          break;

        case L']':
          --level_sq;
          break;

        case L'/':
          state = PreparingComment;
          break;
        }
    };

  for(int i = i0; i < i1; ++i)
    {
      switch(state)
        {
        case Normal:
          execNormal(code.at(i));
          globalScope.setBit(i, (level==0) && (level_rnd==0) && (level_sq==0));
          break;

        case PreparingComment:
          switch(code.at(i).unicode())
            {
            case L'*':
              globalScope.setBit(i-1, false);
              globalScope.setBit(i, false);
              state = IsComment;
              break;

            case L'/':
              globalScope.setBit(i-1, false);
              globalScope.setBit(i, false);
              state = IsOneLineComment;
              break;

            default:
              state = Normal;
              globalScope.setBit(i, (level==0) && (level_rnd==0) && (level_sq==0));
            }
          break;

        case IsComment:
          if(code.at(i).unicode() == L'*')
            state = PreparingUncomment;
          globalScope.setBit(i, false);
          break;

        case IsOneLineComment:
          if(code.at(i).unicode() == L'\t')
            state = Normal;
          globalScope.setBit(i, false);
          break;

        case PreparingUncomment:
          if(code.at(i).unicode() == L'/')
            state = Normal;
          else
            state = IsComment;
          globalScope.setBit(i, false);
          break;
        }
    }
}

void KawaiiShader::readName()
{
  static QRegularExpression reg(QStringLiteral("module\\s+(\\w+);"));

  auto match = reg.match(code);
  while(match.hasMatch()) {
      if(!moduleName.isNull())
        throw KawaiiParseExcep("Multiple modules is not allowed in a single shader");

      QString name = match.captured(1);
      emit moduleNameAboutToChange(name);

      setObjectName(name);
      moduleName = name;
      int i0 = match.capturedStart(),
          n = match.capturedLength();

      codeRemove(i0, n);
      match = reg.match(code);
    }
}

void KawaiiShader::parseInlineStructures()
{
  static QRegularExpression reg(QStringLiteral("struct\\s+(\\w+)\\s*{[^{}]*}\\s*(\\w+)\\s*;")),
      structReg(QStringLiteral("struct\\s+(\\w+)\\s*{[^{}]*}"));

  QSet<QString> declaredStructs;

  {
    auto i = structReg.globalMatch(code);
    while(i.hasNext())
      declaredStructs.insert(i.next().captured(1));
  }

  auto i = reg.match(code);
  while(i.hasMatch())
    {
      QString after;
      int i0 = i.capturedStart();
      if(!declaredStructs.contains(i.captured(1)))
        {
          QStringView str(code.constData() + i0, i.capturedStart(2) - i0);
          after += str.trimmed().toString() + '\t';
          declaredStructs.insert(i.captured(1));
        }
      after += i.captured(1) + ' ' + i.captured(2) + '\t';
      code.replace(i0, i.capturedLength(), after);

      i = reg.match(code);
    }
}

void KawaiiShader::parseExport()
{
  static QRegularExpression reg(QStringLiteral("export\\s+((?:\\w+\\s*,\\s*)*(?:\\w+));"));

  auto match = reg.match(code);
  while(match.hasMatch())
    {
      bool globScope = globalScope.at(match.capturedStart());

      codeRemove(match.capturedStart(), match.capturedLength());

      if(globScope)
        for(auto &varName: match.captured(1).split(','))
          prepareExport(varName.trimmed());
      else
        if(KawaiiConfig::getInstance().getDebugLevel() >= 2)
          qDebug() << QLatin1String("Kawaii3D shader parser: WARNING: \'export\' directive in non global scope ignored! ") << match.captured();

      match = reg.match(code);
    }
}

void KawaiiShader::prepareExport(const QString &varName)
{
  QRegularExpression variableReg(QStringLiteral("([^;{}\\s#]*)\\s+%1\\s*(;)").arg(varName)),
      arrayReg(QStringLiteral("([^;{}\\s#]*)\\s+%1\\s*(\\[.*\\])\\s*;").arg(varName)),
      functionReg(QStringLiteral("([^;{}\\s#]*)\\s+%1\\s*(\\([^()]*\\))\\s*[;{]").arg(varName)),
      initializedVarReg(QStringLiteral("([^;{}\\s#]*)\\s+%1\\s*(=)[^;]*;").arg(varName)),
      structureReg(QStringLiteral("(struct)\\s+%1\\s*{[^{}]*}\\s*;").arg(varName));

  QRegularExpression *exportRegs[] = {&variableReg, &arrayReg, &functionReg, &initializedVarReg, &structureReg};

  QString newVar = QStringLiteral("%1_%2").arg(varName, moduleName);
  bool registred = false;
  for(auto *reg: exportRegs)
    {
      auto j = reg->globalMatch(code);
      while(j.hasNext())
        {
          auto match = j.next();
          auto qualifer = match.capturedView(1).trimmed(),
              str = match.capturedView(0);

          if(match.hasMatch() && isExportAvaliable(qualifer))
            {
              registerExportedDefenition(varName, str.toString());
              registred = true;
            }
        }
    }
  if(registred)
    replaceGlobalVariable(varName, newVar);
  else
    if(KawaiiConfig::getInstance().getDebugLevel() >= 2)
      qDebug().noquote() << QStringLiteral("Kawaii3D shader parser: WARNING: can not \'export\' term \"%1\" (not found)!").arg(varName);
}

namespace {
    QRegularExpression spaceReg(QStringLiteral("\\s+"));
}

QString KawaiiShader::getDefenition(QString &defenition)
{
  static QRegularExpression finalizeDefenitionReg(QStringLiteral("\\)\\s*{"));
  static QRegularExpression bracket(QStringLiteral(" ?([\\(,\\)]) ?"));

  defenition = defenition.trimmed();
  defenition.replace(spaceReg, QStringLiteral(" "));
  defenition.replace(bracket, QStringLiteral("\\1"));

  int i0 = defenition.lastIndexOf('='),
      n = defenition.size() - i0 - 1;
  if(i0 != -1)
    defenition.remove(i0, n);

  i0 = defenition.lastIndexOf(finalizeDefenitionReg) + 1;
  n = defenition.size() - i0;
  if(i0 != 0)
    defenition.replace(i0, n, ';');

  return defenition;
}

QString& KawaiiShader::replaceVariable(QString &src, const QString &before, const QString &after)
{
  QRegularExpression exp(QStringLiteral("(\\W)%1(\\W)").arg(before));
  src.replace(exp, QStringLiteral("\\1%1\\2").arg(after));
  return src;
}

void KawaiiShader::replaceGlobalVariable(const QString &before, const QString &after)
{
  replaceVariable(code, before, after);
  readGlobalScope();
}

void KawaiiShader::registerExportedDefenition(const QString &varName, QString defenition)
{
  auto el = exported.find(varName);
  if(el != exported.end())
    {
      if(el->isNull())
        *el = getDefenition(defenition);

      if(getDefenition(defenition) != el.value())
        throw KawaiiParseExcep("Conflicting defenitions");
    } else {
      el = exported.insert(varName, getDefenition(defenition));
    }
}

void KawaiiShader::parseImport()
{
  static QRegularExpression exp(QStringLiteral("import\\s+((?:\\w+\\s*,\\s*)*(?:\\w+))\\s+from\\s+(\\w+)\\s*;"));
  auto match = exp.match(code);
  while(match.hasMatch())
    if(globalScopeAt(match.capturedStart()))
      {
        QString fromModule = match.captured(2);
        QString after;
        QStringList importList = match.captured(1).split(',');

        KawaiiShader *module = nullptr;
        if(owner)
          module = owner->findModule(fromModule, type);

        if(!module)
          throw KawaiiImportFailedExcep(getModuleName(), fromModule);

        QVector<QPair<QString, QString>> replaces;
        replaces.reserve(importList.size());
        for(auto &varName: importList)
          {
            varName = varName.trimmed();
            auto el = module->getExported().find(varName);
            if(el == module->getExported().end())
              throw KawaiiNoDefenitionExcep(getModuleName(), fromModule, varName);

            QString newVar = QStringLiteral("%1_%2").arg(varName, fromModule);
            after += replaceVariable(QString(el.value() + '\t'), varName, newVar);
            replaces << qMakePair(varName, newVar);
          }
        code.replace(match.capturedStart(), match.capturedLength(), after);
        for(auto &i: replaces)
          replaceGlobalVariable(i.first, i.second);

        dependencies.insert(module);
        match = exp.match(code);
      } else
      match = exp.match(code, match.capturedEnd());
}

void KawaiiShader::parseParameters()
{
  static QRegularExpression reg0(QStringLiteral("\\$(\\w+)")),
      reg1(QStringLiteral("\\${([^}]+)}"));

  auto execRegExp = [this] (QRegularExpression &reg) {
    auto match = reg.match(code);
    while(match.hasMatch())
      {
        QString after = getParameter(match.captured(1));
        int e = match.capturedEnd(),
            l = match.capturedLength();
        shiftBlocks(e-1, after.length() - l);

        int i0 = match.capturedStart();
        if(i0 >= 0 && i0+l <= code.length())
          code.replace(i0, l, after);
        match = reg.match(code);
      }
  };

  execRegExp(reg0);
  execRegExp(reg1);
}

void KawaiiShader::shiftBlocks(int startIndex, int delta)
{
  for(auto *index: {&cameraBlockEnd, &surfaceBlockEnd, &materialBlockEnd, &modelBlockEnd})
    if(*index > startIndex)
      *index += delta;

  for(auto &index: globalUboBockEnd)
    if(index > startIndex)
      index += delta;

  for(auto *gbufsSet: {&inputGbufs, &storageGbufs, &sampledGbufs})
    for(auto &index: *gbufsSet)
      if(index.i0 > startIndex)
        {
          index.i0 += delta;
          index.sz += delta;
        }
}

namespace {
  void extractTextures(const QString &code, QHash<QString, QString> *textures, QStringList &elems, QStringList &nonEmptyElems)
  {
    elems = code.split(';');
    nonEmptyElems.reserve(elems.size());
    for(auto i = elems.begin(); i != elems.end(); )
      {
        *i = i->trimmed();
        if(i->startsWith(QStringLiteral("sampler")))
          {
            if(textures)
              textures->insert(i->split(spaceReg).last(), *i);
            i = elems.erase(i);
            continue;
          }
        if(!i->isEmpty())
          nonEmptyElems.push_back(*i);
        ++i;
      }
  }
}

void KawaiiShader::parseUbo()
{
  auto preprocessUboBlock = [this] (const QString &block, QHash<QString, QString> *textures, int &end) {
      QRegularExpression reg(block + QStringLiteral("\\s*{([^{}]+)}\\s*(\\w*)(\\s*;)"));
      auto match = reg.match(code);
      while(match.hasMatch())
        {
          if(globalScopeAt(match.capturedStart()))
            {
              if(end != -1)
                throw KawaiiParseExcep("Multiple " + block + " blocks is forbiden by Kawaii Shading Language 0.1");

              const QString block_lower = block.toLower();

              if(match.captured(2).trimmed() != block_lower)
                throw KawaiiParseExcep("Renamed " + block + " blocks is forbiden by Kawaii Shading Language 0.1");

              QStringList elems, nonEmptyElems;
              extractTextures(match.captured(1), textures, elems, nonEmptyElems);
              if(!nonEmptyElems.isEmpty())
                {
                  QString after = QStringLiteral("struct %1\t{\t").arg("SIB_" + block.toUpper()) +
                      elems.join(";\t    ") + QStringLiteral("\t};\tlayout(std140, binding = ${%1_UBO_BINDING}) uniform _%1 { %1 %2; }; ").arg("SIB_" + block.toUpper(), block_lower);
                  codeReplace(match.capturedStart(), match.capturedLength(), after);
                  int delta = after.length() - match.capturedLength();
                  shiftBlocks(match.capturedStart(), delta);
                  end = match.capturedStart() + after.length();
                } else
                {
                  codeReplace(match.capturedStart(), match.capturedLength(), QLatin1String(""));
                  shiftBlocks(match.capturedStart(), -match.capturedLength());
                  end = match.capturedStart();
                }
              match = reg.match(code);
            } else
            match = reg.match(code, match.capturedEnd());
        }
    };
  preprocessUboBlock(QStringLiteral("Camera"),   &cameraTextures,   cameraBlockEnd);
  preprocessUboBlock(QStringLiteral("Surface"),  &surfaceTextures,  surfaceBlockEnd);
  preprocessUboBlock(QStringLiteral("Material"), &materialTextures, materialBlockEnd);
  preprocessUboBlock(QStringLiteral("Model"),    &modelTextures,    modelBlockEnd);

  static QRegularExpression samplerExpression(QStringLiteral("\\W(sampler[^;]+);"));
  auto match = samplerExpression.match(code);
  while(match.hasMatch())
    {
      globalTextures.insert(match.capturedView(1).split(spaceReg).last().toString(), match.captured(1));
      codeReplace(match.capturedStart(1), match.capturedLength()-1, QLatin1String(""));
      shiftBlocks(match.capturedStart(), -match.capturedLength());
      match = samplerExpression.match(code);
    }
  static QRegularExpression uboGlobalExp(QStringLiteral("Global_(\\d+)\\s*{([^{}]+)}\\s*(\\w*)(\\s*;)"));
  match = uboGlobalExp.match(code);
  while(match.hasMatch())
    {
      if(globalScopeAt(match.capturedStart()))
        {
          const QString strBlockIndex = match.captured(1);
          uint32_t blockIndex = strBlockIndex.toUInt();
          if(!globalUboBockEnd.contains(blockIndex))
            globalUboBockEnd[blockIndex] = -1;
          int &end = globalUboBockEnd[blockIndex];

          if(end != -1)
            throw KawaiiParseExcep("Multiple Global blocks with the same index is forbiden by Kawaii Shading Language 0.1");

          const QString block = QStringLiteral("Global_%1").arg(blockIndex);
          const QString block_lower = block.toLower();

          if(match.captured(3).trimmed() != block_lower)
            throw KawaiiParseExcep("Renamed Global blocks is forbiden by Kawaii Shading Language 0.1");

          QStringList elems, nonEmptyElems;
          extractTextures(match.captured(2), nullptr, elems, nonEmptyElems);
          if(!nonEmptyElems.isEmpty())
            {
              QString after = QStringLiteral("struct %1\t{\t").arg("SIB_" + block.toUpper()) +
                  elems.join(";\t    ") + QStringLiteral("\t};\tlayout(std140, binding = GLOBAL_UBO_BINDING_%3) uniform _%1 { %1 %2; }; ").arg("SIB_" + block.toUpper(), block_lower, strBlockIndex);
              codeReplace(match.capturedStart(), match.capturedLength(), after);
              int delta = after.length() - match.capturedLength();
              shiftBlocks(match.capturedStart(), delta);
              end = match.capturedStart() + after.length();
            } else
            {
              codeReplace(match.capturedStart(), match.capturedLength(), QLatin1String(""));
              shiftBlocks(match.capturedStart(), -match.capturedLength());
              end = match.capturedStart();
            }
          match = uboGlobalExp.match(code);
        } else
        match = uboGlobalExp.match(code, match.capturedEnd());
    }
}

void KawaiiShader::parseGbufs()
{
  static QRegularExpression input_gbuf_pattern(QStringLiteral("input\\s+(\\w+)\\s+gbuf_(\\d+)\\s*;"));
  static QRegularExpression storage_gbuf_pattern(QStringLiteral("storage\\s+(\\w+)\\s+gbuf_(\\d+)\\s*;"));
  static QRegularExpression const_storage_gbuf_pattern(QStringLiteral("const\\s+storage\\s+(\\w+)\\s+gbuf_(\\d+)\\s*;"));
  static QRegularExpression sampled_gbuf_pattern(QStringLiteral("sampled\\s+(\\w+)\\s+gbuf_(\\d+)\\s*;"));

  auto scanGbufDefinitions = [this] (QRegularExpression &exp, QHash<uint32_t, GBufDefinition> &out) {
    auto match = exp.globalMatch(code);
    while(match.hasNext())
      {
        auto i = match.next();
        const QString fmtStr = i.captured(1);
        const QString indexStr = i.captured(2);
        const uint32_t index = indexStr.toUInt();
        const std::pair<int,int> definition(i.capturedStart(), i.capturedLength());
        if(auto el = out.find(index); el != out.cend())
          {
            if(el->i0 > definition.first)
              {
                codeRemove(el->i0, el->sz);
                shiftBlocks(el->i0, definition.second - (el->sz));
                *el = { .format = fmtStr, .i0 = definition.first, .sz = definition.second, .readonly = false };
                match = exp.globalMatch(code);
              }
          } else
          out.insert(index, { .format = fmtStr, .i0 = definition.first, .sz = definition.second, .readonly = false });
      }
  };

  scanGbufDefinitions(input_gbuf_pattern, inputGbufs);
  for(auto &i: inputGbufs)
    i.readonly = true;
  scanGbufDefinitions(const_storage_gbuf_pattern, storageGbufs);
  for(auto &i: storageGbufs)
    i.readonly = true;
  scanGbufDefinitions(storage_gbuf_pattern, storageGbufs);
  scanGbufDefinitions(sampled_gbuf_pattern, sampledGbufs);
  for(auto &i: sampledGbufs)
    i.readonly = true;
}

void KawaiiShader::detachParent()
{
  owner = nullptr;
}
