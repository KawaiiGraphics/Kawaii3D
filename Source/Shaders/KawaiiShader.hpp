#ifndef KAWAIISHADER_HPP
#define KAWAIISHADER_HPP

#include "../KawaiiDataUnit.hpp"
#include "KawaiiShaderType.hpp"
#include <QIODevice>
#include <QHash>
#include <QSet>
#include <QBitArray>

class KawaiiPackage;
class KawaiiProgram;

class KAWAII3D_SHARED_EXPORT KawaiiShader: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiShader);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
  friend class KawaiiShadersHash;

public:
  struct GBufDefinition
  {
    QString format;
    int i0;
    int sz;
    bool readonly;
  };

  KawaiiShader(KawaiiShaderType type);
  KawaiiShader(const QString &source, KawaiiShaderType type);
  KawaiiShader(QIODevice &io, KawaiiShaderType type);
  KawaiiShader(QIODevice &&io, KawaiiShaderType type);
  ~KawaiiShader();

  ///Check if there is global scope in Kawaii Shading Language source at char_index
  inline bool globalScopeAt(int char_index) const { return globalScope.at(char_index); }

  ///get GLSL code
  inline auto& getCode() const { return code; }

  ///get source code in Kawaii Shading Language
  inline auto& getSource() const { return source; }

  ///set source code in Kawaii Shading Language
  void setSource(const QString &src);

  void parseSource();
  bool tryParseSource();

  void readFrom(QIODevice &io);

  inline void readFrom(QIODevice &&io)
  { readFrom(static_cast<QIODevice&>(io)); }

  const QString& getModuleName() const;

  void copyFrom(const KawaiiShader &another);

  bool isImportFailed() const;
  const QSet<KawaiiShader *> &getDependencies() const;
  const QHash<QString, QString>& getExported() const;

  KawaiiShaderType getType() const;

  inline static constexpr uint32_t getCameraUboLocation() { return 1; }
  inline static constexpr uint32_t getMaterialUboLocation() { return 2; }
  inline static constexpr uint32_t getModelUboLocation() { return 3; }
  inline static constexpr uint32_t getSurfaceUboLocation() { return 4; }
  inline static constexpr uint32_t getUboCount() { return 4; }

  inline static constexpr uint32_t getPosAttrLocation() { return 0; }
  inline static constexpr uint32_t getNormAttrLocation() { return 1; }
  inline static constexpr uint32_t getTexcoordAttrLocation() { return 2; }

  const QHash<QString, QString>& getCameraTextures() const;
  const QHash<QString, QString>& getSurfaceTextures() const;
  const QHash<QString, QString>& getMaterialTextures() const;
  const QHash<QString, QString>& getModelTextures() const;
  const QHash<QString, QString>& getGlobalTextures() const;

  int getCameraBlockEnd() const;
  int getSurfaceBlockEnd() const;
  int getMaterialBlockEnd() const;
  int getModelBlockEnd() const;
  int getGlobalUboBlockEnd(uint32_t index) const;

  GBufDefinition getInputGbuf(uint32_t index) const;
  GBufDefinition getStorageGbuf(uint32_t index) const;
  GBufDefinition getSampledGbuf(uint32_t index) const;
  void forallInputGbuf(const std::function<void (uint32_t, const GBufDefinition &)> &func);
  void forallStorageGbuf(const std::function<void(uint32_t, const GBufDefinition &)> &func);
  void forallSampledGbuf(const std::function<void(uint32_t, const GBufDefinition &)> &func);

  static KawaiiShader* createFromMemento(sib_utils::memento::Memento::DataReader &memento);

  inline KawaiiPackage* getOwner() const
  { return owner; }

  inline KawaiiProgram* getProgram() const
  { return prog; }

  QString getParameter(const QString &key) const;
  void setParameter(const QString &key, const QString &value);
  void setVarParameter(const QString &key, const QVariant &value);
  void removeParameter(const QString &key);

  bool isGloballyLoadedModule() const;

  KawaiiShader *clone();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void reparsed(KawaiiShader *shader);
  void moduleNameAboutToChange(const QString &moduleName);



  //IMPLEMENT
private:
  QString source;
  QString code;
  QString moduleName;

  QSet<KawaiiShader*> dependencies;
  QHash<QString, QString> exported; //<Name, FullDefenition>
  QHash<QString, QString> parameters; //<Name, Value>

  QHash<QString, QString> cameraTextures; //<Name, FullDefenition>
  QHash<QString, QString> surfaceTextures; //<Name, FullDefenition>
  QHash<QString, QString> materialTextures; //<Name, FullDefenition>
  QHash<QString, QString> modelTextures; //<Name, FullDefenition>
  QHash<QString, QString> globalTextures; //<Name, FullDefenition>
  QHash<uint32_t, int> globalUboBockEnd; //<Number, BlockEnd>
  QHash<uint32_t, GBufDefinition> inputGbufs; //<Number, <start, end>>
  QHash<uint32_t, GBufDefinition> storageGbufs; //<Number, <start, end>>
  QHash<uint32_t, GBufDefinition> sampledGbufs; //<Number, <start, end>>

  QBitArray globalScope;

  KawaiiPackage *owner;
  KawaiiProgram *prog;

  KawaiiShaderType type;

  int cameraBlockEnd;
  int surfaceBlockEnd;
  int materialBlockEnd;
  int modelBlockEnd;

  bool failedDep; //true, if any import failed
  bool wasGloballyLoaded;

  void readParent();

  bool isExportAvaliable(const QStringView &str);

  void readGlobalScope();
  void readGlobalScope(int i0, int i1);
  void readName();
  void parseInlineStructures();
  void parseExport();
  void prepareExport(const QString &varName);

  QString getDefenition(QString &defenition);
  QString &replaceVariable(QString &src, const QString &before, const QString &after);

  inline QString &replaceVariable(QString &&src, const QString &before, const QString &after)
  { return replaceVariable(static_cast<QString&>(src), before, after); }

  void replaceGlobalVariable(const QString &before, const QString &after);
  void registerExportedDefenition(const QString &varName, QString defenition);

  void parseImport();

  void parseParameters();

  void shiftBlocks(int startIndex, int delta);
  void parseUbo();

  void parseGbufs();

  template<typename... ArgsT>
  void codeRemove(ArgsT&&... args)
  {
    code.remove(std::forward<ArgsT>(args)...);
    readGlobalScope();
  }

  template<typename... ArgsT>
  void codeReplace(ArgsT&&... args)
  {
    code.replace(std::forward<ArgsT>(args)...);
    readGlobalScope();
  }

  void detachParent();
};

#endif // KAWAIISHADER_HPP
