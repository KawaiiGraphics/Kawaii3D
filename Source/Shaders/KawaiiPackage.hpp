#ifndef KAWAIIPACKAGE_HPP
#define KAWAIIPACKAGE_HPP

#include "KawaiiShadersHash.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiPackage: public KawaiiDataUnit
{
  Q_OBJECT

public:
  KawaiiPackage();
  ~KawaiiPackage() = default;

  virtual void registerShader(KawaiiShader *sh);
  virtual void unregisterShader(KawaiiShader *sh);

  KawaiiShader *findModule(const QString &name, KawaiiShaderType type) const;
  bool usesShader(KawaiiShader *shader) const;

  inline const auto& getVertexShaders() const { return vertShaders.getAllShaders(); }
  inline const auto& getTesselationControllShaders() const { return tessCtrlShaders.getAllShaders(); }
  inline const auto& getTesselationEvalutionShaders() const { return tessEvalShaders.getAllShaders(); }
  inline const auto& getGeometryShaders() const { return geomShaders.getAllShaders(); }
  inline const auto& getFragmentShaders() const { return fragShaders.getAllShaders(); }
  inline const auto& getComputeShaders() const { return computeShaders.getAllShaders(); }
  const QSet<KawaiiShader*>& getShaders(KawaiiShaderType type) const;

  void forallModules(const std::function<void(KawaiiShader*)> &func) const;

protected:
  void addVertexShaders(const KawaiiShadersHash &shaders);
  void addTesselationControllShaderss(const KawaiiShadersHash &shaders);
  void addTesselationEvalutionShaders(const KawaiiShadersHash &shaders);
  void addGeometryShaders(const KawaiiShadersHash &shaders);
  void addFragmentShaders(const KawaiiShadersHash &shaders);
  void addComputeShaders(const KawaiiShadersHash &shaders);

signals:
  void shaderRegistered(KawaiiShader *sh);
  void shaderUnregistered(KawaiiShader *sh);



  //IMPLEMENT
private:
  KawaiiShadersHash vertShaders;
  KawaiiShadersHash tessCtrlShaders;
  KawaiiShadersHash tessEvalShaders;
  KawaiiShadersHash geomShaders;
  KawaiiShadersHash fragShaders;
  KawaiiShadersHash computeShaders;

  KawaiiPackage *parentPkg;

  void onParentUpdated();

  void onParentShaderRegistered(KawaiiShader *sh);

  void observeShader(KawaiiShader *sh);
  void unobserveShader(KawaiiShader *sh);
};

#endif // KAWAIIPACKAGE_HPP
