#include "KawaiiScene.hpp"
#include "KawaiiRoot.hpp"
#include "Geometry/KawaiiMesh3D.hpp"
#include <QMutex>

KawaiiScene::KawaiiScene():
  root(nullptr)
{
  connect(this, &KawaiiDataUnit::parentUpdated, this, &KawaiiScene::onParentChanged);
}

KawaiiScene::~KawaiiScene()
{
}

void KawaiiScene::setProgramToMaterial(KawaiiMaterial *m, KawaiiProgram *prog)
{
  if(!prog)
    return removeMaterial(m);

  auto el = scene.find(m);
  if(el != scene.end()) {
      if(el->shaderProg == prog) return;
      disconnect(el->onShaderProgRebuilt);
      disconnect(el->onShaderProgCullModeChanged);
      el->shaderProg = prog;
      el->onShaderProgRebuilt = connect(el->shaderProg, &KawaiiProgram::rebuild, this, &KawaiiScene::drawPipelineChanged);
      el->onShaderProgCullModeChanged = connect(el->shaderProg, &KawaiiProgram::cullModeChanged, this, &KawaiiScene::drawPipelineChanged);
    } else {
      el = scene.insert(m, RenderProgram(prog, {}));
      el->onShaderProgRebuilt = connect(prog, &KawaiiProgram::rebuild, this, &KawaiiScene::drawPipelineChanged);
      el->onShaderProgCullModeChanged = connect(el->shaderProg, &KawaiiProgram::cullModeChanged, this, &KawaiiScene::drawPipelineChanged);
      el->onMaterialUboChanged = connect(m, &KawaiiMaterial::uniformsChanged, this, &KawaiiScene::drawPipelineChanged);
    }

  emit materialProgramChanged(m, prog);
  emit drawPipelineChanged();
}

void KawaiiScene::removeMaterial(KawaiiMaterial *m)
{
  auto el = scene.find(m);
  if(el != scene.end()) {
      for(auto i: el->instances)
        emit meshRemovedFromMaterial(m, i);

//      auto prog = el->shaderProg;
      disconnect(el->onMaterialUboChanged);
      disconnect(el->onShaderProgRebuilt);
      disconnect(el->onShaderProgCullModeChanged);
      scene.erase(el);
      emit materialProgramChanged(m, nullptr);
      emit drawPipelineChanged();
    }
}

void KawaiiScene::clear()
{
  for(auto el = scene.constBegin(); el != scene.constEnd(); ++el) {
      for(auto i: el->instances) {
          disconnect(i, &KawaiiMeshInstance::drawPipelineChanged, this, &KawaiiScene::drawPipelineChanged);
          disconnect(i, &KawaiiMeshInstance::askedRedraw, this, &KawaiiScene::askedRedraw);
          emit meshRemovedFromMaterial(el.key(), i);
        }
      disconnect(el->onShaderProgRebuilt);
      disconnect(el->onShaderProgCullModeChanged);
      disconnect(el->onMaterialUboChanged);
      emit materialProgramChanged(el.key(), nullptr);
    }
  scene.clear();
  emit drawPipelineChanged();
}

void KawaiiScene::setDefaultProgram(KawaiiProgram *prog)
{
  setProgramToMaterial(nullptr, prog);
}

KawaiiProgram *KawaiiScene::getDefaultProgram() const
{
  auto el = scene.find(nullptr);
  if(el != scene.end())
    return el->shaderProg;
  else
    return nullptr;
}

const QHash<KawaiiMaterial *, KawaiiScene::RenderProgram> &KawaiiScene::getScene() const
{
  return scene;
}

KawaiiRoot *KawaiiScene::getRoot() const
{
  return root;
}

void KawaiiScene::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  for(auto i = scene.begin(); i != scene.end(); ++i)
    {
      sib_utils::memento::Memento *childM;
      memento.createChildMemento("MaterialInfo", childM);
      childM->mutator(memento.getCreatedMemento(), memento.preserveExternalLinks).
          setLink("material", i.key()).
          setLink("shader_program", i->shaderProg);
    }
}

void KawaiiScene::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  //Data can not be stringified to human-readable text
  writeBinary(memento);
}

void KawaiiScene::read(sib_utils::memento::Memento::DataReader &memento)
{
  QMutex sceneLocker;
  memento.forallChildren([this, &memento, &sceneLocker] (sib_utils::memento::Memento &childM) {
      TreeNode *prog = nullptr, *material = prog;
      bool progReaded = false, materialReaded = progReaded;
      QString mementoType;
      auto reader = childM.reader(memento.getCreatedObjects());
      reader.getType(mementoType);
      if(mementoType.toLower() != "materialinfo")
        return;

      reader.get("shader_program", prog, progReaded).get("material", material, materialReaded);

      KawaiiMaterial *mLink = materialReaded? dynamic_cast<KawaiiMaterial*>(material): nullptr;
      KawaiiProgram *pLink = progReaded? dynamic_cast<KawaiiProgram*>(prog): nullptr;
      {
        QMutexLocker l(&sceneLocker);
        setProgramToMaterial(mLink, pLink);
      }
    });
}

void KawaiiScene::onParentChanged()
{
  auto newRoot = KawaiiRoot::getRoot(parent());
  bool rootChanged = newRoot != root;
  if(!rootChanged)
    return;

  if(root) {
      disconnect(root, &KawaiiRoot::meshAboutToChangeMaterial, this, &KawaiiScene::onMeshChangedMaterial);
      disconnect(root, &KawaiiRoot::meshAdded, this, &KawaiiScene::onMeshRegistered);
      disconnect(root, &KawaiiRoot::meshRemoved, this, &KawaiiScene::onMeshUnregistered);
      disconnect(root, &KawaiiRoot::bindedBufferUpdated, this, &KawaiiScene::onBindedBufferUpdated);
      clear();
    }
  root = newRoot;
  if(root) {
      connect(root, &KawaiiRoot::meshAboutToChangeMaterial, this, &KawaiiScene::onMeshChangedMaterial);
      connect(root, &KawaiiRoot::meshAdded, this, &KawaiiScene::onMeshRegistered);
      connect(root, &KawaiiRoot::meshRemoved, this, &KawaiiScene::onMeshUnregistered);
      connect(root, &KawaiiRoot::bindedBufferUpdated, this, &KawaiiScene::onBindedBufferUpdated);
      for(auto *mesh: root->getMeshes())
        onMeshRegistered(mesh);
    }

}

void KawaiiScene::onMeshChangedMaterial(KawaiiMeshInstance *mesh, KawaiiMaterial *material)
{
  onMeshUnregistered(mesh);
  addMesh(mesh, material);
}

void KawaiiScene::onMeshRegistered(KawaiiMeshInstance *mesh)
{
  addMesh(mesh, mesh->getMaterial());
}

void KawaiiScene::onMeshUnregistered(KawaiiMeshInstance *mesh)
{
  auto el = scene.find(mesh->getMaterial());
  if(el != scene.end())
    el->instances.removeAll(mesh);

  disconnect(mesh, &KawaiiMeshInstance::drawPipelineChanged, this, &KawaiiScene::drawPipelineChanged);
  disconnect(mesh, &KawaiiMeshInstance::askedRedraw, this, &KawaiiScene::askedRedraw);

  emit meshRemovedFromMaterial(mesh->getMaterial(), mesh);
  emit drawPipelineChanged();
}

void KawaiiScene::addMesh(KawaiiMeshInstance *mesh, KawaiiMaterial *material)
{
  auto el = scene.find(material);
  if(el != scene.end())
    el->instances.append(mesh);
  else
    scene.insert(material, RenderProgram{nullptr, {mesh}});

  connect(mesh, &KawaiiMeshInstance::drawPipelineChanged, this, &KawaiiScene::drawPipelineChanged);
  connect(mesh, &KawaiiMeshInstance::askedRedraw, this, &KawaiiScene::askedRedraw);

  emit meshAddedToMaterial(material, mesh);
  emit drawPipelineChanged();
}

void KawaiiScene::onBindedBufferUpdated(uint32_t binding, KawaiiBufferTarget target)
{
  bool redraw = target != KawaiiBufferTarget::UBO;

  for(const auto &i: getScene())
    {
      if(redraw) break;

      if(auto *prog = i.shaderProg; prog && !i.instances.empty())
        prog->forallModules([&redraw, binding] (KawaiiShader *sh) {
            if(sh->getGlobalUboBlockEnd(binding) != -1)
              redraw = true;
          });
    }
  if(redraw)
    emit askedRedraw();
}
