#ifndef KAWAIISCENE_HPP
#define KAWAIISCENE_HPP

#include "KawaiiProgram.hpp"
#include "../KawaiiBufferTarget.hpp"
#include "../KawaiiMaterial.hpp"
#include "../Geometry/KawaiiMeshInstance.hpp"
#include <QHash>
#include <QVector>

class KAWAII3D_SHARED_EXPORT KawaiiScene: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiScene);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  struct RenderProgram {
    friend class ::KawaiiScene;

    KawaiiProgram *shaderProg;
    QVector<KawaiiMeshInstance*> instances;

    inline RenderProgram(KawaiiProgram *shaderProg, const QVector<KawaiiMeshInstance*> &instances):
      shaderProg(shaderProg),
      instances(instances)
    {}
  private:
    QMetaObject::Connection onShaderProgRebuilt;
    QMetaObject::Connection onShaderProgCullModeChanged;
    QMetaObject::Connection onMaterialUboChanged;
  };

  KawaiiScene();
  ~KawaiiScene();

  void setProgramToMaterial(KawaiiMaterial *m, KawaiiProgram *prog);
  void removeMaterial(KawaiiMaterial *m);
  void clear();

  void setDefaultProgram(KawaiiProgram *prog);
  KawaiiProgram *getDefaultProgram() const;

  const QHash<KawaiiMaterial*, RenderProgram> &getScene() const;

  KawaiiRoot *getRoot() const;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;
  void read(sib_utils::memento::Memento::DataReader &memento) override;

signals:
  void meshAddedToMaterial(KawaiiMaterial *material, KawaiiMeshInstance *mesh);
  void meshRemovedFromMaterial(KawaiiMaterial *material, KawaiiMeshInstance *mesh);
  void materialProgramChanged(KawaiiMaterial *material, KawaiiProgram *prog);
  void drawPipelineChanged();
  void askedRedraw();



  //IMPLEMENT
private:
  QHash<KawaiiMaterial*, RenderProgram> scene;
  KawaiiRoot *root;

  void onParentChanged();
  void onMeshChangedMaterial(KawaiiMeshInstance *mesh, KawaiiMaterial *material);

  void onMeshRegistered(KawaiiMeshInstance *mesh);
  void onMeshUnregistered(KawaiiMeshInstance *mesh);

  void addMesh(KawaiiMeshInstance *mesh, KawaiiMaterial *material);

  void onBindedBufferUpdated(uint32_t binding, KawaiiBufferTarget target);
};

#endif // KAWAIISCENE_HPP
