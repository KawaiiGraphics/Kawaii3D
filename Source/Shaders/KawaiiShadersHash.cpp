#include <QPointer>
#include "KawaiiConfig.hpp"
#include "KawaiiShadersHash.hpp"
#include "Exceptions/KawaiiParseExcep.hpp"
#include "Exceptions/KawaiiImposibleException.hpp"

KawaiiShadersHash::KawaiiShadersHash(KawaiiShaderType type, KawaiiDataUnit *parent):
  parent(parent), type(type)
{ }

void KawaiiShadersHash::HashElem::destroyHashElem()
{
  QObject::disconnect(onModuleNameChanged);
  QObject::disconnect(onReparsed);
  sh->detachParent();
}

KawaiiShadersHash::~KawaiiShadersHash()
{
  for(auto &shader: ownShaders)
    shader.destroyHashElem();

  for(auto &shader: nullNamedOwnShaders)
    shader.destroyHashElem();
}

void KawaiiShadersHash::insertShader(KawaiiShader *sh)
{
  insert(sh->getModuleName(), sh);
}

void KawaiiShadersHash::removeShader(KawaiiShader *sh)
{
  bool removed = false;

  auto removeFrom = [&removed] (auto &i, auto &from) {
      QObject::disconnect(i->onModuleNameChanged);
      QObject::disconnect(i->onReparsed);
      i->sh->detachParent();
      from.erase(i);
      removed = true;
    };

  if(sh->getModuleName().isNull())
    {
      if(auto i = std::find_if(nullNamedOwnShaders.begin(), nullNamedOwnShaders.end(),
                               [sh](const HashElem &iter) { return (sh == iter.sh); });
         i != nullNamedOwnShaders.end())
        {
          removeFrom(i, nullNamedOwnShaders);
        }
    } else
    if(auto i = ownShaders.find(sh->getModuleName()); i != ownShaders.end())
      removeFrom(i, ownShaders);

  if(removed)
    updateDependencies(sh);
}

KawaiiShader *KawaiiShadersHash::getShader(const QString &name, KawaiiShader *defaultValue) const
{
  if(name.isNull())
    return defaultValue;

  if(auto el = ownShaders.find(name); el != ownShaders.end())
    return el->sh;
  else
    return defaultValue;
}

KawaiiShader *KawaiiShadersHash::getShader(const QString &name, const QList<KawaiiShadersHash *> fallbackModules) const
{
  if(auto ptr = getShader(name))
    return ptr;

  KawaiiShader *ptr = nullptr;
  for(auto *i: fallbackModules)
    if((ptr = i->getShader(name)))
      break;

  return ptr;
}

void KawaiiShadersHash::copyShaders(const KawaiiShadersHash &from)
{
  for(auto el = from.ownShaders.begin(); el != from.ownShaders.end(); ++el)
    {
      auto ptr = parent->createChild<KawaiiShader>(el.value()->getType());
      ptr->copyFrom(**el);
      insert(el.key(), ptr);
    }

  for(const auto &i: from.nullNamedOwnShaders)
    {
      auto ptr = parent->createChild<KawaiiShader>(i->getType());
      ptr->copyFrom(*i);
      insert(ptr->getModuleName(), ptr);
    }
}

void KawaiiShadersHash::loadGlslModules(const QList<QDir> &dirs, const QString &filter)
{
  QSet<QString> loadedShaders;
  for(auto &dr: dirs)
    for(auto &fName: dr.entryList({filter}, QDir::Files | QDir::Readable))
      if(!loadedShaders.contains(fName))
        {
          auto *ptr = parent->createChild<KawaiiShader>(QFile(dr.absoluteFilePath(fName)), type);
          ptr->wasGloballyLoaded = true;
          insert(ptr->getModuleName(), ptr);
          loadedShaders.insert(fName);
        }
}

void KawaiiShadersHash::updateDependencies()
{
  allShaders.clear();
  ownShaders.detach();
  for(const auto &i: ownShaders)
    {
      QPointer<KawaiiShader> sh = i.sh;

      if(sh->isImportFailed() || !sh->getDependencies().isEmpty())
        {
          sh->blockSignals(true);
          sh->parseSource();
          sh->blockSignals(false);
        }

      if(!sh)
        continue;

      if(!sh->isImportFailed())
        allShaders.unite(sh->getDependencies());

      allShaders.insert(sh);
    }
  updateRDep();
}

void KawaiiShadersHash::insert(const QString &moduleName, KawaiiShader *sh)
{
  auto moduleNameChanged = [this, sh](const QString &newName) {
      onShaderModuleNameAboutToChange(sh, newName);
    };

  if(moduleName.isNull())
    {
      nullNamedOwnShaders.append(HashElem());
      nullNamedOwnShaders.back().sh = sh;
      nullNamedOwnShaders.back().onModuleNameChanged = QObject::connect(sh, &KawaiiShader::moduleNameAboutToChange, moduleNameChanged);
      nullNamedOwnShaders.back().onReparsed = QObject::connect(sh, &KawaiiShader::reparsed, [this] (KawaiiShader *sh) { return updateDependencies(sh); });
      return;
    }

  auto el = ownShaders.find(moduleName);
  if(el == ownShaders.end())
    {
      HashElem elem;
      elem.sh = sh;
      elem.onModuleNameChanged = QObject::connect(sh, &KawaiiShader::moduleNameAboutToChange, moduleNameChanged);
      elem.onReparsed = QObject::connect(sh, &KawaiiShader::reparsed, [this] (KawaiiShader *sh) { return updateDependencies(sh); });

      ownShaders.insert(moduleName, elem);
    } else
    if(el->sh != sh)
      throw KawaiiParseExcep(QStringLiteral("KawaiiShadersHash::insert: shader \"") + moduleName + QStringLiteral("\" already exists"));


  for(auto i = ownShaders.begin(); i != ownShaders.end(); ++i)
    if(i->sh->isImportFailed())
      {
        i->sh->blockSignals(true);
        i->sh->parseSource();
        i->sh->blockSignals(false);

        if(!i->sh->isImportFailed())
          allShaders.unite(i->sh->getDependencies());
      }
  allShaders.insert(sh);
  updateRDep();
}

void KawaiiShadersHash::onShaderModuleNameAboutToChange(KawaiiShader *sh, const QString &newName)
{
  if(newName == sh->getModuleName())
    return;

  auto el = ownShaders.end();

  //Check for conflicts
  if(auto i = ownShaders.find(newName); i != ownShaders.end())
    {
      bool conflictResolved = false;
      while (i != ownShaders.end() && i.key() == newName)
        {
          if(KawaiiConfig::getInstance().isSystemShadersProtected())
            {
              if(i->sh->isGloballyLoadedModule())
                {
                  sh->deleteLater();
                  return;
                }
              else
                if(sh->isGloballyLoadedModule())
                  {
                    QObject::disconnect(i->onModuleNameChanged);
                    QObject::disconnect(i->onReparsed);
                    i->sh->detachParent();
                    i->sh->deleteLater();
                    i = ownShaders.erase(i);
                    continue;
                  }
            }

          if(i->sh == sh)
            {
              conflictResolved = true;
              el = i;
              break;
            }

          if(i->sh->getSource() == sh->getSource())
            {
              sh->deleteLater();
              return;
            }

          ++i;
        }
      if(sh->isGloballyLoadedModule())
        conflictResolved = true;
      if(!conflictResolved)
        throw KawaiiParseExcep("Module " + newName + " is already registered in " + (parent? parent->objectName(): QString("%{anonymous shader hash}")));
    }

  //el - iterator to renaming module, or end, if not found

  if(el == ownShaders.end() && sh->getModuleName().isNull())
    {
      auto i = std::find_if(nullNamedOwnShaders.begin(), nullNamedOwnShaders.end(),
                                     [sh](const HashElem &iter) { return (sh == iter.sh); } );

      if(i != nullNamedOwnShaders.end())
        {
          if(newName.isNull())
            return;

          ownShaders.insert(newName, *i);
          nullNamedOwnShaders.erase(i);
        } else
        throw KawaiiImposibleException("Module 0x" + QString::number(reinterpret_cast<size_t>(sh), 16) + " is not registered in " + (parent? parent->objectName(): QString("%{anonymous shader hash}")) + ". But it\'s impossible");
    } else
    {
      if(el == ownShaders.end())
        el = ownShaders.find(sh->getModuleName());
      else
        if(el.key() == newName)
          return;

      if(el != ownShaders.end())
        {
          if(el->sh != sh)
            throw KawaiiImposibleException("Module " + sh->getModuleName() + " is not registered in " + (parent? parent->objectName(): QString("%{anonymous shader hash}")) + ". But it\'s impossible");

          if(newName.isNull())
            nullNamedOwnShaders.push_back(*el);
          else
            ownShaders.insert(newName, *el);

          ownShaders.erase(el);
        } else
        throw KawaiiImposibleException("Module " + sh->getModuleName() + " is not registered in " + (parent? parent->objectName(): QString("%{anonymous shader hash}")) + ". But it\'s impossible");
    }
}

void KawaiiShadersHash::updateDependencies(KawaiiShader *blame)
{
  allShaders.clear();
  ownShaders.detach();
  for(const auto &i: ownShaders)
    {
      QPointer<KawaiiShader> sh = i.sh;

      if(sh != blame && (sh->isImportFailed() || sh->getDependencies().contains(blame)))
        {
          sh->blockSignals(true);
          sh->parseSource();
          sh->blockSignals(false);
        }

      if(!sh->isImportFailed())
        allShaders.unite(sh->getDependencies());

      allShaders.insert(sh);
    }
  updateRDep();
}

void KawaiiShadersHash::updateRDep()
{
  for(auto *sh: allShaders)
    {
      if(!sh->isImportFailed())
        allShaders.unite(sh->getDependencies());
    }
}
