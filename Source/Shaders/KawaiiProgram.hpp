#ifndef KAWAIIPROGRAM_HPP
#define KAWAIIPROGRAM_HPP

#include "KawaiiPackage.hpp"
#include <QSet>
#include <QHash>

class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiProgram: public KawaiiPackage
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiProgram);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  enum class CullMode: uint8_t {
    CullNone = 0,
    CullFront = 1,
    CullBack = 2,
    CullAll = 3
  };

  KawaiiProgram();
  ~KawaiiProgram();

  template<typename... ArgsT>
  KawaiiShader* createShaderModule(KawaiiShaderType type, ArgsT&&... args)
  { return createChild<KawaiiShader>(args..., type); }

  void setCullMode(CullMode newCullingMode);
  CullMode getCullMode() const;

signals:
  void rebuild();
  void cullModeChanged(CullMode cullingMode);



  //IMPLEMENT
private:
  CullMode cullMode;

  void registerShader(KawaiiShader *sh) override final;
  void unregisterShader(KawaiiShader *sh) override final;

  void onParentChanged();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void read(sib_utils::memento::Memento::DataReader &mem) override final;
};

#endif // KAWAIIPROGRAM_HPP
