#include "Exceptions/KawaiiImportFailedExcep.hpp"
#include "KawaiiProgram.hpp"
#include "KawaiiRoot.hpp"
#include <QDir>

KawaiiProgram::KawaiiProgram():
  cullMode(CullMode::CullNone)
{
}

KawaiiProgram::~KawaiiProgram()
{
}

void KawaiiProgram::setCullMode(CullMode newCullingMode)
{
  if(newCullingMode != cullMode)
    {
      cullMode = newCullingMode;
      emit cullModeChanged(cullMode);
    }
}

KawaiiProgram::CullMode KawaiiProgram::getCullMode() const
{
  return cullMode;
}

void KawaiiProgram::registerShader(KawaiiShader *sh)
{
  KawaiiPackage::registerShader(sh);

  emit rebuild();
  connect(sh, &KawaiiShader::reparsed, this, &KawaiiProgram::rebuild);
}

void KawaiiProgram::unregisterShader(KawaiiShader *sh)
{
  KawaiiPackage::unregisterShader(sh);

  emit rebuild();
  disconnect(sh, &KawaiiShader::reparsed, this, &KawaiiProgram::rebuild);
}

namespace {
  const QString cullModeStrLit = QStringLiteral("cull_mode");
}

void KawaiiProgram::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  mem.setData(cullModeStrLit, QByteArray(1, static_cast<char>(getCullMode())));
}

void KawaiiProgram::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  static const std::unordered_map<CullMode, QString> cullModeNames = {
    {CullMode::CullNone, QStringLiteral("cull_none")},
    {CullMode::CullBack, QStringLiteral("cull_back")},
    {CullMode::CullFront, QStringLiteral("cull_front")},
    {CullMode::CullAll, QStringLiteral("cull_all")},
  };
  mem.setData(cullModeStrLit, cullModeNames.at(getCullMode()));
}

void KawaiiProgram::read(sib_utils::memento::Memento::DataReader &mem)
{
  mem.read(cullModeStrLit, [this] (const QByteArray &data) {
      setCullMode(static_cast<CullMode>(data.at(0)));
    })
      .readText(cullModeStrLit, [this] (const QString &str) {
      static const QHash<QString, CullMode> knownCullModes = {
        {QStringLiteral("cull_none"), CullMode::CullNone},
        {QStringLiteral("cull_back"), CullMode::CullBack},
        {QStringLiteral("cull_front"), CullMode::CullFront},
        {QStringLiteral("cull_all"), CullMode::CullAll},
      };
      setCullMode(knownCullModes.value(str.toLower(), getCullMode()));
    });
}
