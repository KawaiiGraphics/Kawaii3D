#ifndef KAWAIISHADERSHASH_HPP
#define KAWAIISHADERSHASH_HPP

#include "../Kawaii3D_global.hpp"
#include "KawaiiShader.hpp"
#include <QHash>
#include <QDir>

class KAWAII3D_SHARED_EXPORT KawaiiShadersHash
{
public:
  KawaiiShadersHash(KawaiiShaderType type, KawaiiDataUnit *parent=nullptr);
  ~KawaiiShadersHash();

  void insertShader(KawaiiShader *sh);
  void removeShader(KawaiiShader *sh);

  KawaiiShader* getShader(const QString &name, KawaiiShader *defaultValue = nullptr) const;
  KawaiiShader* getShader(const QString &name, const QList<KawaiiShadersHash*> fallbackModules) const;

  template<typename T0, typename... ArgsT>
  KawaiiShader* getShader(const QString &name, KawaiiShader *defaultValue, T0 *arg0, ArgsT*... args) const
  {
    if(arg0)
      return getShader(name, arg0->getShader(name, defaultValue, args...));
    else
      return getShader(name, defaultValue, args...);
  }

  void copyShaders(const KawaiiShadersHash &from);

  void loadGlslModules(const QList<QDir> &dirs, const QString &filter);

  inline auto begin() { return allShaders.begin(); }
  inline auto end()   { return allShaders.end(); }

  inline auto begin() const { return allShaders.begin(); }
  inline auto end()   const { return allShaders.end(); }

  inline const auto& getAllShaders() const { return allShaders; }

  void updateDependencies();



  //IMPLEMENT
private:
  struct HashElem {
    KawaiiShader *sh;
    QMetaObject::Connection onModuleNameChanged;
    QMetaObject::Connection onReparsed;

    inline KawaiiShader* operator->() { return sh; }
    inline const KawaiiShader* operator->() const { return sh; }

    inline KawaiiShader& operator*() { return *sh; }
    inline const KawaiiShader& operator*() const { return *sh; }

    void destroyHashElem();
  };

  QHash<QString, HashElem> ownShaders;
  QVector<HashElem> nullNamedOwnShaders;
  KawaiiDataUnit *parent;
  KawaiiShaderType type;
  QSet<KawaiiShader*> allShaders;

  void insert(const QString &moduleName, KawaiiShader *sh);
  void onShaderModuleNameAboutToChange(KawaiiShader *sh, const QString &newName);
  void updateDependencies(KawaiiShader *blame);
  void updateRDep();
};

#endif // KAWAIISHADERSHASH_HPP
