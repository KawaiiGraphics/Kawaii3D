#include "KawaiiPackage.hpp"
#include "KawaiiRoot.hpp"

KawaiiPackage::KawaiiPackage():
  vertShaders(KawaiiShaderType::Vertex, this),
  tessCtrlShaders(KawaiiShaderType::TesselationControll, this),
  tessEvalShaders(KawaiiShaderType::TesselationEvaluion, this),
  geomShaders(KawaiiShaderType::Geometry, this),
  fragShaders(KawaiiShaderType::Fragment, this),
  computeShaders(KawaiiShaderType::Compute, this),

  parentPkg(nullptr)
{
  connect(this, &TreeNode::parentUpdated, this, &KawaiiPackage::onParentUpdated);
}

void KawaiiPackage::registerShader(KawaiiShader *sh)
{
  switch(sh->getType())
    {
    case KawaiiShaderType::Vertex:
      vertShaders.insertShader(sh);
      break;
    case KawaiiShaderType::TesselationControll:
      tessCtrlShaders.insertShader(sh);
      break;
    case KawaiiShaderType::TesselationEvaluion:
      tessEvalShaders.insertShader(sh);
      break;
    case KawaiiShaderType::Geometry:
      geomShaders.insertShader(sh);
      break;
    case KawaiiShaderType::Fragment:
      fragShaders.insertShader(sh);
      break;
    case KawaiiShaderType::Compute:
      computeShaders.insertShader(sh);
      break;
    }
  emit shaderRegistered(sh);
}

void KawaiiPackage::unregisterShader(KawaiiShader *sh)
{
  switch(sh->getType())
    {
    case KawaiiShaderType::Vertex:
      vertShaders.removeShader(sh);
      break;
    case KawaiiShaderType::TesselationControll:
      tessCtrlShaders.removeShader(sh);
      break;
    case KawaiiShaderType::TesselationEvaluion:
      tessEvalShaders.removeShader(sh);
      break;
    case KawaiiShaderType::Geometry:
      geomShaders.removeShader(sh);
      break;
    case KawaiiShaderType::Fragment:
      fragShaders.removeShader(sh);
      break;
    case KawaiiShaderType::Compute:
      computeShaders.removeShader(sh);
      break;
    }
  emit shaderUnregistered(sh);
}

KawaiiShader *KawaiiPackage::findModule(const QString &name, KawaiiShaderType type) const
{
  const KawaiiPackage *i = this;
  KawaiiShader *result = nullptr;

  do {
      switch(type)
        {
        case KawaiiShaderType::Vertex:
          result = i->vertShaders.getShader(name);
          break;

        case KawaiiShaderType::TesselationControll:
          result = i->tessCtrlShaders.getShader(name);
          break;

        case KawaiiShaderType::TesselationEvaluion:
          result = i->tessEvalShaders.getShader(name);
          break;

        case KawaiiShaderType::Geometry:
          result = i->geomShaders.getShader(name);
          break;

        case KawaiiShaderType::Fragment:
          result = i->fragShaders.getShader(name);
          break;

        case KawaiiShaderType::Compute:
          result = i->computeShaders.getShader(name);
          break;

        default:
          return nullptr;
        }
      i = i->parentPkg;
    } while(i && !result);

  return result;
}

bool KawaiiPackage::usesShader(KawaiiShader *shader) const
{
  switch(shader->getType())
    {
    case KawaiiShaderType::Vertex:
      return vertShaders.getAllShaders().contains(shader);

    case KawaiiShaderType::TesselationControll:
      return tessCtrlShaders.getAllShaders().contains(shader);

    case KawaiiShaderType::TesselationEvaluion:
      return tessEvalShaders.getAllShaders().contains(shader);

    case KawaiiShaderType::Geometry:
      return geomShaders.getAllShaders().contains(shader);

    case KawaiiShaderType::Fragment:
      return fragShaders.getAllShaders().contains(shader);

    case KawaiiShaderType::Compute:
      return computeShaders.getAllShaders().contains(shader);

    default:
      return false;
    }
}

const QSet<KawaiiShader*>& KawaiiPackage::getShaders(KawaiiShaderType type) const
{
  switch(type)
    {
    case KawaiiShaderType::Vertex:
      return vertShaders.getAllShaders();

    case KawaiiShaderType::TesselationControll:
      return tessCtrlShaders.getAllShaders();

    case KawaiiShaderType::TesselationEvaluion:
      return tessEvalShaders.getAllShaders();

    case KawaiiShaderType::Geometry:
      return geomShaders.getAllShaders();

    case KawaiiShaderType::Fragment:
      return fragShaders.getAllShaders();

    case KawaiiShaderType::Compute:
      return computeShaders.getAllShaders();

    default:
      Q_UNREACHABLE();
    }
}

void KawaiiPackage::forallModules(const std::function<void (KawaiiShader *)> &func) const
{
  for(auto i: vertShaders)
    func(i);
  for(auto i: geomShaders)
    func(i);
  for(auto i: tessCtrlShaders)
    func(i);
  for(auto i: tessEvalShaders)
    func(i);
  for(auto i: fragShaders)
    func(i);
}

void KawaiiPackage::addVertexShaders(const KawaiiShadersHash &shaders)
{
  vertShaders.copyShaders(shaders);
}

void KawaiiPackage::addTesselationControllShaderss(const KawaiiShadersHash &shaders)
{
  tessCtrlShaders.copyShaders(shaders);
}

void KawaiiPackage::addTesselationEvalutionShaders(const KawaiiShadersHash &shaders)
{
  tessEvalShaders.copyShaders(shaders);
}

void KawaiiPackage::addGeometryShaders(const KawaiiShadersHash &shaders)
{
  geomShaders.copyShaders(shaders);
}

void KawaiiPackage::addFragmentShaders(const KawaiiShadersHash &shaders)
{
  fragShaders.copyShaders(shaders);
}

void KawaiiPackage::addComputeShaders(const KawaiiShadersHash &shaders)
{
  computeShaders.copyShaders(shaders);
}

void KawaiiPackage::onParentUpdated()
{
  auto newParentNamespace = KawaiiRoot::getRoot<KawaiiPackage>(parent());
  if(newParentNamespace != parentPkg)
    {
      if(parentPkg)
        {
          disconnect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::shaderRegistered);
          disconnect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::shaderUnregistered);

          disconnect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::observeShader);
          disconnect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::unobserveShader);

          disconnect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::onParentShaderRegistered);
          disconnect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::onParentShaderRegistered);
        }

      parentPkg = newParentNamespace;

      if(parentPkg)
        {
          connect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::shaderRegistered);
          connect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::shaderUnregistered);

          connect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::observeShader);
          connect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::unobserveShader);

          connect(parentPkg, &KawaiiPackage::shaderRegistered, this, &KawaiiPackage::onParentShaderRegistered);
          connect(parentPkg, &KawaiiPackage::shaderUnregistered, this, &KawaiiPackage::onParentShaderRegistered);
        }

      vertShaders.updateDependencies();
      tessCtrlShaders.updateDependencies();
      tessEvalShaders.updateDependencies();
      geomShaders.updateDependencies();
      fragShaders.updateDependencies();
      computeShaders.updateDependencies();
    }
}

void KawaiiPackage::onParentShaderRegistered(KawaiiShader *sh)
{
  if(sh->getOwner() == this)
    return;

  switch(sh->getType())
    {
    case KawaiiShaderType::Vertex:
      vertShaders.updateDependencies();
      break;

    case KawaiiShaderType::TesselationControll:
      tessCtrlShaders.updateDependencies();
      break;

    case KawaiiShaderType::TesselationEvaluion:
      tessEvalShaders.updateDependencies();
      break;

    case KawaiiShaderType::Geometry:
      geomShaders.updateDependencies();
      break;

    case KawaiiShaderType::Fragment:
      fragShaders.updateDependencies();
      break;

    case KawaiiShaderType::Compute:
      computeShaders.updateDependencies();
      break;
    }
  connect(sh, &KawaiiShader::reparsed, this, &KawaiiPackage::onParentShaderRegistered, Qt::UniqueConnection);
}

void KawaiiPackage::observeShader(KawaiiShader *sh)
{
  connect(sh, &KawaiiShader::reparsed, this, &KawaiiPackage::onParentShaderRegistered, Qt::UniqueConnection);
}

void KawaiiPackage::unobserveShader(KawaiiShader *sh)
{
  disconnect(sh, &KawaiiShader::reparsed, this, &KawaiiPackage::onParentShaderRegistered);
}
