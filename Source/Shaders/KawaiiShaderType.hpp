#ifndef KAWAIISHADERTYPE_HPP
#define KAWAIISHADERTYPE_HPP

#include <stdint.h>
#include <functional>

enum class KawaiiShaderType: uint8_t
{
  Fragment = 0,
  TesselationControll = 1,
  TesselationEvaluion = 2,
  Vertex = 3,
  Geometry = 4,
  Compute = 5
};

namespace std {
  template<>
  struct hash<KawaiiShaderType>
  {
    size_t operator()(const KawaiiShaderType &type) const
    {
      return std::hash<uint8_t>()(static_cast<uint8_t>(type));
    }
  };
}

#endif // KAWAIISHADERTYPE_HPP
