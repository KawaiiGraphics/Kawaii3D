#ifndef KAWAIIPLUGIN_HPP
#define KAWAIIPLUGIN_HPP

#include <QDir>
#include <QHash>
#include <QDebug>
#include <memory>
#include <QLibrary>
#include <type_traits>
#include <QJsonObject>
#include "Kawaii3D_global.hpp"
#include "Exceptions/KawaiiPluginNotFound.hpp"

class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiPlugin
{
  friend class Kawaii3D_Initializer;
public:
  using InitFunc = std::add_pointer_t<void (KawaiiRoot*)>;

  struct SemanticVersion
  {
    uint16_t maj;
    uint16_t min;
    uint32_t patch;

    SemanticVersion():
      maj(0),
      min(0),
      patch(0)
    {}

    SemanticVersion(uint16_t maj, uint16_t min, uint32_t patch):
      maj(maj),
      min(min),
      patch(patch)
    {}

    bool isCompatible(const SemanticVersion &another) const;

    static SemanticVersion fromString(const QString &str);
  };



  KawaiiPlugin(const QString &cfgFilePath);

  KawaiiPlugin(KawaiiPlugin &another);
  KawaiiPlugin(KawaiiPlugin &&another);

  KawaiiPlugin& operator=(KawaiiPlugin &another);
  KawaiiPlugin& operator=(KawaiiPlugin &&another);

  ~KawaiiPlugin() = default;

  void init(KawaiiRoot *root);


  QFunctionPointer resolveFunction(const QByteArray &symbol);

  template<typename ResultT, typename... ArgsT>
  ResultT callFunction(const QByteArray &symbol, ArgsT... args)
  {
    auto *func = reinterpret_cast<std::add_pointer_t<ResultT(ArgsT...)>>(resolveFunction(symbol));
    if(!func)
      throw KawaiiPluginFailed(name, QString("lookup symbol \"%1\"").arg(QLatin1String(symbol.constData())));
    return func(args...);
  }

  const QString &getConfigFilePath();
  const QString &getPluginName() const;

  static KawaiiPlugin& findBasicPlugin(const QString &name);



  template<typename T = KawaiiPlugin>
  static void loadPlugins(const QList<QDir> &dirList, std::vector<T> &result)
  {
    static_assert(std::is_base_of_v<KawaiiPlugin, T>, "T must be based on KawaiiPlugin");
    static_assert(std::is_constructible_v<T, const QString&>, "T must be constructible by config file name");
    QSet<QString> loadedPlugins;

    for(const auto &d: dirList)
      {
        QStringList confFiles = d.entryList({"*.conf"}, QDir::Files | QDir::Readable);
        result.reserve(result.size() + confFiles.size());
        for(const auto &i: confFiles)
          if(!loadedPlugins.contains(i))
            {
              try
                {
                  result.emplace_back(d.absoluteFilePath(i));
                  loadedPlugins.insert(i);
                }
              catch(const KawaiiPluginFailed &excep)
                {
                  qWarning().noquote() << QString("Failed to load plugin \"%1\": unable to %2").arg(excep.getPluginName(), excep.getAction());
                }
            }
      }
  }

protected:
  void reparseConfig();
  const QJsonObject& getConfig() const;



  //IMPLEMENT
private:
  QJsonObject jsonRoot;
  QString cfgFilePath;
  QString name;
  QByteArray initFuncName;
  QByteArray onLoadFuncName;
  InitFunc initFunc;
  QFunctionPointer onLoadFunc;
  std::unique_ptr<QLibrary> lib;
  QHash<QByteArray, QFunctionPointer> loadedFunctions;

  void load();

  struct PluginCollection {
    std::vector<KawaiiPlugin> vec;
    QHash<QString, decltype(vec)::iterator> hash;

    PluginCollection() = default;

    template<typename... ArgsT>
    PluginCollection(ArgsT&&... args):
      vec(args...)
    {
      for(auto i = vec.begin(); i < vec.end(); ++i)
        hash[i->getPluginName()] = i;
    }

    template<typename T>
    PluginCollection& operator=(T &&arg)
    {
      vec = std::move(arg);
      hash.clear();
      for(auto i = vec.begin(); i < vec.end(); ++i)
        hash[i->getPluginName()] = i;
      return *this;
    }

    KawaiiPlugin& findPlugin(const QString &plugin) const
    {
      auto el = hash.find(plugin);
      if(el == hash.end())
        throw KawaiiPluginNotFound(plugin);
      return *el.value();
    }
  };
  static PluginCollection basicPlugins;
};

template<>
inline KawaiiPlugin::PluginCollection::PluginCollection(KawaiiPlugin::PluginCollection &&origin):
  vec(std::move(origin.vec)),
  hash(std::move(origin.hash))
{}

template<>
inline KawaiiPlugin::PluginCollection::PluginCollection(std::vector<KawaiiPlugin> &&origin):
  vec(std::move(origin))
{
  for(auto i = vec.begin(); i < vec.end(); ++i)
    hash[i->getPluginName()] = i;
}

#endif // KAWAIIPLUGIN_HPP
