#include "KawaiiConfig.hpp"
#include <sib_utils/Strings.hpp>
#include <QProcessEnvironment>
#include <QImageWriter>
#include <QDebug>

KawaiiConfig &KawaiiConfig::getInstance()
{
  {
    QWriteLocker l(&cfgGuard);
    if(!cfg)
      cfg = std::make_unique<KawaiiConfig>();
  }
  return *cfg;
}

namespace {
  QList<QDir> getDirList(const QProcessEnvironment &env, const QString &key, const QString &subdir = ".")
  {
    auto val = env.value(key).split(':', Qt::SkipEmptyParts);
    QList<QDir> result;
    result.reserve(val.size());
    for(const auto &i: val)
      {
        QDir d(i);
        if(!d.cd(subdir) || !d.exists()) continue;

        d.setPath(d.canonicalPath());
        result.push_back(d);
      }
    return result;
  }

  QList<QDir> subdirList(const QList<QDir> &origin, const QString &subdirName)
  {
    QList<QDir> result;
    result.reserve(origin.size());
    for(QDir d: origin)
      if(d.cd(subdirName))
        result.push_back(d);
    return result;
  }

  KawaiiImgFormat choseImgWriteFormat()
  {
    const QStringList fmtLst =
        QProcessEnvironment::systemEnvironment().value("SIB3D_IMG_FORMAT", "PNG(9),WEBP(100),JPG").split(',', Qt::SkipEmptyParts);

    static const auto supportetFmts = QImageWriter::supportedImageFormats();

    QByteArray fmt = supportetFmts.first();
    int quality = -1;

    QStringList::const_iterator fmtIter = fmtLst.begin();
    do {
        int i0 = fmtIter->lastIndexOf('('),
            i1 = i0? fmtIter->indexOf(')', i0) - 1: -1;

        QStringView fmtName(QStringView(fmtIter->constData(), i0)),
            fmtQuality(QStringView(*fmtIter).sliced(i0+1, i1 - i0));

        fmt = fmtName.toLatin1().toLower();
        if(supportetFmts.contains(fmt))
          {
            fmtIter = fmtLst.end();
            bool ok;
            quality = fmtQuality.toInt(&ok);
            if(!ok)
              quality = -1;
          } else {
            fmt = supportetFmts.first();
            ++fmtIter;
          }
      } while(fmtIter != fmtLst.end());

    return {fmt, quality};
  }
}

void KawaiiConfig::reset()
{
  QWriteLocker wl(&cfgGuard);

  if(!writable)
    return;

  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
# ifdef KAWAII3D_DEBUG
#   define DEFAULT_DEBUG "2"
#   define DEFAULT_DEBUG_INT 2
# else
#   define DEFAULT_DEBUG "1"
#   define DEFAULT_DEBUG_INT 1
# endif
  bool ok;
  debug = static_cast<uint8_t>(env.value("SIB3D_DEBUG_LEVEL", DEFAULT_DEBUG).toUShort(&ok));
  if(!ok)
    debug = static_cast<uint8_t>(DEFAULT_DEBUG_INT);
#undef DEFAULT_DEBUG

  resDirs.clear();
  if(env.contains("SIB3D_PATH"))
    resDirs << getDirList(env, "SIB3D_PATH");
  else
    resDirs << getDirList(env, "XDG_DATA_DIRS", "Kawaii3D")
            << QDir::currentPath() + "/Kawaii3D/"
            << QDir::currentPath() + "/../share/Kawaii3D/"
            << QCoreApplication::applicationDirPath() + "/Kawaii3D/"
            << QCoreApplication::applicationDirPath() + "/../share/Kawaii3D/"
            << QDir(QDir::homePath()+"/.local/share/Kawaii3D/")
            << QDir("/usr/local/share/Kawaii3D/")
            << QDir("/usr/share/Kawaii3D/")
            << QDir("/share/Kawaii3D/");

  resDirs.erase(std::remove_if(resDirs.begin(), resDirs.end(), [](QDir &d) { return !d.exists(); }),
      resDirs.end());

  for(auto &d: resDirs)
    d.setPath(d.canonicalPath());

  pluginDirs = subdirList(resDirs, "Plugins");
  renderDirs = subdirList(pluginDirs, "Renders");
  shaderModuleDirs = subdirList(resDirs, "ShaderModules");

  fmt = QSurfaceFormat::defaultFormat();
  fmt.setOption(QSurfaceFormat::DebugContext, (debug > 0));
  fmt.setSamples(env.value("SIB3D_MSAA", QString::number(fmt.samples())).toInt());
  fmt.setDepthBufferSize(env.value("SIB3D_DEPTHBUFFER", "24").toInt());
  fmt.setAlphaBufferSize(env.value("SIB3D_ALPHABUFFER", "0").toInt());
  fmt.setRedBufferSize(env.value("SIB3D_REDBUFFER", "8").toInt());
  fmt.setGreenBufferSize(env.value("SIB3D_GREENBUFFER", "8").toInt());
  fmt.setBlueBufferSize(env.value("SIB3D_BLUEBUFFER", "8").toInt());
  fmt.setSwapInterval(env.value("SIB3D_VSYNC", "1").toInt());

  bool explicitBuffering;
  renderbuffersCount = env.value("SIB3D_BUFFERING", "").toInt(&explicitBuffering);
  if(explicitBuffering)
    {
      switch(*renderbuffersCount)
        {
        case 0:
        case 1:
          fmt.setSwapBehavior(QSurfaceFormat::SingleBuffer);
          break;

        case 2:
          fmt.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
          break;

        case 3:
          fmt.setSwapBehavior(QSurfaceFormat::TripleBuffer);
          break;
        }
    } else
    renderbuffersCount = std::nullopt;

  preferredRenderers = env.value("SIB3D_RENDERER", "").split(':', Qt::SkipEmptyParts);

  writable = !env.value("SIB3D_CFG_READ_ONLY", "0").toInt(&ok);
  if(!ok)
    writable = false;

  imgFormat = choseImgWriteFormat();

  protectSystemShaders = env.value("SIB3D_PROTECT_SYSTEM_SHADERS", "1").toInt(&ok);
  if(!ok)
    protectSystemShaders = true;
}

uint16_t KawaiiConfig::getDebugLevel() const
{
  QReadLocker rl(&cfgGuard);
  return debug;
}

void KawaiiConfig::setDebugLevel(uint16_t lvl)
{
  QWriteLocker l(&cfgGuard);
  if(writable)
    debug = lvl;
}

const QList<QDir> &KawaiiConfig::getResourceDirs() const
{
  QReadLocker rl(&cfgGuard);
  return resDirs;
}

const QList<QDir> &KawaiiConfig::getPluginsConfigDirs() const
{
  QReadLocker rl(&cfgGuard);
  return pluginDirs;
}

const QList<QDir> &KawaiiConfig::getRendersConfigDirs() const
{
  QReadLocker rl(&cfgGuard);
  return renderDirs;
}

const QList<QDir> &KawaiiConfig::getShaderModuleDirs() const
{
  QReadLocker rl(&cfgGuard);
  return shaderModuleDirs;
}

const QStringList &KawaiiConfig::getPreferredRenderers() const
{
  QReadLocker rl(&cfgGuard);
  return preferredRenderers;
}

const QSurfaceFormat &KawaiiConfig::getPreferredSfcFormat() const
{
  QReadLocker rl(&cfgGuard);
  return fmt;
}

const KawaiiImgFormat &KawaiiConfig::getPreferredImgFormat() const
{
  QReadLocker rl(&cfgGuard);
  return imgFormat;
}

const std::optional<uint32_t>& KawaiiConfig::getRenderbuffersCount() const
{
  QReadLocker rl(&cfgGuard);
  return renderbuffersCount;
}

bool KawaiiConfig::isSystemShadersProtected() const
{
  QReadLocker rl(&cfgGuard);
  return protectSystemShaders;
}

KawaiiConfig::KawaiiConfig():
  writable(true),
  protectSystemShaders(false)
{
  reset();
}
