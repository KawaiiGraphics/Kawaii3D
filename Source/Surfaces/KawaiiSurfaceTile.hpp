#ifndef KAWAIISURFACETILE_HPP
#define KAWAIISURFACETILE_HPP

#include <QRectF>
#include <QRect>

#include "KawaiiSurface.hpp"
#include "../Renderpass/KawaiiRenderTarget.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiSurface::Tile: public KawaiiRenderTarget
{
  friend class ::KawaiiSurface;

public:
  Tile();
  Tile(const QRect &abs, const QRectF &rel, KawaiiRenderpass *renderpass);
  ~Tile();

  void onParentResized(int w, int h);

  const QRect &getRectAbs() const;
  void setRectAbs(const QRect &value);

  const QRectF &getRectRel() const;
  void setRectRel(const QRectF &value);

  const QRect &getResultRect() const;

  QPointF mapCoord(const QPointF &p) const;
  QPointF unmapCoord(const QPointF &p) const;

  // KawaiiRenderTarget interface
public:
  void setRenderpass(KawaiiRenderpass *newRenderpass) override final;



  //IMPLEMENT
private:
  std::function<void(const Tile&, KawaiiRenderpass*)> renderpassChangedCallback;
  QRect resultRect;

  QRect rectAbs;
  QRectF rectRel;

  KawaiiSurface* sfc;

  glm::uvec2 size;

  uint32_t parentW;
  uint32_t parentH;

  void onRectChanged();
  void setSfc(KawaiiSurface *newSfc);
};

#endif // KAWAIISURFACETILE_HPP
