#ifndef KAWAIIRENDER_HPP
#define KAWAIIRENDER_HPP

#include <memory>
#include <QWindow>
#include <QLibrary>
#include <type_traits>
#include "../Kawaii3D_global.hpp"
#include "../KawaiiRendererImpl.hpp"
#include "../KawaiiPlugin.hpp"

class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiRender: public KawaiiPlugin
{
  friend class Kawaii3D_Initializer;
public:
  using CreateRenderFunc = std::add_pointer_t<KawaiiRendererImpl* (KawaiiRoot*)>;
  using CheckSystemFunc = std::add_pointer_t<bool ()>;

  KawaiiRender(const QString &cfgFilePath);

  KawaiiRender(KawaiiRender &another);
  KawaiiRender(KawaiiRender &&another);

  KawaiiRender& operator=(KawaiiRender &another);
  KawaiiRender& operator=(KawaiiRender &&another);

  ~KawaiiRender() = default;

  KawaiiRendererImpl *create(KawaiiRoot *root);

  bool isApiVerCompatible(uint16_t major, uint16_t minor, uint32_t patch) const;

  bool isAvaliable();

  inline bool isInvalid()
  { return !isAvaliable(); }

  static KawaiiRender& getPreferredRender(const QStringList &request = {});
  static QStringList getAvailableRenderers();



  //IMPLEMENT
private:
  static std::vector<KawaiiRender> loadedRenders;
  static KawaiiRender *preferredRenderer;

  QByteArray createRenderFuncName;
  QByteArray checkSystemFuncName;

  CreateRenderFunc createRenderFunc;
  CheckSystemFunc checkSystemFunc;

  SemanticVersion apiVer;

  void load();

  static KawaiiRender* findRenderer(const QStringList &request) noexcept;
  static void selectRenderer() noexcept;
};

#endif // KAWAIIRENDER_HPP
