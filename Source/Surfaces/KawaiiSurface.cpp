#include "Exceptions/KawaiiRootMismatchException.hpp"
#include "KawaiiSurfaceTile.hpp"
#include "KawaiiConfig.hpp"
#include "KawaiiRoot.hpp"

#include <QCoreApplication>
#include <QResizeEvent>

KawaiiSurface::KawaiiSurface(KawaiiRenderpass *renderpass):
  renderFunc(nullptr),
  wnd(nullptr),
  root(KawaiiRoot::getRoot(renderpass)),
  frametime_ms(nullptr),
  rendertime_ms(nullptr),
  pendingUpdates(0)
{
  wnd = std::make_unique<QWindow>();
  wnd->setFormat(KawaiiConfig::getInstance().getPreferredSfcFormat());
  onSfcResized(wnd->size());
  connect(wnd.get(), &QObject::destroyed, this, &QObject::deleteLater);
  wnd->installEventFilter(this);

  addTile(QRect(0,0, 0,0), QRectF(0,0, 1,1), renderpass);
}

KawaiiSurface::~KawaiiSurface()
{
  detachRenderFuncs();
}

bool KawaiiSurface::eventFilter(QObject *obj, QEvent *event)
{
  if(obj != wnd.get())
    return false;

  const bool continuouslyMode = (KawaiiConfig::getInstance().getPreferredSfcFormat().swapInterval() == 0) /*&& wnd->isActive()*/;
  switch (event->type())
    {
    case QEvent::UpdateRequest:
      if(!continuouslyMode)
        render();
      else
        render_continuously();
      break;

//    case QEvent::UpdateLater:
//      requestUpdate();
//      break;

    case QEvent::Expose:
      render();
      if(continuouslyMode)
        requestUpdate();
      break;

    case QEvent::Resize:
      onSfcResized(static_cast<QResizeEvent*>(event)->size());
      break;

    default: break;
    }
  return false;
}

KawaiiRoot *KawaiiSurface::getRoot() const
{
  return root;
}

void KawaiiSurface::setFrametimeCounter(float *counter)
{
  if(frametime_ms && counter)
    *counter = *frametime_ms;
  frametime_ms = counter;
}

float *KawaiiSurface::getFrametimeCounter() const
{
  return frametime_ms;
}

void KawaiiSurface::setRendertimeCounter(float *counter)
{
  if(rendertime_ms && counter)
    *counter = *rendertime_ms;
  rendertime_ms = counter;
}

float *KawaiiSurface::getRendertimeCounter() const
{
  return rendertime_ms;
}

void KawaiiSurface::setRenderFunc(const std::function<void ()> &func)
{
  renderFunc = func;
}

void KawaiiSurface::detachRenderFuncs()
{
  if(renderFunc)
    renderFunc = {};
}

void KawaiiSurface::removeTiles(size_t first, size_t n)
{
  tiles.erase(tiles.begin() + first, tiles.begin() + first + n);
  emit tilesRemoved(first, n);
}

void KawaiiSurface::addTile(const QRect &absRect, const QRectF &relRect, KawaiiRenderpass *renderpass)
{
  auto renderpassRoot = KawaiiRoot::getRoot(renderpass->parent());
  if(!tiles.empty())
    {
      if(renderpassRoot != root)
        throw KawaiiRootMismatchException();
    } else
    setRoot(renderpassRoot);

  tiles.push_back(std::make_unique<Tile>(absRect, relRect, renderpass));
  tiles.back()->setSfc(this);
  emit tileAdded(*tiles.back());
}

void KawaiiSurface::swapTiles(size_t index0, size_t index1)
{
  swap(tiles[index0], tiles[index1]);
  emit tilesSwaped(index0, index1);
}

void KawaiiSurface::resize(const QSize &size)
{
  wnd->resize(size);
}

QSize KawaiiSurface::getSize() const
{
  return wnd->size();
}

void KawaiiSurface::show() const
{
  wnd->show();
}

void KawaiiSurface::showNormal() const
{
  wnd->showNormal();
}

void KawaiiSurface::showMinimized() const
{
  wnd->showMinimized();
}

void KawaiiSurface::showMaximized() const
{
  wnd->showMaximized();
}

void KawaiiSurface::showFullScreen() const
{
  wnd->showFullScreen();
}

void KawaiiSurface::onSfcResized(const QSize &s)
{
  for(const auto &tile: tiles)
    tile->onParentResized(s.width(), s.height());

  emit resized(s);
}

void KawaiiSurface::onTileRenderpassChanged(const Tile &tile, KawaiiRenderpass *renderpass)
{
  auto el = std::find_if(tiles.begin(), tiles.end(),
                         [&tile] (const std::unique_ptr<Tile> &i) { return i.get() == &tile; });

  if(el == tiles.end())
    return;

  auto renderpassRoot = renderpass? KawaiiRoot::getRoot(renderpass->parent()): nullptr;
  if(tiles.size() > 1)
    {
      if(renderpassRoot != root)
        throw KawaiiRootMismatchException();
    } else
    setRoot(renderpassRoot);

  emit renderpassChanged(el - tiles.begin(), renderpass);
}

void KawaiiSurface::setRoot(KawaiiRoot *root)
{
  if(this->root != root)
    {
      this->root = root;
      emit rootChanged(root);
    }
}

void KawaiiSurface::render_frame()
{
  if(!renderFunc)
    {
      if(frametime_ms)
        *frametime_ms = -1;

      if(rendertime_ms)
        *rendertime_ms = -1;

      return;
    }
  using namespace std;

  chrono::time_point<chrono::steady_clock, chrono::nanoseconds> t1, t2;
  if(rendertime_ms)
    t1 = chrono::steady_clock::now();

  renderFunc();

  if(frametime_ms || rendertime_ms)
    t2 = chrono::steady_clock::now();

  if(frametime_ms)
    {
      const int elapsed = chrono::duration_cast<chrono::microseconds>(t2-timestamp).count();
      *frametime_ms = 0.001 * elapsed;
    }

  if(rendertime_ms)
    {
      const int elapsed = chrono::duration_cast<chrono::microseconds>(t2-t1).count();
      *rendertime_ms = 0.001 * elapsed;
    }

  timestamp = t2;
}

void KawaiiSurface::render()
{
  if(wnd->isExposed())
    render_frame();
}

void KawaiiSurface::render_continuously()
{
  while(wnd->isExposed() /*&& wnd->isActive()*/)
    {
      render_frame();
      QCoreApplication::instance()->processEvents();
    }
  if(wnd->isExposed())
    requestUpdate();
}

void KawaiiSurface::requestUpdate()
{
  if(frametime_ms)
    timestamp = std::chrono::steady_clock::now();

  wnd->requestUpdate();
}
