#include "KawaiiRender.hpp"
#include "KawaiiConfig.hpp"

#include "Exceptions/KawaiiParseExcep.hpp"
#include "Exceptions/KawaiiPluginFailed.hpp"
#include "Exceptions/KawaiiNoRenderExcep.hpp"

#include <QProcessEnvironment>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDir>

KawaiiRender::KawaiiRender(const QString &jsonFileInfo):
  KawaiiPlugin(jsonFileInfo),
  createRenderFunc(nullptr),
  checkSystemFunc(nullptr),
  apiVer {0, 0, 9}
{
  //Read render
  if(auto render = getConfig().value("render"); render.isObject())
    {
      auto obj = render.toObject();
      createRenderFuncName = obj.value("create_render_func").toString("createRender").toLatin1();
      checkSystemFuncName = obj.value("check_system_func").toString("checkSystem").toLatin1();
      apiVer = SemanticVersion::fromString(obj.value("api_version").toString(QString("%1.%2.%3").arg(apiVer.maj, apiVer.min, apiVer.patch)));
    } else
    throw KawaiiPluginFailed(getPluginName(), "parse (missing \"render\" section)");
}

KawaiiRender::KawaiiRender(KawaiiRender &another):
  KawaiiRender(another.getConfigFilePath())
{ }

KawaiiRender::KawaiiRender(KawaiiRender &&another):
  KawaiiPlugin(another),
  createRenderFuncName(std::move(another.createRenderFuncName)),
  checkSystemFuncName(std::move(another.checkSystemFuncName)),
  createRenderFunc(another.createRenderFunc),
  checkSystemFunc(another.checkSystemFunc)
{
  another.createRenderFunc = nullptr;
  another.checkSystemFunc = nullptr;
}

KawaiiRender &KawaiiRender::operator=(KawaiiRender &another)
{
  *this = KawaiiRender(another);
  return *this;
}

KawaiiRender &KawaiiRender::operator=(KawaiiRender &&another)
{
  createRenderFuncName = std::move(another.createRenderFuncName);
  checkSystemFuncName = std::move(another.checkSystemFuncName);

  createRenderFunc = another.createRenderFunc;
  checkSystemFunc = another.checkSystemFunc;

  another.createRenderFunc = nullptr;
  another.checkSystemFunc = nullptr;
  KawaiiPlugin::operator=(std::move(another));

  return *this;
}

KawaiiRendererImpl *KawaiiRender::create(KawaiiRoot *root)
{
  if(!createRenderFunc)
    load();

  return createRenderFunc(root);
}

bool KawaiiRender::isApiVerCompatible(uint16_t major, uint16_t minor, uint32_t patch) const
{
  return apiVer.isCompatible(SemanticVersion(major, minor, patch));
}

bool KawaiiRender::isAvaliable()
{
  if(!checkSystemFunc)
    load();

  return checkSystemFunc();
}

KawaiiRender& KawaiiRender::getPreferredRender(const QStringList &request)
{
  KawaiiRender *result = nullptr;
  if(KawaiiConfig::getInstance().getPreferredRenderers().isEmpty())
    result = findRenderer(request);
  if(result)
    return *result;

  if(!preferredRenderer)
    throw KawaiiNoRenderExcep();
  return *preferredRenderer;
}

QStringList KawaiiRender::getAvailableRenderers()
{
  QStringList result;
  result.reserve(loadedRenders.size());
  for(auto &el: loadedRenders)
    result.push_back(el.getPluginName());
  return result;
}

void KawaiiRender::load()
{
  createRenderFunc = reinterpret_cast<CreateRenderFunc>(resolveFunction(createRenderFuncName));
  checkSystemFunc = reinterpret_cast<CheckSystemFunc>(resolveFunction(checkSystemFuncName));
}

KawaiiRender *KawaiiRender::findRenderer(const QStringList &request) noexcept
{
  for(auto i = loadedRenders.begin(); i != loadedRenders.end(); )
    {
      if(i->isAvaliable())
        ++i;
      else
        i = loadedRenders.erase(i);
    }

  for(const auto &str: request)
    {
      auto el = std::find_if(loadedRenders.begin(), loadedRenders.end(),
                             [&str] (const KawaiiRender &i) { return i.getPluginName().compare(str, Qt::CaseInsensitive) == 0; });

      if(el != loadedRenders.end())
        return &(*el);
    }
  return nullptr;
}

void KawaiiRender::selectRenderer() noexcept
{
  preferredRenderer = findRenderer(KawaiiConfig::getInstance().getPreferredRenderers());
  if(preferredRenderer)
    return;

  static const auto rndr_ok = std::bind(&KawaiiRender::isAvaliable, std::placeholders::_1);
  auto el = std::find_if(loadedRenders.begin(), loadedRenders.end(), rndr_ok);
  if(el == loadedRenders.end())
    preferredRenderer = nullptr;
  else
    preferredRenderer = &(*el);
}
