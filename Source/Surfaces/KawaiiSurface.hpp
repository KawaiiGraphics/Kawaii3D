#ifndef KAWAIISURFACE_HPP
#define KAWAIISURFACE_HPP

#include "../Renderpass/KawaiiRenderpass.hpp"
#include <functional>
#include <QPainter>
#include <QWindow>

class KawaiiRoot;

class KAWAII3D_SHARED_EXPORT KawaiiSurface: public KawaiiDataUnit
{
  Q_OBJECT
public:
  class Tile;

  KawaiiSurface(KawaiiRenderpass *renderpass);
  ~KawaiiSurface();

  inline const Tile& getTile(int index) const
  { return *tiles[index]; }

  inline Tile& getTile(int index)
  { return *tiles[index]; }

  inline size_t tileCount() const
  { return tiles.size(); }

  void removeTiles(size_t first, size_t n);
  void addTile(const QRect &absRect, const QRectF &relRect, KawaiiRenderpass *renderpass);
  void swapTiles(size_t index0, size_t index1);

  void resize(const QSize &size);
  QSize getSize() const;

  void show() const;
  void showNormal() const;
  void showMinimized() const;
  void showMaximized() const;
  void showFullScreen() const;

  inline QWindow& getWindow() const
  { return *wnd; }

  KawaiiRoot *getRoot() const;

  void setFrametimeCounter(float *counter);
  float* getFrametimeCounter() const;

  void setRendertimeCounter(float *counter);
  float* getRendertimeCounter() const;

  /// Should be called from renderer
  void setRenderFunc(const std::function<void()> &func);

  /// Should be called from renderer
  void detachRenderFuncs();

  // QObject interface
  bool eventFilter(QObject *, QEvent *event) override;


signals:
  void rootChanged(KawaiiRoot *root);
  void renderpassChanged(size_t tileIndex, KawaiiRenderpass *renderpass);

  void tilesRemoved(size_t first, size_t n);
  void tileAdded(const Tile &tile);
  void tilesSwaped(size_t index0, size_t index1);

  void resized(const QSize &size);



  //IMPLEMENT
private:
  std::function<void()> renderFunc;
  std::vector<std::unique_ptr<Tile>> tiles;
  std::unique_ptr<QWindow> wnd;
  std::chrono::time_point<std::chrono::steady_clock> timestamp;
  KawaiiRoot *root;
  float *frametime_ms;
  float *rendertime_ms;
  uint16_t pendingUpdates;

  void setRoot(KawaiiRoot *root);
  void onSfcResized(const QSize &s);
  void onTileRenderpassChanged(const Tile &tile, KawaiiRenderpass *renderpass);

  void render_frame();
  void render();
  void render_continuously();

  void requestUpdate();
};
#endif // KAWAIISURFACE_HPP

#include "KawaiiSurfaceTile.hpp"
