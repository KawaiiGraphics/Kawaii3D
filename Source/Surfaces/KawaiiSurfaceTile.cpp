#include <cmath>
#include "KawaiiSurfaceTile.hpp"

KawaiiSurface::Tile::Tile():
  sfc(nullptr),
  parentW(0), parentH(0)
{
}

KawaiiSurface::Tile::Tile(const QRect &abs, const QRectF &rel, KawaiiRenderpass *renderpass):
  Tile()
{
  rectAbs = abs;
  rectRel = rel;

  onRectChanged();
  setRenderpass(renderpass);
}

KawaiiSurface::Tile::~Tile()
{
}

void KawaiiSurface::Tile::onParentResized(int w, int h)
{
  parentW = w;
  parentH = h;

  onRectChanged();
}

const QRect& KawaiiSurface::Tile::getRectAbs() const
{
  return rectAbs;
}

void KawaiiSurface::Tile::setRectAbs(const QRect &value)
{
  rectAbs = value;
  onRectChanged();
}

const QRectF& KawaiiSurface::Tile::getRectRel() const
{
  return rectRel;
}

void KawaiiSurface::Tile::setRectRel(const QRectF &value)
{
  rectRel = value;
  onRectChanged();
}

const QRect &KawaiiSurface::Tile::getResultRect() const
{
  return resultRect;
}

QPointF KawaiiSurface::Tile::mapCoord(const QPointF &p) const
{
  return QPointF(resultRect.left() + p.x() * static_cast<float>(size.x),
                 resultRect.top() + p.y() * static_cast<float>(size.y));
}

QPointF KawaiiSurface::Tile::unmapCoord(const QPointF &p) const
{
  glm::vec2 coord_vec(p.x(), p.y());
  coord_vec.x -= resultRect.left();
  coord_vec.y -= resultRect.top();

  coord_vec.x /= 0.5 * static_cast<float>(size.x);
  coord_vec.y /= -0.5 * static_cast<float>(size.y);

  coord_vec.x -= 1.0;
  coord_vec.y += 1.0;
  return QPointF(coord_vec.x, coord_vec.y);
}

void KawaiiSurface::Tile::setRenderpass(KawaiiRenderpass *newRenderpass)
{
  if(newRenderpass != getRenderpass())
    {
      KawaiiRenderTarget::setRenderpass(newRenderpass);
      if(renderpassChangedCallback)
        renderpassChangedCallback(*this, newRenderpass);
    }
}

void KawaiiSurface::Tile::onRectChanged()
{
  resultRect = QRect(rectAbs.x() + rectRel.x() * parentW,
                     rectAbs.y() + rectRel.y() * parentH,
                     rectAbs.width()  + rectRel.width() * parentW,
                     rectAbs.height() + rectRel.height() * parentH);

  size = { resultRect.width(), resultRect.height() };
  if(size.x <= 0 || size.y <= 0)
    size = {1, 1};

  setRenderableSize(size);
}

void KawaiiSurface::Tile::setSfc(KawaiiSurface *newSfc)
{
  sfc = newSfc;
  if(sfc)
    {
      renderpassChangedCallback = std::bind(&KawaiiSurface::onTileRenderpassChanged, sfc, std::placeholders::_1, std::placeholders::_2);
      parentW = sfc->getWindow().width();
      parentH = sfc->getWindow().height();
    } else
    {
      renderpassChangedCallback = {};
      parentW = parentH = 0;
    }
  onRectChanged();
}
