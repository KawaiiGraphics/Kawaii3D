#ifndef KAWAIICAMERA_HPP
#define KAWAIICAMERA_HPP

#include "Shaders/KawaiiScene.hpp"
#include "KawaiiGpuBuf.hpp"
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>

class KAWAII3D_SHARED_EXPORT KawaiiCamera: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiCamera);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);
public:
  struct GpuBuffer {
    glm::vec4 pos;
    glm::mat4 view;
    glm::mat4 viewInverted;
    glm::mat3 normal;
  };

  KawaiiCamera();
  ~KawaiiCamera() = default;

  const GpuBuffer* getBuffer() const;

  inline static constexpr size_t getBufferSize()
  { return sizeof(GpuBuffer); }

  inline auto getUniforms() const
  { return &uniforms; }

  const glm::mat4 &getViewMat() const;
  const glm::mat4 &getViewMatInverted() const;
  const glm::mat3 &getNormalMat() const;

  glm::vec3 getUpVec() const;
  glm::vec3 getEyePos() const;
  glm::vec3 getViewCenter() const;
  glm::vec3 getViewDir() const;

  QVector3D getQUpVec() const;
  QVector3D getQEyePos() const;
  QVector3D getQViewCenter() const;
  QVector3D getQViewDir() const;

  QMatrix4x4 getQViewMat() const;
  QMatrix4x4 getQViewMatInverted() const;
  QMatrix4x4 getQNormalMat() const;

  void setViewMat(const glm::mat4 &mat);
  void setViewMat(const QMatrix4x4 &mat);

  void transform(const glm::mat4 &trMat);
  void transform(const QMatrix4x4 &trMat);

  glm::mat4 getViewProjectionMat(const glm::mat4 &proj) const;

  inline KawaiiScene* getScene() const
  { return scene; }

signals:
  void viewMatUpdated();
  void sceneChanged(KawaiiScene *scene);
  void drawPipelineChanged();
  void askedRedraw();



  //IMPLEMENT
private:
  KawaiiGpuBuf uniforms;
  GpuBuffer *gpuBuffer;
  KawaiiScene *scene;

  void updateViewMatrix();
  void onParentChanged();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &memento) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &memento) const override;

  void read(sib_utils::memento::Memento::DataReader &memento) override;
};

#endif // KAWAIICAMERA_HPP
