#include "KawaiiImplFactory.hpp"
#include <QChildEvent>
#include <algorithm>

KawaiiImplFactory::KawaiiImplFactory(QObject *parent):
  QObject(parent)
{
}

KawaiiImplFactory::~KawaiiImplFactory()
{
}

KawaiiRendererImpl* KawaiiImplFactory::createRenderer(KawaiiDataUnit &data)
{
  //If current renderer impl is compatible with this factory, return it without changes
  if(checkCompatibility(data))
    return data.rendererImpl.data();

  auto dataType = data.metaObject();

  //Find appropriate renderer creator
  auto el = creators.find(dataType);

  //Find renderer creator for base class
  if(el == creators.end())
    for(el = creators.begin(); el != creators.end(); ++el)
      if(dataType->inherits(el.key()))
        {
          //Cache
          el = creators.insert(dataType, el.value());
          break;
        }

  if(el == creators.end())
    return nullptr;

  auto d = el.value()(&data); //Create renderer impl
  auto d0 = data.rendererImpl.data();

  if(d)
    d->factory = this;

  data.rendererImpl = d;
  if(d0)
    delete d0;
  return d;
}

bool KawaiiImplFactory::checkCompatibility(const KawaiiDataUnit &data)
{
  auto dataType = data.metaObject();

  auto el = metaObjects.find(dataType);
  if(el == metaObjects.end())
    for(el = metaObjects.begin(); el != metaObjects.end(); ++el)
      if(dataType->inherits(el.key()))
        {
          el = metaObjects.insert(dataType, el.value());
          break;
        }

  if(el == metaObjects.end())
    return !data.rendererImpl;
  return data.rendererImpl && el.value() == data.rendererImpl->metaObject();
}
