#include "KawaiiShaderEffect.hpp"
#include "Textures/KawaiiTexture.hpp"
#include "KawaiiRoot.hpp"

// Need redraw:
//  when camera UBO changed;
//  when surface UBO changed;
//  (when input GBufs changed) (global);

KawaiiShaderEffect::KawaiiShaderEffect(KawaiiProgram *effect):
  effect(nullptr),
  sfcUbo(nullptr),
  cam(nullptr),
  root(nullptr)
{
  setEffect(effect);
  connect(this, &sib_utils::TreeNode::parentUpdated, this, &KawaiiShaderEffect::onParentChanged);
}

KawaiiProgram *KawaiiShaderEffect::getEffect() const
{
  return effect;
}

void KawaiiShaderEffect::setEffect(KawaiiProgram *newEffect)
{
  if(newEffect != effect)
    {
      if(effect)
        disconnect(effect, &QObject::destroyed, this, &KawaiiShaderEffect::detachEffect);
      effect = newEffect;
      if(effect)
        connect(effect, &QObject::destroyed, this, &KawaiiShaderEffect::detachEffect);
      askRefresh();
    }
}

void KawaiiShaderEffect::detachEffect()
{
  setEffect(nullptr);
}

KawaiiGpuBuf *KawaiiShaderEffect::getSurfaceUBO() const
{
  return sfcUbo;
}

void KawaiiShaderEffect::setSurfaceUBO(KawaiiGpuBuf *newSfcUbo)
{
  if(newSfcUbo == sfcUbo) return;

  if(sfcUbo)
    {
      disconnect(sfcUbo, &QObject::destroyed, this, &KawaiiShaderEffect::detachSurfaceUBO);
      disconnect(sfcUbo, &KawaiiGpuBuf::updated, this, &KawaiiShaderEffect::askRedraw);
      disconnect(sfcUbo, &KawaiiGpuBuf::textureBinded, this, &KawaiiShaderEffect::askRefresh);
    }
  sfcUbo = newSfcUbo;
  if(sfcUbo)
    {
      connect(sfcUbo, &QObject::destroyed, this, &KawaiiShaderEffect::detachSurfaceUBO);
      connect(sfcUbo, &KawaiiGpuBuf::updated, this, &KawaiiShaderEffect::askRedraw);
      connect(sfcUbo, &KawaiiGpuBuf::textureBinded, this, &KawaiiShaderEffect::askRefresh);
    }
  askRefresh();
}

void KawaiiShaderEffect::detachSurfaceUBO()
{
  setSurfaceUBO(nullptr);
}

KawaiiCamera *KawaiiShaderEffect::getCamera() const
{
  return cam;
}

void KawaiiShaderEffect::setCamera(KawaiiCamera *newCam)
{
  if(newCam == cam) return;

  if(cam)
    {
      disconnect(cam, &QObject::destroyed, this, &KawaiiShaderEffect::detachCamera);
//      disconnect(cam, &KawaiiCamera::drawPipelineChanged, this, &KawaiiShaderEffect::askRefresh);
      disconnect(cam->getUniforms(), &KawaiiGpuBuf::updated, this, &KawaiiShaderEffect::askRedraw);
    }
  cam = newCam;
  if(cam)
    {
      connect(cam, &QObject::destroyed, this, &KawaiiShaderEffect::detachCamera);
//      connect(cam, &KawaiiCamera::drawPipelineChanged, this, &KawaiiShaderEffect::askRefresh);
      connect(cam->getUniforms(), &KawaiiGpuBuf::updated, this, &KawaiiShaderEffect::askRedraw);
    }
  askRefresh();
}

void KawaiiShaderEffect::detachCamera()
{
  setCamera(nullptr);
}

void KawaiiShaderEffect::onParentChanged()
{
  auto newRoot = KawaiiRoot::getRoot(parent());
  bool rootChanged = newRoot != root;
  if(!rootChanged)
    return;
  if(root)
    disconnect(root, &KawaiiRoot::bindedBufferUpdated, this, &KawaiiShaderEffect::onBindedBufferUpdated);
  root = newRoot;
  if(root)
    connect(root, &KawaiiRoot::bindedBufferUpdated, this, &KawaiiShaderEffect::onBindedBufferUpdated);
}

void KawaiiShaderEffect::onBindedBufferUpdated(uint32_t binding, KawaiiBufferTarget target)
{
  if(getEffect())
    {
      bool redraw = target != KawaiiBufferTarget::UBO;
      if(!redraw)
        {
          getEffect()->forallModules([&redraw, binding] (KawaiiShader *sh) {
              if(sh->getGlobalUboBlockEnd(binding) != -1)
                redraw = true;
            });
        }
      if(redraw)
        askRedraw();
    }
}

void KawaiiShaderEffect::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  KawaiiImageSource::writeBinary(mem);
  mem.setLink(QStringLiteral("effect"), effect)
      .setLink(QStringLiteral("surfaceUbo"), sfcUbo)
      .setLink(QStringLiteral("camera"), cam);
}

void KawaiiShaderEffect::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiShaderEffect::read(sib_utils::memento::Memento::DataReader &mem)
{
  KawaiiImageSource::read(mem);
  mem.read(QStringLiteral("effect"), [this] (sib_utils::TreeNode *node) {
    if(auto effect = qobject_cast<KawaiiProgram*>(node); effect)
      setEffect(effect);
  })
  .read(QStringLiteral("surfaceUbo"), [this] (sib_utils::TreeNode *node) {
    if(auto ubo = qobject_cast<KawaiiGpuBuf*>(node); ubo)
      setSurfaceUBO(ubo);
  })
  .read(QStringLiteral("camera"), [this] (sib_utils::TreeNode *node) {
    if(auto cam = qobject_cast<KawaiiCamera*>(node); cam)
      setCamera(cam);
  });
}
