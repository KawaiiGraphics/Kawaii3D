#ifndef KAWAIIRENDERTARGET_HPP
#define KAWAIIRENDERTARGET_HPP

#include "../KawaiiProjection.hpp"
#include "KawaiiRenderpass.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiRenderTarget
{
  KawaiiRenderTarget(const KawaiiRenderTarget &orig) = delete;
  KawaiiRenderTarget& operator=(const KawaiiRenderTarget &orig) = delete;

public:
  bool renderingEnabled;

  KawaiiRenderTarget();
  KawaiiRenderTarget(KawaiiRenderTarget &&orig);
  KawaiiRenderTarget& operator=(KawaiiRenderTarget &&orig);
  virtual ~KawaiiRenderTarget();

  const glm::uvec2 &getRenderableSize() const;

  KawaiiRenderpass *getRenderpass() const;
  virtual void setRenderpass(KawaiiRenderpass *newRenderpass);
  void detachRenderpass();

protected:
  void setRenderableSize(const glm::uvec2 &newRenderableSize);



  // IMPLEMENT
private:
  glm::uvec2 renderableSize;
  KawaiiRenderpass *renderpass;
};

#endif // KAWAIIRENDERTARGET_HPP
