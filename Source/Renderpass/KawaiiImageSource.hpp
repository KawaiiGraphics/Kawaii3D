#ifndef KAWAIIIMAGESOURCE_HPP
#define KAWAIIIMAGESOURCE_HPP

#include "../KawaiiDataUnit.hpp"
#include <glm/vec2.hpp>
#include <functional>
#include <QSize>

class KawaiiRenderpass;

class KAWAII3D_SHARED_EXPORT KawaiiImageSource: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiImageSource);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  friend class KawaiiRenderpass;

public:
  enum class BlendFactor: uint8_t
  {
    FactorZero = 0,
    FactorOne = 1,
    FactorSrcAlpha = 2,
    FactorOneMinusSrcAlpha = 3,
    FactorDstAlpha = 4,
    FactorOneMinusDstAlpha = 5,
  };

  enum class DepthFunc: uint8_t
  {
    Never = 0,
    Less = 1,
    LessEqual = 2,
    Equal = 3,
    Greater = 4,
    GreaterEqual = 5,
    Always = 6
  };

  KawaiiImageSource();

  bool isRefreshNeeded() const;
  bool isRedrawNeeded() const;

  /// Should be called from Renderer
  void markRefreshed();

  /// Should be called from Renderer
  void markRedrawn();

  BlendFactor getSrcColorFactor() const;
  void setSrcColorFactor(BlendFactor newSrcColorFactor);

  BlendFactor getDstColorFactor() const;
  void setDstColorFactor(BlendFactor newDstColorFactor);

  BlendFactor getSrcAlphaFactor() const;
  void setSrcAlphaFactor(BlendFactor newSrcAlphaFactor);

  BlendFactor getDstAlphaFactor() const;
  void setDstAlphaFactor(BlendFactor newDstAlphaFactor);

  DepthFunc getDepthTestFunc() const;
  void setDepthTestFunc(DepthFunc newDepthTestFunc);

  bool getDepthTest() const;
  void setDepthTest(bool newDepthTest);

  bool getDepthWrite() const;
  void setDepthWrite(bool newDepthWrite);

  bool getBlendEnabled() const;
  void setBlendEnabled(bool newBlendEnabled);

  KawaiiRenderpass *getOwner() const;
  quint32 getSubpass() const;

  const glm::uvec2 &getRenderableSize() const;

private:
  void setRenderableSize(const glm::uvec2 &newRenderableSize);

signals:
  void refreshRequest();
  void redrawRequest();
  void renderableSizeChanged();

protected:
  void askRefresh();
  void askRedraw();



  //IMPLEMENT
private:
  KawaiiRenderpass *owner;

  glm::uvec2 renderableSize;

  quint32 subpass;

  BlendFactor srcColorFactor;
  BlendFactor dstColorFactor;
  BlendFactor srcAlphaFactor;
  BlendFactor dstAlphaFactor;

  DepthFunc depthTestFunc;

  bool depthTest;
  bool depthWrite;

  bool blendEnabled;

  bool needRefresh;
  bool needRedraw;

  void setOwner(KawaiiRenderpass *newOwner, quint32 newSubpass);

  inline virtual void onRenderableSizeChanged() {}

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;
};

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiImageSource::BlendFactor blendFactor);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiImageSource::BlendFactor &blendFactor);

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiImageSource::DepthFunc depthFunc);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiImageSource::DepthFunc &depthFunc);

#endif // KAWAIIIMAGESOURCE_HPP
