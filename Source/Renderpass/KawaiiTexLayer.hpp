#ifndef KAWAIITEXLAYER_HPP
#define KAWAIITEXLAYER_HPP

#include "KawaiiImageSource.hpp"
#include "../Textures/KawaiiTexture.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiTexLayer: public KawaiiImageSource
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiTexLayer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiTexLayer(KawaiiTexture *texture);
  KawaiiTexLayer();

  KawaiiTexture *getTexture() const;
  void setTexture(KawaiiTexture *newTexture);
  void detachTexture();

  bool isKeepAspectRatio() const;
  void setKeepAspectRatio(bool newKeepAspectRatio);

signals:
  void keepAspectRatioChanged(bool keepAspectRatio);



  // IMPLEMENT
private:
  KawaiiTexture *tex;
  bool keepAspectRatio;
  void onTexResolutionChanged();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;
};

#endif // KAWAIITEXLAYER_HPP
