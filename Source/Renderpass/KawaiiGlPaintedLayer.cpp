#include "KawaiiGlPaintedLayer.hpp"

KawaiiGlPaintedLayer::KawaiiGlPaintedLayer(const std::function<bool ()> &checkDirty,
                                           const std::function<void (QOpenGLContext *)> &redraw):
  checkDirty(checkDirty),
  redraw(redraw)
{
  setBlendEnabled(true);
}

KawaiiGlPaintedLayer::KawaiiGlPaintedLayer():
  KawaiiGlPaintedLayer({}, {})
{ }

bool KawaiiGlPaintedLayer::isRedrawNeeded(const std::function<QOpenGLContext *()> &ctxGetter) const
{
  if(!ctxGetter || !redraw)
    return false;

  const bool dirty = !checkDirty || checkDirty();
  return dirty;
}

const std::function<bool ()> &KawaiiGlPaintedLayer::getCheckDirty() const
{
  return checkDirty;
}

void KawaiiGlPaintedLayer::setCheckDirty(const std::function<bool ()> &newCheckDirty)
{
  checkDirty = newCheckDirty;
}

const std::function<void (QOpenGLContext *)> &KawaiiGlPaintedLayer::getRedraw() const
{
  return redraw;
}

void KawaiiGlPaintedLayer::setRedraw(const std::function<void (QOpenGLContext *)> &newRedraw)
{
  redraw = newRedraw;
}
