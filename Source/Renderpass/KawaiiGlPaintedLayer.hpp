#ifndef KAWAIIGLPAINTEDLAYER_HPP
#define KAWAIIGLPAINTEDLAYER_HPP

#include "KawaiiImageSource.hpp"
#include <QOpenGLContext>
#include <functional>

class KAWAII3D_SHARED_EXPORT KawaiiGlPaintedLayer : public KawaiiImageSource
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiGlPaintedLayer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiGlPaintedLayer(const std::function<bool()> &checkDirty,
                      const std::function<void(QOpenGLContext*)> &redraw);
  KawaiiGlPaintedLayer();

  bool isRedrawNeeded(const std::function<QOpenGLContext*()> &ctxGetter) const;

  const std::function<bool()> &getCheckDirty() const;
  void setCheckDirty(const std::function<bool()> &newCheckDirty);

  const std::function<void(QOpenGLContext *)> &getRedraw() const;
  void setRedraw(const std::function<void(QOpenGLContext *)> &newRedraw);

  using KawaiiImageSource::askRedraw;



  // IMPLEMENT
private:
  std::function<bool()> checkDirty;
  std::function<void(QOpenGLContext*)> redraw;
};

#endif // KAWAIIGLPAINTEDLAYER_HPP
