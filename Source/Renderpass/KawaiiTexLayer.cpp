#include "KawaiiTexLayer.hpp"
#include <sib_utils/qDataStreamExpansion.hpp>

KawaiiTexLayer::KawaiiTexLayer(KawaiiTexture *texture):
  tex(nullptr),
  keepAspectRatio(false)
{
  setBlendEnabled(true);
  setDepthWrite(false);
  setDepthTest(false);
  setTexture(texture);
}

KawaiiTexLayer::KawaiiTexLayer():
  KawaiiTexLayer(nullptr)
{ }

KawaiiTexture *KawaiiTexLayer::getTexture() const
{
  return tex;
}

void KawaiiTexLayer::setTexture(KawaiiTexture *newTexture)
{
  if(newTexture != tex)
    {
      if(tex)
        {
          disconnect(tex, &QObject::destroyed, this, &KawaiiTexLayer::detachTexture);
          disconnect(tex, &KawaiiTexture::updated, this, &KawaiiTexLayer::askRedraw);
          disconnect(tex, &KawaiiTexture::resolutionChanged, this, &KawaiiTexLayer::onTexResolutionChanged);
        }
      tex = newTexture;
      if(tex)
        {
          connect(tex, &QObject::destroyed, this, &KawaiiTexLayer::detachTexture);
          connect(tex, &KawaiiTexture::updated, this, &KawaiiTexLayer::askRedraw);
          connect(tex, &KawaiiTexture::resolutionChanged, this, &KawaiiTexLayer::onTexResolutionChanged);
        }
      askRefresh();
    }
}

void KawaiiTexLayer::detachTexture()
{
  setTexture(nullptr);
}

bool KawaiiTexLayer::isKeepAspectRatio() const
{
  return keepAspectRatio;
}

void KawaiiTexLayer::setKeepAspectRatio(bool newKeepAspectRatio)
{
  if(newKeepAspectRatio != keepAspectRatio)
    {
      keepAspectRatio = newKeepAspectRatio;
      askRefresh();
      emit keepAspectRatioChanged(keepAspectRatio);
    }
}

void KawaiiTexLayer::onTexResolutionChanged()
{
  if(isKeepAspectRatio())
    askRefresh();
}

void KawaiiTexLayer::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  KawaiiImageSource::writeBinary(mem);
  mem.setLink(QStringLiteral("texture"), getTexture());
}

void KawaiiTexLayer::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiTexLayer::read(sib_utils::memento::Memento::DataReader &mem)
{
  KawaiiImageSource::read(mem);
  mem.read(QStringLiteral("texture"), [this] (TreeNode *node) {
    if(auto tex = qobject_cast<KawaiiTexture*>(node); tex)
      setTexture(tex);
  });
}
