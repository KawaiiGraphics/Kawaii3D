#include "KawaiiSceneLayer.hpp"
#include "KawaiiPerspective.hpp"
#include <glm/gtc/matrix_transform.hpp>

// Need redraw:
//  when camera UBO updated;
//  when surface UBO updated;
//  (when input GBufs changed) (global);
//  when any mesh instance UBO updated;
//  when any mesh instance geometry changed;

KawaiiSceneLayer::KawaiiSceneLayer():
  KawaiiProjectionHandler(&getRenderableSize()),
  sfcUniforms(nullptr),
  sfcGpuData(nullptr),
  cam(nullptr)
{
  sfcUniforms = createChild<KawaiiGpuBuf>(nullptr, getSurfaceBufferSize());
  sfcUniforms->setObjectName(QStringLiteral("SurfaceUniforms"));
  sfcGpuData = static_cast<SurfaceGpuData*>(sfcUniforms->getData());
  sfcGpuData->projectionMat = glm::mat4(1.0);
  sfcGpuData->viewProjectionMat = glm::mat4(1.0);
  setProperty("ExcludedChildren", QVariant::fromValue<QList<QVariant>>({QVariant::fromValue<QObject*>(sfcUniforms)}));
  createProjection<KawaiiPerspective>();
  connect(sfcUniforms, &KawaiiGpuBuf::updated, this, &KawaiiSceneLayer::askRedraw);
}

KawaiiSceneLayer::~KawaiiSceneLayer()
{
}

const glm::mat4 &KawaiiSceneLayer::getProjectionMatrix() const
{
  return sfcGpuData->projectionMat;
}

const glm::mat4 &KawaiiSceneLayer::getViewProjectionMatrix() const
{
  return sfcGpuData->viewProjectionMat;
}

void KawaiiSceneLayer::onProjectionMatChanged()
{
  sfcGpuData->projectionMat = calculateProjectionMatrix();
  emit projectionMatChanged(sfcGpuData->projectionMat);
  calculateViewProjectionMat();
  sfcUniforms->dataChanged(0, sizeof(SurfaceGpuData));
}

void KawaiiSceneLayer::calculateViewProjectionMat()
{
  if(cam)
    sfcGpuData->viewProjectionMat = cam->getViewProjectionMat(getProjectionMatrix());
  else
    sfcGpuData->viewProjectionMat = getProjectionMatrix();
  emit viewProjectionMatChanged(getViewProjectionMatrix());
}

void KawaiiSceneLayer::onViewMatChanged()
{
  calculateViewProjectionMat();
  sfcUniforms->dataChanged(offsetof(SurfaceGpuData, viewProjectionMat), sizeof(SurfaceGpuData::viewProjectionMat));
}

void KawaiiSceneLayer::onRenderableSizeChanged()
{
  KawaiiProjectionHandler::sizeChanged();
}

void KawaiiSceneLayer::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  KawaiiImageSource::writeBinary(mem);
  mem.setLink(QStringLiteral("camera"), cam);
  if(getProjectionPtr())
    getProjectionPtr()->writeBinaryMemento(mem);
}

void KawaiiSceneLayer::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiSceneLayer::read(sib_utils::memento::Memento::DataReader &mem)
{
  KawaiiImageSource::read(mem);
  mem.read(QStringLiteral("camera"), [this] (sib_utils::TreeNode *node) {
    setCamera(qobject_cast<KawaiiCamera*>(node));
  });
  setProjection(KawaiiProjection::createProjection(mem));
}

KawaiiCamera *KawaiiSceneLayer::getCamera() const
{
  return cam;
}

void KawaiiSceneLayer::setCamera(KawaiiCamera *newCam)
{
  if(newCam != cam)
    {
      if(cam)
        {
          disconnect(cam, &QObject::destroyed, this, &KawaiiSceneLayer::detachCamera);
          disconnect(cam, &KawaiiCamera::viewMatUpdated, this, &KawaiiSceneLayer::onViewMatChanged);
          disconnect(cam, &KawaiiCamera::drawPipelineChanged, this, &KawaiiSceneLayer::askRefresh);
          disconnect(cam, &KawaiiCamera::askedRedraw, this, &KawaiiSceneLayer::askRedraw);
        }
      cam = newCam;
      if(cam)
        {
          connect(cam, &QObject::destroyed, this, &KawaiiSceneLayer::detachCamera);
          connect(cam, &KawaiiCamera::viewMatUpdated, this, &KawaiiSceneLayer::onViewMatChanged);
          connect(cam, &KawaiiCamera::drawPipelineChanged, this, &KawaiiSceneLayer::askRefresh);
          connect(cam, &KawaiiCamera::askedRedraw, this, &KawaiiSceneLayer::askRedraw);
        }
      onViewMatChanged();
      askRefresh();
    }
}

void KawaiiSceneLayer::detachCamera()
{
  setCamera(nullptr);
}

QPointF KawaiiSceneLayer::mapCoord(const QVector3D &worldCoord) const
{
  auto A = getViewProjectionMatrix();

  auto mapped = A * glm::vec4(worldCoord.x(), worldCoord.y(), worldCoord.z(), 1.0);
  const auto clipCorrectionInverted = getClipCorrectionInverted();
  if(clipCorrectionInverted != glm::mat4(1.0))
    mapped = clipCorrectionInverted * mapped;

  QPointF resF(0.5 + 0.5 * mapped.x / mapped.w,
               0.5 + 0.5 * mapped.y / mapped.w);
  return QPointF(resF.x() * static_cast<float>(getRenderableSize().x),
                 (1.0 - resF.y()) * static_cast<float>(getRenderableSize().y));
}

QVector3D KawaiiSceneLayer::unmapCoord(const QPointF &coord, float depth) const
{
  glm::vec4 coord_vec(coord.x(), coord.y(), depth, 1.0);

  coord_vec.x /= 0.5 * static_cast<float>(getRenderableSize().x);
  coord_vec.y /= -0.5 * static_cast<float>(getRenderableSize().y);

  coord_vec.x -= 1.0;
  coord_vec.y += 1.0;

  const auto clipCorrection = getClipCorrection();
  if(clipCorrection != glm::mat4(1.0))
    coord_vec = clipCorrection * coord_vec;

  coord_vec.z = depth;
  coord_vec = glm::inverse(getViewProjectionMatrix()) * coord_vec;
  coord_vec /= coord_vec.w;
  return QVector3D(coord_vec.x, coord_vec.y, coord_vec.z);
}

KawaiiGpuBuf *KawaiiSceneLayer::getSurfaceUBO() const
{
  return sfcUniforms;
}
