#include "KawaiiQPainterLayer.hpp"

KawaiiQPainterLayer::KawaiiQPainterLayer(const std::function<bool()> &checkDirty,
                                         const std::function<void(QPainter&)> &redraw):
  checkDirty(checkDirty),
  redraw(redraw)
{
  setBlendEnabled(true);
  setDepthWrite(false);
  setDepthTest(false);
}

KawaiiQPainterLayer::KawaiiQPainterLayer():
  KawaiiQPainterLayer({}, {})
{ }

bool KawaiiQPainterLayer::isRedrawNeeded(const std::function<QPainter*()> &painterCreater) const
{
  if(!painterCreater || !redraw)
    return false;

  const bool dirty = !checkDirty || checkDirty();
  return dirty;
}

const std::function<bool()> &KawaiiQPainterLayer::getCheckDirty() const
{
  return checkDirty;
}

void KawaiiQPainterLayer::setCheckDirty(const std::function<bool()> &newCheckDirty)
{
  checkDirty = newCheckDirty;
}

const std::function<void (QPainter &)> &KawaiiQPainterLayer::getRedraw() const
{
  return redraw;
}

void KawaiiQPainterLayer::setRedraw(const std::function<void (QPainter &)> &newRedraw)
{
  redraw = newRedraw;
}
