#include "KawaiiRenderTarget.hpp"

KawaiiRenderTarget::KawaiiRenderTarget():
  renderingEnabled(true),
  renderableSize(1,1),
  renderpass(nullptr)
{
}

KawaiiRenderTarget::KawaiiRenderTarget(KawaiiRenderTarget &&orig):
  renderableSize(orig.renderableSize),
  renderpass(nullptr)
{
  auto *rp = orig.renderpass;
  orig.detachRenderpass();
  setRenderpass(rp);
}

KawaiiRenderTarget &KawaiiRenderTarget::operator=(KawaiiRenderTarget &&orig)
{
  auto *rp = orig.renderpass;
  orig.detachRenderpass();
  setRenderableSize(orig.renderableSize);
  setRenderpass(rp);
  return *this;
}

KawaiiRenderTarget::~KawaiiRenderTarget()
{
  detachRenderpass();
}

const glm::uvec2 &KawaiiRenderTarget::getRenderableSize() const
{
  return renderableSize;
}

void KawaiiRenderTarget::setRenderableSize(const glm::uvec2 &newRenderableSize)
{
  if(newRenderableSize != renderableSize)
    {
      renderableSize = newRenderableSize;
      if(renderpass)
        renderpass->renderableSizeChanged();
    }
}

KawaiiRenderpass *KawaiiRenderTarget::getRenderpass() const
{
  return renderpass;
}

void KawaiiRenderTarget::setRenderpass(KawaiiRenderpass *newRenderpass)
{
  if(newRenderpass != renderpass)
    {
      if(renderpass)
        {
          if(Q_LIKELY(renderpass->target == this))
            {
              renderpass->target = nullptr;
              renderpass->renderableSizeChanged();
            }
        }
      renderpass = newRenderpass;
      if(renderpass)
        {
          if(Q_LIKELY(renderpass->target != this))
            {
              renderpass->target = this;
              renderpass->renderableSizeChanged();
            }
        }
    }
}

void KawaiiRenderTarget::detachRenderpass()
{
  if(renderpass)
    {
      if(Q_LIKELY(renderpass->target == this))
        {
          renderpass->target = nullptr;
          renderpass->renderableSizeChanged();
        }
      renderpass = nullptr;
    }
}
