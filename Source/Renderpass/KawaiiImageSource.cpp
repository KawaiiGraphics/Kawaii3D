#include "KawaiiImageSource.hpp"
#include "KawaiiRenderpass.hpp"

#include <sib_utils/qDataStreamExpansion.hpp>

#include <QIODevice>

using BlendFactor = KawaiiImageSource::BlendFactor;
using DepthFunc = KawaiiImageSource::DepthFunc;

KawaiiImageSource::KawaiiImageSource():
  owner(nullptr),
  renderableSize(0,0),
  subpass(0),
  srcColorFactor(BlendFactor::FactorSrcAlpha),
  dstColorFactor(BlendFactor::FactorOneMinusSrcAlpha),
  srcAlphaFactor(BlendFactor::FactorZero),
  dstAlphaFactor(BlendFactor::FactorOne),
  depthTestFunc(DepthFunc::Less),
  depthTest(true),
  depthWrite(true),
  blendEnabled(false),
  needRefresh(true),
  needRedraw(true)
{
}

bool KawaiiImageSource::isRefreshNeeded() const
{
  return needRefresh;
}

bool KawaiiImageSource::isRedrawNeeded() const
{
  return needRedraw;
}

void KawaiiImageSource::markRefreshed()
{
  needRefresh = false;
}

void KawaiiImageSource::markRedrawn()
{
  needRedraw = false;
}

void KawaiiImageSource::askRefresh()
{
  if(!needRefresh)
    {
      needRefresh = true;
      emit refreshRequest();
    }
}

void KawaiiImageSource::askRedraw()
{
  if(!needRedraw)
    {
      needRedraw = true;
      emit redrawRequest();
    }
}

void KawaiiImageSource::setOwner(KawaiiRenderpass *newOwner, quint32 newSubpass)
{
  if(newOwner != owner)
    {
      owner = newOwner;
      if(owner)
        setRenderableSize(owner->getRenderableSize());
      else
        setRenderableSize(glm::uvec2(0,0));
    }
  subpass = newSubpass;
}

void KawaiiImageSource::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  QByteArray bytes;
  QDataStream st(&bytes, QIODevice::WriteOnly);
  st << srcColorFactor
     << dstColorFactor
     << srcAlphaFactor
     << dstAlphaFactor
     << depthTestFunc
     << depthTest
     << depthWrite
     << blendEnabled;
  st.setDevice(nullptr);
  mem.setData(QStringLiteral("properties"), bytes);
}

void KawaiiImageSource::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void KawaiiImageSource::read(sib_utils::memento::Memento::DataReader &mem)
{
  mem.read(QStringLiteral("properties"), [this] (const QByteArray &bytes) {
      QDataStream st(bytes);
      st >> srcColorFactor
         >> dstColorFactor
         >> srcAlphaFactor
         >> dstAlphaFactor
         >> depthTestFunc
         >> depthTest
         >> depthWrite
         >> blendEnabled;
      askRefresh();
    });
}

const glm::uvec2 &KawaiiImageSource::getRenderableSize() const
{
  return renderableSize;
}

void KawaiiImageSource::setRenderableSize(const glm::uvec2 &newRenderableSize)
{
  if(newRenderableSize != renderableSize)
    {
      renderableSize = newRenderableSize;
      onRenderableSizeChanged();
      emit renderableSizeChanged();
      askRefresh();
    }
}

KawaiiRenderpass *KawaiiImageSource::getOwner() const
{
  return owner;
}

quint32 KawaiiImageSource::getSubpass() const
{
  return subpass;
}

bool KawaiiImageSource::getBlendEnabled() const
{
  return blendEnabled;
}

void KawaiiImageSource::setBlendEnabled(bool newBlendEnabled)
{
  if(newBlendEnabled != blendEnabled)
    {
      blendEnabled = newBlendEnabled;
      askRefresh();
    }
}

bool KawaiiImageSource::getDepthWrite() const
{
  return depthWrite;
}

void KawaiiImageSource::setDepthWrite(bool newDepthWrite)
{
  if(newDepthWrite != depthWrite)
    {
      depthWrite = newDepthWrite;
      askRefresh();
    }
}

bool KawaiiImageSource::getDepthTest() const
{
  return depthTest;
}

void KawaiiImageSource::setDepthTest(bool newDepthTest)
{
  if(newDepthTest != depthTest)
    {
      depthTest = newDepthTest;
      askRefresh();
    }
}

DepthFunc KawaiiImageSource::getDepthTestFunc() const
{
  return depthTestFunc;
}

void KawaiiImageSource::setDepthTestFunc(DepthFunc newDepthTestFunc)
{
  if(newDepthTestFunc != depthTestFunc)
    {
      depthTestFunc = newDepthTestFunc;
      askRefresh();
    }
}

BlendFactor KawaiiImageSource::getDstAlphaFactor() const
{
  return dstAlphaFactor;
}

void KawaiiImageSource::setDstAlphaFactor(BlendFactor newDstAlphaFactor)
{
  if(newDstAlphaFactor != dstAlphaFactor)
    {
      dstAlphaFactor = newDstAlphaFactor;
      askRefresh();
    }
}

BlendFactor KawaiiImageSource::getSrcAlphaFactor() const
{
  return srcAlphaFactor;
}

void KawaiiImageSource::setSrcAlphaFactor(BlendFactor newSrcAlphaFactor)
{
  if(newSrcAlphaFactor != srcAlphaFactor)
    {
      srcAlphaFactor = newSrcAlphaFactor;
      askRefresh();
    }
}

BlendFactor KawaiiImageSource::getDstColorFactor() const
{
  return dstColorFactor;
}

void KawaiiImageSource::setDstColorFactor(BlendFactor newDstColorFactor)
{
  if(newDstColorFactor != dstColorFactor)
    {
      dstColorFactor = newDstColorFactor;
      askRefresh();
    }
}

BlendFactor KawaiiImageSource::getSrcColorFactor() const
{
  return srcColorFactor;
}

void KawaiiImageSource::setSrcColorFactor(BlendFactor newSrcColorFactor)
{
  if(newSrcColorFactor != srcColorFactor)
    {
      srcColorFactor = newSrcColorFactor;
      askRefresh();
    }
}



KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiImageSource::BlendFactor blendFactor)
{
  return st << static_cast<quint8>(blendFactor);
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiImageSource::BlendFactor &blendFactor)
{
  quint8 byte;
  st >> byte;
  if(st.status() == QDataStream::Ok)
    blendFactor = static_cast<BlendFactor>(byte);
  return st;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiImageSource::DepthFunc depthFunc)
{
  return st << static_cast<quint8>(depthFunc);
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiImageSource::DepthFunc &depthFunc)
{
  quint8 byte;
  st >> byte;
  if(st.status() == QDataStream::Ok)
    depthFunc = static_cast<DepthFunc>(byte);
  return st;
}

