#ifndef KAWAIIQPAINTERLAYER_HPP
#define KAWAIIQPAINTERLAYER_HPP

#include "KawaiiImageSource.hpp"
#include <functional>
#include <QPainter>

class KAWAII3D_SHARED_EXPORT KawaiiQPainterLayer: public KawaiiImageSource
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiQPainterLayer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiQPainterLayer(const std::function<bool()> &checkDirty,
                      const std::function<void(QPainter&)> &redraw);
  KawaiiQPainterLayer();

  bool isRedrawNeeded(const std::function<QPainter*()> &painterCreater) const;

  const std::function<bool()> &getCheckDirty() const;
  void setCheckDirty(const std::function<bool ()> &newCheckDirty);

  const std::function<void(QPainter &)> &getRedraw() const;
  void setRedraw(const std::function<void(QPainter &)> &newRedraw);

  using KawaiiImageSource::askRedraw;



  // IMPLEMENT
private:
  std::function<bool()> checkDirty;
  std::function<void(QPainter&)> redraw;
};

#endif // KAWAIIQPAINTERLAYER_HPP
