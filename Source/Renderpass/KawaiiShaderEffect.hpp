#ifndef KAWAIISHADEREFFECT_HPP
#define KAWAIISHADEREFFECT_HPP

#include "KawaiiImageSource.hpp"
#include "../KawaiiBufferTarget.hpp"
#include "../Shaders/KawaiiProgram.hpp"
#include "../KawaiiGpuBuf.hpp"
#include "../KawaiiCamera.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiShaderEffect: public KawaiiImageSource
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiShaderEffect);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  KawaiiShaderEffect(KawaiiProgram *effect);

  KawaiiProgram *getEffect() const;
  void setEffect(KawaiiProgram *newEffect);
  void detachEffect();

  KawaiiGpuBuf *getSurfaceUBO() const;
  void setSurfaceUBO(KawaiiGpuBuf *newSfcUbo);
  void detachSurfaceUBO();

  KawaiiCamera *getCamera() const;
  void setCamera(KawaiiCamera *newCam);
  void detachCamera();



  // IMPLEMENT
private:
  KawaiiProgram *effect;
  KawaiiGpuBuf *sfcUbo;
  KawaiiCamera *cam;
  KawaiiRoot *root;

  void onParentChanged();
  void onBindedBufferUpdated(uint32_t binding, KawaiiBufferTarget target);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void read(sib_utils::memento::Memento::DataReader &mem) override final;
};

#endif // KAWAIISHADEREFFECT_HPP
