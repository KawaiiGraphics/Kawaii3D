#include "KawaiiRenderTarget.hpp"
#include <sib_utils/qDataStreamExpansion.hpp>
#include <QIODevice>
#include <atomic>

KawaiiRenderpass::KawaiiRenderpass():
  target(nullptr),
  dirty(true)
{
}

KawaiiRenderpass::~KawaiiRenderpass()
{
  auto target_ = target;
  target = nullptr;
  if(target_)
    {
      target_->detachRenderpass();
      renderableSizeChanged();
    }
}

bool KawaiiRenderpass::isDirty() const
{
  return dirty;
}

void KawaiiRenderpass::markUpdated()
{
  dirty = false;
}

size_t KawaiiRenderpass::addGBuf(const GBuffer &gbuf)
{
  const size_t i = gBufs.size();
  gBufs.push_back(gbuf);
  markDirty();
  return i;
}

void KawaiiRenderpass::setGBuf(size_t index, const GBuffer &value)
{
  if(gBufs[index] != value)
    {
      gBufs[index] = value;
      markDirty();
    }
}

void KawaiiRenderpass::changeGBuf(size_t index, const std::function<void (GBuffer &)> &func)
{
  const auto buf = getGBuf(index);
  func(gBufs[index]);
  if(getGBuf(index) != buf)
    markDirty();
}

const KawaiiRenderpass::GBuffer &KawaiiRenderpass::getGBuf(size_t index) const
{
  return gBufs[index];
}

bool KawaiiRenderpass::removeGBuf(size_t index)
{
  if(index >= gBufs.size())
    return false;

  gBufs.erase(gBufs.begin() + index);
  markDirty();
  return true;
}

size_t KawaiiRenderpass::gBufCount() const
{
  return gBufs.size();
}

void KawaiiRenderpass::forallGBufs(const std::function<void (const GBuffer &)> &func)
{
  if(Q_LIKELY(func))
    for(const auto &i: gBufs)
      func(i);
}

size_t KawaiiRenderpass::addSubpass(const Subpass &subpass)
{
  const size_t i = subpasses.size();
  subpasses.push_back(subpass);
  attachImgSrc(subpass.imgSrc, i);
  markDirty();
  return i;
}

void KawaiiRenderpass::setSubpass(size_t index, const Subpass &value)
{
  if(Q_UNLIKELY(index >= subpasses.size()))
    return;

  if(subpasses[index] != value)
    {
      auto *oldImgSrc = subpasses[index].imgSrc;
      subpasses[index] = value;
      if(oldImgSrc != subpasses[index].imgSrc)
        {
          detachImgSrc(oldImgSrc);
          attachImgSrc(subpasses[index].imgSrc, index);
        }
      markDirty();
    }
}

void KawaiiRenderpass::changeSubpass(size_t index, const std::function<void (Subpass &)> &func)
{
  if(Q_UNLIKELY(!func))
    return;
  const auto subpass = getSubpass(index);
  func(subpasses[index]);
  if(getSubpass(index) != subpass)
    {
      if(getSubpass(index).imgSrc != subpass.imgSrc)
        {
          detachImgSrc(subpass.imgSrc);
          attachImgSrc(getSubpass(index).imgSrc, index);
        }
      markDirty();
    }
}

const KawaiiRenderpass::Subpass &KawaiiRenderpass::getSubpass(size_t index) const
{
  return subpasses[index];
}

bool KawaiiRenderpass::removeSubpass(size_t index)
{
  if(Q_UNLIKELY(index >= subpasses.size()))
    return false;

  auto *oldImgSrc = getSubpass(index).imgSrc;
  subpasses.erase(subpasses.begin() + index);
  detachImgSrc(oldImgSrc);
  markDirty();
  return true;
}

size_t KawaiiRenderpass::subpassCount() const
{
  return subpasses.size();
}

void KawaiiRenderpass::forallSubpasses(const std::function<void (const Subpass &)> &func)
{
  if(Q_LIKELY(func))
    for(const auto &i: subpasses)
      func(i);
}

void KawaiiRenderpass::addLocalDependency(quint32 src, quint32 dst)
{
  if(auto el = findDependencyIter(src, dst); el != dependencies.end())
    {
      if(!el->isLocal)
        {
          el->isLocal = true;
          markDirty();
        }
      return;
    }
  dependencies.push_back({
                           .srcSubpass = src,
                           .dstSubpass = dst,
                           .isLocal = true
                         });
  markDirty();
}

void KawaiiRenderpass::addNonLocalDependency(quint32 src, quint32 dst)
{
  if(auto el = findDependencyIter(src, dst); el != dependencies.end())
    {
      if(el->isLocal)
        {
          el->isLocal = false;
          markDirty();
        }
      return;
    }
  dependencies.push_back({
                           .srcSubpass = src,
                           .dstSubpass = dst,
                           .isLocal = false
                         });
  markDirty();
}

bool KawaiiRenderpass::removeDependency(quint32 src, quint32 dst)
{
  if(auto el = findDependencyIter(src, dst); el != dependencies.end())
    {
      dependencies.erase(el);
      markDirty();
      return true;
    } else
    return false;
}

std::optional<KawaiiRenderpass::Dependency> KawaiiRenderpass::findDependency(quint32 src, quint32 dst) const
{
  if(auto el = findDependencyIter(src, dst); el != dependencies.cend())
    return *el;
  else
    return std::nullopt;
}

void KawaiiRenderpass::forallDependencies(const std::function<void (const Dependency &)> &func) const
{
  if(Q_LIKELY(func))
    for(const auto &i: dependencies)
      func(i);
}

namespace {
  bool canSubpassLocalWritingTo(const KawaiiRenderpass::Subpass &subpass, quint32 gbuf)
  {
    return (
          subpass.depthGBuf == gbuf
          || std::find(subpass.outputGBufs.cbegin(), subpass.outputGBufs.cend(), gbuf) != subpass.outputGBufs.cend()
        );
  }

  bool canSubpassNonLocalWritingTo(const KawaiiRenderpass::Subpass &subpass, quint32 gbuf)
  {
    return std::find(subpass.storageGBufs.cbegin(), subpass.storageGBufs.cend(), gbuf) != subpass.storageGBufs.cend();
  }
}

void KawaiiRenderpass::autodetectDependencies()
{
  for(quint32 i = 0; i < subpassCount(); ++i)
    for(quint32 j = i+1; j < subpassCount(); ++j)
      {
        const auto &srcSubpass = getSubpass(i);
        const auto &dstSubpass = getSubpass(j);

        auto checkGBuf = [this, &srcSubpass, i, j] (quint32 usedGbuf) {
            if(canSubpassNonLocalWritingTo(srcSubpass, usedGbuf))
              addNonLocalDependency(i, j);
            else if(canSubpassLocalWritingTo(srcSubpass, usedGbuf))
              addLocalDependency(i, j);
          };

        for(quint32 usedGbuf: dstSubpass.outputGBufs)
          checkGBuf(usedGbuf);

        for(quint32 usedGbuf: dstSubpass.inputGBufs)
          checkGBuf(usedGbuf);

        if(dstSubpass.depthGBuf)
          checkGBuf(*dstSubpass.depthGBuf);

        for(quint32 usedGbuf: dstSubpass.storageGBufs)
          if(canSubpassNonLocalWritingTo(srcSubpass, usedGbuf)
             || canSubpassLocalWritingTo(srcSubpass, usedGbuf))
            addNonLocalDependency(i, j);

        for(quint32 usedGbuf: dstSubpass.sampledGBufs)
          checkGBuf(usedGbuf);
      }
}

const glm::uvec2 &KawaiiRenderpass::getRenderableSize() const
{
  static const glm::uvec2 nullSz(0,0);
  if(target)
    return target->getRenderableSize();
  else
    return nullSz;
}

bool KawaiiRenderpass::isRedrawNeeded()
{
  if(usedImgSources.empty())
    {
      struct GBufDependency {
        uint32_t lastSubpass;
        uint32_t gbufIndex;
      };

      std::list<GBufDependency> gbufQueue;
      for(size_t i = 0; i < gBufCount(); ++i)
        if(getGBuf(i).needStore)
          gbufQueue.push_back({static_cast<uint32_t>(subpassCount()), static_cast<uint32_t>(i)});
      while(!gbufQueue.empty())
        {
          for(size_t j = 0; j < gbufQueue.front().lastSubpass; ++j)
            if(canSubpassLocalWritingTo(getSubpass(j), gbufQueue.front().gbufIndex)
               || canSubpassNonLocalWritingTo(getSubpass(j), gbufQueue.front().gbufIndex))
              {
                if(getSubpass(j).imgSrc)
                  {
                    usedImgSources.push_back(getSubpass(j).imgSrc);
                    for(const auto &inputGbuf: getSubpass(j).sampledGBufs)
                      gbufQueue.push_back({static_cast<uint32_t>(j), inputGbuf});
                    for(const auto &inputGbuf: getSubpass(j).inputGBufs)
                      gbufQueue.push_back({static_cast<uint32_t>(j), inputGbuf});
                    for(const auto &storageGbuf: getSubpass(j).storageGBufs)
                      gbufQueue.push_back({static_cast<uint32_t>(j), storageGbuf});
                    if(getSubpass(j).depthGBuf)
                      gbufQueue.push_back({static_cast<uint32_t>(j), *getSubpass(j).depthGBuf});
                  }
              }
          gbufQueue.pop_front();
        }
    }
  for(auto *src: usedImgSources)
    if(src->isRedrawNeeded())
      return true;
  return false;
}

void KawaiiRenderpass::markRedrawn()
{
  for(size_t i = 0; i < subpassCount(); ++i)
    if(Q_LIKELY(getSubpass(i).imgSrc))
      getSubpass(i).imgSrc->markRedrawn();
}

void KawaiiRenderpass::setReadGbufUFunc(const std::function<std::vector<uint32_t> (uint32_t, const QRect &)> &newReadGbufUFunc)
{
  readGbufUFunc = newReadGbufUFunc;
}

std::vector<float> KawaiiRenderpass::readGbufF(uint32_t gbufIndex, const QRect &region) const
{
  if(readGbufFFunc)
    return readGbufFFunc(gbufIndex, region);
  else
    return {};
}

std::vector<uint32_t> KawaiiRenderpass::readGbufU(uint32_t gbufIndex, const QRect &region) const
{
  if(readGbufUFunc)
    return readGbufUFunc(gbufIndex, region);
  else
    return {};
}

std::vector<int32_t> KawaiiRenderpass::readGbufI(uint32_t gbufIndex, const QRect &region) const
{
  if(readGbufIFunc)
    return readGbufIFunc(gbufIndex, region);
  else
    return {};
}

void KawaiiRenderpass::setReadGbufIFunc(const std::function<std::vector<int32_t> (uint32_t, const QRect &)> &newReadGbufIFunc)
{
  readGbufIFunc = newReadGbufIFunc;
}

void KawaiiRenderpass::setReadGbufFFunc(const std::function<std::vector<float> (uint32_t, const QRect &)> &newReadGbufFFunc)
{
  readGbufFFunc = newReadGbufFFunc;
}

void KawaiiRenderpass::markDirty()
{
  if(!dirty)
    {
      dirty = true;
      usedImgSources.clear();
      emit updateRequest();
    }
}

std::vector<KawaiiRenderpass::Dependency>::iterator KawaiiRenderpass::findDependencyIter(quint32 src, quint32 dst)
{
  return std::find_if(dependencies.begin(), dependencies.end(),
                      [src, dst](const Dependency &i) { return i.srcSubpass == src && i.dstSubpass == dst; });
}

std::vector<KawaiiRenderpass::Dependency>::const_iterator KawaiiRenderpass::findDependencyIter(quint32 src, quint32 dst) const
{
  return std::find_if(dependencies.cbegin(), dependencies.cend(),
                      [src, dst](const Dependency &i) { return i.srcSubpass == src && i.dstSubpass == dst; });
}

void KawaiiRenderpass::attachImgSrc(KawaiiImageSource *imgSrc, size_t subpass)
{
  if(imgSrc)
    {
      for(size_t i = 0; i < subpassCount(); ++i)
        if(i != subpass && getSubpass(i).imgSrc == imgSrc)
          subpasses[i].imgSrc = nullptr;

      connect(imgSrc, &QObject::destroyed, this, &KawaiiRenderpass::imgSrcDestroyed, Qt::UniqueConnection);
      imgSrc->setOwner(this, subpass);
    }
}

void KawaiiRenderpass::detachImgSrc(KawaiiImageSource *imgSrc)
{
  if(imgSrc)
    {
      disconnect(imgSrc, &QObject::destroyed, this, &KawaiiRenderpass::imgSrcDestroyed);
      imgSrc->setOwner(nullptr, 0);
    }
}

void KawaiiRenderpass::imgSrcDestroyed(QObject *imgSrc)
{
  for(auto &subpass: subpasses)
    if(subpass.imgSrc == imgSrc)
      {
        subpass.imgSrc = nullptr;
        markDirty();
      }
}

void KawaiiRenderpass::renderableSizeChanged()
{
  const auto &renderableSz = getRenderableSize();
  for(const auto &subpass: subpasses)
    if(subpass.imgSrc)
      subpass.imgSrc->setRenderableSize(renderableSz);
  markDirty();
}

void KawaiiRenderpass::writeOwnMemData(sib_utils::memento::Memento::DataMutator &mem) const
{
  QByteArray bytes;
  QDataStream st(&bytes, QIODevice::WriteOnly);
  st << gBufs << dependencies;
  st.setDevice(nullptr);
  mem.setData(QStringLiteral("properties"), bytes);
  for(quint64 i = 0; i < subpassCount(); ++i)
    {
      const auto &subpass = subpasses[i];
      sib_utils::memento::Memento *childM;
      mem.createChildMemento(QStringLiteral("KawaiiRenderpass::Subpass"), childM);

      QByteArray childBytes;
      QDataStream st(&childBytes, QIODevice::WriteOnly);
      st << i << subpass.inputGBufs << subpass.outputGBufs << subpass.storageGBufs << subpass.depthGBuf;
      st.setDevice(nullptr);
      childM->mutator(mem.getCreatedMemento(), mem.preserveExternalLinks)
          .setData(QStringLiteral("properties"), childBytes)
          .setLink(QStringLiteral("imgSrc"), subpass.imgSrc);
    }
}

void KawaiiRenderpass::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeOwnMemData(mem);
  KawaiiDataUnit::writeBinary(mem);
}

void KawaiiRenderpass::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeOwnMemData(mem);
  KawaiiDataUnit::writeText(mem);
}

void KawaiiRenderpass::read(sib_utils::memento::Memento::DataReader &mem)
{
  mem.read(QStringLiteral("properties"), [this] (const QByteArray &bytes) {
      QDataStream st(bytes);
      st >> gBufs >> dependencies;
    });

  std::atomic_uint64_t subpassesCounter = 0;
  mem.forallChildren([&mem, &subpassesCounter] (sib_utils::memento::Memento &childMem) {
      QString nodeType;
      childMem.reader(mem.getCreatedObjects()).readType([&nodeType] (const QString &str) { nodeType = str; });
      if(nodeType.compare(QStringLiteral("KawaiiRenderpass::Subpass")) == 0)
        subpassesCounter.fetch_add(1);
    });

  subpasses.resize(subpassesCounter.load(), Subpass {
                     .imgSrc = nullptr,
                   });
  mem.forallChildren([this, &mem] (sib_utils::memento::Memento &childMem) {
    QString nodeType;
    childMem.reader(mem.getCreatedObjects()).readType([&nodeType] (const QString &str) { nodeType = str; });
    if(nodeType.compare(QStringLiteral("KawaiiRenderpass::Subpass")) == 0)
      {
        Subpass subpass;
        quint64 index = subpassCount();
        childMem.reader(mem.getCreatedObjects()).read(QStringLiteral("imgSrc"), [&subpass] (sib_utils::TreeNode *node) {
          subpass.imgSrc = qobject_cast<KawaiiImageSource*>(node);
        })
        .read(QStringLiteral("properties"), [&subpass, &index] (const QByteArray &bytes) {
            QDataStream st(bytes);
            st >> index >> subpass.inputGBufs >> subpass.outputGBufs >> subpass.storageGBufs >> subpass.depthGBuf;
          });
        if(Q_LIKELY(index < subpassCount()))
          setSubpass(index, subpass);
      }
  });

  KawaiiDataUnit::read(mem);
  markDirty();
}


KAWAII3D_SHARED_EXPORT bool operator==(const KawaiiRenderpass::GBuffer &a, const KawaiiRenderpass::GBuffer &b)
{
  return a.format == b.format
      && a.needClear == b.needClear
      && a.needStore == b.needStore;
}

KAWAII3D_SHARED_EXPORT bool operator==(const KawaiiRenderpass::Subpass &a, const KawaiiRenderpass::Subpass &b)
{
  return a.imgSrc == b.imgSrc
      && a.inputGBufs == b.inputGBufs
      && a.outputGBufs == b.outputGBufs
      && a.storageGBufs == b.storageGBufs
      && a.depthGBuf == b.depthGBuf;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiRenderpass::InternalImgFormat fmt)
{
  return st << static_cast<quint8>(fmt);
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::InternalImgFormat &fmt)
{
  quint8 byte;
  st >> byte;
  if(st.status() == QDataStream::Ok)
    fmt = static_cast<KawaiiRenderpass::InternalImgFormat>(byte);
  return st;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiRenderpass::GBuffer &gbuf)
{
  quint8 byte = 0;
  if(gbuf.needClear)
    byte |= 0x1;
  if(gbuf.needStore)
    byte |= 0x2;
  return st << byte << gbuf.format;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::GBuffer &gbuf)
{
  quint8 byte = 0;
  st >> byte >> gbuf.format;
  gbuf.needClear = ((byte & 0x1) == 0x1);
  gbuf.needStore = ((byte & 0x2) == 0x2);
  return st;
}

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiRenderpass::Dependency &dependency)
{
  return st << dependency.srcSubpass
            << dependency.dstSubpass
            << static_cast<quint8>(dependency.isLocal);
}

KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::Dependency &dependency)
{
  quint8 local;
  st >> dependency.srcSubpass
      >> dependency.dstSubpass
      >> local;
  dependency.isLocal = static_cast<bool>(local);
  return st;
}

