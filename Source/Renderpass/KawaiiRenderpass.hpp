#ifndef KAWAIIRENDERPASS_HPP
#define KAWAIIRENDERPASS_HPP

#include "KawaiiImageSource.hpp"
#include <functional>
#include <optional>
#include <vector>

class KawaiiRenderTarget;

class KAWAII3D_SHARED_EXPORT KawaiiRenderpass: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiRenderpass);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  friend class KawaiiRenderTarget;
public:
  enum class InternalImgFormat: uint8_t
  {
    RGBA8_Unorm = 0,
    RGBA8_Snorm = 1,
    RG8_Unorm = 2,
    RG8_Snorm = 3,
    R8_Unorm = 4,
    R8_Snorm = 5,

    RGBA32_F = 6,
    RGBA32_SInt = 7,
    RGBA32_UInt = 8,
    RG32_F = 9,
    RG32_SInt = 10,
    RG32_UInt = 11,
    R32_F = 12,
    R32_SInt = 13,
    R32_UInt = 14,

    RGBA16_F = 15,
    RGBA16_SInt = 16,
    RGBA16_UInt = 17,
    RG16_F = 18,
    RG16_SInt = 19,
    RG16_UInt = 20,
    R16_F = 21,
    R16_SInt = 22,
    R16_UInt = 23,

    Depth16 = 24,
    Depth24 = 25,
    Depth32 = 26,
    Stencil8 = 27,
    Depth16Stencil8 = 28,
    Depth24Stencil8 = 29,
    Depth32Stencil8 = 30
  };

  struct GBuffer
  {
    InternalImgFormat format;
    bool needClear;
    bool needStore;
  };

  struct Subpass
  {
    KawaiiImageSource *imgSrc;

    std::vector<quint32> inputGBufs;
    std::vector<quint32> outputGBufs;
    std::vector<quint32> storageGBufs;
    std::vector<quint32> sampledGBufs;
    std::optional<quint32> depthGBuf;
  };

  struct Dependency
  {
    quint32 srcSubpass;
    quint32 dstSubpass;
    bool isLocal;
  };

  KawaiiRenderpass();
  ~KawaiiRenderpass();

  bool isDirty() const;

  /// Should be called from Renderer
  void markUpdated();

  size_t addGBuf(const GBuffer &gbuf);
  void setGBuf(size_t index, const GBuffer &value);
  void changeGBuf(size_t index, const std::function<void(GBuffer &gbuf)> &func);
  const GBuffer& getGBuf(size_t index) const;
  bool removeGBuf(size_t index);
  size_t gBufCount() const;
  void forallGBufs(const std::function<void(const GBuffer &)> &func);

  size_t addSubpass(const Subpass &subpass);
  void setSubpass(size_t index, const Subpass &value);
  void changeSubpass(size_t index, const std::function<void(Subpass &gbuf)> &func);
  const Subpass& getSubpass(size_t index) const;
  bool removeSubpass(size_t index);
  size_t subpassCount() const;
  void forallSubpasses(const std::function<void(const Subpass &)> &func);

  void addLocalDependency(quint32 src, quint32 dst);
  void addNonLocalDependency(quint32 src, quint32 dst);
  bool removeDependency(quint32 src, quint32 dst);
  std::optional<Dependency> findDependency(quint32 src, quint32 dst) const;
  void forallDependencies(const std::function<void(const Dependency&)> &func) const;
  void autodetectDependencies();

  const glm::uvec2 &getRenderableSize() const;

  bool isRedrawNeeded();

  /// Should be called from Renderer
  void markRedrawn();

  /// Should be called from Renderer
  void setReadGbufFFunc(const std::function<std::vector<float>(uint32_t, const QRect &)> &newReadGbufFFunc);

  /// Should be called from Renderer
  void setReadGbufIFunc(const std::function<std::vector<int32_t>(uint32_t, const QRect &)> &newReadGbufIFunc);

  /// Should be called from Renderer
  void setReadGbufUFunc(const std::function<std::vector<uint32_t>(uint32_t, const QRect &)> &newReadGbufUFunc);

  std::vector<float> readGbufF(uint32_t gbufIndex, const QRect &region) const;
  std::vector<uint32_t> readGbufU(uint32_t gbufIndex, const QRect &region) const;
  std::vector<int32_t> readGbufI(uint32_t gbufIndex, const QRect &region) const;

signals:
  void updateRequest();



  // IMPLEMENT
private:
  KawaiiRenderTarget *target;
  std::vector<GBuffer> gBufs;
  std::vector<Subpass> subpasses;
  std::vector<Dependency> dependencies;
  std::vector<KawaiiImageSource*> usedImgSources;
  std::function<std::vector<float>(uint32_t, const QRect&)> readGbufFFunc;
  std::function<std::vector<int32_t>(uint32_t, const QRect&)> readGbufIFunc;
  std::function<std::vector<uint32_t>(uint32_t, const QRect&)> readGbufUFunc;

  bool dirty;

  void markDirty();
  std::vector<Dependency>::iterator findDependencyIter(quint32 src, quint32 dst);
  std::vector<Dependency>::const_iterator findDependencyIter(quint32 src, quint32 dst) const;

  void attachImgSrc(KawaiiImageSource *imgSrc, size_t subpass);
  void detachImgSrc(KawaiiImageSource *imgSrc);
  void imgSrcDestroyed(QObject *imgSrc);

  void renderableSizeChanged();

  // TreeNode interface
protected:
  void writeOwnMemData(sib_utils::memento::Memento::DataMutator &mem) const;
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void read(sib_utils::memento::Memento::DataReader &mem) override final;
};

KAWAII3D_SHARED_EXPORT bool operator==(const KawaiiRenderpass::GBuffer &a, const KawaiiRenderpass::GBuffer &b);
inline bool operator!=(const KawaiiRenderpass::GBuffer &a, const KawaiiRenderpass::GBuffer &b)
{ return !(a==b); }

KAWAII3D_SHARED_EXPORT bool operator==(const KawaiiRenderpass::Subpass &a, const KawaiiRenderpass::Subpass &b);
inline bool operator!=(const KawaiiRenderpass::Subpass &a, const KawaiiRenderpass::Subpass &b)
{ return !(a==b); }

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, KawaiiRenderpass::InternalImgFormat fmt);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::InternalImgFormat &fmt);

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiRenderpass::GBuffer &gbuf);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::GBuffer &gbuf);

KAWAII3D_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const KawaiiRenderpass::Dependency &dependency);
KAWAII3D_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, KawaiiRenderpass::Dependency &dependency);

#endif // KAWAIIRENDERPASS_HPP
