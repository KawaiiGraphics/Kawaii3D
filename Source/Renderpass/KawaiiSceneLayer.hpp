#ifndef KAWAIISCENELAYER_HPP
#define KAWAIISCENELAYER_HPP

#include "KawaiiImageSource.hpp"
#include "../KawaiiCamera.hpp"
#include "../KawaiiProjection.hpp"
#include <QPointF>
#include <QVector3D>

class KAWAII3D_SHARED_EXPORT KawaiiSceneLayer: public KawaiiImageSource, public KawaiiProjectionHandler
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiSceneLayer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  struct SurfaceGpuData
  {
    glm::mat4 projectionMat;
    glm::mat4 viewProjectionMat;
  };

  KawaiiSceneLayer();
  ~KawaiiSceneLayer();

  inline static constexpr size_t getSurfaceBufferSize()
  { return sizeof(SurfaceGpuData); }

  const glm::mat4& getProjectionMatrix() const;
  const glm::mat4& getViewProjectionMatrix() const;

  KawaiiCamera *getCamera() const;
  void setCamera(KawaiiCamera *newCam);
  void detachCamera();

  QPointF mapCoord(const QVector3D &worldCoord) const;
  QVector3D unmapCoord(const QPointF &coord, float depth) const;

  KawaiiGpuBuf *getSurfaceUBO() const;

  // KawaiiProjectionHandler interface
protected:
  void onProjectionMatChanged() override final;

signals:
  void projectionMatChanged(const glm::mat4 &mat);
  void viewProjectionMatChanged(const glm::mat4 &mat);



  // IMPLEMENT
private:
  KawaiiGpuBuf *sfcUniforms;
  SurfaceGpuData *sfcGpuData;
  KawaiiCamera *cam;

  void calculateViewProjectionMat();
  void onViewMatChanged();

  // KawaiiImageSource interface
private:
  void onRenderableSizeChanged() override;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override final;
  void read(sib_utils::memento::Memento::DataReader &mem) override final;
};

#endif // KAWAIISCENELAYER_HPP
