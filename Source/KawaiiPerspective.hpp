#ifndef KAWAIIPERSPECTIVE_HPP
#define KAWAIIPERSPECTIVE_HPP

#include "KawaiiProjection.hpp"

class KAWAII3D_SHARED_EXPORT KawaiiPerspective: public KawaiiProjection
{
public:
  KAWAII_UNIT_DEF(KawaiiPerspective);

  KawaiiPerspective(float fovRadians, float nearClip, float farClip);
  KawaiiPerspective(float nearClip = 0.1f, float farClip = 100.0f);

  glm::mat4 calculate(const glm::uvec2 &surfaceSize) const override final;

  void setFovDegrees(float deg);
  void setFovRadians(float rad);
  float getFovDegrees() const;
  float getFovRadians() const;

  // KawaiiProjection interface
  KawaiiProjection *clone() const override;

  void writeTextMemento(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeBinaryMemento(sib_utils::memento::Memento::DataMutator &mem) const override;

private:
  void readMemento(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
private:
  float fovRadians;
};

#endif // KAWAIIPERSPECTIVE_HPP
