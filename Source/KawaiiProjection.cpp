#include "KawaiiProjection.hpp"
#include <QEvent>

KawaiiProjection::KawaiiProjection(float nearClip, float farClip):
  clipCorrection(1.0),
  clipCorrectionInverted(1.0),
  size(128, 128),
  nearClip(nearClip),
  farClip(farClip)
{
}

const glm::mat4 &KawaiiProjection::getClipCorrection() const
{
  return clipCorrection;
}

const glm::mat4 &KawaiiProjection::getClipCorrectionInverted() const
{
  return clipCorrectionInverted;
}

void KawaiiProjection::setClipCorrection(const glm::mat4 &value)
{
  if(clipCorrection != value)
    {
      clipCorrection = value;
      clipCorrectionInverted = glm::inverse(clipCorrection);
      updateRequest();
    }
}

const glm::uvec2 &KawaiiProjection::getSize() const
{
  return size;
}

void KawaiiProjection::setSize(const glm::uvec2 &value)
{
  if(size != value)
    {
      size = value;
      updateRequest();
    }
}

float KawaiiProjection::getNearClip() const
{
  return nearClip;
}

void KawaiiProjection::setNearClip(float value)
{
  if(!qFuzzyCompare(nearClip, value))
    {
      nearClip = value;
      updateRequest();
    }
}

float KawaiiProjection::getFarClip() const
{
  return farClip;
}

void KawaiiProjection::setFarClip(float value)
{
  if(!qFuzzyCompare(farClip, value))
    {
      farClip = value;
      updateRequest();
    }
}

glm::mat4 KawaiiProjection::getMatrix() const
{
  if(size.x != 0 && size.y != 0)
    return clipCorrection * calculate(size);
  else
    return clipCorrection;
}

KawaiiProjection *KawaiiProjection::createProjection(sib_utils::memento::Memento::DataReader &mem)
{
  KawaiiProjection *result = nullptr;
  mem.readText(QStringLiteral("projectionType"), [&result] (const QString &type) {
      if(auto el = creators.find(type); el != creators.end() && el.value())
        result = el.value()();
    });
  if(result)
    result->readMemento(mem);
  return result;
}

void KawaiiProjection::updateRequest()
{
  if(onUpdate)
    onUpdate();
}

//=============================================

KawaiiProjectionHandler::KawaiiProjectionHandler(const glm::uvec2 *surfaceSize):
  p(nullptr),
  size(*surfaceSize)
{
}

KawaiiProjectionHandler::~KawaiiProjectionHandler()
{
  if(p)
    delete p;
}

glm::mat4 KawaiiProjectionHandler::calculateProjectionMatrix() const
{
  static const glm::mat4 identify(1.0);
  if(p)
    return p->getMatrix();
  else
    return identify;
}

namespace {
  const glm::mat4 nullMat(0.0);
}

const glm::mat4 &KawaiiProjectionHandler::getClipCorrection() const
{
  if(p)
    return p->getClipCorrection();
  else
    return nullMat;
}

const glm::mat4 &KawaiiProjectionHandler::getClipCorrectionInverted() const
{
  if(p)
    return p->getClipCorrectionInverted();
  else
    return nullMat;
}

void KawaiiProjectionHandler::setClipCorrection(const glm::mat4 &value)
{
  if(p)
    p->setClipCorrection(value);
}

void KawaiiProjectionHandler::setProjection(KawaiiProjection *ptr)
{
  if(p == ptr) return;

  if(p)
    delete p;
  p = ptr;
  if(p)
    {
      p->onUpdate = nullptr;
      p->setSize(size);
      p->onUpdate = std::bind(&KawaiiProjectionHandler::onProjectionMatChanged, this);
    }

  onProjectionMatChanged();
}

void KawaiiProjectionHandler::copyProjectionFrom(const KawaiiProjectionHandler &from)
{
  if(from.getProjectionPtr())
    setProjection(from.getProjectionPtr()->clone());
  else
    setProjection(nullptr);
}

void KawaiiProjectionHandler::sizeChanged()
{
  if(p)
    p->setSize(size);
}
