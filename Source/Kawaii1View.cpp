#include <glm/gtc/matrix_transform.hpp>
#include <sib_utils/dirty_cast.hpp>
#include "Kawaii1View.hpp"
#include <cstring>

using sib_utils::dirty_cast;

Kawaii1View::Kawaii1View(const QQuaternion &rotation, const QVector3D &position):
  rotation(rotation),
  position(position),
  m(std::make_unique<QReadWriteLock>())
{
}

Kawaii1View::Kawaii1View(const glm::mat4 &trMat):
  m(std::make_unique<QReadWriteLock>())
{
  position = dirty_cast<QVector4D>(trMat * glm::vec4(0,0,0,1)).toVector3DAffine();

  QVector3D dir = dirty_cast<QVector4D>(trMat * glm::vec4(0,0,-1,0)).toVector3D(),
      upDir = dirty_cast<QVector4D>(trMat * glm::vec4(0,1,0,0)).toVector3D();

  rotation = QQuaternion::fromDirection(dir.normalized(), upDir.normalized());
}

Kawaii1View::Kawaii1View(const KawaiiCamera &cam):
  Kawaii1View(cam.getViewMatInverted())
{
}

Kawaii1View::~Kawaii1View()
{
}

void Kawaii1View::setRotation(const QQuaternion &value)
{
  {
    QWriteLocker l(m.get());
    rotation = value;
  }
  emit updated();
}

const QQuaternion &Kawaii1View::getRotation() const
{
  QReadLocker l(m.get());
  return rotation;
}

void Kawaii1View::setPosition(const QVector3D &value)
{
  {
    QWriteLocker l(m.get());
    position = value;
  }
  emit updated();
}

const QVector3D &Kawaii1View::getPosition() const
{
  QReadLocker l(m.get());
  return position;
}

namespace {
  glm::vec3 fromQVec(const QVector3D &vec)
  {
    return {vec.x(), vec.y(), vec.z()};
  }
}

glm::mat4 Kawaii1View::getCameraMatrix() const
{
  static const QVector3D forwardVec(0,0,1);
  static const QVector3D upVec(0,1,0);

  QReadLocker l(m.get());
  return glm::lookAt(fromQVec(position),
                     fromQVec(position + rotation.rotatedVector(forwardVec)),
                     fromQVec(upVec));
}

glm::mat4 Kawaii1View::getModelMatrix() const
{
  QReadLocker l(m.get());
  return glm::rotate(glm::translate(glm::mat4(), fromQVec(position)),
                     rotation.scalar(),
                     fromQVec(rotation.vector()));
}

QVector3D Kawaii1View::transform(const QVector3D &localPos) const
{
  QReadLocker l(m.get());
  return position + rotation.rotatedVector(localPos);
}

void Kawaii1View::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  QByteArray unitedData(sizeof(QQuaternion) + sizeof(QVector3D), '\0');
  {
    QReadLocker l(m.get());
    std::memcpy(unitedData.data(), &rotation, sizeof (QQuaternion));
    std::memcpy(unitedData.data() + sizeof (QQuaternion), &position, sizeof (QVector3D));
  }
  mem.setData("data", unitedData);
}

namespace {
  template<typename T>
  QString vec3ToString(const T &arg)
  {
    return QString("%1 %2 %3").arg(arg.x(), 0, 'f', 10).arg(arg.y(), 0, 'f', 10).arg(arg.z(), 0, 'f', 10);
  }

  QString vec4ToString(const QVector4D &arg)
  {
    return vec3ToString(arg) + ' ' + QString::number(arg.w(), 'f', 10);
  }
}

void Kawaii1View::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  QReadLocker l(m.get());
  mem.setData("rotation", vec4ToString(rotation.toVector4D()));
  mem.setData("position", vec3ToString(position));
}

void Kawaii1View::read(sib_utils::memento::Memento::DataReader &mem)
{
  struct {
    bool rotation: 1;
    bool position: 1;
  } readed;
  static_assert (sizeof(readed) == sizeof(uint8_t));

  QString str; QVector<QStringView> terms; bool ok;
  Transaction mementoReading(this);

  mem.get("rotation", str, ok);
  terms = QStringView(str).split(' ');
  if(terms.size() >= 4)
    {
      readed.rotation = ok;
      mementoReading.rotation = QQuaternion(QVector4D(terms.at(0).toFloat(), terms.at(1).toFloat(), terms.at(2).toFloat(), terms.at(3).toFloat()));
    } else
    readed.rotation = false;

  str.clear();
  mem.get("position", str, ok);
  terms = QStringView(str).split(' ');
  if(terms.size() >= 3)
    {
      readed.position = ok;
      mementoReading.position = QVector3D(terms.at(0).toFloat(), terms.at(1).toFloat(), terms.at(2).toFloat());
    } else
    readed.position = false;

  static constexpr uint8_t readed_mask = 3;
  if((sib_utils::dirty_cast<uint8_t>(readed) & readed_mask) != readed_mask)
    {
      QByteArray data;
      mem.get("data", data, ok);
      size_t N = data.size();
      if(!readed.rotation)
        {
          if(N >= sizeof(QQuaternion))
            rotation = *reinterpret_cast<const QQuaternion*>(data.constData());
          else
            rotation = QQuaternion();
        }

      if(!readed.position)
        {
          if(N >= sizeof(QQuaternion) + sizeof(QVector3D))
            position = *reinterpret_cast<const QVector3D*>(data.constData() + sizeof(QQuaternion));
          else
            position = QVector3D(0,0,0);
        }
    }
}



Kawaii1View::Transaction::Transaction(Kawaii1View *target):
  target(target),
  rotation(target->getRotation()),
  position(target->getPosition())
{ }

Kawaii1View::Transaction::Transaction(Kawaii1View::Transaction &&orig):
  target(orig.target),
  rotation(orig.rotation),
  position(orig.position)
{
  orig.target.clear();
}

Kawaii1View::Transaction& Kawaii1View::Transaction::operator =(Kawaii1View::Transaction &&orig)
{
  target = orig.target;
  rotation = orig.rotation;
  position = orig.position;

  orig.target.clear();
  return *this;
}

Kawaii1View::Transaction::~Transaction()
{
  if(target)
    {
      {
        QWriteLocker l(target->m.get());
        target->position = position;
        target->rotation = rotation;
      }
      emit target->updated();
    }
}
