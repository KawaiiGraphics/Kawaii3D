#include "KawaiiDataUnit.hpp"
#include <QtConcurrent>
#include <QChildEvent>
#include <QMatrix4x4>
#include <memory>

KawaiiDataUnit::KawaiiDataUnit():
  rendererImpl(nullptr)
{
}

KawaiiDataUnit::~KawaiiDataUnit()
{
  removeRendererImplRecursive();
}

KawaiiDataUnit* KawaiiDataUnit::loadChild(sib_utils::memento::Memento &memento)
{
  auto node = mementoFactory.loadSingleObject(memento);
  auto unit = dynamic_cast<KawaiiDataUnit*>(node);
  if(!unit && node)
    delete node;

  return unit;
}

KawaiiRendererImpl* KawaiiDataUnit::getRendererImpl() const
{
  return rendererImpl.data();
}

void KawaiiDataUnit::removeRendererImplRecursive()
{
  std::vector<KawaiiDataUnit*> targetUnits;
  std::vector<KawaiiDataUnit*> stack = { this };

  while(!stack.empty())
    {
      auto *u = stack.back();
      stack.pop_back();

      if(u->getRendererImpl())
        targetUnits.push_back(u);

      std::vector<KawaiiDataUnit*> children;
      children.reserve(u->children().size());
      for(auto *obj: u->children())
        if(auto *childUnit = qobject_cast<KawaiiDataUnit*>(obj); childUnit)
          children.push_back(childUnit);

      stack.insert(stack.end(), children.cbegin(), children.cend());
      for(auto *ch: children)
        if(ch->getRendererImpl())
          targetUnits.push_back(ch);
    }
  for(auto i = targetUnits.crbegin(); i != targetUnits.crend(); ++i)
    (*i)->removeRendererImpl();
}

void KawaiiDataUnit::removeRendererImpl()
{
  if(rendererImpl)
    {
      auto d = rendererImpl.data();
      rendererImpl.clear();
      delete d;
    }
}

bool KawaiiDataUnit::loadAsset(const QString &str)
{
  for(const auto &func: loadhooks)
    if(std::holds_alternative<std::function<bool (const QString&, KawaiiDataUnit*)>>(func))
      {
        bool success = false;
        try
        {
          success = std::get<std::function<bool (const QString&, KawaiiDataUnit*)>>(func)(str, this);
        } catch(...) { success = false; }
        if(success)
          return true;
      }

  QMatrix4x4 mat;
  for(const auto &func: loadhooks)
    if(std::holds_alternative<std::function<bool (const QString&, const QMatrix4x4&, KawaiiDataUnit*)>>(func))
      {
        bool success = false;
        try
        {
          success = std::get<std::function<bool (const QString&, const QMatrix4x4&, KawaiiDataUnit*)>>(func)(str, mat, this);
        } catch(...) { success = false; }
        if(success)
          return true;
      }

  return false;
}

QFuture<bool> KawaiiDataUnit::loadAssetP(const QString &str, QThreadPool *threadPool)
{
  if(!threadPool)
    threadPool = QThreadPool::globalInstance();

  return QtConcurrent::run(threadPool, [this](const QString &str){ return loadAsset(str); }, str);
}

bool KawaiiDataUnit::loadAsset(const QString &str, const QMatrix4x4 &trMat)
{
  for(const auto &func: loadhooks)
    if(std::holds_alternative<std::function<bool (const QString&, const QMatrix4x4&, KawaiiDataUnit*)>>(func))
      {
        bool success = false;
        try
        {
          success = std::get<std::function<bool (const QString&, const QMatrix4x4&, KawaiiDataUnit*)>>(func)(str, trMat, this);
        } catch(...) { success = false; }
        if(success)
          return true;
      }

  return false;
}

QFuture<bool> KawaiiDataUnit::loadAssetP(const QString &str, const QMatrix4x4 &trMat, QThreadPool *threadPool)
{
  if(!threadPool)
    threadPool = QThreadPool::globalInstance();

  return QtConcurrent::run(threadPool,
                           [this](const QString &str, const QMatrix4x4 &trMat) { return loadAsset(str, trMat); }, str, trMat);
}

std::unique_ptr<KawaiiDataUnit> KawaiiDataUnit::clone(bool preserveExternalLinks) const
{
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(getMementoFactory().saveObjectTreeBinary(*this, preserveExternalLinks));

  auto loadedNode = getMementoFactory().loadObjectTree(*memento);
  auto result = dynamic_cast<KawaiiDataUnit*>(loadedNode);
  if(Q_UNLIKELY(!result && loadedNode))
    delete loadedNode;
  return std::unique_ptr<KawaiiDataUnit>(result);
}

void KawaiiDataUnit::childEvent(QChildEvent *event)
{
  if(event->type() == QEvent::ChildAdded)
    {
      std::list<KawaiiDataUnit*> tasks;
      if(KawaiiDataUnit *childUnit = dynamic_cast<KawaiiDataUnit*>(event->child()); childUnit)
        tasks.push_back(childUnit);

      while(!tasks.empty())
        {
          auto *unit = tasks.front();
          tasks.pop_front();

          createChildRenderer(unit);

          for(auto obj: unit->children())
            if(KawaiiDataUnit *childUnit = dynamic_cast<KawaiiDataUnit*>(obj); childUnit)
              tasks.push_back(childUnit);
        }
    }
  TreeNode::childEvent(event);
}

void KawaiiDataUnit::createChildRenderer(KawaiiDataUnit *childUnit)
{
  auto parentRenderer = rendererImpl.data();
  {
    auto obj = this;
    while(obj->parent() && !parentRenderer)
      {
        obj = dynamic_cast<KawaiiDataUnit*>(obj->parent());
        if(obj)
          parentRenderer = obj->rendererImpl.data();
      }
  }
  if(parentRenderer)
    {
      auto childRenderer = parentRenderer->createRenderer(*childUnit);
      if(childRenderer)
        {
          childRenderer->initConnections();
          parentRenderer->initConnections();
        }
    }
}



using LoadhookDescr = KawaiiDataUnit::LoadhookDescr;


LoadhookDescr::LoadhookDescr(size_t i):
  i(i)
{
  KawaiiDataUnit::loadhookDescrs.push_back(this);
}

LoadhookDescr::LoadhookDescr(const std::function<bool (const QString &, KawaiiDataUnit *)> &hook):
  LoadhookDescr(loadhooks.size())
{
  loadhooks.push_back(hook);
}

LoadhookDescr::LoadhookDescr(const std::function<bool (const QString &, const QMatrix4x4 &, KawaiiDataUnit *)> &hook):
  LoadhookDescr(loadhooks.size())
{
  loadhooks.push_back(hook);
}

LoadhookDescr::LoadhookDescr(LoadhookDescr &&orig):
  i(orig.i)
{
  orig.i = loadhooks.size();
  std::replace(loadhookDescrs.begin(), loadhookDescrs.end(), &orig, this);
}

LoadhookDescr &LoadhookDescr::operator=(LoadhookDescr &&orig)
{
  i = orig.i;
  orig.i = loadhooks.size();
  std::replace(loadhookDescrs.begin(), loadhookDescrs.end(), &orig, this);
  return *this;
}

LoadhookDescr::~LoadhookDescr()
{
  loadhookDescrs.remove(this);

  if(i < loadhooks.size())
    {
      loadhooks.erase(loadhooks.begin() + i);
      for(auto j = loadhookDescrs.begin(); j != loadhookDescrs.end(); ++j)
        {
          //Indexes eq to index of removed hook and invalid index should be invalidated
          if((*j)->i == i || (*j)->i == loadhooks.size() - 1)
            (*j)->i = loadhooks.size();
          else

            //Decrease indexes grater than index of removed hook
            if((*j)->i > i)
              --(*j)->i;
        }
    }
}
