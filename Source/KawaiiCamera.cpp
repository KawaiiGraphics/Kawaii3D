#include "KawaiiRoot.hpp"
#include "KawaiiCamera.hpp"
#include "AssetZygote/KawaiiParam.hpp"

#include <QSet>
#include <QVector>
#include <QVariant>
#include <QChildEvent>
#include <sib_utils/dirty_cast.hpp>
#include <sib_utils/Strings.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

KawaiiCamera::KawaiiCamera():
  uniforms(nullptr, getBufferSize()),
  gpuBuffer(reinterpret_cast<GpuBuffer*>(uniforms.getData())),
  scene(nullptr)
{
  uniforms.updateParent(this);
  uniforms.setObjectName("Uniforms");
  *gpuBuffer = GpuBuffer();
  setViewMat(glm::lookAt(glm::vec3(0,0,0), glm::vec3(0,0,1), glm::vec3(0,1,0)));

  setProperty("ExcludedChildren", QVariant::fromValue<QList<QVariant>>({QVariant::fromValue<QObject*>(&uniforms)}));

  connect(this, &sib_utils::TreeNode::parentUpdated, this, &KawaiiCamera::onParentChanged);
  connect(&uniforms, &KawaiiGpuBuf::updated, this, &KawaiiCamera::askedRedraw);
}

const KawaiiCamera::GpuBuffer *KawaiiCamera::getBuffer() const
{
  return gpuBuffer;
}

const glm::mat4 &KawaiiCamera::getViewMat() const
{
  return gpuBuffer->view;
}

const glm::mat4 &KawaiiCamera::getViewMatInverted() const
{
  return gpuBuffer->viewInverted;
}

const glm::mat3 &KawaiiCamera::getNormalMat() const
{
  return gpuBuffer->normal;
}

glm::vec3 KawaiiCamera::getUpVec() const
{
  return glm::vec3(getViewMatInverted() * glm::vec4(0,1,0, 0));
}

glm::vec3 KawaiiCamera::getEyePos() const
{
  glm::vec4 v4 = getViewMatInverted() * glm::vec4(0,0,0,1);
  return glm::vec3(v4) / v4.w;
}

glm::vec3 KawaiiCamera::getViewCenter() const
{
  return getEyePos() + getViewDir();
}

glm::vec3 KawaiiCamera::getViewDir() const
{
  return glm::vec3(getViewMatInverted() * glm::vec4(0,0,-1, 0));
}

namespace {
  QVector3D fromGlmVec(const glm::vec3 &vec)
  {
    return {vec.x, vec.y, vec.z};
  }
}

QVector3D KawaiiCamera::getQUpVec() const
{
  return fromGlmVec(getUpVec());
}

QVector3D KawaiiCamera::getQEyePos() const
{
  return fromGlmVec(getEyePos());
}

QVector3D KawaiiCamera::getQViewCenter() const
{
  return fromGlmVec(getViewCenter());
}

QVector3D KawaiiCamera::getQViewDir() const
{
  return fromGlmVec(getViewDir());
}

QMatrix4x4 KawaiiCamera::getQViewMat() const
{
  return *KawaiiParam(getViewMat()).getValue<QMatrix4x4>();
}

QMatrix4x4 KawaiiCamera::getQViewMatInverted() const
{
  return *KawaiiParam(getViewMatInverted()).getValue<QMatrix4x4>();
}

QMatrix4x4 KawaiiCamera::getQNormalMat() const
{
  return QMatrix4x4(*KawaiiParam(getNormalMat()).getValue<QMatrix3x3>());
}

glm::mat4 KawaiiCamera::getViewProjectionMat(const glm::mat4 &proj) const
{
  return proj * gpuBuffer->view;
}

void KawaiiCamera::setViewMat(const glm::mat4 &mat)
{
  gpuBuffer->view = mat;
  updateViewMatrix();
}

void KawaiiCamera::setViewMat(const QMatrix4x4 &mat)
{
  setViewMat(*KawaiiParam(mat).getValue<glm::mat4>());
}

void KawaiiCamera::transform(const glm::mat4 &trMat)
{
  auto eye_v3 = trMat * glm::vec4(getEyePos(), 1);
  glm::vec3 eye(eye_v3);
  glm::vec3 viewDir(trMat * glm::vec4(getViewDir(), 0));
  glm::vec3 up(trMat * glm::vec4(getUpVec(), 0));
  setViewMat(glm::lookAt(eye, eye + viewDir, glm::normalize(up)));
}

void KawaiiCamera::transform(const QMatrix4x4 &trMat)
{
  transform(*KawaiiParam(trMat).getValue<glm::mat4>());
}

void KawaiiCamera::updateViewMatrix()
{
  gpuBuffer->normal = glm::inverseTranspose(glm::mat3(gpuBuffer->view));
  gpuBuffer->viewInverted = glm::inverse(gpuBuffer->view);
  gpuBuffer->pos = gpuBuffer->viewInverted * glm::vec4(0,0,0, 1);
  uniforms.dataChanged(0, getBufferSize());
  emit viewMatUpdated();
}

void KawaiiCamera::onParentChanged()
{
  auto newScene = KawaiiRoot::getRoot<KawaiiScene>(parent());
  if(newScene != scene)
    {
      if(scene)
        {
          disconnect(scene, &KawaiiScene::drawPipelineChanged, this, &KawaiiCamera::drawPipelineChanged);
          disconnect(scene, &KawaiiScene::askedRedraw, this, &KawaiiCamera::askedRedraw);
        }
      scene = newScene;
      if(scene)
        {
          connect(scene, &KawaiiScene::drawPipelineChanged, this, &KawaiiCamera::drawPipelineChanged);
          connect(scene, &KawaiiScene::askedRedraw, this, &KawaiiCamera::askedRedraw);
        }
      emit sceneChanged(scene);
      emit drawPipelineChanged();
    }
}

void KawaiiCamera::writeBinary(sib_utils::memento::Memento::DataMutator &memento) const
{
  memento.setData("view_matrix", QByteArray(reinterpret_cast<const char*>(&gpuBuffer->view), sizeof(gpuBuffer->view)));
}

void KawaiiCamera::writeText(sib_utils::memento::Memento::DataMutator &memento) const
{
  QString viewMatStr = "\n";
  for(int i = 0; i < 4; ++i)
    for(int j = 0; j < 4; ++j)
      {
        viewMatStr += QString::number(gpuBuffer->view[i][j], 'f', 10);
        viewMatStr += (j != 3)? ' ': '\n';
      }
  memento.setData("view_matrix", viewMatStr);
}

void KawaiiCamera::read(sib_utils::memento::Memento::DataReader &memento)
{
  memento.read("view_matrix", [this](const QByteArray &buff) {
      if(static_cast<size_t>(buff.size()) >= sizeof(glm::mat4))
        setViewMat(*reinterpret_cast<const glm::mat4*>(buff.constData()));
    }).readText("view_matrix", [this](const QString &text) {
      QStringView str(text);
      QVector<QStringView> lines = str.split('\n', Qt::SkipEmptyParts);
      QVector<QVector<QStringView>> terms(lines.size());
#     pragma omp parallel shared(terms)
      {
#       pragma omp for
        for(int i = 0; i < terms.size(); ++i)
          terms[i] = lines.at(i).split(' ', Qt::SkipEmptyParts);

#       pragma omp for
        for(auto i = 0; i < terms.size(); ++i)
          for(int j = 0; j < terms.at(i).size(); ++j)
            {
              bool ok = true;
              float f = terms.at(i).at(j).toFloat(&ok);
              gpuBuffer->view[i][j] = ok? f: 0.0;
            }
      }
      updateViewMatrix();
    });
}
