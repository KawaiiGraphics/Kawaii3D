#include "KawaiiRendererImpl.hpp"
#include "KawaiiImplFactory.hpp"
#include "KawaiiDataUnit.hpp"
#include <QCoreApplication>

KawaiiRendererImpl::KawaiiRendererImpl(KawaiiDataUnit *data):
  _data(data)
{
  if(data)
    {
      if(!data->objectName().isEmpty() && !data->objectName().isNull())
        {
          setObjectName("RendererOf"+data->objectName());
          lastModelName = data->objectName();
        }
      connect(data, &KawaiiDataUnit::parentUpdated, this, &KawaiiRendererImpl::initConnections);
      connect(data, &QObject::objectNameChanged, this, &KawaiiRendererImpl::onModelObjectNameChanged);
    }
}

KawaiiRendererImpl::~KawaiiRendererImpl()
{
}

KawaiiRendererImpl *KawaiiRendererImpl::createRenderer(KawaiiDataUnit &data)
{
  return factory->createRenderer(data);
}

bool KawaiiRendererImpl::checkRendererCompatibility(const KawaiiDataUnit &data) const
{
  return factory->checkCompatibility(data);
}

void KawaiiRendererImpl::onModelObjectNameChanged(const QString &objectName)
{
  QString name = this->objectName();
  name.replace(lastModelName, objectName);
  lastModelName = objectName;
  setObjectName(name);
}
