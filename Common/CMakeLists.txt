cmake_minimum_required (VERSION 3.10)
set(LIBNAME Kawaii3D)
project (${LIBNAME}-Common)

install(DIRECTORY ../Source/ DESTINATION include/${LIBNAME} FILES_MATCHING PATTERN "*.hpp")
install(DIRECTORY ShaderModules/ DESTINATION share/${LIBNAME}/ShaderModules FILES_MATCHING PATTERN "*.glsl")

