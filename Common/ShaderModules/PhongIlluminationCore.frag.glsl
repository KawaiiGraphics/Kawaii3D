module PhongIlluminationCore;
precision mediump int; precision highp float;

struct MaterialInfo
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;
};

MaterialInfo phong_material = MaterialInfo(vec3(0.05), vec3(0.5), vec3(1.0), 0.0);

vec3 getLo(in vec3 n, in vec3 light_dir, in vec3 to_camera, in vec3 light_diffuse, in vec3 light_specular)
{
  vec3 result = light_diffuse * phong_material.diffuse * max(dot(n, light_dir), 0.0);

  vec3 reflect_v = -normalize(reflect(light_dir, n));

  float spec_f = max(dot(reflect_v, to_camera), 0.0);
  vec3 spec = light_specular * phong_material.specular * pow(spec_f, phong_material.shininess);
  return result + spec;
}

vec3 getAmbient(in vec3 N, in vec3 to_camera, in vec3 irradiance)
{
    return irradiance * phong_material.ambient;
}

export MaterialInfo, getLo, getAmbient;
