module CelIlluminationCore;
precision mediump int; precision highp float;

struct MaterialInfo
{
  vec3 ambient;
  vec3 diffuse;
  vec4 specular;
  vec3 outline_color;
  vec2 outline_thickness;
  float shininess;
  float diffuse_threshold;
  float specular_threshold;
};

MaterialInfo cel_material = MaterialInfo(
            vec3(0.05), // ambient
            vec3(0.5), // diffuse
            vec4(1.0, 1.0, 1.0, 0.7), // specular
            vec3(0.0), // outline_color
            vec2(0.1, 0.4), // outline_thickness
            0.0, // shininess
            0.1, // diffuse_threshold
            0.5 // specular_threshold
            );

vec3 getLo(in vec3 n, in vec3 light_dir, in vec3 to_camera, in vec3 light_diffuse, in vec3 light_specular)
{
  vec3 result = vec3(0.0); // 0.25 * sqrt(cel_material.diffuse);

  float n_dot_l = dot(n, light_dir);

  if(n_dot_l >= cel_material.diffuse_threshold)
      result = light_diffuse * cel_material.diffuse;

  if(dot(to_camera, n) < mix(cel_material.outline_thickness.x, cel_material.outline_thickness.y, n_dot_l))
      result = light_diffuse * cel_material.outline_color;

  vec3 reflect_v = -normalize(reflect(light_dir, n));

  float spec_f = max(dot(reflect_v, to_camera), 0.0);
  spec_f = pow(spec_f, cel_material.shininess);

  if(n_dot_l > 0.0 && spec_f > cel_material.specular_threshold)
  {
      vec3 spec = light_specular * cel_material.specular.rgb * spec_f;
      return mix(result, spec, cel_material.specular.a);
  } else
      return result;
}

vec3 getAmbient(in vec3 N, in vec3 to_camera, in vec3 irradiance)
{
    return irradiance * cel_material.ambient;
}

export MaterialInfo, getLo, getAmbient;
