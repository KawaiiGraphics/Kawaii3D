module PbrIlluminationCore;
precision mediump int; precision highp float;

struct MaterialInfo
{
    vec3 albedo;
    float metallic;
    float roughness;
    float ao;
};

MaterialInfo pbr_material = MaterialInfo(vec3(0.1), 0.5, 0.5, 1.0);

const float PI = 3.14159265359;

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}

float distributionGGX(vec3 normal, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(normal, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float geometrySchlickGGX(float n_dot_v, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = n_dot_v;
    float denom = n_dot_v * (1.0 - k) + k;

    return num / denom;
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = geometrySchlickGGX(NdotV, roughness);
    float ggx1  = geometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 getLo(in vec3 n, in vec3 light_dir, in vec3 to_camera, in vec3 light_color)
{
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, pbr_material.albedo, pbr_material.metallic);
    vec3 H = normalize(to_camera + light_dir);

    // Cook-Torrance BRDF
    float NDF = distributionGGX(n, H, pbr_material.roughness);
    float G   = geometrySmith(n, to_camera, light_dir, pbr_material.roughness);
    vec3 F    = fresnelSchlick(max(dot(H, to_camera), 0.0), F0);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - pbr_material.metallic;

    vec3 numerator    = NDF * G * F;
    float denominator = 4.0 * max(dot(n, to_camera), 0.0) * max(dot(n, light_dir), 0.0) + 0.001;
    vec3 specular     = numerator / denominator;

    float NdotL = max(dot(n, light_dir), 0.0);
    return (kD * pbr_material.albedo / PI + specular) * light_color * NdotL;
}

vec3 getAmbient(in vec3 N, in vec3 to_camera, in vec3 irradiance)
{
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, pbr_material.albedo, pbr_material.metallic);

    vec3 kS = fresnelSchlick(max(dot(N, to_camera), 0.0), F0);
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - pbr_material.metallic;
    vec3 diffuse    = irradiance * pbr_material.albedo;
    return (kD * diffuse) * pbr_material.ao;
}

export MaterialInfo, getLo, getAmbient;
