#include "TestCamera.hpp"
#include "../Source/AssetZygote/KawaiiParam.hpp"

#include <QtTest/QtTest>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <sib_utils/Math.hpp>
#include <sib_utils/dirty_cast.hpp>
#include <QMatrix4x4>

TestCamera::TestCamera(QObject *parent):
  QObject(parent)
{
  connect(&cam, &KawaiiCamera::viewMatUpdated, this, &TestCamera::updateInvNormMat);
}

namespace {
  template<int N, int M>
  bool apprEq(const glm::mat<N, M, float, glm::defaultp> &a, const glm::mat<N, M, float, glm::defaultp> &b, int ulp = 2)
  {
    for(int i = 0; i < N; ++i)
      for(int j = 0; j < M; ++j)
        if(!sib_utils::Math::almost_equal(a[i][j], b[i][j], ulp))
          return false;
    return true;
  }

  template<int N>
  bool apprEq(const glm::vec<N, float, glm::defaultp> &a, const glm::vec<N, float, glm::defaultp> &b, int ulp = 2)
  {
    for(int i = 0; i < N; ++i)
      if(!sib_utils::Math::almost_equal(a[i], b[i], ulp))
        return false;
    return true;
  }

  bool apprEq(const QVector3D &a, const QVector3D &b, int ulp = 2)
  {
    return sib_utils::Math::almost_equal(a.x(), b.x(), ulp) &&
        sib_utils::Math::almost_equal(a.y(), b.y(), ulp) &&
        sib_utils::Math::almost_equal(a.z(), b.z(), ulp);
  }

  void testEq(const KawaiiCamera &a, const KawaiiCamera &b)
  {
    QVERIFY(apprEq(a.getViewMat(), b.getViewMat()));
    QVERIFY(apprEq(a.getViewMatInverted(), b.getViewMatInverted()));
    QVERIFY(apprEq(a.getNormalMat(), b.getNormalMat()));
    QVERIFY(a.children().size() == b.children().size());
  }

  const glm::vec3 upVec(0.0f, -1.0f, 0.0f);
  const glm::vec3 centerPos(0.0f, -1.5f, 0.0f);
  const glm::vec3 eye(2.0f, -1.5f, -1.0f);
}

void TestCamera::updateInvNormMat()
{
  v = cam.getViewMat();
  inv = cam.getViewMatInverted();
  norm = cam.getNormalMat();
}

void TestCamera::setViewMat()
{
  glm::mat4 vMat = glm::lookAt(eye, centerPos, upVec);
  cam.setViewMat(vMat);
  QVERIFY(v == vMat);
  QVERIFY(inv == glm::inverse(vMat));
  //  QVERIFY(norm == );
}

void TestCamera::getUpVec()
{
  auto res = cam.getUpVec();
//  auto res1 = glm::vec3(cam.getViewMatInverted() * glm::vec4(0,1,0,0));
//  QVERIFY(apprEq(res, res1));
  QVERIFY(apprEq(res, upVec));
}

void TestCamera::getEyePos()
{
  auto res = cam.getEyePos();
  QVERIFY(apprEq(res, eye));
}

void TestCamera::getViewDir()
{
  auto res = cam.getViewDir();
  QVERIFY(apprEq(res, glm::normalize(centerPos - eye)));
}

void TestCamera::transform()
{
  static const auto trVec = glm::vec3(0,1,0);

  glm::mat4 trMat = glm::mat4(1.0);
  trMat = glm::translate(glm::mat4(trMat), trVec);
  cam.transform(trMat);

  auto res = cam.getUpVec();
  QVERIFY(apprEq(res, upVec));

  res = cam.getEyePos();
  QVERIFY(apprEq(res, eye + trVec));

  res = cam.getViewDir();
  QVERIFY(apprEq(res, glm::normalize(centerPos - eye)));



  setViewMat();
  QQuaternion q = QQuaternion::fromEulerAngles(QVector3D(45, -45, 0));
  QMatrix4x4 qtrMat;
  qtrMat.rotate(q);
  cam.transform(qtrMat);

  auto qres = cam.getQUpVec();
  auto expected = q.rotatedVector(*KawaiiParam(upVec).getValue<QVector3D>());
  QVERIFY(apprEq(qres, expected, 20));

  qres = cam.getQEyePos();
  expected = q.rotatedVector(*KawaiiParam(eye).getValue<QVector3D>());
  QVERIFY(apprEq(qres, expected, 20));

  qres = cam.getQViewDir();
  expected = q.rotatedVector(*KawaiiParam(glm::normalize(centerPos - eye)).getValue<QVector3D>());
  QVERIFY(apprEq(qres, expected, 20));
}

void TestCamera::mementoText()
{
  QVERIFY(cam.property("MementoType").toString() == cam.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeText(cam));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  auto cam2 = dynamic_cast<KawaiiCamera*>(restoredNode.get());
  QVERIFY(cam2);
  QVERIFY(restoredNode->children().size() == cam.children().size());

  testEq(cam, *cam2);
}

void TestCamera::mementoBinary()
{
  QVERIFY(cam.property("MementoType").toString() == cam.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(cam));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  auto cam2 = dynamic_cast<KawaiiCamera*>(restoredNode.get());
  QVERIFY(cam2);
  QVERIFY(restoredNode->children().size() == cam.children().size());

  testEq(cam, *cam2);
}

QTEST_MAIN(TestCamera)
