#ifndef TESTCAMERA_HPP
#define TESTCAMERA_HPP

#include <QObject>
#include <glm/mat4x4.hpp>
#include "../Source/KawaiiCamera.hpp"

class TestCamera : public QObject
{
  Q_OBJECT

  KawaiiCamera cam;

  glm::mat4 v;
  glm::mat4 inv;
  glm::mat4 norm;
public:
  explicit TestCamera(QObject *parent = nullptr);

private:
  void updateInvNormMat();

private slots:
  void setViewMat();

  void getUpVec();
  void getEyePos();
  void getViewDir();

  void mementoText();
  void mementoBinary();

  void transform();
};

#endif // TESTCAMERA_HPP
