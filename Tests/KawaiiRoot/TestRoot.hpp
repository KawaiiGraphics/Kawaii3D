#ifndef TESTROOT_HPP
#define TESTROOT_HPP

#include <QObject>
#include "../Source/KawaiiRoot.hpp"

class TestRoot : public QObject
{
  Q_OBJECT

  KawaiiRoot root;
  KawaiiMeshInstance *meshInst;
  KawaiiMaterial *material;
  bool meshExists;

public:
  explicit TestRoot(QObject *parent = nullptr);

private slots:
  void meshInstanceRegistration();
  void meshInstanceUpdate();
  void meshInstanceUnregistration();

private:
  void meshInstRegistered(KawaiiMeshInstance *inst);
  void meshInstUnregistered(KawaiiMeshInstance *inst);
  void meshInstUpdated(KawaiiMeshInstance *inst, KawaiiMaterial *material);
};

#endif // TESTROOT_HPP
