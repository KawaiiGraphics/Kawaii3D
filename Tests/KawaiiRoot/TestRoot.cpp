#include "TestRoot.hpp"
#include <QtTest/QtTest>
#include "../Source/Geometry/KawaiiMeshInstance.hpp"
#include "../Source/KawaiiMaterial.hpp"

TestRoot::TestRoot(QObject *parent):
  QObject(parent),
  meshInst(nullptr),
  material(nullptr),
  meshExists(false)
{
  connect(&root, &KawaiiRoot::meshAdded, this, &TestRoot::meshInstRegistered);
  connect(&root, &KawaiiRoot::meshRemoved, this, &TestRoot::meshInstUnregistered);
  connect(&root, &KawaiiRoot::meshAboutToChangeMaterial, this, &TestRoot::meshInstUpdated);
}

void TestRoot::meshInstanceRegistration()
{
  auto dataUnit = new KawaiiDataUnit();
  dataUnit->updateParent(&root);

  meshInst = new KawaiiMeshInstance;
  meshInst->updateParent(dataUnit);
  QVERIFY(meshExists);
  QVERIFY(root.getMeshes().contains(meshInst));
}

void TestRoot::meshInstanceUpdate()
{
  auto m = new KawaiiMaterial;
  m->updateParent(&root);
  meshInst->setMaterial(m);
  QVERIFY(material == m);
  delete m;
  QVERIFY(material == nullptr);
}

void TestRoot::meshInstanceUnregistration()
{
  QVERIFY(meshInst && meshInst->parent());

  delete meshInst->parent();
  meshInst = nullptr;
  QVERIFY(!meshExists);
  QVERIFY(!root.getMeshes().contains(meshInst));
}

void TestRoot::meshInstRegistered(KawaiiMeshInstance *inst)
{
  if(inst == meshInst)
    meshExists = true;
}

void TestRoot::meshInstUnregistered(KawaiiMeshInstance *inst)
{
  if(inst == meshInst)
    meshExists = false;
}

void TestRoot::meshInstUpdated(KawaiiMeshInstance *inst, KawaiiMaterial *material)
{
  if(inst == meshInst)
    this->material = material;
}

QTEST_MAIN(TestRoot)
