#ifndef TESTMESH3D_HPP
#define TESTMESH3D_HPP

#include <QObject>
#include "../Source/Geometry/KawaiiMesh3D.hpp"

class TestMesh3D : public QObject
{
  Q_OBJECT
  KawaiiMesh3D mesh;

  const KawaiiPoint3D *lastAddedPoint;
  std::vector<const KawaiiPoint3D*> lastUpdatedPoints;
  std::vector<size_t> lastRemovedPoints;

  KawaiiTriangle3D::Instance lastAddedTr;
  std::vector<size_t> lastUpdatedIndices;
  std::vector<size_t> lastRemovedTriangles;

public:
  explicit TestMesh3D(QObject *parent = nullptr);

private slots:
  void verticesAdd();
  void verticesUpdate();
  void verticesRemove();

  void trianglesAdd();
  void indicesUpdate();
  void trianglesRemove();

  void mementoText();
  void mementoBinary();

  void clear();

private:
  void vertexAdded(KawaiiPoint3D *vert);
  void verticesUpdated(size_t index, size_t count);
  void verticesRemoved(size_t index, size_t count);

  void triangleAdded(const KawaiiTriangle3D::Instance &triangle);
  void trianglesRemoved(size_t index, size_t count);
  void indicesUpdated(size_t index, size_t count);
};

#endif // TESTMESH3D_HPP
