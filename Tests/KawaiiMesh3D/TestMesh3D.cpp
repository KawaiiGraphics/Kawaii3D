#include <QtTest/QtTest>
#include "TestMesh3D.hpp"
#include <sib_utils/Math.hpp>

namespace {
  bool operator==(const KawaiiTriangle3D::Instance &a, const KawaiiTriangle3D::Instance &b)
  {
    bool aV = a.isValid(),
        bV = b.isValid();

    if(!aV || !bV)
      return aV == bV;

    return &a.getFirstVertex() == &b.getFirstVertex() &&
        &a.getSecondVertex() == &b.getSecondVertex() &&
        &a.getThirdVertex() == &b.getThirdVertex();
  }

  bool testEq(const KawaiiPoint3D &a, const KawaiiPoint3D &b, int ulp = 2)
  {
    using sib_utils::Math;

    bool result = Math::almost_equal(a.positionRef().x(), b.positionRef().x(), ulp) &&
        Math::almost_equal(a.positionRef().y(), b.positionRef().y(), ulp) &&
        Math::almost_equal(a.positionRef().z(), b.positionRef().z(), ulp) &&
        Math::almost_equal(a.positionRef().w(), b.positionRef().w(), ulp) &&

        Math::almost_equal(a.normalRef().x(), b.normalRef().x(), ulp) &&
        Math::almost_equal(a.normalRef().y(), b.normalRef().y(), ulp) &&
        Math::almost_equal(a.normalRef().z(), b.normalRef().z(), ulp) &&

        Math::almost_equal(a.texCoordRef().x(), b.texCoordRef().x(), ulp) &&
        Math::almost_equal(a.texCoordRef().y(), b.texCoordRef().y(), ulp) &&
        Math::almost_equal(a.texCoordRef().z(), b.texCoordRef().z(), ulp);

    return result;
  }
}

TestMesh3D::TestMesh3D(QObject *parent):
  QObject(parent),
  mesh(KawaiiMesh3D()),
  lastAddedPoint(nullptr),
  lastAddedTr(KawaiiTriangle3D::Instance(nullptr, 0, nullptr))
{
  connect(&mesh, &KawaiiMesh3D::vertexAdded, this, &TestMesh3D::vertexAdded);
  connect(&mesh, &KawaiiMesh3D::verticesUpdated, this, &TestMesh3D::verticesUpdated);
  connect(&mesh, &KawaiiMesh3D::verticesRemoved, this, &TestMesh3D::verticesRemoved);

  connect(&mesh, &KawaiiMesh3D::triangleAdded, this, &TestMesh3D::triangleAdded);
  connect(&mesh, &KawaiiMesh3D::trianglesRemoved, this, &TestMesh3D::trianglesRemoved);
  connect(&mesh, &KawaiiMesh3D::indicesUpdated, this, &TestMesh3D::indicesUpdated);
}

void TestMesh3D::verticesAdd()
{
  mesh.addVertex(QVector4D(1,2,3,4));
  QVERIFY(mesh.vertexCount() == 1);
  QVERIFY(lastAddedPoint == &mesh.getVertex(0));

  QVERIFY(mesh.getVertex(0).positionRef().x() == 1 &&
          mesh.getVertex(0).positionRef().y() == 2 &&
          mesh.getVertex(0).positionRef().z() == 3 &&
          mesh.getVertex(0).positionRef().w() == 4);

  QVector3D n(9, 8, 7);
  mesh.addVertex(QVector4D(11,22,33,44), n);
  QVERIFY(mesh.vertexCount() == 2);
  QVERIFY(lastAddedPoint == &mesh.getVertex(1));

  QVERIFY(mesh.getVertex(1).positionRef().x() == 11 &&
          mesh.getVertex(1).positionRef().y() == 22 &&
          mesh.getVertex(1).positionRef().z() == 33 &&
          mesh.getVertex(1).positionRef().w() == 44);

  QVERIFY(mesh.getVertex(1).normalRef() == n.normalized());

  n = QVector3D(0,1,2);
  mesh.addVertex(QVector4D(10,20,30,40), n, QVector3D(5, 1, 5));
  QVERIFY(mesh.vertexCount() == 3);
  QVERIFY(lastAddedPoint == &mesh.getVertex(2));

  QVERIFY(mesh.getVertex(2).positionRef().x() == 10 &&
          mesh.getVertex(2).positionRef().y() == 20 &&
          mesh.getVertex(2).positionRef().z() == 30 &&
          mesh.getVertex(2).positionRef().w() == 40);

  QVERIFY(mesh.getVertex(2).normalRef() == n.normalized());

  QVERIFY(mesh.getVertex(2).texCoordRef().x() == 5 &&
          mesh.getVertex(2).texCoordRef().y() == 1 &&
          mesh.getVertex(2).texCoordRef().z() == 5);

  mesh.addVertex();
  QVERIFY(mesh.vertexCount() == 4);
  QVERIFY(lastAddedPoint == &mesh.getVertex(3));
}

void TestMesh3D::verticesUpdate()
{
  lastUpdatedPoints.clear();
  mesh.setVertexPos(3, QVector4D(1, 2, 3, 4));
  QVERIFY(lastUpdatedPoints.size() == 1);
  QVERIFY(lastUpdatedPoints[0]->positionRef().x() == 1 &&
          lastUpdatedPoints[0]->positionRef().y() == 2 &&
          lastUpdatedPoints[0]->positionRef().z() == 3 &&
          lastUpdatedPoints[0]->positionRef().w() == 4);

  QVector3D n(1, 1, 0);
  mesh.setVertexNormal(3, n);
  QVERIFY(lastUpdatedPoints.size() == 2);
  QVERIFY(lastUpdatedPoints[1]->normalRef() == n.normalized());

  mesh.setVertexTexCoord(3, QVector3D(0,1,2));
  QVERIFY(lastUpdatedPoints.size() == 3);
  QVERIFY(lastUpdatedPoints[2]->texCoordRef().x() == 0 &&
          lastUpdatedPoints[2]->texCoordRef().y() == 1 &&
          lastUpdatedPoints[2]->texCoordRef().z() == 2);

  n = QVector3D(1,-7,15);
  mesh.changeVertex(3, [&n] (KawaiiPoint3D &v, size_t i) { v.normalRef() = n; return i == 3; });
  QVERIFY(lastUpdatedPoints.size() == 4);
  QVERIFY(lastUpdatedPoints[3]->normalRef() == n);
  lastUpdatedPoints.clear();
}

void TestMesh3D::verticesRemove()
{
  lastRemovedPoints.clear();
  mesh.removeVertices(3, 1);
  QVERIFY(mesh.vertexCount() == 3);
  QVERIFY(lastRemovedPoints.size() == 1);
  QVERIFY(lastRemovedPoints[0] == 3);
  lastRemovedPoints.clear();
}

void TestMesh3D::trianglesAdd()
{
  mesh.addTriangle(0, 1, 2);
  QVERIFY(mesh.getTriangle(0) == lastAddedTr);

  mesh.addTriangle(1, 0, 2);
  QVERIFY(mesh.getTriangle(1) == lastAddedTr);

  QVERIFY(mesh.trianglesCount() == 2);

  QVERIFY(mesh.getTriangle(0).getFirstIndex() == 0 &&
          mesh.getTriangle(0).getSecondIndex() == 1 &&
          mesh.getTriangle(0).getThirdIndex() == 2);

  QVERIFY(mesh.getTriangle(1).getFirstIndex() == 1 &&
          mesh.getTriangle(1).getSecondIndex() == 0 &&
          mesh.getTriangle(1).getThirdIndex() == 2);

  QVERIFY(mesh.getTriangle(0).getFirstVertex() == mesh.getVertex(0) &&
          mesh.getTriangle(0).getSecondVertex() == mesh.getVertex(1) &&
          mesh.getTriangle(0).getThirdVertex() == mesh.getVertex(2));

  QVERIFY(mesh.getTriangle(1).getFirstVertex() == mesh.getVertex(1) &&
          mesh.getTriangle(1).getSecondVertex() == mesh.getVertex(0) &&
          mesh.getTriangle(1).getThirdVertex() == mesh.getVertex(2));
}

void TestMesh3D::indicesUpdate()
{
  lastUpdatedIndices.clear();
  mesh.getTriangle(0).setSecondIndex(2);
  QVERIFY(mesh.getTriangle(0).getSecondIndex() == 2);
  QVERIFY(lastUpdatedIndices.size() == 1);
  QVERIFY(lastUpdatedIndices[0] == 1 &&
          mesh.getRawTriangles()[0][1] == 2);

  mesh.getTriangle(0).setSecondIndex(1);
  QVERIFY(lastUpdatedIndices.size() == 2);
  QVERIFY(mesh.getTriangle(0).getSecondIndex() == 1);
  QVERIFY(lastUpdatedIndices[0] == 1 &&
          mesh.getRawTriangles()[0][1] == 1);
  lastUpdatedIndices.clear();
}

void TestMesh3D::trianglesRemove()
{
  lastRemovedTriangles.clear();
  mesh.removeTriangles(1, 1);
  QVERIFY(lastRemovedTriangles.size() == 1);
  QVERIFY(lastRemovedTriangles[0] == 1);
  lastRemovedTriangles.clear();
}

void TestMesh3D::mementoText()
{
  QVERIFY(mesh.property("MementoType").toString() == mesh.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeText(mesh));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  KawaiiMesh3D *tmpMesh = dynamic_cast<KawaiiMesh3D*>(restoredNode.get());
  QVERIFY(tmpMesh);
  QVERIFY(restoredNode->children().size() == mesh.children().size());

  QVERIFY(tmpMesh->vertexCount() == mesh.vertexCount());
  for(size_t i = 0; i < tmpMesh->vertexCount(); ++i)
    QVERIFY(testEq(tmpMesh->getVertex(i), mesh.getVertex(i)));

  QVERIFY(tmpMesh->trianglesCount() == mesh.trianglesCount());
  for(size_t i = 0; i < tmpMesh->trianglesCount(); ++i)
    {
      QVERIFY(tmpMesh->getTriangle(i).getFirstIndex() == mesh.getTriangle(i).getFirstIndex());
      QVERIFY(tmpMesh->getTriangle(i).getSecondIndex() == mesh.getTriangle(i).getSecondIndex());
      QVERIFY(tmpMesh->getTriangle(i).getThirdIndex() == mesh.getTriangle(i).getThirdIndex());
    }
}

void TestMesh3D::mementoBinary()
{
  QVERIFY(mesh.property("MementoType").toString() == mesh.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(mesh));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  KawaiiMesh3D *tmpMesh = dynamic_cast<KawaiiMesh3D*>(restoredNode.get());
  QVERIFY(tmpMesh);
  QVERIFY(restoredNode->children().size() == mesh.children().size());

  QVERIFY(tmpMesh->vertexCount() == mesh.vertexCount());
  for(size_t i = 0; i < tmpMesh->vertexCount(); ++i)
    QVERIFY(tmpMesh->getVertex(i) == mesh.getVertex(i));

  QVERIFY(tmpMesh->trianglesCount() == mesh.trianglesCount());
  for(size_t i = 0; i < tmpMesh->trianglesCount(); ++i)
    {
      QVERIFY(tmpMesh->getTriangle(i).getFirstIndex() == mesh.getTriangle(i).getFirstIndex());
      QVERIFY(tmpMesh->getTriangle(i).getSecondIndex() == mesh.getTriangle(i).getSecondIndex());
      QVERIFY(tmpMesh->getTriangle(i).getThirdIndex() == mesh.getTriangle(i).getThirdIndex());
    }
}

void TestMesh3D::clear()
{
  lastRemovedPoints.clear();
  lastRemovedTriangles.clear();

  size_t points = mesh.vertexCount(),
      triangles = mesh.trianglesCount();

  mesh.clear();
  QVERIFY(mesh.vertexCount() == 0);
  QVERIFY(mesh.trianglesCount() == 0);
  QVERIFY(lastRemovedPoints.size() == points);
  QVERIFY(lastRemovedTriangles.size() == triangles);

  lastRemovedPoints.clear();
  lastRemovedTriangles.clear();
}

void TestMesh3D::vertexAdded(KawaiiPoint3D *vert)
{
  lastAddedPoint = vert;
}

void TestMesh3D::verticesUpdated(size_t index, size_t count)
{
  for(size_t i = index; i < index + count; ++i)
    lastUpdatedPoints.push_back(&mesh.getVertex(i));
}

void TestMesh3D::verticesRemoved(size_t index, size_t count)
{
  for(size_t i = index; i < index + count; ++i)
    lastRemovedPoints.push_back(i);
}

void TestMesh3D::triangleAdded(const KawaiiTriangle3D::Instance &triangle)
{
  lastAddedTr = triangle;
}

void TestMesh3D::trianglesRemoved(size_t index, size_t count)
{
  for(size_t i = index; i < index + count; ++i)
    lastRemovedTriangles.push_back(i);
}

void TestMesh3D::indicesUpdated(size_t index, size_t count)
{
  for(size_t i = index; i < index + count; ++i)
    lastUpdatedIndices.push_back(i);
}

QTEST_MAIN(TestMesh3D)
