#ifndef TESTMATERIAL_HPP
#define TESTMATERIAL_HPP

#include <QObject>
#include "../Source/KawaiiMaterial.hpp"

class TestMaterial : public QObject
{
  Q_OBJECT

  KawaiiMaterial material;
  KawaiiGpuBuf *gpuBuf;
public:
  explicit TestMaterial(QObject *parent = nullptr);

private slots:
  void gpuBufSet();
  void mementoText();
  void mementoBinary();
  void gpuBufDestroy();

private:
  void setGpuBuf(KawaiiGpuBuf *gpuBuf);
};

#endif // TESTMATERIAL_HPP
