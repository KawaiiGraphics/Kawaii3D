#include "TestMaterial.hpp"
#include "../Source/KawaiiGpuBuf.hpp"
#include <QtTest/QtTest>

TestMaterial::TestMaterial(QObject *parent):
  QObject(parent), gpuBuf(nullptr)
{
  connect(&material, &KawaiiMaterial::uniformsChanged, this, &TestMaterial::setGpuBuf);
}

void TestMaterial::gpuBufSet()
{
  float f = 42.42f;
  auto buf = new KawaiiGpuBuf(&f, sizeof(float));
  buf->updateParent(&material);

  material.setUniforms(buf);
  QVERIFY(material.getUniforms() == buf);
  QVERIFY(gpuBuf == buf);
}

void TestMaterial::mementoText()
{
  QVERIFY(material.property("MementoType").toString() == material.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeText(material));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  KawaiiMaterial *material2 = dynamic_cast<KawaiiMaterial*>(restoredNode.get());

  QVERIFY(material2);
  QVERIFY(restoredNode->children().size() == material.children().size());

  auto ubo_origin = material.getUniforms(),
      ubo_restored = material2->getUniforms();

  QVERIFY(ubo_origin);
  QVERIFY(ubo_restored);
  QVERIFY(ubo_origin->getDataSize() == ubo_restored->getDataSize());
  QVERIFY(memcmp(ubo_origin->getData(), ubo_restored->getData(), ubo_origin->getDataSize()) == 0);
}

void TestMaterial::mementoBinary()
{
  QVERIFY(material.property("MementoType").toString() == material.staticMetaObject.className());
  std::unique_ptr<sib_utils::memento::Memento> memento;
  memento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(material));

  std::unique_ptr<sib_utils::TreeNode> restoredNode;
  restoredNode.reset(KawaiiDataUnit::getMementoFactory().loadObjectTree(*memento));

  KawaiiMaterial *material2 = dynamic_cast<KawaiiMaterial*>(restoredNode.get());

  QVERIFY(material2);
  QVERIFY(restoredNode->children().size() == material.children().size());

  auto ubo_origin = material.getUniforms(),
      ubo_restored = material2->getUniforms();

  QVERIFY(ubo_origin);
  QVERIFY(ubo_restored);
  QVERIFY(ubo_origin->getDataSize() == ubo_restored->getDataSize());
  QVERIFY(memcmp(ubo_origin->getData(), ubo_restored->getData(), ubo_origin->getDataSize()) == 0);
}

void TestMaterial::gpuBufDestroy()
{
  QVERIFY(material.getUniforms());
  delete material.getUniforms();
  QVERIFY(!material.getUniforms());
  QVERIFY(!gpuBuf);
}

void TestMaterial::setGpuBuf(KawaiiGpuBuf *gpuBuf)
{
  this->gpuBuf = gpuBuf;
}

QTEST_MAIN(TestMaterial)
