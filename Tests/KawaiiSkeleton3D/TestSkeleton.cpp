#include "TestSkeleton.hpp"
#include "../Source/Geometry/KawaiiSkeleton3D.hpp"
#include "../Source/Geometry/KawaiiModel3D.hpp"
#include "../Source/Geometry/KawaiiBone3D.hpp"
#include <sib_utils/ioReadAll.hpp>
#include <sib_utils/Math.hpp>
#include <QTemporaryFile>
#include <QFile>
#include <QDir>
#include <QtTest/QtTest>

namespace {
  template<int N, int M>
  bool apprEq(const glm::mat<N, M, float, glm::defaultp> &a, const glm::mat<N, M, float, glm::defaultp> &b, int ulp = 2)
  {
    for(int i = 0; i < N; ++i)
      for(int j = 0; j < M; ++j)
        if(!sib_utils::Math::almost_equal(a[i][j], b[i][j], ulp))
          return false;
    return true;
  }

  bool operator==(const KawaiiSkeleton3D::Node &a, const KawaiiSkeleton3D::Node &b)
  {
    if(a.getName() != b.getName())
      return false;

    if(!apprEq(a.getOwnTransform(), b.getOwnTransform()))
      return false;

    if((a.getParent() == nullptr) != (b.getParent() == nullptr))
      return false;

    if(!a.getParent())
      return true;

    return (*a.getParent() == *b.getParent());
  }

  bool operator==(const KawaiiSkeleton3D &a, const KawaiiSkeleton3D &b)
  {
    if(a.getBoneCount() != b.getBoneCount()) return false;

    bool eq = true;
    a.forallNodes([&b, &eq] (const KawaiiSkeleton3D::Node &a_node) {
        const auto *b_node = b.findNode(a_node.getName());
        eq &= (a_node == *b_node);
      });

    for(size_t i = 0; i < a.getBoneCount(); ++i)
      {
        if(!apprEq(a.getBone(i)->getTr(), b.getBone(i)->getTr()))
          return false;
        if(a.getBone(i)->objectName() != b.getBone(i)->objectName())
          return false;
      }

    return eq;
  }
}


TestSkeleton::TestSkeleton(QObject *parent):
  QObject(parent)
{
}

void TestSkeleton::mementoText()
{
  KawaiiModel3D testingModel;
  testingModel.loadAsset(":/TestModel/model.dae");

  std::unique_ptr<sib_utils::memento::Memento> rootMemento(KawaiiDataUnit::getMementoFactory().saveObjectTreeText(testingModel));

  std::unique_ptr<sib_utils::TreeNode> loadedModel = std::unique_ptr<sib_utils::TreeNode>(KawaiiDataUnit::getMementoFactory().loadObjectTree(*rootMemento));

  auto *model_ptr = qobject_cast<KawaiiModel3D*>(loadedModel.get());
  QVERIFY(model_ptr->getSkeleton() != nullptr);
  model_ptr->getSkeleton()->updateBones();
  QVERIFY(*testingModel.getSkeleton() == *model_ptr->getSkeleton());
}

void TestSkeleton::mementoBinary()
{
  KawaiiModel3D testingModel;
  testingModel.loadAsset(":/TestModel/model.dae");

  std::unique_ptr<sib_utils::memento::Memento> rootMemento(KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(testingModel));

  std::unique_ptr<sib_utils::TreeNode> loadedModel = std::unique_ptr<sib_utils::TreeNode>(KawaiiDataUnit::getMementoFactory().loadObjectTree(*rootMemento));

  auto *model_ptr = qobject_cast<KawaiiModel3D*>(loadedModel.get());
  QVERIFY(model_ptr->getSkeleton() != nullptr);
  model_ptr->getSkeleton()->updateBones();
  QVERIFY(*testingModel.getSkeleton() == *model_ptr->getSkeleton());
}

QTEST_MAIN(TestSkeleton)
