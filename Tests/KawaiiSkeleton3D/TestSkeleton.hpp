#ifndef TESTSKELETON_HPP
#define TESTSKELETON_HPP

#include <QObject>

class TestSkeleton : public QObject
{
  Q_OBJECT
public:
  explicit TestSkeleton(QObject *parent = nullptr);

private slots:
  void mementoText();
  void mementoBinary();
};

#endif // TESTSKELETON_HPP
